webpackJsonp(["car.module"],{

/***/ "../../../../../src/app/cars/add/add.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".FormClass\n{\n    text-align: right;\n    direction: rtl;\n}\n\n.row\n{\n    margin-top: 20px;\n}\n\n.textWhite\n{\n    background-color: white;\n}\n\ninput.ng-invalid.ng-touched\n{\n    border:1px solid red;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/cars/add/add.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row FormClass\">\n  <div class=\"col-lg-12\">\n    <div class=\"card\">\n      <div class=\"card-header\">\n        הוסף חברה\n      </div>\n      <div class=\"card-body\">\n        <form (ngSubmit)=\"onSubmit(f)\" #f=\"ngForm\">\n          <div class=\"row\">\n            <div class=\"form-group\" class=\"col-lg-12\">\n              <label for=\"formGroupExampleInput\">הכנס שם חברה </label>\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" name=\"name\" ngModel required>\n            </div>\n          </div>\n\n          <div class=\"row\">\n            <div class=\"form-group\" class=\"col-lg-6\">\n              <label for=\"formGroupExampleInput\">הכנס כתובת </label>\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" name=\"address\"\n                     ngModel required>\n            </div>\n            <div class=\"form-group\" class=\"col-lg-6\">\n              <label for=\"formGroupExampleInput2\">הכנס טלפון</label>\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput2\" name=\"phone\"\n                     ngModel required>\n            </div>\n          </div>\n\n          <div class=\"row\">\n            <div class=\"form-group\" class=\"col-lg-6\">\n              <label for=\"formGroupExampleInput\">הזן סיסמה</label>\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" name=\"password\" ngModel required>\n            </div>\n            <div class=\"form-group\" class=\"col-lg-6\">\n              <label for=\"formGroupExampleInput2\">הכנס אימייל</label>\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput2\" name=\"email\" ngModel required email>\n            </div>\n          </div>\n\n          <div class=\"row\">\n            <div class=\"form-group\" class=\"col-lg-6\">\n              <label for=\"formGroupExampleInput\">הכנס אתר אינטרנט</label>\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" name=\"website\"\n                     ngModel>\n            </div>\n            <div class=\"form-group\" class=\"col-lg-6\">\n              <label for=\"formGroupExampleInput2\">הכנס WAZE</label>\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput2\" name=\"waze\"\n                     ngModel>\n            </div>\n          </div>\n          <div class=\"row\">\n            <div class=\"form-group\" class=\"col-lg-6\">\n              <label for=\"formGroupExampleInput\">הכנס פייסבוק</label>\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" name=\"facebook\"\n                     ngModel>\n            </div>\n            <div class=\"form-group\" class=\"col-lg-6\">\n              <label for=\"formGroupExampleInput2\">הכנס subDomain</label>\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput2\" name=\"subdomain\"\n                     ngModel required>\n            </div>\n          </div>\n          <div class=\"row\">\n            <div class=\"form-group\" class=\"col-lg-12\">\n              <label for=\"formGroupExampleInput\">פרטים נוספים</label>\n              <textarea  rows=\"4\" cols=\"50\"  class=\"form-control textWhite\" id=\"formGroupExampleInput\" name=\"description\" ngModel required> </textarea>\n            </div>\n          </div>\n          <div class=\"row\">\n            <label class=\"uploader\">\n              <img [src]=\"imageSrc\"  [class.loaded]=\"imageLoaded\" style=\"width: 100px;\"/>\n              <input type=\"file\" name=\"file\" accept=\"image/*\" (change)=\"handleInputChange($event)\">\n            </label>\n          </div>\n          <div class=\"row\" align=\"left\" style=\"float: left; margin-left: 5px\">\n            <button [disabled]=\"!f.valid\" type=\"submit\" class=\"btn btn-primary btn-icon loading-demo mr-1 mb-1\"\n                    style=\"padding:0 20px !important; cursor: pointer; width: 205px; text-align: center; font-weight: bold; font-size: 16px\">\n              <span style=\"text-align: center; padding: 10px;  width: 100% !important;\">שלח טופס</span>\n            </button>\n          </div>\n        </form>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/cars/add/add.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__car_service__ = __webpack_require__("../../../../../src/app/cars/car.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AddComponent = (function () {
    function AddComponent(route, http, service, router) {
        this.route = route;
        this.http = http;
        this.service = service;
        this.router = router;
        this.navigateTo = '/company/index';
        this.imageSrc = '';
    }
    AddComponent.prototype.onSubmit = function (form) {
        var _this = this;
        console.log(form.value);
        this.service.AddCompany('AddCompany1', form.value).then(function (data) {
            console.log("AddCompany : ", data);
            _this.router.navigate([_this.navigateTo]);
        });
    };
    AddComponent.prototype.handleInputChange = function (e) {
        var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
        var pattern = /image-*/;
        var reader = new FileReader();
        if (!file.type.match(pattern)) {
            alert('invalid format');
            return;
        }
        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsDataURL(file);
        console.log("2 : ", reader);
    };
    AddComponent.prototype._handleReaderLoaded = function (e) {
        var reader = e.target;
        this.imageSrc = reader.result;
        console.log("3 : ", this.imageSrc);
    };
    AddComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-add',
            template: __webpack_require__("../../../../../src/app/cars/add/add.component.html"),
            styles: [__webpack_require__("../../../../../src/app/cars/add/add.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__car_service__["a" /* CarService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__car_service__["a" /* CarService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _d || Object])
    ], AddComponent);
    return AddComponent;
    var _a, _b, _c, _d;
}());

//# sourceMappingURL=add.component.js.map

/***/ }),

/***/ "../../../../../src/app/cars/car.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CarModule", function() { return CarModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__car_routing__ = __webpack_require__("../../../../../src/app/cars/car.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_datatable__ = __webpack_require__("../../../../@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_datatable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_datatable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__edit_edit_component__ = __webpack_require__("../../../../../src/app/cars/edit/edit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__index_index_component__ = __webpack_require__("../../../../../src/app/cars/index/index.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__add_add_component__ = __webpack_require__("../../../../../src/app/cars/add/add.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_ng2_file_upload__ = __webpack_require__("../../../../ng2-file-upload/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__car_service__ = __webpack_require__("../../../../../src/app/cars/car.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_ng_sidebar__ = __webpack_require__("../../../../ng-sidebar/lib/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_ng_sidebar___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_13_ng_sidebar__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};














var CarModule = (function () {
    function CarModule() {
    }
    CarModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__car_routing__["a" /* CarRoutes */]),
                __WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_datatable__["NgxDatatableModule"],
                __WEBPACK_IMPORTED_MODULE_5__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_9__ng_bootstrap_ng_bootstrap__["g" /* NgbModule */],
                __WEBPACK_IMPORTED_MODULE_10_ng2_file_upload__["FileUploadModule"],
                __WEBPACK_IMPORTED_MODULE_11__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_13_ng_sidebar__["SidebarModule"]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__edit_edit_component__["a" /* EditComponent */],
                __WEBPACK_IMPORTED_MODULE_7__index_index_component__["a" /* IndexComponent */],
                __WEBPACK_IMPORTED_MODULE_8__add_add_component__["a" /* AddComponent */]
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_12__car_service__["a" /* CarService */]]
        })
    ], CarModule);
    return CarModule;
}());

//# sourceMappingURL=car.module.js.map

/***/ }),

/***/ "../../../../../src/app/cars/car.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CarRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__edit_edit_component__ = __webpack_require__("../../../../../src/app/cars/edit/edit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__add_add_component__ = __webpack_require__("../../../../../src/app/cars/add/add.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__index_index_component__ = __webpack_require__("../../../../../src/app/cars/index/index.component.ts");



var CarRoutes = [{
        path: '',
        children: [{
                path: 'index',
                component: __WEBPACK_IMPORTED_MODULE_2__index_index_component__["a" /* IndexComponent */],
                data: {
                    heading: 'Car'
                }
            }, {
                path: 'index',
                component: __WEBPACK_IMPORTED_MODULE_0__edit_edit_component__["a" /* EditComponent */],
                data: {
                    heading: 'Edit Car'
                }
            }, {
                path: 'add',
                component: __WEBPACK_IMPORTED_MODULE_1__add_add_component__["a" /* AddComponent */],
                data: {
                    heading: 'Edit Car'
                }
            }]
    }];
/*

component: CompanyComponent,
    data: {
        heading: 'Company',
        removeFooter: true
    },



*/ 
//# sourceMappingURL=car.routing.js.map

/***/ }),

/***/ "../../../../../src/app/cars/car.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CarService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__("../../../../rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__email_mock_messages__ = __webpack_require__("../../../../../src/app/email/mock-messages.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ServerUrl = "http://www.tapper.co.il/salecar/laravel/public/api/";
var CarService = (function () {
    function CarService(http) {
        this.http = http;
        this.headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/x-www-form-urlencoded' });
        this.options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["f" /* RequestOptions */]({ headers: this.headers });
        this.Companies = [];
        this.CompanyArray = [];
        this.Cars = [];
    }
    ;
    CarService.prototype.getMessages = function () {
        return Promise.resolve(__WEBPACK_IMPORTED_MODULE_3__email_mock_messages__["a" /* MESSAGES */]);
    };
    CarService.prototype.GetAllCars = function (url) {
        var _this = this;
        var body = new FormData();
        body.append('id', '26');
        return this.http.post(ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { _this.Cars = data; }).toPromise();
    };
    CarService.prototype.AddCompany = function (url, Company) {
        this.CompanyArray = Company;
        var body = 'company=' + JSON.stringify(this.CompanyArray);
        return this.http.post(ServerUrl + '' + url, body, this.options).map(function (res) { return res; }).do(function (data) { }).toPromise();
    };
    CarService.prototype.EditCompany = function (url, Company) {
        this.CompanyArray = Company;
        var body = 'company=' + JSON.stringify(this.CompanyArray);
        return this.http.post(ServerUrl + '' + url, body, this.options).map(function (res) { return res; }).do(function (data) { }).toPromise();
    };
    CarService.prototype.DeleteCompany = function (url, Id) {
        var body = 'id=' + Id;
        return this.http.post(ServerUrl + '' + url, body, this.options).map(function (res) { return res.json(); }).do(function (data) { }).toPromise();
    };
    CarService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */]) === "function" && _a || Object])
    ], CarService);
    return CarService;
    var _a;
}());

;
//# sourceMappingURL=car.service.js.map

/***/ }),

/***/ "../../../../../src/app/cars/edit/edit.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".FormClass\n{\n    text-align: right;\n    direction: rtl;\n}\n\n.row\n{\n    margin-top: 20px;\n}\n\n.textWhite\n{\n    background-color: white;\n}\n\ninput.ng-invalid.ng-touched\n{\n    border:1px solid red;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/cars/edit/edit.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row FormClass\">\n    <div class=\"col-lg-12\">\n        <div class=\"card\">\n            <div class=\"card-header\">\n                ערוך חברה\n            </div>\n            <div class=\"card-body\">\n                <form (ngSubmit)=\"onSubmit(f)\" #f=\"ngForm\">\n                    <div class=\"row\">\n                        <div class=\"form-group\" class=\"col-lg-12\">\n                            <label for=\"formGroupExampleInput\">הכנס שם חברה </label>\n                            <input type=\"text\" class=\"form-control textWhite\" [(ngModel)]=\"Company.name\" id=\"formGroupExampleInput\" name=\"name\" ngModel required>\n                        </div>\n\n                    </div>\n\n                    <div class=\"row\">\n                        <div class=\"form-group\" class=\"col-lg-6\">\n                            <label for=\"formGroupExampleInput\">הכנס כתובת </label>\n                            <input type=\"text\" class=\"form-control textWhite\" [(ngModel)]=\"Company.address\" id=\"formGroupExampleInput\" name=\"address\"\n                                   ngModel>\n                        </div>\n                        <div class=\"form-group\" class=\"col-lg-6\">\n                            <label for=\"formGroupExampleInput2\">הכנס טלפון</label>\n                            <input type=\"text\" class=\"form-control textWhite\" [(ngModel)]=\"Company.phone\" id=\"formGroupExampleInput2\" name=\"phone\"\n                                   ngModel>\n                        </div>\n                    </div>\n\n\n                    <div class=\"row\">\n                        <div class=\"form-group\" class=\"col-lg-6\">\n                            <label for=\"formGroupExampleInput\">הזן סיסמה</label>\n                            <input type=\"text\" class=\"form-control textWhite\" [(ngModel)]=\"Company.password\" id=\"formGroupExampleInput\" name=\"pass\" ngModel>\n                        </div>\n                        <div class=\"form-group\" class=\"col-lg-6\">\n                            <label for=\"formGroupExampleInput2\">הכנס אימייל</label>\n                            <input type=\"text\" class=\"form-control textWhite\" [(ngModel)]=\"Company.email\" id=\"formGroupExampleInput2\" name=\"email\" ngModel required email>\n                        </div>\n                    </div>\n\n                    <div class=\"row\">\n                        <div class=\"form-group\" class=\"col-lg-6\">\n                            <label for=\"formGroupExampleInput\">הכנס אתר אינטרנט</label>\n                            <input type=\"text\" class=\"form-control textWhite\" [(ngModel)]=\"Company.website\" id=\"formGroupExampleInput\" name=\"web\"\n                                   ngModel>\n                        </div>\n                        <div class=\"form-group\" class=\"col-lg-6\">\n                            <label for=\"formGroupExampleInput2\">הכנס WAZE</label>\n                            <input type=\"text\" class=\"form-control textWhite\" [(ngModel)]=\"Company.waze\" id=\"formGroupExampleInput2\" name=\"waze\"\n                                   ngModel>\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                        <div class=\"form-group\" class=\"col-lg-6\">\n                            <label for=\"formGroupExampleInput\">הכנס פייסבוק</label>\n                            <input type=\"text\" class=\"form-control textWhite\" [(ngModel)]=\"Company.facebook\" id=\"formGroupExampleInput\" name=\"facebook\"\n                                   ngModel>\n                        </div>\n                        <div class=\"form-group\" class=\"col-lg-6\">\n                            <label for=\"formGroupExampleInput2\">הכנס subDomain</label>\n                            <input type=\"text\" class=\"form-control textWhite\" [(ngModel)]=\"Company.subdomain\" id=\"formGroupExampleInput2\" name=\"sub\"\n                                   ngModel>\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                        <div class=\"form-group\" class=\"col-lg-12\">\n                            <label for=\"formGroupExampleInput\">פרטים נוספים</label>\n                            <textarea  rows=\"4\" cols=\"50\"  class=\"form-control textWhite\" id=\"formGroupExampleInput\" [(ngModel)]=\"Company.description\" name=\"description\" ngModel required> </textarea>\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                        <label class=\"uploader\">\n                            <img [src]=\"imageSrc\"  [class.loaded]=\"imageLoaded\" style=\"width: 100px;\"/>\n                            <input type=\"file\" name=\"file\" accept=\"image/*\" (change)=\"handleInputChange($event)\">\n                        </label>\n                    </div>\n                    <div class=\"row\" align=\"left\" style=\"float: left; margin-left: 5px\">\n                        <button [disabled]=\"!f.valid\" type=\"submit\" class=\"btn btn-primary btn-icon loading-demo mr-1 mb-1\"\n                                style=\"padding:0 20px !important; cursor: pointer; width: 205px; text-align: center; font-weight: bold; font-size: 16px\">\n                            <span style=\"text-align: center; padding: 10px;  width: 100% !important;\">שלח טופס</span>\n                        </button>\n                    </div>\n                </form>\n\n\n\n\n                <!-- <img src=\"{{imageSrc}}\" style=\"width: 100%\" /> <div class=\"row\" align=\"left\"  style=\"float: left; margin-left: 5px\">\n                     <button type=\"button\"  class=\"btn btn-primary btn-icon loading-demo mr-1 mb-1\" style=\"padding:0 20px !important; cursor: pointer; width: 205px; text-align: center; font-weight: bold; font-size: 16px\">\n                         <span style=\"text-align: center; padding: 10px;  width: 100% !important;\">שלח טופס</span>\n                     </button>\n                 </div>-->\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/cars/edit/edit.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__car_service__ = __webpack_require__("../../../../../src/app/cars/car.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var URL = 'https://evening-anchorage-3159.herokuapp.com/api/';
var EditComponent = (function () {
    function EditComponent(route, http, service, router) {
        var _this = this;
        this.route = route;
        this.http = http;
        this.service = service;
        this.router = router;
        this.navigateTo = '/company/index';
        this.imageSrc = '';
        this.Company = [];
        this.route.params.subscribe(function (params) {
            _this.Id = params['id'];
            _this.Company = _this.service.Companies[_this.Id];
            console.log("CP : ", _this.service.Companies);
        });
    }
    EditComponent.prototype.onSubmit = function (form) {
        var _this = this;
        console.log("Edit : ", this.Company);
        this.service.EditCompany('EditCompany', this.Company).then(function (data) {
            console.log("AddCompany : ", data);
            _this.router.navigate([_this.navigateTo]);
        });
    };
    EditComponent.prototype.handleInputChange = function (e) {
        var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
        var pattern = /image-*/;
        var reader = new FileReader();
        if (!file.type.match(pattern)) {
            alert('invalid format');
            return;
        }
        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsDataURL(file);
        console.log("2 : ", reader);
    };
    EditComponent.prototype._handleReaderLoaded = function (e) {
        var reader = e.target;
        this.imageSrc = reader.result;
        console.log("3 : ", this.imageSrc);
    };
    EditComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-edit',
            template: __webpack_require__("../../../../../src/app/cars/edit/edit.component.html"),
            styles: [__webpack_require__("../../../../../src/app/icons/fontawesome/fontawesome.component.scss"), __webpack_require__("../../../../../src/app/components/buttons/buttons.component.scss"), __webpack_require__("../../../../../src/app/cars/edit/edit.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__car_service__["a" /* CarService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__car_service__["a" /* CarService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _d || Object])
    ], EditComponent);
    return EditComponent;
    var _a, _b, _c, _d;
}());

//# sourceMappingURL=edit.component.js.map

/***/ }),

/***/ "../../../../../src/app/cars/index/index.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".card\n{\n    text-align: right;\n    direction: rtl;\n}\n\n.card-body\n{\n    border-bottom: 1px solid #f2f1f2;\n}\n\n.mr-auto\n{\n    text-align: right;\n    direction: rtl;\n    float: right;\n}\n\n.mr-3\n{\n    background-color: green;\n    float: right;\n}\n\n.IconClass\n{\n    margin-top: 6px;\n    text-align: center;\n    padding-left: -13px !important;\n}\n\n.d-icon{\n    margin-top: -20px;\n}\n\n.card-title\n{\n    font-size: 16px;\n}\n\n.card-text\n{\n    margin-top: -10px;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/cars/index/index.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\" card-body\" style=\"width: 100% !important; overflow: hidden; background-color: #f1f1f1\">\n    <div style=\"float:right; width:40%; text-align: right\">\n        <h6 class=\"text-uppercase\">הרכבים של רויאל קאר</h6>\n        <span>כרגע יש 4 חברות במערכת</span>\n    </div>\n    <div style=\"float:left; width:30%; \">\n        <button [routerLink]=\"['/', 'company' , 'add' ]\" class=\"btn btn-icon btn-facebook mb-1 mr-1 \" style=\"width: 170px; background-color:#3b5998; cursor: pointer; color: white; \">\n            <i class=\"fa fa-plus-circle\"></i>\n            חזרה לעמוד החברה\n        </button>\n        <button [routerLink]=\"['/', 'car' , 'add' ]\" class=\"btn btn-icon btn-facebook mb-1 mr-1 \" style=\"width: 140px; background-color:#3b5998; cursor: pointer; color: white; \">\n            <i class=\"fa fa-plus-circle\"></i>\n            הוסף רכב חדש\n        </button>\n    </div>\n</div>\n\n\n<div class=\"row\" style=\"margin-top: 30px;\">\n    <div class=\"col-lg-12\" align=\"center\">\n        <div class=\"col-lg-10\">\n\n            <div class=\"card-deck-wrapper\">\n                <div class=\"card-deck\">\n                    <div class=\"card\" *ngFor=\"let item of ItemsArray let i=index\">\n                        <img class=\"card-img-top img-fluid\" src=\"{{host}}{{item.images[0].url}}\" alt=\"Card image\"/>\n                        <div class=\"card-body\">\n                            <h5 class=\"card-title\">\n                                {{item.carName}} - {{item.carModelName}}\n                            </h5>\n                            <p class=\"card-text\">\n                                {{item.ComapnyName}}\n                            </p>\n                            <p class=\"card-text\">\n                                <small class=\"text-muted\">\n                                    שנה: {{item.year}} | יד {{item.hand}} | מנוע {{item.size}} סמ\"ק\n                                </small>\n                            </p>\n                            <hr>\n                            <button [routerLink]=\"['/', 'car' , 'edit' , { id: i}]\"\n                                    class=\"btn btn-icon btn-facebook mb-1 mr-1 \"\n                                    style=\"width:80px; font-size: 10px; cursor: pointer; background-color: #3b5998; color: white\">\n                                <i class=\"fa fa-edit\"></i>\n                                Edit\n                            </button>\n                            <button class=\"btn btn-icon btn-instagram mb-1 mr-1 \"\n                                    style=\"width:80px; font-size: 10px; background-color:lightgrey; cursor: pointer;\"\n                                    (click)=\"DeleteItem(i)\">\n                                <i class=\"fa fa-close\"></i>\n                                Delete\n                            </button>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/cars/index/index.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IndexComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__car_service__ = __webpack_require__("../../../../../src/app/cars/car.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__settings_settings_service__ = __webpack_require__("../../../../../src/settings/settings.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var IndexComponent = (function () {
    function IndexComponent(CarService, settings) {
        var _this = this;
        this.CarService = CarService;
        this.ItemsArray = [];
        this.host = '';
        this.settings = '';
        this.messageOpen = false;
        this.isOpened = true;
        this._autoCollapseWidth = 991;
        this.CarService.GetAllCars('GetAllCars').then(function (data) {
            console.log("getAllCars : ", data),
                _this.ItemsArray = data,
                _this.selectedItem = _this.ItemsArray[0];
            _this.host = settings.host,
                console.log(_this.ItemsArray[0].logo);
        });
    }
    IndexComponent.prototype.DeleteItem = function (i) {
        console.log("Del 1 : ", this.ItemsArray[i].id);
        /*this.CarService.DeleteCompany('DeleteCompany', this.ItemsArray[i].id).then((data: any) => {
            this.ItemsArray = data , console.log("Del 2 : ", data);
        })*/
    };
    IndexComponent.prototype.ngOnInit = function () {
        if (this.isOver()) {
            this.isOpened = false;
        }
    };
    IndexComponent.prototype.ngAfterContentInit = function () {
    };
    IndexComponent.prototype.toogleSidebar = function () {
        this.isOpened = !this.isOpened;
    };
    IndexComponent.prototype.isOver = function () {
        return window.matchMedia("(max-width: 991px)").matches;
    };
    IndexComponent.prototype.onSelect = function (message) {
        this.selectedItem = message;
        if (this.isOver()) {
            this.isOpened = false;
        }
    };
    IndexComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-index',
            template: __webpack_require__("../../../../../src/app/cars/index/index.component.html"),
            styles: [__webpack_require__("../../../../../src/app/icons/fontawesome/fontawesome.component.scss"), __webpack_require__("../../../../../src/app/media/list/list.component.scss"), __webpack_require__("../../../../../src/app/email/email.component.scss"), __webpack_require__("../../../../../src/app/cars/index/index.component.css")],
            providers: [__WEBPACK_IMPORTED_MODULE_1__car_service__["a" /* CarService */]]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__car_service__["a" /* CarService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__car_service__["a" /* CarService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__settings_settings_service__["a" /* SettingsService */]) === "function" && _b || Object])
    ], IndexComponent);
    return IndexComponent;
    var _a, _b;
}());

//# sourceMappingURL=index.component.js.map

/***/ }),

/***/ "../../../../../src/app/email/email.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/* $colors\n ------------------------------------------*/\n:host /deep/ {\n  height: 100%; }\n  :host /deep/ .email-panel.ng-sidebar {\n    width: 300px;\n    border-right: 1px solid rgba(0, 0, 0, 0.1); }\n  :host /deep/ .list-group-item.selected {\n    background-color: #fff7cc;\n    border-color: rgba(255, 214, 0, 0.2); }\n    :host /deep/ .list-group-item.selected + .list-group-item {\n      border-color: rgba(255, 214, 0, 0.2); }\n    :host /deep/ .list-group-item.selected:hover, :host /deep/ .list-group-item.selected:focus, :host /deep/ .list-group-item.selected:active {\n      background-color: #fff7cc; }\n  :host /deep/ .list-group-item a .time {\n    font-size: 10px; }\n\n:host {\n  padding: 0 !important; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/email/mock-messages.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MESSAGES; });
/* tslint:disable:max-line-length */
var MESSAGES = [{
        from: 'Social',
        date: 1427207139000,
        subject: 'Check out this weeks most popular website designs in the Milkyway!',
        avatar: 'assets/images/face4.jpg',
        icon: 'group',
        iconClass: 'mat-text-primary',
        body: '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.</p>\n\n        <p>Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.</p>\n\n        <blockquote>\n            <i>Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet.</i>\n        </blockquote>\n\n        <p>Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum.</p>\n\n        <p>You don’t need to see his identification … These aren’t the droids you’re looking for … He can go about his business … Move along.</p>',
        tag: 'Personal',
        type: 'danger',
        important: true,
        id: 1
    }, {
        from: 'Promotions',
        date: 1427412725000,
        subject: 'eBook: The complete guide to creating Angularjs single page applications is here.',
        avatar: 'assets/images/face2.jpg',
        icon: 'local_offer',
        iconClass: 'mat-text-warn',
        body: '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.</p>\n\n        <p>Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.</p>\n\n        <blockquote>\n            <i>Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet.</i>\n        </blockquote>\n\n        <p>Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum.</p>\n\n        <p>You don’t need to see his identification … These aren’t the droids you’re looking for … He can go about his business … Move along.</p>',
        tag: 'Personal',
        type: 'success',
        important: false,
        id: 2
    }, {
        from: 'Updates',
        date: 1427546580000,
        subject: 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
        avatar: 'assets/images/face3.jpg',
        icon: 'info',
        iconClass: 'mat-text-accent',
        body: '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.</p>\n\n        <p>Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.</p>\n\n        <blockquote>\n            <i>Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet.</i>\n        </blockquote>\n\n        <p>Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum.</p>\n\n        <p>You don’t need to see his identification … These aren’t the droids you’re looking for … He can go about his business … Move along.</p>',
        tag: 'Clients',
        type: 'info',
        important: false,
        id: 3
    }, {
        from: 'Melissa Welch',
        date: 1427891640000,
        subject: 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
        avatar: 'assets/images/face1.jpg',
        icon: false,
        iconClass: false,
        body: '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.</p>\n\n        <p>Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.</p>\n\n        <blockquote>\n            <i>Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet.</i>\n        </blockquote>\n\n        <p>Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum.</p>\n\n        <p>You don’t need to see his identification … These aren’t the droids you’re looking for … He can go about his business … Move along.</p>',
        tag: 'Family',
        type: 'warning',
        important: true,
        id: 4
    }, {
        from: 'Vincent Peterson',
        date: 1428275520000,
        subject: 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
        avatar: 'assets/images/face2.jpg',
        icon: false,
        iconClass: false,
        body: '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.</p>\n\n        <p>Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.</p>\n\n        <blockquote>\n            <i>Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet.</i>\n        </blockquote>\n\n        <p>Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum.</p>\n\n        <p>You don’t need to see his identification … These aren’t the droids you’re looking for … He can go about his business … Move along.</p>',
        tag: 'Friends',
        type: 'info',
        important: false,
        id: 5
    }, {
        from: 'Pamela Wood',
        date: 1428830580000,
        subject: 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
        avatar: 'assets/images/face3.jpg',
        icon: false,
        iconClass: false,
        body: '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.</p>\n\n        <p>Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.</p>\n\n        <blockquote>\n            <i>Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet.</i>\n        </blockquote>\n\n        <p>Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum.</p>\n\n        <p>You don’t need to see his identification … These aren’t the droids you’re looking for … He can go about his business … Move along.</p>',
        tag: 'Personal',
        type: 'success',
        important: false,
        id: 6
    }, {
        from: 'Tammy Carpenter',
        date: 1429363920000,
        subject: 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
        avatar: 'assets/images/face4.jpg',
        icon: false,
        iconClass: false,
        body: '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.</p>\n\n        <p>Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.</p>\n\n        <blockquote>\n            <i>Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet.</i>\n        </blockquote>\n\n        <p>Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum.</p>\n\n        <p>You don’t need to see his identification … These aren’t the droids you’re looking for … He can go about his business … Move along.</p>',
        tag: 'Personal',
        type: 'none',
        important: false,
        id: 7
    }, {
        from: 'Emma Sullican',
        date: 1430274720000,
        subject: 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
        avatar: 'assets/images/face5.jpg',
        icon: false,
        iconClass: false,
        body: '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.</p>\n\n        <p>Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.</p>\n\n        <blockquote>\n            <i>Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet.</i>\n        </blockquote>\n\n        <p>Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum.</p>\n\n        <p>You don’t need to see his identification … These aren’t the droids you’re looking for … He can go about his business … Move along.</p>',
        tag: 'Clients',
        type: 'none',
        important: false,
        id: 8
    }, {
        from: 'Andrea Brewer',
        date: 1431293520000,
        subject: 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
        avatar: 'assets/images/face2.jpg',
        icon: false,
        iconClass: false,
        body: '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.</p>\n\n        <p>Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.</p>\n\n        <blockquote>\n            <i>Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet.</i>\n        </blockquote>\n\n        <p>Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum.</p>\n\n        <p>You don’t need to see his identification … These aren’t the droids you’re looking for … He can go about his business … Move along.</p>',
        tag: 'Family',
        type: 'success',
        important: false,
        id: 9
    }, {
        from: 'Sean Carpenter',
        date: 1433115240000,
        subject: 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
        avatar: 'assets/images/face1.jpg',
        icon: false,
        iconClass: false,
        body: '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.</p>\n\n        <p>Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.</p>\n\n        <blockquote>\n            <i>Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet.</i>\n        </blockquote>\n\n        <p>Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum.</p>\n\n        <p>You don’t need to see his identification … These aren’t the droids you’re looking for … He can go about his business … Move along.</p>',
        tag: 'Friends',
        type: 'info',
        important: true,
        id: 10
    }];
//# sourceMappingURL=mock-messages.js.map

/***/ })

});
//# sourceMappingURL=car.module.chunk.js.map