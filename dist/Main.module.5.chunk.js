webpackJsonp(["Main.module.5"],{

/***/ "../../../../../src/app/category/Main.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainModule", function() { return MainModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Main_routing__ = __webpack_require__("../../../../../src/app/category/Main.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_datatable__ = __webpack_require__("../../../../@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_datatable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_datatable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__MainService_service__ = __webpack_require__("../../../../../src/app/category/MainService.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__edit_edit_component__ = __webpack_require__("../../../../../src/app/category/edit/edit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__index_index_component__ = __webpack_require__("../../../../../src/app/category/index/index.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__add_add_component__ = __webpack_require__("../../../../../src/app/category/add/add.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ng2_file_upload__ = __webpack_require__("../../../../ng2-file-upload/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













var MainModule = (function () {
    function MainModule() {
    }
    MainModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__Main_routing__["a" /* MaindRoutes */]),
                __WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_datatable__["NgxDatatableModule"],
                __WEBPACK_IMPORTED_MODULE_6__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_10__ng_bootstrap_ng_bootstrap__["g" /* NgbModule */],
                __WEBPACK_IMPORTED_MODULE_11_ng2_file_upload__["FileUploadModule"],
                __WEBPACK_IMPORTED_MODULE_12__angular_forms__["FormsModule"]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_7__edit_edit_component__["a" /* EditComponent */],
                __WEBPACK_IMPORTED_MODULE_8__index_index_component__["a" /* IndexComponent */],
                __WEBPACK_IMPORTED_MODULE_9__add_add_component__["a" /* AddComponent */]
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_5__MainService_service__["a" /* MainService */]]
        })
    ], MainModule);
    return MainModule;
}());

//# sourceMappingURL=Main.module.js.map

/***/ }),

/***/ "../../../../../src/app/category/Main.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MaindRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__edit_edit_component__ = __webpack_require__("../../../../../src/app/category/edit/edit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__add_add_component__ = __webpack_require__("../../../../../src/app/category/add/add.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__index_index_component__ = __webpack_require__("../../../../../src/app/category/index/index.component.ts");



var MaindRoutes = [{
        path: '',
        children: [{
                path: 'index',
                component: __WEBPACK_IMPORTED_MODULE_2__index_index_component__["a" /* IndexComponent */],
                data: {
                    heading: 'קטגוריות'
                }
            }, {
                path: 'edit',
                component: __WEBPACK_IMPORTED_MODULE_0__edit_edit_component__["a" /* EditComponent */],
                data: {
                    heading: 'ערוך קטגוריה'
                }
            }, {
                path: 'add',
                component: __WEBPACK_IMPORTED_MODULE_1__add_add_component__["a" /* AddComponent */],
                data: {
                    heading: 'הוסף קטגוריה'
                }
            }]
    }];
//# sourceMappingURL=Main.routing.js.map

/***/ }),

/***/ "../../../../../src/app/category/MainService.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MainService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__("../../../../rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__settings_settings_service__ = __webpack_require__("../../../../../src/settings/settings.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//public this.ServerUrl = "";//http://www.tapper.co.il/salecar/laravel/public/api/";
var MainService = (function () {
    function MainService(http, settings) {
        this.http = http;
        this.ServerUrl = "";
        this.headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/x-www-form-urlencoded' });
        this.options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["f" /* RequestOptions */]({ headers: this.headers });
        this.Items = [];
        this.ServerUrl = settings.ServerUrl;
    }
    ;
    MainService.prototype.GetCategories = function (url) {
        var _this = this;
        var body = new FormData();
        body.append('uid', window.localStorage.identify);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { _this.Items = data; }).toPromise();
    };
    MainService.prototype.AddItem = function (url, Item, File) {
        this.Items = Item;
        var body = new FormData();
        body.append("file", File);
        body.append("category", JSON.stringify(this.Items));
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res; }).do(function (data) { }).toPromise();
    };
    MainService.prototype.EditPosition = function (url, id, position) {
        var body = new FormData();
        body.append("id", id);
        body.append("position", position);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res; }).do(function (data) { }).toPromise();
    };
    MainService.prototype.EditItem = function (url, Item, File) {
        console.log("IT : ", File);
        this.Items = Item;
        var body = new FormData();
        body.append("file", File);
        body.append("category", JSON.stringify(this.Items));
        //let body = 'category=' + JSON.stringify(this.Items);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { }).toPromise();
    };
    MainService.prototype.DeleteItem = function (url, Id) {
        var body = 'id=' + Id;
        return this.http.post(this.ServerUrl + '' + url, body, this.options).map(function (res) { return res.json(); }).do(function (data) { }).toPromise();
    };
    MainService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__settings_settings_service__["a" /* SettingsService */]) === "function" && _b || Object])
    ], MainService);
    return MainService;
    var _a, _b;
}());

;
//# sourceMappingURL=MainService.service.js.map

/***/ }),

/***/ "../../../../../src/app/category/add/add.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".FormClass\n{\n    text-align: right;\n    direction: rtl;\n}\n\n.row\n{\n    margin-top: 20px;\n}\n\n.textWhite\n{\n    background-color: white;\n}\n\ninput.ng-invalid.ng-touched\n{\n    border:1px solid red;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/category/add/add.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row FormClass\">\n  <div class=\"col-lg-12\">\n    <div class=\"card\">\n      <div class=\"card-header\">\n        הוסף קטגוריה\n      </div>\n      <div class=\"card-body\">\n        <form (ngSubmit)=\"onSubmit(f)\" #f=\"ngForm\">\n          <div class=\"row\" >\n            <div class=\"form-group\" class=\"col-lg-6\" *ngFor=\"let row of rows let i=index\" style=\"margin-top: 15px;\">\n              <label for=\"formGroupExampleInput\">הכנס {{rowsNames[i]}} </label>\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" name={{row}} ngModel required>\n            </div>\n          </div>\n\n            <!--\n          <div class=\"row\">\n            <div class=\"form-group\" class=\"col-lg-12\">\n              <label for=\"formGroupExampleInput\">פרטים נוספים</label>\n              <textarea  rows=\"4\" cols=\"50\"  class=\"form-control textWhite\" id=\"formGroupExampleInput\" name=\"description\" ngModel required> </textarea>\n            </div>\n          </div>-->\n        <div class=\"row\">\n            <input #fileInput type=\"file\" required />\n        </div>\n\n          <div class=\"row\" align=\"left\" style=\"float: left; margin-left: 5px\">\n            <button [disabled]=\"!f.valid\" type=\"submit\" class=\"btn btn-primary btn-icon loading-demo mr-1 mb-1\"\n                    style=\"padding:0 20px !important; cursor: pointer; width: 205px; text-align: center; font-weight: bold; font-size: 16px\">\n              <span style=\"text-align: center; padding: 10px;  width: 100% !important;\">שלח טופס</span>\n            </button>\n          </div>\n\n        </form>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/category/add/add.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__MainService_service__ = __webpack_require__("../../../../../src/app/category/MainService.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AddComponent = (function () {
    function AddComponent(route, http, service, router) {
        this.route = route;
        this.http = http;
        this.service = service;
        this.router = router;
        this.navigateTo = '/category/index';
        this.imageSrc = '';
        this.folderName = 'category';
        this.rowsNames = ['נושא'];
        this.rows = ['title'];
        console.log("Row : ", this.rows);
    }
    AddComponent.prototype.onSubmit = function (form) {
        var _this = this;
        console.log(form.value);
        var fi = this.fileInput.nativeElement;
        var fileToUpload;
        if (fi.files && fi.files[0]) {
            fileToUpload = fi.files[0];
        }
        this.service.AddItem('AddCategory', form.value, fileToUpload).then(function (data) {
            console.log("AddCompany : ", data);
            _this.router.navigate([_this.navigateTo]);
        });
    };
    AddComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.paramsSub = this.route.params.subscribe(function (params) { return _this.id = params['id']; });
    };
    AddComponent.prototype.ngOnDestroy = function () {
        this.paramsSub.unsubscribe();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("fileInput"),
        __metadata("design:type", Object)
    ], AddComponent.prototype, "fileInput", void 0);
    AddComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-add',
            template: __webpack_require__("../../../../../src/app/category/add/add.component.html"),
            styles: [__webpack_require__("../../../../../src/app/category/add/add.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__MainService_service__["a" /* MainService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__MainService_service__["a" /* MainService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _d || Object])
    ], AddComponent);
    return AddComponent;
    var _a, _b, _c, _d;
}());

//# sourceMappingURL=add.component.js.map

/***/ }),

/***/ "../../../../../src/app/category/edit/edit.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".FormClass\n{\n    text-align: right;\n    direction: rtl;\n}\n\n.row\n{\n    margin-top: 20px;\n}\n\n.textWhite\n{\n    background-color: white;\n}\n\ninput.ng-invalid.ng-touched\n{\n    border:1px solid red;\n}\n\n.SearchInput\n{\n    background-color: white;\n    text-align: right;\n}\n\n.p-3\n{\n    padding: 0px;\n    background-color: red;\n}\n\n.KitchensForm\n{\n    direction: rtl;\n    text-align: right;\n}\n\n.formCheck\n{\n    position: relative;\n    left:20px;\n    text-align: right;\n    width: 50px;\n    background-color: red;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/category/edit/edit.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row FormClass\">\n    <div class=\"col-lg-12\">\n        <div class=\"card\">\n            <div class=\"card-header\">\n                ערוך קטגוריה\n            </div>\n            <div class=\"card-body\">\n                <form (ngSubmit)=\"onSubmit(f)\" #f=\"ngForm\" >\n                    <div class=\"row\" >\n                        <div class=\"form-group\" class=\"col-lg-6\" *ngFor=\"let row of rows let i=index\" style=\"margin-top: 15px;\">\n                            <label for=\"formGroupExampleInput\">הכנס {{rowsNames[i]}} </label>\n                            <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" name={{row}} [(ngModel)]=\"Item[row]\"  ngModel required>\n                        </div>\n                    </div>\n                   <!-- <div class=\"row\">\n                        <div class=\"form-group\" class=\"col-lg-12\">\n                            <label for=\"formGroupExampleInput\">פרטים נוספים</label>\n                            <textarea rows=\"4\" cols=\"50\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" [(ngModel)]=\"Company.desc\" name=\"description\" ngModel required> </textarea>\n                        </div>\n                    </div> -->\n                    <div class=\"row\">\n                        <img src=\"{{Image}}\" style=\"width:50px; height: 50px\" />\n                        <input  #fileInput   type=\"file\"(change)=\"onChange($event)\" style=\"margin-right: 20px; margin-top: 10px\"/>\n                    </div>\n\n                    <div class=\"row\" align=\"left\" style=\"float: left; margin-left: 5px\">\n                        <button [disabled]=\"!f.valid\" type=\"submit\"\n                                class=\"btn btn-primary btn-icon loading-demo mr-1 mb-1\"\n                                style=\"padding:0 20px !important; cursor: pointer; width: 205px; text-align: center; font-weight: bold; font-size: 16px\">\n                            <span style=\"text-align: center; padding: 10px;  width: 100% !important;\">שלח טופס</span>\n                        </button>\n                    </div>\n                </form>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/category/edit/edit.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__MainService_service__ = __webpack_require__("../../../../../src/app/category/MainService.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__settings_settings_service__ = __webpack_require__("../../../../../src/settings/settings.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var URL = 'https://evening-anchorage-3159.herokuapp.com/api/';
var EditComponent = (function () {
    function EditComponent(route, http, service, router, settings) {
        var _this = this;
        this.route = route;
        this.http = http;
        this.service = service;
        this.router = router;
        this.settings = settings;
        this.navigateTo = '/category/index';
        this.imageSrc = '';
        this.Items = [];
        this.rowsNames = ['נושא'];
        this.rows = ['title'];
        this.Change = false;
        this.route.params.subscribe(function (params) {
            _this.Id = params['id'];
            _this.Item = _this.service.Items[_this.Id];
            _this.host = settings.host;
            _this.Image = _this.host + '' + _this.Item.image;
            console.log("fkitchens ", _this.Item);
        });
    }
    EditComponent.prototype.onSubmit = function (form) {
        var _this = this;
        var fi = this.fileInput.nativeElement;
        var fileToUpload;
        if (fi.files && fi.files[0]) {
            fileToUpload = fi.files[0];
            console.log("fff : ", fileToUpload);
        }
        this.Item.change = this.Change;
        console.log("Edit12 : ", fileToUpload);
        this.service.EditItem('EditCategory', this.Item, fileToUpload).then(function (data) {
            console.log("AddCompany : ", data);
            _this.router.navigate([_this.navigateTo]);
        });
    };
    EditComponent.prototype.onChange = function (event) {
        var _this = this;
        this.Change = true;
        var files = event.srcElement.files;
        this.Image = event.target.files[0];
        var reader = new FileReader();
        reader.onload = function (e) {
            _this.Image = e.target.result;
        };
        reader.readAsDataURL(event.target.files[0]);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("fileInput"),
        __metadata("design:type", Object)
    ], EditComponent.prototype, "fileInput", void 0);
    EditComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-edit',
            template: __webpack_require__("../../../../../src/app/category/edit/edit.component.html"),
            styles: [__webpack_require__("../../../../../src/app/icons/fontawesome/fontawesome.component.scss"), __webpack_require__("../../../../../src/app/components/buttons/buttons.component.scss"), __webpack_require__("../../../../../src/app/category/edit/edit.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__MainService_service__["a" /* MainService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__MainService_service__["a" /* MainService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__settings_settings_service__["a" /* SettingsService */]) === "function" && _e || Object])
    ], EditComponent);
    return EditComponent;
    var _a, _b, _c, _d, _e;
}());

//# sourceMappingURL=edit.component.js.map

/***/ }),

/***/ "../../../../../src/app/category/index/index.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".card\n{\n    text-align: right;\n    direction: rtl;\n}\n\n.card-body\n{\n    border-bottom: 1px solid #f2f1f2;\n}\n\n.mr-auto\n{\n    text-align: right;\n    direction: rtl;\n    background-color: red;\n    float: right;\n}\n\n.mr-3\n{\n    background-color: green;\n    float: right;\n}\n\n.IconClass\n{\n    margin-top: 6px;\n    text-align: center;\n    padding-left: -13px !important;\n    background-color: red;\n}\n\n.d-icon{\n    margin-top: -20px;\n}\n\n.titleImage\n{\n    width: 80px;\n    border-radius: 70%;\n    height:80px;\n    margin-top:3px;\n    border: 1px solid #f1f1f1;\n}\n\n.textHeader\n{\n    color: #337ab7;;\n    font-size: 15px;\n    font-weight: bold;\n}\n\n.SearchInput{\n    background-color: white;\n    text-align: right;\n    paddding:5px;\n    margin-bottom: -15px;\n    margin-top: -15px;\n}\n\n.p-3{\n    margin-top: 2px;\n    margin-bottom: 2px;\n}\n\n.sideButton\n{\n    width:90%;\n    cursor: pointer;\n    background-color: #3b5998;\n    color: white;\n    text-align: right;\n    padding: 3px;\n    overflow: hidden;\n}\n\n.sideButtonText\n{\n    margin-right: 10px;\n    font-size: 14px;\n    font-weight: bold;\n    top: 7px !important;\n    position: relative;\n}\n\n.sideButtonTextEmpty{\n    margin-right: 10px;\n    font-size: 14px;\n    font-weight: bold;\n    top: 0px !important;\n    position: relative;\n}\n\n.sideButtonBadge\n{\n    background-color: red;\n    border-radius:50%;\n    font-size: 12px;\n    margin-top: 5px;\n    padding: 3px;\n    width: 25px;\n    height: 25px;\n}\n\n.buttonDivBadge\n{\n    float: right;\n    width: 12%;\n}\n\n.buttonDivText\n{\n    float: right;\n    width: 60%;\n}\n\n\n\n.buttonDivIcon\n{\n    float: left;\n    width: 20%;\n}\n\n.badgeText\n{\n    top: 4px;\n    position: relative;\n}\n\n\n\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/category/index/index.component.html":
/***/ (function(module, exports) {

module.exports = "\n<div class=\"row nopadding\" style=\"padding: 0px; background-color: white; margin-top: -15px; margin-left: -17px\">\n    <div class=\"col-lg-2 nopadding\" style=\"background-color: #eff1f1; padding: 0px\" >\n        <div style=\"margin-top: 20px; padding: 10px;\" align=\"center\">\n            <button [routerLink]=\"['/', folderName , 'add' ]\"  class=\"btn btn-icon btn-facebook mb-1 mr-1 sideButton\" style=\"background-color: #666\">\n                <div class=\"buttonDivBadge\">\n\n                </div>\n                <div class=\"buttonDivText\">\n                    <span class=\"sideButtonTextEmpty\">{{addButton}}</span>\n                </div>\n                <div class=\"buttonDivIcon\">\n                    <i class=\"fa fa-chevron-left\"></i>\n                </div>\n            </button>\n\n            <hr>\n\n\n        </div>\n    </div>\n    <div class=\"col-lg-10 nopadding\" style=\"margin-top: 10px;\">\n        <div class=\"p-3\" style=\"\">\n            <div style=\"width: 99%; float: right\">\n                <input type=\"text\" (keyup)='updateFilter($event)' class=\"form-control SearchInput\" placeholder=\"חפש חברה\"  id=\"formGroupExampleInput\" name=\"name\">\n            </div>\n        </div>\n\n        <div class=\"p-3\">\n            <div class=\"card\">\n\n                <div class=\"card-header\">\n                    <div class=\"card-header-text w-100\">\n                        <div class=\"card-title\">\n                            עמוד קטגוריות\n                        </div>\n                        <div class=\"card-subtitle text-capitalize ff-sans\">\n                            כרגע יש {{ItemsArray.length}} קטגוריות במערכת\n                        </div>\n                    </div>\n                    <button [routerLink]=\"['/', folderName , 'add']\" class=\"btn btn-icon btn-facebook mb-1 mr-1 \" style=\"width: 130px; background-color:#3b5998; cursor: pointer; color: white;  float: left\">\n                        <i class=\"fa fa-plus-circle\"></i>{{addButton}}\n                    </button>\n                </div>\n\n                <div class=\"card-body\" *ngFor=\"let item of ItemsArray let i=index\">\n                    <div class=\"row d-flex\">\n                        <div class=\"col-lg-1 \"  align=\"center\">\n                            <img *ngIf=\"item.image != ''\" class=\"titleImage\" src=\"{{host}}{{item.image}}\" style=\"\" />\n                            <img *ngIf=\"item.image == ''\" class=\"titleImage\" src=\"{{avatar}}\" style=\"\" />\n                        </div>\n                        <div class=\"col-lg-7 col-md-9\" style=\"margin-top: 10px;\">\n                            <a href=\"javascript:;\" class=\"textHeader\">{{item.title}}</a>\n                            <div>Position: <strong>{{item.position}}</strong></div>\n                            <div>{{item.date}}</div>\n                        </div>\n                        <div class=\"col-lg-4 col-md-2 d-flex\" align=\"left\" >\n                            <div class=\"col-lg-8 col-md-3\" style=\"margin-top: 15px; cursor: pointer; text-align: center\" align=\"left\">\n                                <div style=\"float: left\"  [routerLink]=\"['/', 'subCategory' , 'index' , { id:item.id}]\">\n                                    <h6>{{item.products_sub_categories.length}}</h6>\n                                    <small class=\"d-block\">תת קטגוריות</small>\n                                </div>\n                                <!--<div style=\"float: left; margin-left: 20px;\" [routerLink]=\"['/', 'order' , 'index' , { id:item.index}]\">-->\n                                    <!--<h6>{{item.Orders}}</h6>-->\n                                    <!--<small class=\"d-block\">הזמנות</small>-->\n                                <!--</div>-->\n                            </div>\n                            <div class=\"col-lg-4 col-md-6\" style=\"padding-left:0px !important; float: left; cursor: pointer;\" align=\"left\">\n                                <button  (click)=\"openRankModal(rank, item)\" class=\"btn btn-icon btn-facebook mb-1 mr-1 \" style=\"width: 100px; cursor: pointer; background-color: #546e7a; color: white\">\n                                    <i class=\"fa fa-star\"></i>\n                                    Position\n                                </button>\n                                <button [routerLink]=\"['/', folderName , 'edit' , { id: i}]\"  class=\"btn btn-icon btn-facebook mb-1 mr-1 \" style=\"width: 100px; cursor: pointer; background-color: #3b5998; color: white\">\n                                    <i class=\"fa fa-edit\"></i>\n                                    Edit\n                                </button>\n                                <button class=\"btn btn-icon btn-instagram mb-1 mr-1 \" style=\"width: 100px; background-color:lightgrey; cursor: pointer;\" (click)=\"DeleteItem(i)\">\n                                    <i class=\"fa fa-close\"></i>\n                                    Delete\n                                </button>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n</div>\n\n\n        <ng-template ngbModalContainer></ng-template>\n        <ng-template #rank>\n            <div class=\"modal-header\">\n                <h6 class=\"modal-title text-uppercase\">עידכון דירוג</h6>\n                <button type=\"button\" class=\"close\" aria-label=\"סגור\" (click)=\"closeRankModal()\">\n                    <span aria-hidden=\"true\">&times;</span>\n                </button>\n            </div>\n            <div class=\"modal-body\">\n                <input type=\"number\"  min=\"0\" dir=\"ltr\" placeholder=\"דירוג\" [(ngModel)]=\"categroyrow.position\" style=\"width: 100%;\">\n            </div>\n            <div class=\"modal-footer\">\n                <button type=\"button\" class=\"btn btn-secondary\" (click)=\"closeRankModal()\">סגור</button>\n                <button type=\"button\" *ngIf=\"!sent\" class=\"btn btn-primary\"  (click)=\"updateCatPosition()\">עידכון</button>\n            </div>\n        </ng-template>\n\n"

/***/ }),

/***/ "../../../../../src/app/category/index/index.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IndexComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__MainService_service__ = __webpack_require__("../../../../../src/app/category/MainService.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__settings_settings_service__ = __webpack_require__("../../../../../src/settings/settings.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var IndexComponent = (function () {
    function IndexComponent(MainService, settings, modalService) {
        this.MainService = MainService;
        this.modalService = modalService;
        this.ItemsArray = [];
        this.ItemsArray1 = [];
        this.host = '';
        this.settings = '';
        this.avatar = '';
        this.folderName = 'category';
        this.addButton = 'הוסף קטגורייה';
        this.host = settings.host;
        this.avatar = settings.avatar;
        this.getItems();
    }
    IndexComponent.prototype.ngOnInit = function () {
    };
    IndexComponent.prototype.getItems = function () {
        var _this = this;
        this.MainService.GetCategories('GetCategories').then(function (data) {
            console.log("GetCategories : ", data);
            _this.ItemsArray = data,
                _this.ItemsArray1 = data;
        });
    };
    IndexComponent.prototype.openRankModal = function (content, row) {
        this.rankModal = this.modalService.open(content);
        this.categroyrow = row;
    };
    IndexComponent.prototype.closeRankModal = function () {
        this.rankModal.close();
    };
    IndexComponent.prototype.updateCatPosition = function () {
        var _this = this;
        this.MainService.EditPosition('WebEditCatPosition', this.categroyrow.id, this.categroyrow.position).then(function (data) {
            _this.closeRankModal();
            _this.getItems();
        });
    };
    IndexComponent.prototype.DeleteItem = function (i) {
        var _this = this;
        var confirmBox = confirm("האם לאשר מחיקת קטגוריה?");
        if (confirmBox == true) {
            console.log("Del 1 : ", this.ItemsArray[i].id);
            this.MainService.DeleteItem('DeleteCategory', this.ItemsArray[i].id).then(function (data) {
                _this.ItemsArray = data, console.log("Del 2 : ", data);
            });
        }
    };
    IndexComponent.prototype.updateFilter = function (event) {
        var val = event.target.value;
        // filter our data
        var temp = this.ItemsArray1.filter(function (d) {
            return d.title.toLowerCase().indexOf(val) !== -1 || !val;
        });
        // update the rows
        this.ItemsArray = temp;
    };
    IndexComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-index',
            template: __webpack_require__("../../../../../src/app/category/index/index.component.html"),
            styles: [__webpack_require__("../../../../../src/app/icons/fontawesome/fontawesome.component.scss"), __webpack_require__("../../../../../src/app/media/list/list.component.scss"), __webpack_require__("../../../../../src/app/category/index/index.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__MainService_service__["a" /* MainService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__MainService_service__["a" /* MainService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__settings_settings_service__["a" /* SettingsService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__ng_bootstrap_ng_bootstrap__["f" /* NgbModal */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__ng_bootstrap_ng_bootstrap__["f" /* NgbModal */]) === "function" && _c || Object])
    ], IndexComponent);
    return IndexComponent;
    var _a, _b, _c;
}());

//# sourceMappingURL=index.component.js.map

/***/ })

});
//# sourceMappingURL=Main.module.5.chunk.js.map