webpackJsonp(["main"],{

/***/ "../../../../../src/$$_gendir lazy recursive":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./CampignGallery/Main.module": [
		"../../../../../src/app/CampignGallery/Main.module.ts",
		"common",
		"Main.module.6"
	],
	"./app_settings_page/Main.module": [
		"../../../../../src/app/app_settings_page/Main.module.ts",
		"common",
		"Main.module.7"
	],
	"./authentication/authentication.module": [
		"../../../../../src/app/authentication/authentication.module.ts",
		"common",
		"authentication.module"
	],
	"./cards/cards.module": [
		"../../../../../src/app/cards/cards.module.ts",
		"common",
		"cards.module"
	],
	"./cars/car.module": [
		"../../../../../src/app/cars/car.module.ts",
		"common",
		"car.module"
	],
	"./category/Main.module": [
		"../../../../../src/app/category/Main.module.ts",
		"common",
		"Main.module.5"
	],
	"./charts/charts.module": [
		"../../../../../src/app/charts/charts.module.ts",
		"common",
		"charts.module"
	],
	"./chat/employee.module": [
		"../../../../../src/app/chat/employee.module.ts",
		"common",
		"employee.module.1"
	],
	"./chat_messages/employee.module": [
		"../../../../../src/app/chat_messages/employee.module.ts",
		"common",
		"employee.module.0"
	],
	"./company/company.module": [
		"../../../../../src/app/company/company.module.ts",
		"common",
		"company.module"
	],
	"./components/components.module": [
		"../../../../../src/app/components/components.module.ts",
		"common",
		"components.module"
	],
	"./dashboard/dashboard.module": [
		"../../../../../src/app/dashboard/dashboard.module.ts",
		"common",
		"dashboard.module"
	],
	"./datatable/datatable.module": [
		"../../../../../src/app/datatable/datatable.module.ts",
		"datatable.module"
	],
	"./docs/docs.module": [
		"../../../../../src/app/docs/docs.module.ts",
		"docs.module"
	],
	"./employee/employee.module": [
		"../../../../../src/app/employee/employee.module.ts",
		"common",
		"employee.module"
	],
	"./error/error.module": [
		"../../../../../src/app/error/error.module.ts",
		"error.module"
	],
	"./form/form.module": [
		"../../../../../src/app/form/form.module.ts",
		"common",
		"form.module"
	],
	"./fullcalendar/fullcalendar.module": [
		"../../../../../src/app/fullcalendar/fullcalendar.module.ts",
		"fullcalendar.module"
	],
	"./icons/icons.module": [
		"../../../../../src/app/icons/icons.module.ts",
		"icons.module"
	],
	"./kitchens/kitchen.module": [
		"../../../../../src/app/kitchens/kitchen.module.ts",
		"common",
		"kitchen.module"
	],
	"./landing/landing.module": [
		"../../../../../src/app/landing/landing.module.ts",
		"landing.module"
	],
	"./maps/maps.module": [
		"../../../../../src/app/maps/maps.module.ts",
		"maps.module"
	],
	"./media/media.module": [
		"../../../../../src/app/media/media.module.ts",
		"common",
		"media.module"
	],
	"./orders/order.module": [
		"../../../../../src/app/orders/order.module.ts",
		"common",
		"order.module"
	],
	"./orders_new/Main.module": [
		"../../../../../src/app/orders_new/Main.module.ts",
		"common",
		"Main.module.4"
	],
	"./pages/pages.module": [
		"../../../../../src/app/pages/pages.module.ts",
		"pages.module"
	],
	"./productImages/Main.module": [
		"../../../../../src/app/productImages/Main.module.ts",
		"common",
		"Main.module.3"
	],
	"./products/Main.module": [
		"../../../../../src/app/products/Main.module.ts",
		"common",
		"Main.module"
	],
	"./social/social.module": [
		"../../../../../src/app/social/social.module.ts",
		"social.module"
	],
	"./subCategory/Main.module": [
		"../../../../../src/app/subCategory/Main.module.ts",
		"common",
		"Main.module.2"
	],
	"./suppliers/Main.module": [
		"../../../../../src/app/suppliers/Main.module.ts",
		"common",
		"Main.module.1"
	],
	"./tables/tables.module": [
		"../../../../../src/app/tables/tables.module.ts",
		"tables.module"
	],
	"./taskboard/taskboard.module": [
		"../../../../../src/app/taskboard/taskboard.module.ts",
		"common",
		"taskboard.module"
	],
	"./users/Main.module": [
		"../../../../../src/app/users/Main.module.ts",
		"common",
		"Main.module.0"
	],
	"./widgets/widgets.module": [
		"../../../../../src/app/widgets/widgets.module.ts",
		"widgets.module"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "../../../../../src/$$_gendir lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "../../../../../src/app/_guards/auth.guard.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthGuard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AuthGuard = (function () {
    function AuthGuard(router) {
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function (route, state) {
        if (localStorage.getItem('id')) {
            return true;
        }
        // not logged in so redirect to login page with the return url
        this.router.navigate(['/authentication']);
        return false;
    };
    AuthGuard = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _a || Object])
    ], AuthGuard);
    return AuthGuard;
    var _a;
}());

//# sourceMappingURL=auth.guard.js.map

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__ = __webpack_require__("../../../../@ngx-translate/core/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = (function () {
    function AppComponent(translate) {
        translate.addLangs(['en', 'fr']);
        translate.setDefaultLang('en');
        var browserLang = translate.getBrowserLang();
        translate.use(browserLang.match(/en|fr/) ? browserLang : 'en');
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root',
            template: '<router-outlet></router-outlet>'
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__["c" /* TranslateService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__["c" /* TranslateService */]) === "function" && _a || Object])
    ], AppComponent);
    return AppComponent;
    var _a;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export createTranslateLoader */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_animations__ = __webpack_require__("../../../platform-browser/@angular/platform-browser/animations.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_common_http__ = __webpack_require__("../../../common/@angular/common/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__ = __webpack_require__("../../../../@ngx-translate/core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ngx_translate_http_loader__ = __webpack_require__("../../../../@ngx-translate/http-loader/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ng_sidebar__ = __webpack_require__("../../../../ng-sidebar/lib/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ng_sidebar___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_ng_sidebar__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_routing__ = __webpack_require__("../../../../../src/app/app.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__layouts_admin_admin_layout_component__ = __webpack_require__("../../../../../src/app/layouts/admin/admin-layout.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__layouts_auth_auth_layout_component__ = __webpack_require__("../../../../../src/app/layouts/auth/auth-layout.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__shared_shared_module__ = __webpack_require__("../../../../../src/app/shared/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__settings_settings_service__ = __webpack_require__("../../../../../src/settings/settings.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__icons_fontawesome_fontawesome_component__ = __webpack_require__("../../../../../src/app/icons/fontawesome/fontawesome.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__icons_linea_linea_component__ = __webpack_require__("../../../../../src/app/icons/linea/linea.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__icons_sli_sli_component__ = __webpack_require__("../../../../../src/app/icons/sli/sli.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__components_button_icons_button_icons_component__ = __webpack_require__("../../../../../src/app/components/button-icons/button-icons.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__datatable_data_table_data_table_component__ = __webpack_require__("../../../../../src/app/datatable/data-table/data-table.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__swimlane_ngx_datatable__ = __webpack_require__("../../../../@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__swimlane_ngx_datatable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_22__swimlane_ngx_datatable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__datatable_table_sorting_table_sorting_component__ = __webpack_require__("../../../../../src/app/datatable/table-sorting/table-sorting.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__datatable_table_selection_table_selection_component__ = __webpack_require__("../../../../../src/app/datatable/table-selection/table-selection.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__datatable_table_pinning_table_pinning_component__ = __webpack_require__("../../../../../src/app/datatable/table-pinning/table-pinning.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__datatable_table_paging_table_paging_component__ = __webpack_require__("../../../../../src/app/datatable/table-paging/table-paging.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__datatable_table_filter_table_filter_component__ = __webpack_require__("../../../../../src/app/datatable/table-filter/table-filter.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__datatable_table_editing_table_editing_component__ = __webpack_require__("../../../../../src/app/datatable/table-editing/table-editing.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__guards_auth_guard__ = __webpack_require__("../../../../../src/app/_guards/auth.guard.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






























function createTranslateLoader(http) {
    return new __WEBPACK_IMPORTED_MODULE_7__ngx_translate_http_loader__["a" /* TranslateHttpLoader */](http, './assets/i18n/', '.json');
}
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_11__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_12__layouts_admin_admin_layout_component__["a" /* AdminLayoutComponent */],
                __WEBPACK_IMPORTED_MODULE_13__layouts_auth_auth_layout_component__["a" /* AuthLayoutComponent */],
                __WEBPACK_IMPORTED_MODULE_17__icons_fontawesome_fontawesome_component__["a" /* FontawesomeComponent */],
                __WEBPACK_IMPORTED_MODULE_19__icons_sli_sli_component__["a" /* SliComponent */],
                __WEBPACK_IMPORTED_MODULE_20__components_button_icons_button_icons_component__["a" /* ButtonIconsComponent */],
                __WEBPACK_IMPORTED_MODULE_18__icons_linea_linea_component__["a" /* LineaComponent */],
                __WEBPACK_IMPORTED_MODULE_21__datatable_data_table_data_table_component__["a" /* DataTableComponent */],
                __WEBPACK_IMPORTED_MODULE_28__datatable_table_editing_table_editing_component__["a" /* TableEditingComponent */],
                __WEBPACK_IMPORTED_MODULE_27__datatable_table_filter_table_filter_component__["a" /* TableFilterComponent */],
                __WEBPACK_IMPORTED_MODULE_26__datatable_table_paging_table_paging_component__["a" /* TablePagingComponent */],
                __WEBPACK_IMPORTED_MODULE_25__datatable_table_pinning_table_pinning_component__["a" /* TablePinningComponent */],
                __WEBPACK_IMPORTED_MODULE_24__datatable_table_selection_table_selection_component__["a" /* TableSelectionComponent */],
                __WEBPACK_IMPORTED_MODULE_23__datatable_table_sorting_table_sorting_component__["a" /* TableSortingComponent */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["BrowserModule"],
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
                __WEBPACK_IMPORTED_MODULE_14__shared_shared_module__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_router__["d" /* RouterModule */].forRoot(__WEBPACK_IMPORTED_MODULE_10__app_routing__["a" /* AppRoutes */]),
                __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_22__swimlane_ngx_datatable__["NgxDatatableModule"],
                __WEBPACK_IMPORTED_MODULE_5__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_16__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__["b" /* TranslateModule */].forRoot({
                    loader: {
                        provide: __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__["a" /* TranslateLoader */],
                        useFactory: (createTranslateLoader),
                        deps: [__WEBPACK_IMPORTED_MODULE_5__angular_common_http__["a" /* HttpClient */]]
                    }
                }),
                __WEBPACK_IMPORTED_MODULE_8__ng_bootstrap_ng_bootstrap__["g" /* NgbModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_9_ng_sidebar__["SidebarModule"].forRoot()
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_15__settings_settings_service__["a" /* SettingsService */], __WEBPACK_IMPORTED_MODULE_29__guards_auth_guard__["a" /* AuthGuard */]],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_11__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "../../../../../src/app/app.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__layouts_admin_admin_layout_component__ = __webpack_require__("../../../../../src/app/layouts/admin/admin-layout.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__layouts_auth_auth_layout_component__ = __webpack_require__("../../../../../src/app/layouts/auth/auth-layout.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__guards_auth_guard__ = __webpack_require__("../../../../../src/app/_guards/auth.guard.ts");



var AppRoutes = [{
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_0__layouts_admin_admin_layout_component__["a" /* AdminLayoutComponent */],
        children: [{
                path: '',
                loadChildren: './dashboard/dashboard.module#DashboardModule'
            },
            {
                path: 'subCategory',
                loadChildren: './subCategory/Main.module#MainModule'
            }, {
                path: 'suppliers',
                loadChildren: './suppliers/Main.module#MainModule'
            }, {
                path: 'products',
                loadChildren: './products/Main.module#MainModule'
            }, {
                path: 'productImages',
                loadChildren: './productImages/Main.module#MainModule'
            },
            {
                path: 'chat_messages',
                loadChildren: './chat_messages/employee.module#EmployeeModule'
            },
            {
                path: 'chat',
                loadChildren: './chat/employee.module#EmployeeModule'
            },
            {
                path: 'CampignGallery',
                loadChildren: './CampignGallery/Main.module#MainModule'
            }, {
                path: 'app_settings_page',
                loadChildren: './app_settings_page/Main.module#MainModule'
            }, {
                path: 'users',
                loadChildren: './users/Main.module#MainModule'
            }, {
                path: 'car',
                loadChildren: './cars/car.module#CarModule'
            }, {
                path: 'employee',
                loadChildren: './employee/employee.module#EmployeeModule'
            }, {
                path: 'order',
                loadChildren: './orders/order.module#OrderModule'
            }, {
                path: 'kitchen',
                loadChildren: './kitchens/kitchen.module#KitchenModule'
            }, {
                path: 'company',
                loadChildren: './company/company.module#CompanyModule'
            }, {
                path: 'category',
                loadChildren: './category/Main.module#MainModule'
            }, {
                path: 'orders_new',
                loadChildren: './orders_new/Main.module#MainModule'
            }, {
                path: 'components',
                loadChildren: './components/components.module#ComponentsModule'
            }, {
                path: 'icons',
                loadChildren: './icons/icons.module#IconsModule'
            }, {
                path: 'cards',
                loadChildren: './cards/cards.module#CardsModule'
            }, {
                path: 'forms',
                loadChildren: './form/form.module#FormModule'
            }, {
                path: 'tables',
                loadChildren: './tables/tables.module#TablesModule'
            }, {
                path: 'datatable',
                loadChildren: './datatable/datatable.module#DatatableModule'
            }, {
                path: 'charts',
                loadChildren: './charts/charts.module#ChartsModule'
            }, {
                path: 'maps',
                loadChildren: './maps/maps.module#MapsModule'
            }, {
                path: 'pages',
                loadChildren: './pages/pages.module#PagesModule'
            }, {
                path: 'taskboard',
                loadChildren: './taskboard/taskboard.module#TaskboardModule'
            }, {
                path: 'calendar',
                loadChildren: './fullcalendar/fullcalendar.module#FullcalendarModule'
            }, {
                path: 'media',
                loadChildren: './media/media.module#MediaModule'
            }, {
                path: 'widgets',
                loadChildren: './widgets/widgets.module#WidgetsModule'
            }, {
                path: 'social',
                loadChildren: './social/social.module#SocialModule'
            }, {
                path: 'docs',
                loadChildren: './docs/docs.module#DocsModule'
            }], canActivate: [__WEBPACK_IMPORTED_MODULE_2__guards_auth_guard__["a" /* AuthGuard */]]
    }, {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_1__layouts_auth_auth_layout_component__["a" /* AuthLayoutComponent */],
        children: [{
                path: 'authentication',
                loadChildren: './authentication/authentication.module#AuthenticationModule'
            }, {
                path: 'error',
                loadChildren: './error/error.module#ErrorModule'
            }, {
                path: 'landing',
                loadChildren: './landing/landing.module#LandingModule'
            }]
    }, {
        path: '**',
        redirectTo: 'error/404'
    }];
//# sourceMappingURL=app.routing.js.map

/***/ }),

/***/ "../../../../../src/app/components/button-icons/button-icons.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"card\">\n  <div class=\"card-header\">Social buttons</div>\n  <div class=\"card-body\">\n    <div>        \n      <button class=\"btn btn-icon btn-adn mb-1 mr-1\">\n        <i class=\"fa fa-adn\"></i>\n        App.net\n      </button>\n\n      <button class=\"btn btn-icon btn-bitbucket mb-1 mr-1\">\n        <i class=\"fa fa-bitbucket\"></i>\n        Bitbucket\n      </button>\n\n      <button class=\"btn btn-icon btn-dropbox mb-1 mr-1\">\n        <i class=\"fa fa-dropbox\"></i>\n        Dropbox\n      </button>\n\n      <button class=\"btn btn-icon btn-facebook mb-1 mr-1\">\n        <i class=\"fa fa-facebook\"></i>\n        Facebook\n      </button>\n\n      <button class=\"btn btn-icon btn-flickr mb-1 mr-1\">\n        <i class=\"fa fa-flickr\"></i>\n        Flickr\n      </button>\n\n      <button class=\"btn btn-icon btn-foursquare mb-1 mr-1\">\n        <i class=\"fa fa-foursquare\"></i>\n        Foursquare\n      </button>\n\n      <button class=\"btn btn-icon btn-github mb-1 mr-1\">\n        <i class=\"fa fa-github\"></i>\n        GitHub\n      </button>\n\n      <button class=\"btn btn-icon btn-google mb-1 mr-1\">\n        <i class=\"fa fa-google-plus\"></i>\n        Google\n      </button>\n\n      <button class=\"btn btn-icon btn-instagram mb-1 mr-1\">\n        <i class=\"fa fa-instagram\"></i>\n        Instagram\n      </button>\n\n      <button class=\"btn btn-icon btn-linkedin mb-1 mr-1\">\n        <i class=\"fa fa-linkedin\"></i>\n        LinkedIn\n      </button>\n\n      <button class=\"btn btn-icon btn-microsoft mb-1 mr-1\">\n        <i class=\"fa fa-windows\"></i>\n        Microsoft\n      </button>\n\n      <button class=\"btn btn-icon btn-odnoklassniki mb-1 mr-1\">\n        <i class=\"fa fa-odnoklassniki\"></i>\n        Odnoklassniki\n      </button>\n\n      <button class=\"btn btn-icon btn-openid mb-1 mr-1\">\n        <i class=\"fa fa-openid\"></i>\n        OpenID\n      </button>\n\n      <button class=\"btn btn-icon btn-pinterest mb-1 mr-1\">\n        <i class=\"fa fa-pinterest\"></i>\n        Pinterest\n      </button>\n\n      <button class=\"btn btn-icon btn-reddit mb-1 mr-1\">\n        <i class=\"fa fa-reddit\"></i>\n        Reddit\n      </button>\n\n      <button class=\"btn btn-icon btn-soundcloud mb-1 mr-1\">\n        <i class=\"fa fa-soundcloud\"></i>\n        SoundCloud\n      </button>\n\n      <button class=\"btn btn-icon btn-tumblr mb-1 mr-1\">\n        <i class=\"fa fa-tumblr\"></i>\n        Tumblr\n      </button>\n\n      <button class=\"btn btn-icon btn-twitter mb-1 mr-1\">\n        <i class=\"fa fa-twitter\"></i>\n        Twitter\n      </button>\n\n      <button class=\"btn btn-icon btn-vimeo mb-1 mr-1\">\n        <i class=\"fa fa-vimeo-square\"></i>\n        Vimeo\n      </button>\n\n      <button class=\"btn btn-icon btn-vk mb-1 mr-1\">\n        <i class=\"fa fa-vk\"></i>\n        VK\n      </button>\n\n      <button class=\"btn btn-icon btn-yahoo mb-1 mr-1\">\n        <i class=\"fa fa-yahoo\"></i>\n        Yahoo!\n      </button>\n    </div>\n\n    <p class=\"pt-3\">Square</p>\n    <div>\n      <button class=\"btn btn-icon-icon btn-adn mb-1 mr-1\">\n        <i class=\"fa fa-adn\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-bitbucket mb-1 mr-1\">\n        <i class=\"fa fa-bitbucket\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-dropbox mb-1 mr-1\">\n        <i class=\"fa fa-dropbox\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-facebook mb-1 mr-1\">\n        <i class=\"fa fa-facebook\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-flickr mb-1 mr-1\">\n        <i class=\"fa fa-flickr\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-foursquare mb-1 mr-1\">\n        <i class=\"fa fa-foursquare\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-github mb-1 mr-1\">\n        <i class=\"fa fa-github\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-google mb-1 mr-1\">\n        <i class=\"fa fa-google-plus\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-instagram mb-1 mr-1\">\n        <i class=\"fa fa-instagram\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-linkedin mb-1 mr-1\">\n        <i class=\"fa fa-linkedin\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-microsoft mb-1 mr-1\">\n        <i class=\"fa fa-windows\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-odnoklassniki mb-1 mr-1\">\n        <i class=\"fa fa-odnoklassniki\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-openid mb-1 mr-1\">\n        <i class=\"fa fa-openid\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-pinterest mb-1 mr-1\">\n        <i class=\"fa fa-pinterest\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-reddit mb-1 mr-1\">\n        <i class=\"fa fa-reddit\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-soundcloud mb-1 mr-1\">\n        <i class=\"fa fa-soundcloud\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-tumblr mb-1 mr-1\">\n        <i class=\"fa fa-tumblr\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-twitter mb-1 mr-1\">\n        <i class=\"fa fa-twitter\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-vimeo mb-1 mr-1\">\n        <i class=\"fa fa-vimeo-square\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-vk mb-1 mr-1\">\n        <i class=\"fa fa-vk\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-yahoo mb-1 mr-1\">\n        <i class=\"fa fa-yahoo\"></i>\n      </button>\n    </div>\n    <p class=\"pt-3\">Circle</p>\n    <div>\n      <button class=\"btn btn-icon-icon btn-adn rounded-circle mb-1 mr-1\">\n        <i class=\"fa fa-adn\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-bitbucket rounded-circle mb-1 mr-1\">\n        <i class=\"fa fa-bitbucket\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-dropbox rounded-circle mb-1 mr-1\">\n        <i class=\"fa fa-dropbox\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-facebook rounded-circle mb-1 mr-1\">\n        <i class=\"fa fa-facebook\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-flickr rounded-circle mb-1 mr-1\">\n        <i class=\"fa fa-flickr\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-foursquare rounded-circle mb-1 mr-1\">\n        <i class=\"fa fa-foursquare\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-github rounded-circle mb-1 mr-1\">\n        <i class=\"fa fa-github\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-google rounded-circle mb-1 mr-1\">\n        <i class=\"fa fa-google-plus\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-instagram rounded-circle mb-1 mr-1\">\n        <i class=\"fa fa-instagram\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-linkedin rounded-circle mb-1 mr-1\">\n        <i class=\"fa fa-linkedin\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-microsoft rounded-circle mb-1 mr-1\">\n        <i class=\"fa fa-windows\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-odnoklassniki rounded-circle mb-1 mr-1\">\n        <i class=\"fa fa-odnoklassniki\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-openid rounded-circle mb-1 mr-1\">\n        <i class=\"fa fa-openid\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-pinterest rounded-circle mb-1 mr-1\">\n        <i class=\"fa fa-pinterest\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-reddit rounded-circle mb-1 mr-1\">\n        <i class=\"fa fa-reddit\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-soundcloud rounded-circle mb-1 mr-1\">\n        <i class=\"fa fa-soundcloud\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-tumblr rounded-circle mb-1 mr-1\">\n        <i class=\"fa fa-tumblr\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-twitter rounded-circle mb-1 mr-1\">\n        <i class=\"fa fa-twitter\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-vimeo rounded-circle mb-1 mr-1\">\n        <i class=\"fa fa-vimeo-square\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-vk rounded-circle mb-1 mr-1\">\n        <i class=\"fa fa-vk\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-yahoo rounded-circle mb-1 mr-1\">\n        <i class=\"fa fa-yahoo\"></i>\n      </button>\n    </div>\n    <p class=\"pt-3\">Large</p>\n    <div>\n      <button class=\"btn btn-icon-icon btn-adn btn-lg mb-1 mr-1\">\n        <i class=\"fa fa-adn\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-bitbucket btn-lg mb-1 mr-1\">\n        <i class=\"fa fa-bitbucket\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-dropbox btn-lg mb-1 mr-1\">\n        <i class=\"fa fa-dropbox\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-facebook btn-lg mb-1 mr-1\">\n        <i class=\"fa fa-facebook\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-flickr btn-lg mb-1 mr-1\">\n        <i class=\"fa fa-flickr\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-foursquare btn-lg mb-1 mr-1\">\n        <i class=\"fa fa-foursquare\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-github btn-lg mb-1 mr-1\">\n        <i class=\"fa fa-github\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-google btn-lg mb-1 mr-1\">\n        <i class=\"fa fa-google-plus\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-instagram btn-lg mb-1 mr-1\">\n        <i class=\"fa fa-instagram\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-linkedin btn-lg mb-1 mr-1\">\n        <i class=\"fa fa-linkedin\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-microsoft btn-lg mb-1 mr-1\">\n        <i class=\"fa fa-windows\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-odnoklassniki btn-lg mb-1 mr-1\">\n        <i class=\"fa fa-odnoklassniki\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-openid btn-lg mb-1 mr-1\">\n        <i class=\"fa fa-openid\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-pinterest btn-lg mb-1 mr-1\">\n        <i class=\"fa fa-pinterest\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-reddit btn-lg mb-1 mr-1\">\n        <i class=\"fa fa-reddit\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-soundcloud btn-lg mb-1 mr-1\">\n        <i class=\"fa fa-soundcloud\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-tumblr btn-lg mb-1 mr-1\">\n        <i class=\"fa fa-tumblr\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-twitter btn-lg mb-1 mr-1\">\n        <i class=\"fa fa-twitter\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-vimeo btn-lg mb-1 mr-1\">\n        <i class=\"fa fa-vimeo-square\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-vk btn-lg mb-1 mr-1\">\n        <i class=\"fa fa-vk\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-yahoo btn-lg mb-1 mr-1\">\n        <i class=\"fa fa-yahoo\"></i>\n      </button>\n    </div>\n    <p class=\"pt-3\">Small</p>\n    <div>\n      <button class=\"btn btn-icon-icon btn-adn btn-sm mb-1 mr-1\">\n        <i class=\"fa fa-adn\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-bitbucket btn-sm mb-1 mr-1\">\n        <i class=\"fa fa-bitbucket\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-dropbox btn-sm mb-1 mr-1\">\n        <i class=\"fa fa-dropbox\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-facebook btn-sm mb-1 mr-1\">\n        <i class=\"fa fa-facebook\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-flickr btn-sm mb-1 mr-1\">\n        <i class=\"fa fa-flickr\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-foursquare btn-sm mb-1 mr-1\">\n        <i class=\"fa fa-foursquare\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-github btn-sm mb-1 mr-1\">\n        <i class=\"fa fa-github\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-google btn-sm mb-1 mr-1\">\n        <i class=\"fa fa-google-plus\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-instagram btn-sm mb-1 mr-1\">\n        <i class=\"fa fa-instagram\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-linkedin btn-sm mb-1 mr-1\">\n        <i class=\"fa fa-linkedin\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-microsoft btn-sm mb-1 mr-1\">\n        <i class=\"fa fa-windows\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-odnoklassniki btn-sm mb-1 mr-1\">\n        <i class=\"fa fa-odnoklassniki\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-openid btn-sm mb-1 mr-1\">\n        <i class=\"fa fa-openid\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-pinterest btn-sm mb-1 mr-1\">\n        <i class=\"fa fa-pinterest\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-reddit btn-sm mb-1 mr-1\">\n        <i class=\"fa fa-reddit\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-soundcloud btn-sm mb-1 mr-1\">\n        <i class=\"fa fa-soundcloud\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-tumblr btn-sm mb-1 mr-1\">\n        <i class=\"fa fa-tumblr\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-twitter btn-sm mb-1 mr-1\">\n        <i class=\"fa fa-twitter\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-vimeo btn-sm mb-1 mr-1\">\n        <i class=\"fa fa-vimeo-square\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-vk btn-sm mb-1 mr-1\">\n        <i class=\"fa fa-vk\"></i>\n      </button>\n      <button class=\"btn btn-icon-icon btn-yahoo btn-sm mb-1 mr-1\">\n        <i class=\"fa fa-yahoo\"></i>\n      </button>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/button-icons/button-icons.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports
exports.i(__webpack_require__("../../../../css-loader/index.js?{\"sourceMap\":false,\"importLoaders\":1}!../../../../postcss-loader/index.js?{\"ident\":\"postcss\"}!../../../../../src/assets/fonts/font-awesome-4.7.0/css/font-awesome.css"), "");

// module
exports.push([module.i, "/* $colors\n ------------------------------------------*/\n.btn-adn {\n  color: #fff;\n  background-color: #d87a68;\n  border-color: #d87a68; }\n  .btn-adn:hover {\n    color: #fff;\n    background-color: #d05f4a;\n    border-color: #ce563f; }\n  .btn-adn:focus, .btn-adn.focus {\n    box-shadow: 0 0 0 3px rgba(216, 122, 104, 0.5); }\n  .btn-adn.disabled, .btn-adn:disabled {\n    background-color: #d87a68;\n    border-color: #d87a68; }\n  .btn-adn:active, .btn-adn.active,\n  .show > .btn-adn.dropdown-toggle {\n    background-color: #d05f4a;\n    background-image: none;\n    border-color: #ce563f; }\n\n.btn-bitbucket {\n  color: #fff;\n  background-color: #205081;\n  border-color: #205081; }\n  .btn-bitbucket:hover {\n    color: #fff;\n    background-color: #183d62;\n    border-color: #163758; }\n  .btn-bitbucket:focus, .btn-bitbucket.focus {\n    box-shadow: 0 0 0 3px rgba(32, 80, 129, 0.5); }\n  .btn-bitbucket.disabled, .btn-bitbucket:disabled {\n    background-color: #205081;\n    border-color: #205081; }\n  .btn-bitbucket:active, .btn-bitbucket.active,\n  .show > .btn-bitbucket.dropdown-toggle {\n    background-color: #183d62;\n    background-image: none;\n    border-color: #163758; }\n\n.btn-dropbox {\n  color: #fff;\n  background-color: #1087dd;\n  border-color: #1087dd; }\n  .btn-dropbox:hover {\n    color: #fff;\n    background-color: #0d71b9;\n    border-color: #0d6aad; }\n  .btn-dropbox:focus, .btn-dropbox.focus {\n    box-shadow: 0 0 0 3px rgba(16, 135, 221, 0.5); }\n  .btn-dropbox.disabled, .btn-dropbox:disabled {\n    background-color: #1087dd;\n    border-color: #1087dd; }\n  .btn-dropbox:active, .btn-dropbox.active,\n  .show > .btn-dropbox.dropdown-toggle {\n    background-color: #0d71b9;\n    background-image: none;\n    border-color: #0d6aad; }\n\n.btn-facebook {\n  color: #fff;\n  background-color: #3b5998;\n  border-color: #3b5998; }\n  .btn-facebook:hover {\n    color: #fff;\n    background-color: #30497c;\n    border-color: #2d4373; }\n  .btn-facebook:focus, .btn-facebook.focus {\n    box-shadow: 0 0 0 3px rgba(59, 89, 152, 0.5); }\n  .btn-facebook.disabled, .btn-facebook:disabled {\n    background-color: #3b5998;\n    border-color: #3b5998; }\n  .btn-facebook:active, .btn-facebook.active,\n  .show > .btn-facebook.dropdown-toggle {\n    background-color: #30497c;\n    background-image: none;\n    border-color: #2d4373; }\n\n.btn-flickr {\n  color: #fff;\n  background-color: #ff0084;\n  border-color: #ff0084; }\n  .btn-flickr:hover {\n    color: #fff;\n    background-color: #d90070;\n    border-color: #cc006a; }\n  .btn-flickr:focus, .btn-flickr.focus {\n    box-shadow: 0 0 0 3px rgba(255, 0, 132, 0.5); }\n  .btn-flickr.disabled, .btn-flickr:disabled {\n    background-color: #ff0084;\n    border-color: #ff0084; }\n  .btn-flickr:active, .btn-flickr.active,\n  .show > .btn-flickr.dropdown-toggle {\n    background-color: #d90070;\n    background-image: none;\n    border-color: #cc006a; }\n\n.btn-foursquare {\n  color: #fff;\n  background-color: #f94877;\n  border-color: #f94877; }\n  .btn-foursquare:hover {\n    color: #fff;\n    background-color: #f8235b;\n    border-color: #f71752; }\n  .btn-foursquare:focus, .btn-foursquare.focus {\n    box-shadow: 0 0 0 3px rgba(249, 72, 119, 0.5); }\n  .btn-foursquare.disabled, .btn-foursquare:disabled {\n    background-color: #f94877;\n    border-color: #f94877; }\n  .btn-foursquare:active, .btn-foursquare.active,\n  .show > .btn-foursquare.dropdown-toggle {\n    background-color: #f8235b;\n    background-image: none;\n    border-color: #f71752; }\n\n.btn-github {\n  color: #fff;\n  background-color: #444;\n  border-color: #444; }\n  .btn-github:hover {\n    color: #fff;\n    background-color: #313131;\n    border-color: #2b2a2a; }\n  .btn-github:focus, .btn-github.focus {\n    box-shadow: 0 0 0 3px rgba(68, 68, 68, 0.5); }\n  .btn-github.disabled, .btn-github:disabled {\n    background-color: #444;\n    border-color: #444; }\n  .btn-github:active, .btn-github.active,\n  .show > .btn-github.dropdown-toggle {\n    background-color: #313131;\n    background-image: none;\n    border-color: #2b2a2a; }\n\n.btn-google {\n  color: #fff;\n  background-color: #dd4b39;\n  border-color: #dd4b39; }\n  .btn-google:hover {\n    color: #fff;\n    background-color: #cd3623;\n    border-color: #c23321; }\n  .btn-google:focus, .btn-google.focus {\n    box-shadow: 0 0 0 3px rgba(221, 75, 57, 0.5); }\n  .btn-google.disabled, .btn-google:disabled {\n    background-color: #dd4b39;\n    border-color: #dd4b39; }\n  .btn-google:active, .btn-google.active,\n  .show > .btn-google.dropdown-toggle {\n    background-color: #cd3623;\n    background-image: none;\n    border-color: #c23321; }\n\n.btn-instagram {\n  color: #fff;\n  background-color: #3f729b;\n  border-color: #3f729b; }\n  .btn-instagram:hover {\n    color: #fff;\n    background-color: #345e80;\n    border-color: #305777; }\n  .btn-instagram:focus, .btn-instagram.focus {\n    box-shadow: 0 0 0 3px rgba(63, 114, 155, 0.5); }\n  .btn-instagram.disabled, .btn-instagram:disabled {\n    background-color: #3f729b;\n    border-color: #3f729b; }\n  .btn-instagram:active, .btn-instagram.active,\n  .show > .btn-instagram.dropdown-toggle {\n    background-color: #345e80;\n    background-image: none;\n    border-color: #305777; }\n\n.btn-linkedin {\n  color: #fff;\n  background-color: #007bb6;\n  border-color: #007bb6; }\n  .btn-linkedin:hover {\n    color: #fff;\n    background-color: #006190;\n    border-color: #005983; }\n  .btn-linkedin:focus, .btn-linkedin.focus {\n    box-shadow: 0 0 0 3px rgba(0, 123, 182, 0.5); }\n  .btn-linkedin.disabled, .btn-linkedin:disabled {\n    background-color: #007bb6;\n    border-color: #007bb6; }\n  .btn-linkedin:active, .btn-linkedin.active,\n  .show > .btn-linkedin.dropdown-toggle {\n    background-color: #006190;\n    background-image: none;\n    border-color: #005983; }\n\n.btn-microsoft {\n  color: #fff;\n  background-color: #2672ec;\n  border-color: #2672ec; }\n  .btn-microsoft:hover {\n    color: #fff;\n    background-color: #135fd9;\n    border-color: #125acd; }\n  .btn-microsoft:focus, .btn-microsoft.focus {\n    box-shadow: 0 0 0 3px rgba(38, 114, 236, 0.5); }\n  .btn-microsoft.disabled, .btn-microsoft:disabled {\n    background-color: #2672ec;\n    border-color: #2672ec; }\n  .btn-microsoft:active, .btn-microsoft.active,\n  .show > .btn-microsoft.dropdown-toggle {\n    background-color: #135fd9;\n    background-image: none;\n    border-color: #125acd; }\n\n.btn-odnoklassniki {\n  color: #fff;\n  background-color: #f4731c;\n  border-color: #f4731c; }\n  .btn-odnoklassniki:hover {\n    color: #fff;\n    background-color: #df600b;\n    border-color: #d35b0a; }\n  .btn-odnoklassniki:focus, .btn-odnoklassniki.focus {\n    box-shadow: 0 0 0 3px rgba(244, 115, 28, 0.5); }\n  .btn-odnoklassniki.disabled, .btn-odnoklassniki:disabled {\n    background-color: #f4731c;\n    border-color: #f4731c; }\n  .btn-odnoklassniki:active, .btn-odnoklassniki.active,\n  .show > .btn-odnoklassniki.dropdown-toggle {\n    background-color: #df600b;\n    background-image: none;\n    border-color: #d35b0a; }\n\n.btn-openid {\n  color: #111;\n  background-color: #f7931e;\n  border-color: #f7931e; }\n  .btn-openid:hover {\n    color: #111;\n    background-color: #e78008;\n    border-color: #da7908; }\n  .btn-openid:focus, .btn-openid.focus {\n    box-shadow: 0 0 0 3px rgba(247, 147, 30, 0.5); }\n  .btn-openid.disabled, .btn-openid:disabled {\n    background-color: #f7931e;\n    border-color: #f7931e; }\n  .btn-openid:active, .btn-openid.active,\n  .show > .btn-openid.dropdown-toggle {\n    background-color: #e78008;\n    background-image: none;\n    border-color: #da7908; }\n\n.btn-pinterest {\n  color: #fff;\n  background-color: #cb2027;\n  border-color: #cb2027; }\n  .btn-pinterest:hover {\n    color: #fff;\n    background-color: #aa1b21;\n    border-color: #9f191f; }\n  .btn-pinterest:focus, .btn-pinterest.focus {\n    box-shadow: 0 0 0 3px rgba(203, 32, 39, 0.5); }\n  .btn-pinterest.disabled, .btn-pinterest:disabled {\n    background-color: #cb2027;\n    border-color: #cb2027; }\n  .btn-pinterest:active, .btn-pinterest.active,\n  .show > .btn-pinterest.dropdown-toggle {\n    background-color: #aa1b21;\n    background-image: none;\n    border-color: #9f191f; }\n\n.btn-reddit {\n  color: #111;\n  background-color: #eff7ff;\n  border-color: #eff7ff;\n  color: block; }\n  .btn-reddit:hover {\n    color: #111;\n    background-color: #c9e4ff;\n    border-color: #bcdeff; }\n  .btn-reddit:focus, .btn-reddit.focus {\n    box-shadow: 0 0 0 3px rgba(239, 247, 255, 0.5); }\n  .btn-reddit.disabled, .btn-reddit:disabled {\n    background-color: #eff7ff;\n    border-color: #eff7ff; }\n  .btn-reddit:active, .btn-reddit.active,\n  .show > .btn-reddit.dropdown-toggle {\n    background-color: #c9e4ff;\n    background-image: none;\n    border-color: #bcdeff; }\n\n.btn-soundcloud {\n  color: #fff;\n  background-color: #f50;\n  border-color: #f50; }\n  .btn-soundcloud:hover {\n    color: #fff;\n    background-color: #d94800;\n    border-color: #cc4400; }\n  .btn-soundcloud:focus, .btn-soundcloud.focus {\n    box-shadow: 0 0 0 3px rgba(255, 85, 0, 0.5); }\n  .btn-soundcloud.disabled, .btn-soundcloud:disabled {\n    background-color: #f50;\n    border-color: #f50; }\n  .btn-soundcloud:active, .btn-soundcloud.active,\n  .show > .btn-soundcloud.dropdown-toggle {\n    background-color: #d94800;\n    background-image: none;\n    border-color: #cc4400; }\n\n.btn-tumblr {\n  color: #fff;\n  background-color: #2c4762;\n  border-color: #2c4762; }\n  .btn-tumblr:hover {\n    color: #fff;\n    background-color: #203448;\n    border-color: #1c2e3f; }\n  .btn-tumblr:focus, .btn-tumblr.focus {\n    box-shadow: 0 0 0 3px rgba(44, 71, 98, 0.5); }\n  .btn-tumblr.disabled, .btn-tumblr:disabled {\n    background-color: #2c4762;\n    border-color: #2c4762; }\n  .btn-tumblr:active, .btn-tumblr.active,\n  .show > .btn-tumblr.dropdown-toggle {\n    background-color: #203448;\n    background-image: none;\n    border-color: #1c2e3f; }\n\n.btn-twitter {\n  color: #111;\n  background-color: #55acee;\n  border-color: #55acee; }\n  .btn-twitter:hover {\n    color: #111;\n    background-color: #329beb;\n    border-color: #2795e9; }\n  .btn-twitter:focus, .btn-twitter.focus {\n    box-shadow: 0 0 0 3px rgba(85, 172, 238, 0.5); }\n  .btn-twitter.disabled, .btn-twitter:disabled {\n    background-color: #55acee;\n    border-color: #55acee; }\n  .btn-twitter:active, .btn-twitter.active,\n  .show > .btn-twitter.dropdown-toggle {\n    background-color: #329beb;\n    background-image: none;\n    border-color: #2795e9; }\n\n.btn-vimeo {\n  color: #fff;\n  background-color: #1ab7ea;\n  border-color: #1ab7ea; }\n  .btn-vimeo:hover {\n    color: #fff;\n    background-color: #139ecb;\n    border-color: #1295bf; }\n  .btn-vimeo:focus, .btn-vimeo.focus {\n    box-shadow: 0 0 0 3px rgba(26, 183, 234, 0.5); }\n  .btn-vimeo.disabled, .btn-vimeo:disabled {\n    background-color: #1ab7ea;\n    border-color: #1ab7ea; }\n  .btn-vimeo:active, .btn-vimeo.active,\n  .show > .btn-vimeo.dropdown-toggle {\n    background-color: #139ecb;\n    background-image: none;\n    border-color: #1295bf; }\n\n.btn-vk {\n  color: #fff;\n  background-color: #587ea3;\n  border-color: #587ea3; }\n  .btn-vk:hover {\n    color: #fff;\n    background-color: #4b6b8a;\n    border-color: #466482; }\n  .btn-vk:focus, .btn-vk.focus {\n    box-shadow: 0 0 0 3px rgba(88, 126, 163, 0.5); }\n  .btn-vk.disabled, .btn-vk:disabled {\n    background-color: #587ea3;\n    border-color: #587ea3; }\n  .btn-vk:active, .btn-vk.active,\n  .show > .btn-vk.dropdown-toggle {\n    background-color: #4b6b8a;\n    background-image: none;\n    border-color: #466482; }\n\n.btn-yahoo {\n  color: #fff;\n  background-color: #720e9e;\n  border-color: #720e9e; }\n  .btn-yahoo:hover {\n    color: #fff;\n    background-color: #590b7b;\n    border-color: #500a6f; }\n  .btn-yahoo:focus, .btn-yahoo.focus {\n    box-shadow: 0 0 0 3px rgba(114, 14, 158, 0.5); }\n  .btn-yahoo.disabled, .btn-yahoo:disabled {\n    background-color: #720e9e;\n    border-color: #720e9e; }\n  .btn-yahoo:active, .btn-yahoo.active,\n  .show > .btn-yahoo.dropdown-toggle {\n    background-color: #590b7b;\n    background-image: none;\n    border-color: #500a6f; }\n\n.btn-icon {\n  min-width: 160px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/button-icons/button-icons.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ButtonIconsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var ButtonIconsComponent = (function () {
    function ButtonIconsComponent() {
    }
    ButtonIconsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-button-icons',
            template: __webpack_require__("../../../../../src/app/components/button-icons/button-icons.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/button-icons/button-icons.component.scss")]
        })
    ], ButtonIconsComponent);
    return ButtonIconsComponent;
}());

//# sourceMappingURL=button-icons.component.js.map

/***/ }),

/***/ "../../../../../src/app/datatable/data-table/data-table.component.html":
/***/ (function(module, exports) {

module.exports = "<ngx-datatable\n  class=\"fullscreen\"\n  [columnMode]=\"'force'\"\n  [headerHeight]=\"40\"\n  [footerHeight]=\"0\"\n  [rowHeight]=\"40\"\n  [scrollbarV]=\"true\"\n  [scrollbarH]=\"true\"\n  [rows]=\"rows\">\n  <ngx-datatable-column name=\"Id\" [width]=\"80\"></ngx-datatable-column>\n  <ngx-datatable-column name=\"Name\" [width]=\"300\"></ngx-datatable-column>\n  <ngx-datatable-column name=\"Gender\"></ngx-datatable-column>\n  <ngx-datatable-column name=\"Age\"></ngx-datatable-column>\n  <ngx-datatable-column name=\"City\" [width]=\"300\" prop=\"address.city\"></ngx-datatable-column>\n  <ngx-datatable-column name=\"State\" [width]=\"300\" prop=\"address.state\"></ngx-datatable-column>\n</ngx-datatable>\n"

/***/ }),

/***/ "../../../../../src/app/datatable/data-table/data-table.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".datatable,\n.datatable > div,\n.datatable.fixed-header .datatable-header .datatable-header-inner {\n  height: 100%;\n  direction: rtl;\n  text-align: right;\n  background-color: red; }\n\ndiv.dataTables_wrapper {\n  direction: rtl;\n  background-color: red; }\n\n/* Ensure that the demo table scrolls */\nth, td {\n  white-space: nowrap; }\n\ndiv.dataTables_wrapper {\n  width: 800px;\n  margin: 0 auto; }\n\n.datatable-header-inner {\n  direction: rtl;\n  text-align: right; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/datatable/data-table/data-table.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DataTableComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DataTableComponent = (function () {
    function DataTableComponent() {
        var _this = this;
        this.rows = [];
        this.fetch(function (data) {
            _this.rows = data;
        });
    }
    DataTableComponent.prototype.fetch = function (cb) {
        var req = new XMLHttpRequest();
        req.open('GET', "assets/data/100k.json");
        req.onload = function () {
            cb(JSON.parse(req.response));
        };
        req.send();
    };
    DataTableComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-data-table',
            template: __webpack_require__("../../../../../src/app/datatable/data-table/data-table.component.html"),
            styles: [__webpack_require__("../../../../../src/app/datatable/data-table/data-table.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], DataTableComponent);
    return DataTableComponent;
}());

//# sourceMappingURL=data-table.component.js.map

/***/ }),

/***/ "../../../../../src/app/datatable/table-editing/table-editing.component.html":
/***/ (function(module, exports) {

module.exports = "<ngx-datatable\n  #mydatatable\n  [headerHeight]=\"40\"\n  [limit]=\"5\"\n  [columnMode]=\"'force'\"\n  [footerHeight]=\"50\"\n  [rowHeight]=\"'auto'\"\n  [rows]=\"rows\">\n  <ngx-datatable-column name=\"Name\">\n    <ng-template ngx-datatable-cell-template let-value=\"value\" let-row=\"row\">\n      <span\n        title=\"Double click to edit\"\n        (dblclick)=\"editing[row.$$index + '-name'] = true\"\n        *ngIf=\"!editing[row.$$index + '-name']\">\n        {{value}}\n      </span>\n      <input\n        autofocus\n        (blur)=\"updateValue($event, 'name', value, row)\"\n        *ngIf=\"editing[row.$$index + '-name']\"\n        type=\"text\"\n        [value]=\"value\"\n      />\n    </ng-template>\n  </ngx-datatable-column>\n  <ngx-datatable-column name=\"Gender\">\n    <ng-template ngx-datatable-cell-template let-row=\"row\" let-value=\"value\">\n       <span\n        title=\"Double click to edit\"\n        (dblclick)=\"editing[row.$$index + '-gender'] = true\"\n        *ngIf=\"!editing[row.$$index + '-gender']\">\n        {{value}}\n      </span>\n      <select\n        *ngIf=\"editing[row.$$index + '-gender']\"\n        (change)=\"updateValue($event, 'gender', value, row)\"\n        [value]=\"value\">\n        <option value=\"male\">Male</option>\n        <option value=\"female\">Female</option>\n      </select>\n    </ng-template>\n  </ngx-datatable-column>\n  <ngx-datatable-column name=\"Age\">\n    <ng-template ngx-datatable-cell-template let-value=\"value\">\n      {{value}}\n    </ng-template>\n  </ngx-datatable-column>\n</ngx-datatable>\n"

/***/ }),

/***/ "../../../../../src/app/datatable/table-editing/table-editing.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/datatable/table-editing/table-editing.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TableEditingComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TableEditingComponent = (function () {
    function TableEditingComponent() {
        var _this = this;
        this.editing = {};
        this.rows = [];
        this.fetch(function (data) {
            _this.rows = data;
        });
    }
    TableEditingComponent.prototype.fetch = function (cb) {
        var req = new XMLHttpRequest();
        req.open('GET', "assets/data/company.json");
        req.onload = function () {
            cb(JSON.parse(req.response));
        };
        req.send();
    };
    TableEditingComponent.prototype.updateValue = function (event, cell, cellValue, row) {
        this.editing[row.$$index + '-' + cell] = false;
        this.rows[row.$$index][cell] = event.target.value;
    };
    TableEditingComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-table-editing',
            template: __webpack_require__("../../../../../src/app/datatable/table-editing/table-editing.component.html"),
            styles: [__webpack_require__("../../../../../src/app/datatable/table-editing/table-editing.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], TableEditingComponent);
    return TableEditingComponent;
}());

//# sourceMappingURL=table-editing.component.js.map

/***/ }),

/***/ "../../../../../src/app/datatable/table-filter/table-filter.component.html":
/***/ (function(module, exports) {

module.exports = "<input type=\"email\" class=\"form-control mb-3\" placeholder=\"Type to filter the name column...\" required (keyup)='updateFilter($event)'>\n<ngx-datatable\n[columns]=\"columns\"\n[columnMode]=\"'force'\"\n[headerHeight]=\"40\"\n[footerHeight]=\"50\"\n[rowHeight]=\"'auto'\"\n[limit]=\"10\"\n[rows]='rows'>\n</ngx-datatable>\n"

/***/ }),

/***/ "../../../../../src/app/datatable/table-filter/table-filter.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/datatable/table-filter/table-filter.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TableFilterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TableFilterComponent = (function () {
    function TableFilterComponent() {
        var _this = this;
        this.rows = [];
        this.temp = [];
        this.columns = [
            { prop: 'name' },
            { name: 'Company' },
            { name: 'Gender' }
        ];
        this.fetch(function (data) {
            // cache our list
            _this.temp = data.slice();
            // push our inital complete list
            _this.rows = data;
        });
    }
    TableFilterComponent.prototype.fetch = function (cb) {
        var req = new XMLHttpRequest();
        req.open('GET', "assets/data/company.json");
        req.onload = function () {
            cb(JSON.parse(req.response));
        };
        req.send();
    };
    TableFilterComponent.prototype.updateFilter = function (event) {
        var val = event.target.value;
        // filter our data
        var temp = this.temp.filter(function (d) {
            console.log();
            return d.CompanyName[0]['name'].toLowerCase().indexOf(val) !== -1 || !val;
        });
        // update the rows
        this.rows = temp;
    };
    TableFilterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-table-filter',
            template: __webpack_require__("../../../../../src/app/datatable/table-filter/table-filter.component.html"),
            styles: [__webpack_require__("../../../../../src/app/datatable/table-filter/table-filter.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], TableFilterComponent);
    return TableFilterComponent;
}());

//# sourceMappingURL=table-filter.component.js.map

/***/ }),

/***/ "../../../../../src/app/datatable/table-paging/table-paging.component.html":
/***/ (function(module, exports) {

module.exports = "<ngx-datatable\n  [rows]=\"rows\"\n  [columns]=\"[{name:'Name'},{name:'Gender'},{name:'Company'}]\"\n  [columnMode]=\"'force'\"\n  [headerHeight]=\"40\"\n  [footerHeight]=\"50\"\n  [rowHeight]=\"'auto'\"\n  [externalPaging]=\"true\"\n  [count]=\"count\"\n  [offset]=\"offset\"\n  [limit]=\"limit\"\n  (page)='onPage($event)'>\n</ngx-datatable>\n"

/***/ }),

/***/ "../../../../../src/app/datatable/table-paging/table-paging.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/datatable/table-paging/table-paging.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TablePagingComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var TablePagingComponent = (function () {
    function TablePagingComponent() {
        this.rows = [];
        this.count = 0;
        this.offset = 0;
        this.limit = 10;
    }
    TablePagingComponent.prototype.ngOnInit = function () {
        this.page(this.offset, this.limit);
    };
    TablePagingComponent.prototype.page = function (offset, limit) {
        var _this = this;
        this.fetch(function (results) {
            _this.count = results.length;
            var start = offset * limit;
            var end = start + limit;
            var rows = _this.rows.slice();
            for (var i = start; i < end; i++) {
                rows[i] = results[i];
            }
            _this.rows = rows;
            console.log('Page Results', start, end, rows);
        });
    };
    TablePagingComponent.prototype.fetch = function (cb) {
        var req = new XMLHttpRequest();
        req.open('GET', "assets/data/company.json");
        req.onload = function () {
            cb(JSON.parse(req.response));
        };
        req.send();
    };
    TablePagingComponent.prototype.onPage = function (event) {
        console.log('Page Event', event);
        this.page(event.offset, event.limit);
    };
    TablePagingComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-table-paging',
            template: __webpack_require__("../../../../../src/app/datatable/table-paging/table-paging.component.html"),
            styles: [__webpack_require__("../../../../../src/app/datatable/table-paging/table-paging.component.scss")]
        })
    ], TablePagingComponent);
    return TablePagingComponent;
}());

//# sourceMappingURL=table-paging.component.js.map

/***/ }),

/***/ "../../../../../src/app/datatable/table-pinning/table-pinning.component.html":
/***/ (function(module, exports) {

module.exports = "<ngx-datatable\n[columnMode]=\"'force'\"\n[headerHeight]=\"40\"\n[footerHeight]=\"50\"\n[rowHeight]=\"40\"\n[scrollbarV]=\"true\"\n[scrollbarH]=\"true\"\n[rows]=\"rows\">\n  <ngx-datatable-column\n    name=\"Name\"\n    [width]=\"300\"\n    [frozenLeft]=\"true\">\n  </ngx-datatable-column>\n  <ngx-datatable-column\n    name=\"Gender\">\n  </ngx-datatable-column>\n  <ngx-datatable-column\n    name=\"Age\">\n  </ngx-datatable-column>\n  <ngx-datatable-column\n    name=\"City\"\n    [width]=\"150\"\n    prop=\"address.city\">\n  </ngx-datatable-column>\n  <ngx-datatable-column\n    name=\"State\"\n    [width]=\"300\"\n    prop=\"address.state\"\n    [frozenRight]=\"true\">\n  </ngx-datatable-column>\n</ngx-datatable>\n"

/***/ }),

/***/ "../../../../../src/app/datatable/table-pinning/table-pinning.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".datatable,\n.datatable > div,\n.datatable.fixed-header .datatable-header .datatable-header-inner {\n  height: 100%; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/datatable/table-pinning/table-pinning.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TablePinningComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TablePinningComponent = (function () {
    function TablePinningComponent() {
        var _this = this;
        this.rows = [];
        this.fetch(function (data) {
            _this.rows = data;
        });
    }
    TablePinningComponent.prototype.fetch = function (cb) {
        var req = new XMLHttpRequest();
        req.open('GET', "assets/data/100k.json");
        req.onload = function () {
            cb(JSON.parse(req.response));
        };
        req.send();
    };
    TablePinningComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-table-pinning',
            template: __webpack_require__("../../../../../src/app/datatable/table-pinning/table-pinning.component.html"),
            styles: [__webpack_require__("../../../../../src/app/datatable/table-pinning/table-pinning.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], TablePinningComponent);
    return TablePinningComponent;
}());

//# sourceMappingURL=table-pinning.component.js.map

/***/ }),

/***/ "../../../../../src/app/datatable/table-selection/table-selection.component.html":
/***/ (function(module, exports) {

module.exports = "<ngx-datatable\n  class=\"selection-cell\"\n  [rows]=\"rows\"\n  [columnMode]=\"'force'\"\n  [columns]=\"columns\"\n  [headerHeight]=\"40\"\n  [footerHeight]=\"50\"\n  [rowHeight]=\"40\"\n  [selected]=\"selected\"\n  [selectionType]=\"'cell'\"\n  (select)=\"onSelect($event)\"\n  (activate)=\"onActivate($event)\">\n</ngx-datatable>\n"

/***/ }),

/***/ "../../../../../src/app/datatable/table-selection/table-selection.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/datatable/table-selection/table-selection.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TableSelectionComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TableSelectionComponent = (function () {
    function TableSelectionComponent() {
        var _this = this;
        this.rows = [];
        this.selected = [];
        this.columns = [
            { prop: 'name' },
            { name: 'Company' },
            { name: 'Gender' }
        ];
        this.fetch(function (data) {
            _this.rows = data;
        });
    }
    TableSelectionComponent.prototype.fetch = function (cb) {
        var req = new XMLHttpRequest();
        req.open('GET', "assets/data/company.json");
        req.onload = function () {
            cb(JSON.parse(req.response));
        };
        req.send();
    };
    TableSelectionComponent.prototype.onSelect = function (event) {
        console.log('Event: select', event, this.selected);
    };
    TableSelectionComponent.prototype.onActivate = function (event) {
        console.log('Event: activate', event);
    };
    TableSelectionComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-table-selection',
            template: __webpack_require__("../../../../../src/app/datatable/table-selection/table-selection.component.html"),
            styles: [__webpack_require__("../../../../../src/app/datatable/table-selection/table-selection.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], TableSelectionComponent);
    return TableSelectionComponent;
}());

//# sourceMappingURL=table-selection.component.js.map

/***/ }),

/***/ "../../../../../src/app/datatable/table-sorting/table-sorting.component.html":
/***/ (function(module, exports) {

module.exports = "<ngx-datatable\n  [rows]=\"rows\"\n  [columns]=\"columns\"\n  [sortType]=\"'multi'\"\n  [columnMode]=\"'force'\"\n  [headerHeight]=\"40\"\n  [footerHeight]=\"50\"\n  [rowHeight]=\"40\">\n</ngx-datatable>\n"

/***/ }),

/***/ "../../../../../src/app/datatable/table-sorting/table-sorting.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".datatable,\n.datatable > div,\n.datatable.fixed-header .datatable-header .datatable-header-inner {\n  height: 100%; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/datatable/table-sorting/table-sorting.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TableSortingComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TableSortingComponent = (function () {
    function TableSortingComponent() {
        var _this = this;
        this.rows = [];
        this.columns = [
            { name: 'Company' },
            { name: 'Name' },
            { name: 'Gender' }
        ];
        this.fetch(function (data) {
            _this.rows = data;
        });
    }
    TableSortingComponent.prototype.fetch = function (cb) {
        var req = new XMLHttpRequest();
        req.open('GET', "assets/data/company.json");
        req.onload = function () {
            var data = JSON.parse(req.response);
            cb(data);
        };
        req.send();
    };
    TableSortingComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-table-sorting',
            template: __webpack_require__("../../../../../src/app/datatable/table-sorting/table-sorting.component.html"),
            styles: [__webpack_require__("../../../../../src/app/datatable/table-sorting/table-sorting.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], TableSortingComponent);
    return TableSortingComponent;
}());

//# sourceMappingURL=table-sorting.component.js.map

/***/ }),

/***/ "../../../../../src/app/icons/fontawesome/fontawesome.component.html":
/***/ (function(module, exports) {

module.exports = "<input type=\"email\" class=\"form-control form-control-lg mb-3\" placeholder=\"Type to filter icons\" required (keyup)='updateFilter($event)'>\n\n<div class=\"icon-list\">\n  <div class=\"row\">\n    <div class=\"fa-hover col-md-3 col-sm-4\" *ngFor=\"let icon of icons\">\n      <a href=\"http://fontawesome.io/icon/{{icon}}\" class=\"text-color\" target=\"_blank\">\n        <i class=\"fa fa-{{icon}}\"></i>\n        <span>{{icon}}</span>\n      </a>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/icons/fontawesome/fontawesome.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports
exports.i(__webpack_require__("../../../../css-loader/index.js?{\"sourceMap\":false,\"importLoaders\":1}!../../../../postcss-loader/index.js?{\"ident\":\"postcss\"}!../../../../../src/assets/fonts/font-awesome-4.7.0/css/font-awesome.css"), "");

// module
exports.push([module.i, ".icon-list a {\n  display: block;\n  padding-left: 40px;\n  height: 32px;\n  line-height: 32px;\n  margin-bottom: 5px; }\n  .icon-list a .icon, .icon-list a .fa,\n  .icon-list a [class^=\"icon-\"],\n  .icon-list a [class*=\" icon-\"] {\n    position: absolute;\n    top: 0;\n    left: 10px;\n    min-width: 30px;\n    text-align: center;\n    line-height: 32px; }\n  .icon-list a:hover .fa,\n  .icon-list a:hover .icon,\n  .icon-list a:hover [class^=\"icon-\"],\n  .icon-list a:hover [class*=\" icon-\"] {\n    top: 5px;\n    font-size: 1.5em; }\n\n.IconClass {\n  font-size: 30px;\n  height: 40px;\n  top: -12px;\n  position: relative; }\n\n.IconClassMt3 {\n  font-size: 33px;\n  position: relative;\n  top: -10px;\n  margin-top: 10px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/icons/fontawesome/fontawesome.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FontawesomeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FontawesomeComponent = (function () {
    function FontawesomeComponent() {
        this.temp = [];
        this.icons = [];
        var icons = [
            '500px',
            'address-book',
            'address-book-o',
            'address-card',
            'address-card-o',
            'adjust',
            'adn',
            'align-center',
            'align-justify',
            'align-left',
            'align-right',
            'amazon',
            'ambulance',
            'american-sign-language-interpreting',
            'anchor',
            'android',
            'angellist',
            'angle-double-down',
            'angle-double-left',
            'angle-double-right',
            'angle-double-up',
            'angle-down',
            'angle-left',
            'angle-right',
            'angle-up',
            'apple',
            'archive',
            'area-chart',
            'arrow-circle-down',
            'arrow-circle-left',
            'arrow-circle-o-down',
            'arrow-circle-o-left',
            'arrow-circle-o-right',
            'arrow-circle-o-up',
            'arrow-circle-right',
            'arrow-circle-up',
            'arrow-down',
            'arrow-left',
            'arrow-right',
            'arrow-up',
            'arrows',
            'arrows-alt',
            'arrows-h',
            'arrows-v',
            'asl-interpreting',
            'assistive-listening-systems',
            'asterisk',
            'at',
            'audio-description',
            'automobile',
            'backward',
            'balance-scale',
            'ban',
            'bandcamp',
            'bank',
            'bar-chart',
            'bar-chart-o',
            'barcode',
            'bars',
            'bath',
            'bathtub',
            'battery',
            'battery-0',
            'battery-1',
            'battery-2',
            'battery-3',
            'battery-4',
            'battery-empty',
            'battery-full',
            'battery-half',
            'battery-quarter',
            'battery-three-quarters',
            'bed',
            'beer',
            'behance',
            'behance-square',
            'bell',
            'bell-o',
            'bell-slash',
            'bell-slash-o',
            'bicycle',
            'binoculars',
            'birthday-cake',
            'bitbucket',
            'bitbucket-square',
            'bitcoin',
            'black-tie',
            'blind',
            'bluetooth',
            'bluetooth-b',
            'bold',
            'bolt',
            'bomb',
            'book',
            'bookmark',
            'bookmark-o',
            'braille',
            'briefcase',
            'btc',
            'bug',
            'building',
            'building-o',
            'bullhorn',
            'bullseye',
            'bus',
            'buysellads',
            'cab',
            'calculator',
            'calendar',
            'calendar-check-o',
            'calendar-minus-o',
            'calendar-o',
            'calendar-plus-o',
            'calendar-times-o',
            'camera',
            'camera-retro',
            'car',
            'caret-down',
            'caret-left',
            'caret-right',
            'caret-square-o-down',
            'caret-square-o-left',
            'caret-square-o-right',
            'caret-square-o-up',
            'caret-up',
            'cart-arrow-down',
            'cart-plus',
            'cc',
            'cc-amex',
            'cc-diners-club',
            'cc-discover',
            'cc-jcb',
            'cc-mastercard',
            'cc-paypal',
            'cc-stripe',
            'cc-visa',
            'certificate',
            'chain',
            'chain-broken',
            'check',
            'check-circle',
            'check-circle-o',
            'check-square',
            'check-square-o',
            'chevron-circle-down',
            'chevron-circle-left',
            'chevron-circle-right',
            'chevron-circle-up',
            'chevron-down',
            'chevron-left',
            'chevron-right',
            'chevron-up',
            'child',
            'chrome',
            'circle',
            'circle-o',
            'circle-o-notch',
            'circle-thin',
            'clipboard',
            'clock-o',
            'clone',
            'close',
            'cloud',
            'cloud-download',
            'cloud-upload',
            'cny',
            'code',
            'code-fork',
            'codepen',
            'codiepie',
            'coffee',
            'cog',
            'cogs',
            'columns',
            'comment',
            'comment-o',
            'commenting',
            'commenting-o',
            'comments',
            'comments-o',
            'compass',
            'compress',
            'connectdevelop',
            'contao',
            'copy',
            'copyright',
            'creative-commons',
            'credit-card',
            'credit-card-alt',
            'crop',
            'crosshairs',
            'css3',
            'cube',
            'cubes',
            'cut',
            'cutlery',
            'dashboard',
            'dashcube',
            'database',
            'deaf',
            'deafness',
            'dedent',
            'delicious',
            'desktop',
            'deviantart',
            'diamond',
            'digg',
            'dollar',
            'dot-circle-o',
            'download',
            'dribbble',
            'drivers-license',
            'drivers-license-o',
            'dropbox',
            'drupal',
            'edge',
            'index',
            'eercast',
            'eject',
            'ellipsis-h',
            'ellipsis-v',
            'empire',
            'envelope',
            'envelope-o',
            'envelope-open',
            'envelope-open-o',
            'envelope-square',
            'envira',
            'eraser',
            'etsy',
            'eur',
            'euro',
            'exchange',
            'exclamation',
            'exclamation-circle',
            'exclamation-triangle',
            'expand',
            'expeditedssl',
            'external-link',
            'external-link-square',
            'eye',
            'eye-slash',
            'eyedropper',
            'fa',
            'facebook',
            'facebook-f',
            'facebook-official',
            'facebook-square',
            'fast-backward',
            'fast-forward',
            'fax',
            'feed',
            'female',
            'fighter-jet',
            'file',
            'file-archive-o',
            'file-audio-o',
            'file-code-o',
            'file-excel-o',
            'file-image-o',
            'file-movie-o',
            'file-o',
            'file-pdf-o',
            'file-photo-o',
            'file-picture-o',
            'file-powerpoint-o',
            'file-sound-o',
            'file-text',
            'file-text-o',
            'file-video-o',
            'file-word-o',
            'file-zip-o',
            'files-o',
            'film',
            'filter',
            'fire',
            'fire-extinguisher',
            'firefox',
            'first-order',
            'flag',
            'flag-checkered',
            'flag-o',
            'flash',
            'flask',
            'flickr',
            'floppy-o',
            'folder',
            'folder-o',
            'folder-open',
            'folder-open-o',
            'font',
            'font-awesome',
            'fonticons',
            'fort-awesome',
            'forumbee',
            'forward',
            'foursquare',
            'free-code-camp',
            'frown-o',
            'futbol-o',
            'gamepad',
            'gavel',
            'gbp',
            'ge',
            'gear',
            'gears',
            'genderless',
            'get-pocket',
            'gg',
            'gg-circle',
            'gift',
            'git',
            'git-square',
            'github',
            'github-alt',
            'github-square',
            'gitlab',
            'gittip',
            'glass',
            'glide',
            'glide-g',
            'globe',
            'google',
            'google-plus',
            'google-plus-circle',
            'google-plus-official',
            'google-plus-square',
            'google-wallet',
            'graduation-cap',
            'gratipay',
            'grav',
            'group',
            'h-square',
            'hacker-news',
            'hand-grab-o',
            'hand-lizard-o',
            'hand-o-down',
            'hand-o-left',
            'hand-o-right',
            'hand-o-up',
            'hand-paper-o',
            'hand-peace-o',
            'hand-pointer-o',
            'hand-rock-o',
            'hand-scissors-o',
            'hand-spock-o',
            'hand-stop-o',
            'handshake-o',
            'hard-of-hearing',
            'hashtag',
            'hdd-o',
            'header',
            'headphones',
            'heart',
            'heart-o',
            'heartbeat',
            'history',
            'home',
            'hospital-o',
            'hotel',
            'hourglass',
            'hourglass-1',
            'hourglass-2',
            'hourglass-3',
            'hourglass-end',
            'hourglass-half',
            'hourglass-o',
            'hourglass-start',
            'houzz',
            'html5',
            'i-cursor',
            'id-badge',
            'id-card',
            'id-card-o',
            'ils',
            'image',
            'imdb',
            'inbox',
            'indent',
            'industry',
            'info',
            'info-circle',
            'inr',
            'instagram',
            'institution',
            'internet-explorer',
            'intersex',
            'ioxhost',
            'italic',
            'joomla',
            'jpy',
            'jsfiddle',
            'key',
            'keyboard-o',
            'krw',
            'language',
            'laptop',
            'lastfm',
            'lastfm-square',
            'leaf',
            'leanpub',
            'legal',
            'lemon-o',
            'level-down',
            'level-up',
            'life-bouy',
            'life-buoy',
            'life-ring',
            'life-saver',
            'lightbulb-o',
            'line-chart',
            'link',
            'linkedin',
            'linkedin-square',
            'linode',
            'linux',
            'list',
            'list-alt',
            'list-ol',
            'list-ul',
            'location-arrow',
            'lock',
            'long-arrow-down',
            'long-arrow-left',
            'long-arrow-right',
            'long-arrow-up',
            'low-vision',
            'magic',
            'magnet',
            'mail-forward',
            'mail-reply',
            'mail-reply-all',
            'male',
            'map',
            'map-marker',
            'map-o',
            'map-pin',
            'map-signs',
            'mars',
            'mars-double',
            'mars-stroke',
            'mars-stroke-h',
            'mars-stroke-v',
            'maxcdn',
            'meanpath',
            'medium',
            'medkit',
            'meetup',
            'meh-o',
            'mercury',
            'microchip',
            'microphone',
            'microphone-slash',
            'minus',
            'minus-circle',
            'minus-square',
            'minus-square-o',
            'mixcloud',
            'mobile',
            'mobile-phone',
            'modx',
            'money',
            'moon-o',
            'mortar-board',
            'motorcycle',
            'mouse-pointer',
            'music',
            'navicon',
            'neuter',
            'newspaper-o',
            'object-group',
            'object-ungroup',
            'odnoklassniki',
            'odnoklassniki-square',
            'opencart',
            'openid',
            'opera',
            'optin-monster',
            'outdent',
            'pagelines',
            'paint-brush',
            'paper-plane',
            'paper-plane-o',
            'paperclip',
            'paragraph',
            'paste',
            'pause',
            'pause-circle',
            'pause-circle-o',
            'paw',
            'paypal',
            'pencil',
            'pencil-square',
            'pencil-square-o',
            'percent',
            'phone',
            'phone-square',
            'photo',
            'picture-o',
            'pie-chart',
            'pied-piper',
            'pied-piper-alt',
            'pied-piper-pp',
            'pinterest',
            'pinterest-p',
            'pinterest-square',
            'plane',
            'play',
            'play-circle',
            'play-circle-o',
            'plug',
            'plus',
            'plus-circle',
            'plus-square',
            'plus-square-o',
            'podcast',
            'power-off',
            'print',
            'product-hunt',
            'puzzle-piece',
            'qq',
            'qrcode',
            'question',
            'question-circle',
            'question-circle-o',
            'quora',
            'quote-left',
            'quote-right',
            'ra',
            'random',
            'ravelry',
            'rebel',
            'recycle',
            'reddit',
            'reddit-alien',
            'reddit-square',
            'refresh',
            'registered',
            'remove',
            'renren',
            'reorder',
            'repeat',
            'reply',
            'reply-all',
            'resistance',
            'retweet',
            'rmb',
            'road',
            'rocket',
            'rotate-left',
            'rotate-right',
            'rouble',
            'rss',
            'rss-square',
            'rub',
            'ruble',
            'rupee',
            's15',
            'safari',
            'save',
            'scissors',
            'scribd',
            'search',
            'search-minus',
            'search-plus',
            'sellsy',
            'send',
            'send-o',
            'server',
            'share',
            'share-alt',
            'share-alt-square',
            'share-square',
            'share-square-o',
            'shekel',
            'sheqel',
            'shield',
            'ship',
            'shirtsinbulk',
            'shopping-bag',
            'shopping-basket',
            'shopping-cart',
            'shower',
            'sign-in',
            'sign-language',
            'sign-out',
            'signal',
            'signing',
            'simplybuilt',
            'sitemap',
            'skyatlas',
            'skype',
            'slack',
            'sliders',
            'slideshare',
            'smile-o',
            'snapchat',
            'snapchat-ghost',
            'snapchat-square',
            'snowflake-o',
            'soccer-ball-o',
            'sort',
            'sort-alpha-asc',
            'sort-alpha-desc',
            'sort-amount-asc',
            'sort-amount-desc',
            'sort-asc',
            'sort-desc',
            'sort-down',
            'sort-numeric-asc',
            'sort-numeric-desc',
            'sort-up',
            'soundcloud',
            'space-shuttle',
            'spinner',
            'spoon',
            'spotify',
            'square',
            'square-o',
            'stack-exchange',
            'stack-overflow',
            'star',
            'star-half',
            'star-half-empty',
            'star-half-full',
            'star-half-o',
            'star-o',
            'steam',
            'steam-square',
            'step-backward',
            'step-forward',
            'stethoscope',
            'sticky-note',
            'sticky-note-o',
            'stop',
            'stop-circle',
            'stop-circle-o',
            'street-view',
            'strikethrough',
            'stumbleupon',
            'stumbleupon-circle',
            'subscript',
            'subway',
            'suitcase',
            'sun-o',
            'superpowers',
            'superscript',
            'support',
            'table',
            'tablet',
            'tachometer',
            'tag',
            'tags',
            'tasks',
            'taxi',
            'telegram',
            'television',
            'tencent-weibo',
            'terminal',
            'text-height',
            'text-width',
            'th',
            'th-large',
            'th-list',
            'themeisle',
            'thermometer',
            'thermometer-0',
            'thermometer-1',
            'thermometer-2',
            'thermometer-3',
            'thermometer-4',
            'thermometer-empty',
            'thermometer-full',
            'thermometer-half',
            'thermometer-quarter',
            'thermometer-three-quarters',
            'thumb-tack',
            'thumbs-down',
            'thumbs-o-down',
            'thumbs-o-up',
            'thumbs-up',
            'ticket',
            'times',
            'times-circle',
            'times-circle-o',
            'times-rectangle',
            'times-rectangle-o',
            'tint',
            'toggle-down',
            'toggle-left',
            'toggle-off',
            'toggle-on',
            'toggle-right',
            'toggle-up',
            'trademark',
            'train',
            'transgender',
            'transgender-alt',
            'trash',
            'trash-o',
            'tree',
            'trello',
            'tripadvisor',
            'trophy',
            'truck',
            'try',
            'tty',
            'tumblr',
            'tumblr-square',
            'turkish-lira',
            'tv',
            'twitch',
            'twitter',
            'twitter-square',
            'umbrella',
            'underline',
            'undo',
            'universal-access',
            'university',
            'unlink',
            'unlock',
            'unlock-alt',
            'unsorted',
            'upload',
            'usb',
            'usd',
            'user',
            'user-circle',
            'user-circle-o',
            'user-md',
            'user-o',
            'user-plus',
            'user-secret',
            'user-times',
            'users',
            'vcard',
            'vcard-o',
            'venus',
            'venus-double',
            'venus-mars',
            'viacoin',
            'viadeo',
            'viadeo-square',
            'video-camera',
            'vimeo',
            'vimeo-square',
            'vine',
            'vk',
            'volume-control-phone',
            'volume-down',
            'volume-off',
            'volume-up',
            'warning',
            'wechat',
            'weibo',
            'weixin',
            'whatsapp',
            'wheelchair',
            'wheelchair-alt',
            'wifi',
            'wikipedia-w',
            'window-close',
            'window-close-o',
            'window-maximize',
            'window-minimize',
            'window-restore',
            'windows',
            'won',
            'wordpress',
            'wpbeginner',
            'wpexplorer',
            'wpforms',
            'wrench',
            'xing',
            'xing-square',
            'y-combinator',
            'y-combinator-square',
            'yahoo',
            'yc',
            'yc-square',
            'yelp',
            'yen',
            'yoast',
            'youtube',
            'youtube-play',
            'youtube-square'
        ];
        this.temp = icons;
        this.icons = icons;
    }
    FontawesomeComponent.prototype.updateFilter = function (event) {
        var query = event.target.value;
        var filtered = this.temp.filter(function (el) {
            return el.toLowerCase().indexOf(query.toLowerCase()) > -1;
        });
        this.icons = filtered;
    };
    FontawesomeComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-fontawesome',
            template: __webpack_require__("../../../../../src/app/icons/fontawesome/fontawesome.component.html"),
            styles: [__webpack_require__("../../../../../src/app/icons/fontawesome/fontawesome.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], FontawesomeComponent);
    return FontawesomeComponent;
}());

//# sourceMappingURL=fontawesome.component.js.map

/***/ }),

/***/ "../../../../../src/app/icons/linea/linea.component.html":
/***/ (function(module, exports) {

module.exports = "<input type=\"email\" class=\"form-control form-control-lg mb-3\" placeholder=\"Type to filter icons\" required (keyup)='updateFilter($event)'>\n\n<div class=\"icon-list\">\n  <div class=\"mb-3\" *ngFor=\"let group of collection\">\n    <p class=\"ff-headers text-uppercase mb-3 fw-600\">{{group.name}}</p>\n    <div class=\"row\">\n      <div class=\"fa-hover col-md-3 col-sm-4\" *ngFor=\"let icon of group.icons\">\n        <a href=\"javascript:;\" class=\"text-color\" target=\"_blank\">\n          <i class=\"icon {{icon}}\"></i>\n          <span>{{icon}}</span>\n        </a>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/icons/linea/linea.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".icon-list a {\n  display: block;\n  padding-left: 40px;\n  height: 32px;\n  line-height: 32px;\n  margin-bottom: 5px; }\n  .icon-list a .icon, .icon-list a .fa,\n  .icon-list a [class^=\"icon-\"],\n  .icon-list a [class*=\" icon-\"] {\n    position: absolute;\n    top: 0;\n    left: 10px;\n    min-width: 30px;\n    text-align: center;\n    line-height: 32px; }\n  .icon-list a:hover .fa,\n  .icon-list a:hover .icon,\n  .icon-list a:hover [class^=\"icon-\"],\n  .icon-list a:hover [class*=\" icon-\"] {\n    top: 5px;\n    font-size: 1.5em; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/icons/linea/linea.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LineaComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LineaComponent = (function () {
    function LineaComponent() {
        this.temp = [];
        this.collection = [];
        var linea = [{
                name: 'arrows',
                icons: [
                    'icon-arrows-anticlockwise',
                    'icon-arrows-anticlockwise-dashed',
                    'icon-arrows-button-down',
                    'icon-arrows-button-off',
                    'icon-arrows-button-on',
                    'icon-arrows-button-up',
                    'icon-arrows-check',
                    'icon-arrows-circle-check',
                    'icon-arrows-circle-down',
                    'icon-arrows-circle-downleft',
                    'icon-arrows-circle-downright',
                    'icon-arrows-circle-left',
                    'icon-arrows-circle-minus',
                    'icon-arrows-circle-plus',
                    'icon-arrows-circle-remove',
                    'icon-arrows-circle-right',
                    'icon-arrows-circle-up',
                    'icon-arrows-circle-upleft',
                    'icon-arrows-circle-upright',
                    'icon-arrows-clockwise',
                    'icon-arrows-clockwise-dashed',
                    'icon-arrows-compress',
                    'icon-arrows-deny',
                    'icon-arrows-diagonal',
                    'icon-arrows-diagonal2',
                    'icon-arrows-down',
                    'icon-arrows-down-double',
                    'icon-arrows-downleft',
                    'icon-arrows-downright',
                    'icon-arrows-drag-down',
                    'icon-arrows-drag-down-dashed',
                    'icon-arrows-drag-horiz',
                    'icon-arrows-drag-left',
                    'icon-arrows-drag-left-dashed',
                    'icon-arrows-drag-right',
                    'icon-arrows-drag-right-dashed',
                    'icon-arrows-drag-up',
                    'icon-arrows-drag-up-dashed',
                    'icon-arrows-drag-vert',
                    'icon-arrows-exclamation',
                    'icon-arrows-expand',
                    'icon-arrows-expand-diagonal1',
                    'icon-arrows-expand-horizontal1',
                    'icon-arrows-expand-vertical1',
                    'icon-arrows-fit-horizontal',
                    'icon-arrows-fit-vertical',
                    'icon-arrows-glide',
                    'icon-arrows-glide-horizontal',
                    'icon-arrows-glide-vertical',
                    'icon-arrows-hamburger1',
                    'icon-arrows-hamburger-2',
                    'icon-arrows-horizontal',
                    'icon-arrows-info',
                    'icon-arrows-keyboard-alt',
                    'icon-arrows-keyboard-cmd',
                    'icon-arrows-keyboard-delete',
                    'icon-arrows-keyboard-down',
                    'icon-arrows-keyboard-left',
                    'icon-arrows-keyboard-return',
                    'icon-arrows-keyboard-right',
                    'icon-arrows-keyboard-shift',
                    'icon-arrows-keyboard-tab',
                    'icon-arrows-keyboard-up',
                    'icon-arrows-left',
                    'icon-arrows-left-double-32',
                    'icon-arrows-minus',
                    'icon-arrows-move',
                    'icon-arrows-move2',
                    'icon-arrows-move-bottom',
                    'icon-arrows-move-left',
                    'icon-arrows-move-right',
                    'icon-arrows-move-top',
                    'icon-arrows-plus',
                    'icon-arrows-question',
                    'icon-arrows-remove',
                    'icon-arrows-right',
                    'icon-arrows-right-double',
                    'icon-arrows-rotate',
                    'icon-arrows-rotate-anti',
                    'icon-arrows-rotate-anti-dashed',
                    'icon-arrows-rotate-dashed',
                    'icon-arrows-shrink',
                    'icon-arrows-shrink-diagonal1',
                    'icon-arrows-shrink-diagonal2',
                    'icon-arrows-shrink-horizonal2',
                    'icon-arrows-shrink-horizontal1',
                    'icon-arrows-shrink-vertical1',
                    'icon-arrows-shrink-vertical2',
                    'icon-arrows-sign-down',
                    'icon-arrows-sign-left',
                    'icon-arrows-sign-right',
                    'icon-arrows-sign-up',
                    'icon-arrows-slide-down1',
                    'icon-arrows-slide-down2',
                    'icon-arrows-slide-left1',
                    'icon-arrows-slide-left2',
                    'icon-arrows-slide-right1',
                    'icon-arrows-slide-right2',
                    'icon-arrows-slide-up1',
                    'icon-arrows-slide-up2',
                    'icon-arrows-slim-down',
                    'icon-arrows-slim-down-dashed',
                    'icon-arrows-slim-left',
                    'icon-arrows-slim-left-dashed',
                    'icon-arrows-slim-right',
                    'icon-arrows-slim-right-dashed',
                    'icon-arrows-slim-up',
                    'icon-arrows-slim-up-dashed',
                    'icon-arrows-square-check',
                    'icon-arrows-square-down',
                    'icon-arrows-square-downleft',
                    'icon-arrows-square-downright',
                    'icon-arrows-square-left',
                    'icon-arrows-square-minus',
                    'icon-arrows-square-plus',
                    'icon-arrows-square-remove',
                    'icon-arrows-square-right',
                    'icon-arrows-square-up',
                    'icon-arrows-square-upleft',
                    'icon-arrows-square-upright',
                    'icon-arrows-squares',
                    'icon-arrows-stretch-diagonal1',
                    'icon-arrows-stretch-diagonal2',
                    'icon-arrows-stretch-diagonal3',
                    'icon-arrows-stretch-diagonal4',
                    'icon-arrows-stretch-horizontal1',
                    'icon-arrows-stretch-horizontal2',
                    'icon-arrows-stretch-vertical1',
                    'icon-arrows-stretch-vertical2',
                    'icon-arrows-switch-horizontal',
                    'icon-arrows-switch-vertical',
                    'icon-arrows-up',
                    'icon-arrows-up-double-33',
                    'icon-arrows-upleft',
                    'icon-arrows-upright',
                    'icon-arrows-vertical'
                ]
            },
            {
                name: 'basic',
                icons: [
                    'icon-basic-accelerator',
                    'icon-basic-alarm',
                    'icon-basic-anchor',
                    'icon-basic-anticlockwise',
                    'icon-basic-archive',
                    'icon-basic-archive-full',
                    'icon-basic-ban',
                    'icon-basic-battery-charge',
                    'icon-basic-battery-empty',
                    'icon-basic-battery-full',
                    'icon-basic-battery-half',
                    'icon-basic-bolt',
                    'icon-basic-book',
                    'icon-basic-book-pen',
                    'icon-basic-book-pencil',
                    'icon-basic-bookmark',
                    'icon-basic-calculator',
                    'icon-basic-calendar',
                    'icon-basic-cards-diamonds',
                    'icon-basic-cards-hearts',
                    'icon-basic-case',
                    'icon-basic-chronometer',
                    'icon-basic-clessidre',
                    'icon-basic-clock',
                    'icon-basic-clockwise',
                    'icon-basic-cloud',
                    'icon-basic-clubs',
                    'icon-basic-compass',
                    'icon-basic-cup',
                    'icon-basic-diamonds',
                    'icon-basic-display',
                    'icon-basic-download',
                    'icon-basic-exclamation',
                    'icon-basic-eye',
                    'icon-basic-eye-closed',
                    'icon-basic-female',
                    'icon-basic-flag1',
                    'icon-basic-flag2',
                    'icon-basic-floppydisk',
                    'icon-basic-folder',
                    'icon-basic-folder-multiple',
                    'icon-basic-gear',
                    'icon-basic-geolocalize-01',
                    'icon-basic-geolocalize-05',
                    'icon-basic-globe',
                    'icon-basic-gunsight',
                    'icon-basic-hammer',
                    'icon-basic-headset',
                    'icon-basic-heart',
                    'icon-basic-heart-broken',
                    'icon-basic-helm',
                    'icon-basic-home',
                    'icon-basic-info',
                    'icon-basic-ipod',
                    'icon-basic-joypad',
                    'icon-basic-key',
                    'icon-basic-keyboard',
                    'icon-basic-laptop',
                    'icon-basic-life-buoy',
                    'icon-basic-lightbulb',
                    'icon-basic-link',
                    'icon-basic-lock',
                    'icon-basic-lock-open',
                    'icon-basic-magic-mouse',
                    'icon-basic-magnifier',
                    'icon-basic-magnifier-minus',
                    'icon-basic-magnifier-plus',
                    'icon-basic-mail',
                    'icon-basic-mail-multiple',
                    'icon-basic-mail-open',
                    'icon-basic-mail-open-text',
                    'icon-basic-male',
                    'icon-basic-map',
                    'icon-basic-message',
                    'icon-basic-message-multiple',
                    'icon-basic-message-txt',
                    'icon-basic-mixer2',
                    'icon-basic-mouse',
                    'icon-basic-notebook',
                    'icon-basic-notebook-pen',
                    'icon-basic-notebook-pencil',
                    'icon-basic-paperplane',
                    'icon-basic-pencil-ruler',
                    'icon-basic-pencil-ruler-pen',
                    'icon-basic-photo',
                    'icon-basic-picture',
                    'icon-basic-picture-multiple',
                    'icon-basic-pin1',
                    'icon-basic-pin2',
                    'icon-basic-postcard',
                    'icon-basic-postcard-multiple',
                    'icon-basic-printer',
                    'icon-basic-question',
                    'icon-basic-rss',
                    'icon-basic-server',
                    'icon-basic-server2',
                    'icon-basic-server-cloud',
                    'icon-basic-server-download',
                    'icon-basic-server-upload',
                    'icon-basic-settings',
                    'icon-basic-share',
                    'icon-basic-sheet',
                    'icon-basic-sheet-multiple',
                    'icon-basic-sheet-pen',
                    'icon-basic-sheet-pencil',
                    'icon-basic-sheet-txt',
                    'icon-basic-signs',
                    'icon-basic-smartphone',
                    'icon-basic-spades',
                    'icon-basic-spread',
                    'icon-basic-spread-bookmark',
                    'icon-basic-spread-text',
                    'icon-basic-spread-text-bookmark',
                    'icon-basic-star',
                    'icon-basic-tablet',
                    'icon-basic-target',
                    'icon-basic-todo',
                    'icon-basic-todo-pen',
                    'icon-basic-todo-pencil',
                    'icon-basic-todo-txt',
                    'icon-basic-todolist-pen',
                    'icon-basic-todolist-pencil',
                    'icon-basic-trashcan',
                    'icon-basic-trashcan-full',
                    'icon-basic-trashcan-refresh',
                    'icon-basic-trashcan-remove',
                    'icon-basic-upload',
                    'icon-basic-usb',
                    'icon-basic-video',
                    'icon-basic-watch',
                    'icon-basic-webpage',
                    'icon-basic-webpage-img-txt',
                    'icon-basic-webpage-multiple',
                    'icon-basic-webpage-txt',
                    'icon-basic-world'
                ]
            },
            {
                name: 'basic-elaboration',
                icons: [
                    'icon-basic-elaboration-bookmark-checck',
                    'icon-basic-elaboration-bookmark-minus',
                    'icon-basic-elaboration-bookmark-plus',
                    'icon-basic-elaboration-bookmark-remove',
                    'icon-basic-elaboration-briefcase-check',
                    'icon-basic-elaboration-briefcase-download',
                    'icon-basic-elaboration-briefcase-flagged',
                    'icon-basic-elaboration-briefcase-minus',
                    'icon-basic-elaboration-briefcase-plus',
                    'icon-basic-elaboration-briefcase-refresh',
                    'icon-basic-elaboration-briefcase-remove',
                    'icon-basic-elaboration-briefcase-search',
                    'icon-basic-elaboration-briefcase-star',
                    'icon-basic-elaboration-briefcase-upload',
                    'icon-basic-elaboration-browser-check',
                    'icon-basic-elaboration-browser-download',
                    'icon-basic-elaboration-browser-minus',
                    'icon-basic-elaboration-browser-plus',
                    'icon-basic-elaboration-browser-refresh',
                    'icon-basic-elaboration-browser-remove',
                    'icon-basic-elaboration-browser-search',
                    'icon-basic-elaboration-browser-star',
                    'icon-basic-elaboration-browser-upload',
                    'icon-basic-elaboration-calendar-check',
                    'icon-basic-elaboration-calendar-cloud',
                    'icon-basic-elaboration-calendar-download',
                    'icon-basic-elaboration-calendar-empty',
                    'icon-basic-elaboration-calendar-flagged',
                    'icon-basic-elaboration-calendar-heart',
                    'icon-basic-elaboration-calendar-minus',
                    'icon-basic-elaboration-calendar-next',
                    'icon-basic-elaboration-calendar-noaccess',
                    'icon-basic-elaboration-calendar-pencil',
                    'icon-basic-elaboration-calendar-plus',
                    'icon-basic-elaboration-calendar-previous',
                    'icon-basic-elaboration-calendar-refresh',
                    'icon-basic-elaboration-calendar-remove',
                    'icon-basic-elaboration-calendar-search',
                    'icon-basic-elaboration-calendar-star',
                    'icon-basic-elaboration-calendar-upload',
                    'icon-basic-elaboration-cloud-check',
                    'icon-basic-elaboration-cloud-download',
                    'icon-basic-elaboration-cloud-minus',
                    'icon-basic-elaboration-cloud-noaccess',
                    'icon-basic-elaboration-cloud-plus',
                    'icon-basic-elaboration-cloud-refresh',
                    'icon-basic-elaboration-cloud-remove',
                    'icon-basic-elaboration-cloud-search',
                    'icon-basic-elaboration-cloud-upload',
                    'icon-basic-elaboration-document-check',
                    'icon-basic-elaboration-document-cloud',
                    'icon-basic-elaboration-document-download',
                    'icon-basic-elaboration-document-flagged',
                    'icon-basic-elaboration-document-graph',
                    'icon-basic-elaboration-document-heart',
                    'icon-basic-elaboration-document-minus',
                    'icon-basic-elaboration-document-next',
                    'icon-basic-elaboration-document-noaccess',
                    'icon-basic-elaboration-document-note',
                    'icon-basic-elaboration-document-pencil',
                    'icon-basic-elaboration-document-picture',
                    'icon-basic-elaboration-document-plus',
                    'icon-basic-elaboration-document-previous',
                    'icon-basic-elaboration-document-refresh',
                    'icon-basic-elaboration-document-remove',
                    'icon-basic-elaboration-document-search',
                    'icon-basic-elaboration-document-star',
                    'icon-basic-elaboration-document-upload',
                    'icon-basic-elaboration-folder-check',
                    'icon-basic-elaboration-folder-cloud',
                    'icon-basic-elaboration-folder-document',
                    'icon-basic-elaboration-folder-download',
                    'icon-basic-elaboration-folder-flagged',
                    'icon-basic-elaboration-folder-graph',
                    'icon-basic-elaboration-folder-heart',
                    'icon-basic-elaboration-folder-minus',
                    'icon-basic-elaboration-folder-next',
                    'icon-basic-elaboration-folder-noaccess',
                    'icon-basic-elaboration-folder-note',
                    'icon-basic-elaboration-folder-pencil',
                    'icon-basic-elaboration-folder-picture',
                    'icon-basic-elaboration-folder-plus',
                    'icon-basic-elaboration-folder-previous',
                    'icon-basic-elaboration-folder-refresh',
                    'icon-basic-elaboration-folder-remove',
                    'icon-basic-elaboration-folder-search',
                    'icon-basic-elaboration-folder-star',
                    'icon-basic-elaboration-folder-upload',
                    'icon-basic-elaboration-mail-check',
                    'icon-basic-elaboration-mail-cloud',
                    'icon-basic-elaboration-mail-document',
                    'icon-basic-elaboration-mail-download',
                    'icon-basic-elaboration-mail-flagged',
                    'icon-basic-elaboration-mail-heart',
                    'icon-basic-elaboration-mail-next',
                    'icon-basic-elaboration-mail-noaccess',
                    'icon-basic-elaboration-mail-note',
                    'icon-basic-elaboration-mail-pencil',
                    'icon-basic-elaboration-mail-picture',
                    'icon-basic-elaboration-mail-previous',
                    'icon-basic-elaboration-mail-refresh',
                    'icon-basic-elaboration-mail-remove',
                    'icon-basic-elaboration-mail-search',
                    'icon-basic-elaboration-mail-star',
                    'icon-basic-elaboration-mail-upload',
                    'icon-basic-elaboration-message-check',
                    'icon-basic-elaboration-message-dots',
                    'icon-basic-elaboration-message-happy',
                    'icon-basic-elaboration-message-heart',
                    'icon-basic-elaboration-message-minus',
                    'icon-basic-elaboration-message-note',
                    'icon-basic-elaboration-message-plus',
                    'icon-basic-elaboration-message-refresh',
                    'icon-basic-elaboration-message-remove',
                    'icon-basic-elaboration-message-sad',
                    'icon-basic-elaboration-smartphone-cloud',
                    'icon-basic-elaboration-smartphone-heart',
                    'icon-basic-elaboration-smartphone-noaccess',
                    'icon-basic-elaboration-smartphone-note',
                    'icon-basic-elaboration-smartphone-pencil',
                    'icon-basic-elaboration-smartphone-picture',
                    'icon-basic-elaboration-smartphone-refresh',
                    'icon-basic-elaboration-smartphone-search',
                    'icon-basic-elaboration-tablet-cloud',
                    'icon-basic-elaboration-tablet-heart',
                    'icon-basic-elaboration-tablet-noaccess',
                    'icon-basic-elaboration-tablet-note',
                    'icon-basic-elaboration-tablet-pencil',
                    'icon-basic-elaboration-tablet-picture',
                    'icon-basic-elaboration-tablet-refresh',
                    'icon-basic-elaboration-tablet-search',
                    'icon-basic-elaboration-todolist-2',
                    'icon-basic-elaboration-todolist-check',
                    'icon-basic-elaboration-todolist-cloud',
                    'icon-basic-elaboration-todolist-download',
                    'icon-basic-elaboration-todolist-flagged',
                    'icon-basic-elaboration-todolist-minus',
                    'icon-basic-elaboration-todolist-noaccess',
                    'icon-basic-elaboration-todolist-pencil',
                    'icon-basic-elaboration-todolist-plus',
                    'icon-basic-elaboration-todolist-refresh',
                    'icon-basic-elaboration-todolist-remove',
                    'icon-basic-elaboration-todolist-search',
                    'icon-basic-elaboration-todolist-star',
                    'icon-basic-elaboration-todolist-upload'
                ]
            },
            {
                name: 'ecommerce',
                icons: [
                    'icon-ecommerce-bag',
                    'icon-ecommerce-bag-check',
                    'icon-ecommerce-bag-cloud',
                    'icon-ecommerce-bag-download',
                    'icon-ecommerce-bag-minus',
                    'icon-ecommerce-bag-plus',
                    'icon-ecommerce-bag-refresh',
                    'icon-ecommerce-bag-remove',
                    'icon-ecommerce-bag-search',
                    'icon-ecommerce-bag-upload',
                    'icon-ecommerce-banknote',
                    'icon-ecommerce-banknotes',
                    'icon-ecommerce-basket',
                    'icon-ecommerce-basket-check',
                    'icon-ecommerce-basket-cloud',
                    'icon-ecommerce-basket-download',
                    'icon-ecommerce-basket-minus',
                    'icon-ecommerce-basket-plus',
                    'icon-ecommerce-basket-refresh',
                    'icon-ecommerce-basket-remove',
                    'icon-ecommerce-basket-search',
                    'icon-ecommerce-basket-upload',
                    'icon-ecommerce-bath',
                    'icon-ecommerce-cart',
                    'icon-ecommerce-cart-check',
                    'icon-ecommerce-cart-cloud',
                    'icon-ecommerce-cart-content',
                    'icon-ecommerce-cart-download',
                    'icon-ecommerce-cart-minus',
                    'icon-ecommerce-cart-plus',
                    'icon-ecommerce-cart-refresh',
                    'icon-ecommerce-cart-remove',
                    'icon-ecommerce-cart-search',
                    'icon-ecommerce-cart-upload',
                    'icon-ecommerce-cent',
                    'icon-ecommerce-colon',
                    'icon-ecommerce-creditcard',
                    'icon-ecommerce-diamond',
                    'icon-ecommerce-dollar',
                    'icon-ecommerce-euro',
                    'icon-ecommerce-franc',
                    'icon-ecommerce-gift',
                    'icon-ecommerce-graph1',
                    'icon-ecommerce-graph2',
                    'icon-ecommerce-graph3',
                    'icon-ecommerce-graph-decrease',
                    'icon-ecommerce-graph-increase',
                    'icon-ecommerce-guarani',
                    'icon-ecommerce-kips',
                    'icon-ecommerce-lira',
                    'icon-ecommerce-megaphone',
                    'icon-ecommerce-money',
                    'icon-ecommerce-naira',
                    'icon-ecommerce-pesos',
                    'icon-ecommerce-pound',
                    'icon-ecommerce-receipt',
                    'icon-ecommerce-receipt-bath',
                    'icon-ecommerce-receipt-cent',
                    'icon-ecommerce-receipt-dollar',
                    'icon-ecommerce-receipt-euro',
                    'icon-ecommerce-receipt-franc',
                    'icon-ecommerce-receipt-guarani',
                    'icon-ecommerce-receipt-kips',
                    'icon-ecommerce-receipt-lira',
                    'icon-ecommerce-receipt-naira',
                    'icon-ecommerce-receipt-pesos',
                    'icon-ecommerce-receipt-pound',
                    'icon-ecommerce-receipt-rublo',
                    'icon-ecommerce-receipt-rupee',
                    'icon-ecommerce-receipt-tugrik',
                    'icon-ecommerce-receipt-won',
                    'icon-ecommerce-receipt-yen',
                    'icon-ecommerce-receipt-yen2',
                    'icon-ecommerce-recept-colon',
                    'icon-ecommerce-rublo',
                    'icon-ecommerce-rupee',
                    'icon-ecommerce-safe',
                    'icon-ecommerce-sale',
                    'icon-ecommerce-sales',
                    'icon-ecommerce-ticket',
                    'icon-ecommerce-tugriks',
                    'icon-ecommerce-wallet',
                    'icon-ecommerce-won',
                    'icon-ecommerce-yen',
                    'icon-ecommerce-yen2'
                ]
            },
            {
                name: 'music',
                icons: [
                    'icon-music-beginning-button',
                    'icon-music-bell',
                    'icon-music-cd',
                    'icon-music-diapason',
                    'icon-music-eject-button',
                    'icon-music-end-button',
                    'icon-music-fastforward-button',
                    'icon-music-headphones',
                    'icon-music-ipod',
                    'icon-music-loudspeaker',
                    'icon-music-microphone',
                    'icon-music-microphone-old',
                    'icon-music-mixer',
                    'icon-music-mute',
                    'icon-music-note-multiple',
                    'icon-music-note-single',
                    'icon-music-pause-button',
                    'icon-music-play-button',
                    'icon-music-playlist',
                    'icon-music-radio-ghettoblaster',
                    'icon-music-radio-portable',
                    'icon-music-record',
                    'icon-music-recordplayer',
                    'icon-music-repeat-button',
                    'icon-music-rewind-button',
                    'icon-music-shuffle-button',
                    'icon-music-stop-button',
                    'icon-music-tape',
                    'icon-music-volume-down',
                    'icon-music-volume-up'
                ]
            },
            {
                name: 'software',
                icons: [
                    'icon-software-add-vectorpoint',
                    'icon-software-box-oval',
                    'icon-software-box-polygon',
                    'icon-software-box-rectangle',
                    'icon-software-box-roundedrectangle',
                    'icon-software-character',
                    'icon-software-crop',
                    'icon-software-eyedropper',
                    'icon-software-font-allcaps',
                    'icon-software-font-baseline-shift',
                    'icon-software-font-horizontal-scale',
                    'icon-software-font-kerning',
                    'icon-software-font-leading',
                    'icon-software-font-size',
                    'icon-software-font-smallcapital',
                    'icon-software-font-smallcaps',
                    'icon-software-font-strikethrough',
                    'icon-software-font-tracking',
                    'icon-software-font-underline',
                    'icon-software-font-vertical-scale',
                    'icon-software-horizontal-align-center',
                    'icon-software-horizontal-align-left',
                    'icon-software-horizontal-align-right',
                    'icon-software-horizontal-distribute-center',
                    'icon-software-horizontal-distribute-left',
                    'icon-software-horizontal-distribute-right',
                    'icon-software-indent-firstline',
                    'icon-software-indent-left',
                    'icon-software-indent-right',
                    'icon-software-lasso',
                    'icon-software-layers1',
                    'icon-software-layers2',
                    'icon-software-layout',
                    'icon-software-layout-2columns',
                    'icon-software-layout-3columns',
                    'icon-software-layout-4boxes',
                    'icon-software-layout-4columns',
                    'icon-software-layout-4lines',
                    'icon-software-layout-8boxes',
                    'icon-software-layout-header',
                    'icon-software-layout-header-2columns',
                    'icon-software-layout-header-3columns',
                    'icon-software-layout-header-4boxes',
                    'icon-software-layout-header-4columns',
                    'icon-software-layout-header-complex',
                    'icon-software-layout-header-complex2',
                    'icon-software-layout-header-complex3',
                    'icon-software-layout-header-complex4',
                    'icon-software-layout-header-sideleft',
                    'icon-software-layout-header-sideright',
                    'icon-software-layout-sidebar-left',
                    'icon-software-layout-sidebar-right',
                    'icon-software-magnete',
                    'icon-software-pages',
                    'icon-software-paintbrush',
                    'icon-software-paintbucket',
                    'icon-software-paintroller',
                    'icon-software-paragraph',
                    'icon-software-paragraph-align-left',
                    'icon-software-paragraph-align-right',
                    'icon-software-paragraph-center',
                    'icon-software-paragraph-justify-all',
                    'icon-software-paragraph-justify-center',
                    'icon-software-paragraph-justify-left',
                    'icon-software-paragraph-justify-right',
                    'icon-software-paragraph-space-after',
                    'icon-software-paragraph-space-before',
                    'icon-software-pathfinder-exclude',
                    'icon-software-pathfinder-intersect',
                    'icon-software-pathfinder-subtract',
                    'icon-software-pathfinder-unite',
                    'icon-software-pen',
                    'icon-software-pen-add',
                    'icon-software-pen-remove',
                    'icon-software-pencil',
                    'icon-software-polygonallasso',
                    'icon-software-reflect-horizontal',
                    'icon-software-reflect-vertical',
                    'icon-software-remove-vectorpoint',
                    'icon-software-scale-expand',
                    'icon-software-scale-reduce',
                    'icon-software-selection-oval',
                    'icon-software-selection-polygon',
                    'icon-software-selection-rectangle',
                    'icon-software-selection-roundedrectangle',
                    'icon-software-shape-oval',
                    'icon-software-shape-polygon',
                    'icon-software-shape-rectangle',
                    'icon-software-shape-roundedrectangle',
                    'icon-software-slice',
                    'icon-software-transform-bezier',
                    'icon-software-vector-box',
                    'icon-software-vector-composite',
                    'icon-software-vector-line',
                    'icon-software-vertical-align-bottom',
                    'icon-software-vertical-align-center',
                    'icon-software-vertical-align-top',
                    'icon-software-vertical-distribute-bottom',
                    'icon-software-vertical-distribute-center',
                    'icon-software-vertical-distribute-top'
                ]
            },
            {
                name: 'weather',
                icons: [
                    'icon-weather-aquarius',
                    'icon-weather-aries',
                    'icon-weather-cancer',
                    'icon-weather-capricorn',
                    'icon-weather-cloud',
                    'icon-weather-cloud-drop',
                    'icon-weather-cloud-lightning',
                    'icon-weather-cloud-snowflake',
                    'icon-weather-downpour-fullmoon',
                    'icon-weather-downpour-halfmoon',
                    'icon-weather-downpour-sun',
                    'icon-weather-drop',
                    'icon-weather-first-quarter',
                    'icon-weather-fog',
                    'icon-weather-fog-fullmoon',
                    'icon-weather-fog-halfmoon',
                    'icon-weather-fog-sun',
                    'icon-weather-fullmoon',
                    'icon-weather-gemini',
                    'icon-weather-hail',
                    'icon-weather-hail-fullmoon',
                    'icon-weather-hail-halfmoon',
                    'icon-weather-hail-sun',
                    'icon-weather-last-quarter',
                    'icon-weather-leo',
                    'icon-weather-libra',
                    'icon-weather-lightning',
                    'icon-weather-mistyrain',
                    'icon-weather-mistyrain-fullmoon',
                    'icon-weather-mistyrain-halfmoon',
                    'icon-weather-mistyrain-sun',
                    'icon-weather-moon',
                    'icon-weather-moondown-full',
                    'icon-weather-moondown-half',
                    'icon-weather-moonset-full',
                    'icon-weather-moonset-half',
                    'icon-weather-move2',
                    'icon-weather-newmoon',
                    'icon-weather-pisces',
                    'icon-weather-rain',
                    'icon-weather-rain-fullmoon',
                    'icon-weather-rain-halfmoon',
                    'icon-weather-rain-sun',
                    'icon-weather-sagittarius',
                    'icon-weather-scorpio',
                    'icon-weather-snow',
                    'icon-weather-snow-fullmoon',
                    'icon-weather-snow-halfmoon',
                    'icon-weather-snow-sun',
                    'icon-weather-snowflake',
                    'icon-weather-star',
                    'icon-weather-storm-11',
                    'icon-weather-storm-32',
                    'icon-weather-storm-fullmoon',
                    'icon-weather-storm-halfmoon',
                    'icon-weather-storm-sun',
                    'icon-weather-sun',
                    'icon-weather-sundown',
                    'icon-weather-sunset',
                    'icon-weather-taurus',
                    'icon-weather-tempest',
                    'icon-weather-tempest-fullmoon',
                    'icon-weather-tempest-halfmoon',
                    'icon-weather-tempest-sun',
                    'icon-weather-variable-fullmoon',
                    'icon-weather-variable-halfmoon',
                    'icon-weather-variable-sun',
                    'icon-weather-virgo',
                    'icon-weather-waning-cresent',
                    'icon-weather-waning-gibbous',
                    'icon-weather-waxing-cresent',
                    'icon-weather-waxing-gibbous',
                    'icon-weather-wind',
                    'icon-weather-wind-e',
                    'icon-weather-wind-fullmoon',
                    'icon-weather-wind-halfmoon',
                    'icon-weather-wind-n',
                    'icon-weather-wind-ne',
                    'icon-weather-wind-nw',
                    'icon-weather-wind-s',
                    'icon-weather-wind-se',
                    'icon-weather-wind-sun',
                    'icon-weather-wind-sw',
                    'icon-weather-wind-w',
                    'icon-weather-windgust'
                ]
            }];
        this.temp = linea;
        this.collection = linea;
    }
    LineaComponent.prototype.updateFilter = function (event) {
        var query = event.target.value;
        var updated = [];
        var filtered = this.temp.filter(function (el) {
            el.ico = el.icons.filter(function (elm) {
                return elm.toLowerCase().indexOf(query) !== -1 || !query;
            });
            updated.push({
                name: el.name,
                icons: el.ico
            });
        });
        this.collection = updated;
    };
    LineaComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-linea',
            template: __webpack_require__("../../../../../src/app/icons/linea/linea.component.html"),
            styles: [__webpack_require__("../../../../../src/app/icons/linea/linea.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], LineaComponent);
    return LineaComponent;
}());

//# sourceMappingURL=linea.component.js.map

/***/ }),

/***/ "../../../../../src/app/icons/sli/sli.component.html":
/***/ (function(module, exports) {

module.exports = "<input type=\"email\" class=\"form-control form-control-lg mb-3\" placeholder=\"Type to filter icons\" required (keyup)='updateFilter($event)'>\n\n<div class=\"icon-group\">\n  <div class=\"row\">\n    <div class=\"fa-hover col-md-3 col-sm-4\" *ngFor=\"let icon of icons\">\n      <a href=\"javascript:;\" class=\"text-color\" target=\"_blank\">\n        <i class=\"icon-{{icon}}\"></i>\n        <span>{{icon}}</span>\n      </a>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/icons/sli/sli.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports
exports.i(__webpack_require__("../../../../css-loader/index.js?{\"sourceMap\":false,\"importLoaders\":1}!../../../../postcss-loader/index.js?{\"ident\":\"postcss\"}!../../../../../src/assets/fonts/simple-line-icons/simple-line-icons.css"), "");

// module
exports.push([module.i, ".icon-group a {\n  display: block;\n  padding-left: 40px;\n  height: 32px;\n  line-height: 32px;\n  margin-bottom: 5px; }\n  .icon-group a .icon, .icon-group a .fa,\n  .icon-group a [class^=\"icon-\"],\n  .icon-group a [class*=\" icon-\"] {\n    position: absolute;\n    top: 0;\n    left: 10px;\n    min-width: 30px;\n    text-align: center;\n    line-height: 32px; }\n  .icon-group a:hover .fa,\n  .icon-group a:hover .icon,\n  .icon-group a:hover [class^=\"icon-\"],\n  .icon-group a:hover [class*=\" icon-\"] {\n    top: 5px;\n    font-size: 1.5em; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/icons/sli/sli.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SliComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SliComponent = (function () {
    function SliComponent() {
        this.temp = [];
        this.icons = [];
        var icons = [
            'user',
            'people',
            'user-female',
            'user-follow',
            'user-following',
            'user-unfollow',
            'login',
            'logout',
            'emotsmile',
            'phone',
            'call-end',
            'call-in',
            'call-out',
            'map',
            'location-pin',
            'direction',
            'directions',
            'compass',
            'layers',
            'menu',
            'list',
            'options-vertical',
            'options',
            'arrow-down',
            'arrow-left',
            'arrow-right',
            'arrow-up',
            'arrow-up-circle',
            'arrow-left-circle',
            'arrow-right-circle',
            'arrow-down-circle',
            'check',
            'clock',
            'plus',
            'minus',
            'close',
            'event',
            'exclamation',
            'organization',
            'trophy',
            'screen-smartphone',
            'screen-desktop',
            'plane',
            'notebook',
            'mustache',
            'mouse',
            'magnet',
            'energy',
            'disc',
            'cursor',
            'cursor-move',
            'crop',
            'chemistry',
            'speedometer',
            'shield',
            'screen-tablet',
            'magic-wand',
            'hourglass',
            'graduation',
            'ghost',
            'game-controller',
            'fire',
            'eyeglass',
            'envelope-open',
            'envelope-letter',
            'bell',
            'badge',
            'anchor',
            'wallet',
            'vector',
            'speech',
            'puzzle',
            'printer',
            'present',
            'playlist',
            'pin',
            'picture',
            'handbag',
            'globe-alt',
            'globe',
            'folder-alt',
            'folder',
            'film',
            'feed',
            'drop',
            'drawer',
            'docs',
            'doc',
            'diamond',
            'cup',
            'calculator',
            'bubbles',
            'briefcase',
            'book-open',
            'basket-loaded',
            'basket',
            'bag',
            'action-undo',
            'action-redo',
            'wrench',
            'umbrella',
            'trash',
            'tag',
            'support',
            'frame',
            'size-fullscreen',
            'size-actual',
            'shuffle',
            'share-alt',
            'share',
            'rocket',
            'question',
            'pie-chart',
            'pencil',
            'note',
            'loop',
            'home',
            'grid',
            'graph',
            'microphone',
            'music-tone-alt',
            'music-tone',
            'earphones-alt',
            'earphones',
            'equalizer',
            'like',
            'dislike',
            'control-start',
            'control-rewind',
            'control-play',
            'control-pause',
            'control-forward',
            'control-end',
            'volume-1',
            'volume-2',
            'volume-off',
            'calendar',
            'bulb',
            'chart',
            'ban',
            'bubble',
            'camrecorder',
            'camera',
            'cloud-download',
            'cloud-upload',
            'envelope',
            'eye',
            'flag',
            'heart',
            'info',
            'key',
            'link',
            'lock',
            'lock-open',
            'magnifier',
            'magnifier-add',
            'magnifier-remove',
            'paper-clip',
            'paper-plane',
            'power',
            'refresh',
            'reload',
            'settings',
            'star',
            'symbol-female',
            'symbol-male',
            'target',
            'credit-card',
            'paypal',
            'social-tumblr',
            'social-twitter',
            'social-facebook',
            'social-instagram',
            'social-linkedin',
            'social-pinterest',
            'social-github',
            'social-google',
            'social-reddit',
            'social-skype',
            'social-dribbble',
            'social-behance',
            'social-foursqare',
            'social-soundcloud',
            'social-spotify',
            'social-stumbleupon',
            'social-youtube',
            'social-dropbox',
            'social-vkontakte',
            'social-steam'
        ];
        this.temp = icons;
        this.icons = icons;
    }
    SliComponent.prototype.updateFilter = function (event) {
        var query = event.target.value;
        var filtered = this.temp.filter(function (el) {
            return el.toLowerCase().indexOf(query.toLowerCase()) > -1;
        });
        this.icons = filtered;
    };
    SliComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-sli',
            template: __webpack_require__("../../../../../src/app/icons/sli/sli.component.html"),
            styles: [__webpack_require__("../../../../../src/app/icons/sli/sli.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], SliComponent);
    return SliComponent;
}());

//# sourceMappingURL=sli.component.js.map

/***/ }),

/***/ "../../../../../src/app/layouts/admin/admin-layout.component.html":
/***/ (function(module, exports) {

module.exports = "<ng-sidebar-container class=\"app\" [ngClass]=\"{'mode-boxed': isBoxed, 'sidebar-opened': isOpened, 'theme-light': theme == 'light', 'theme-dark': theme == 'dark', 'mode-push': _mode == 'push', 'mode-dock': _mode == 'dock', 'mode-over': _mode == 'over', 'mode-slide': _mode == 'slide', 'no-footer': options?.removeFooter, 'map-header': options?.mapHeader}\">\n  <ng-sidebar\n    [(opened)]=\"isOpened\"\n    [(mode)]=\"_mode\"\n    [position]=\"'left'\"\n    [dockedSize]=\"'80px'\"\n    [autoCollapseWidth]=\"'991'\"\n    [closeOnClickOutside]=\"isOver()\"\n    [showBackdrop]=\"isOver()\"\n    [sidebarClass]=\"'sidebar-panel'\" #sidebar>\n    <nav class=\"navbar custom-navbar main-brand\">\n      <a class=\"navbar-brand mr-auto\" [routerLink]=\"['/']\">\n        <img src=\"assets/images/logo.png\" class=\"navbar-brand-logo\" alt=\"\">\n        <span class=\"docked-hidden\">&nbsp;{{DataItem.admin_title}}</span>\n      </a>\n      <ul class=\"navbar-nav\">\n        <li class=\"nav-item\">\n          <a href=\"javascript:;\" class=\"nav-link\" (click)=\"toogleSidebar()\">\n            <i class=\"hamburger-icon v2\" *ngIf=\"_mode === 'over' && !isOver()\">\n              <span></span>\n            </i>\n          </a>\n        </li>\n      </ul>\n    </nav>\n    <!-- main navigation -->\n    <nav class=\"menu\">\n      <ul class=\"navigation\" appAccordion>\n        <li class=\"navigation-item\" appAccordionLink *ngFor=\"let menuitem of menuItems.getAll()\" group=\"{{menuitem.state}}\">\n          <a class=\"navigation-link\" appAccordionToggle [routerLink]=\"['/', menuitem.state,menuitem.child]\" *ngIf=\"menuitem.type === 'new'\">\n            <i class=\"icon icon-{{ menuitem.icon }}\"></i>\n            <span>{{ menuitem.name | translate }}</span>\n            <span class=\"mr-auto\"></span>\n            <span class=\"badge badge-{{ badge.type }}\" *ngFor=\"let badge of menuitem.badge\">{{ badge.value }}</span>\n          </a>\n          <a class=\"navigation-link\" appAccordionToggle [routerLink]=\"['/', menuitem.state]\" *ngIf=\"menuitem.type === 'link'\">\n            <i class=\"icon icon-{{ menuitem.icon }}\"></i>\n            <span>{{ menuitem.name | translate }}</span>\n            <span class=\"mr-auto\"></span>\n            <span class=\"badge badge-{{ badge.type }}\" *ngFor=\"let badge of menuitem.badge\">{{ badge.value }}</span>\n          </a>\n          <a class=\"navigation-link\" appAccordionToggle href=\"{{menuitem.state}}\" *ngIf=\"menuitem.type === 'extLink'\">\n            <i class=\"icon icon-{{ menuitem.icon }}\"></i>\n            <span>{{ menuitem.name | translate }}</span>\n            <span class=\"mr-auto\"></span>\n            <span class=\"badge badge-{{ badge.type }}\" *ngFor=\"let badge of menuitem.badge\">{{ badge.value }}</span>\n          </a>\n          <a class=\"navigation-link\" appAccordionToggle href=\"{{menuitem.state}}\" target=\"_blank\" *ngIf=\"menuitem.type === 'extTabLink'\">\n            <i class=\"icon icon-{{ menuitem.icon }}\"></i>\n            <span>{{ menuitem.name | translate }}</span>\n            <span class=\"mr-auto\"></span>\n            <span class=\"badge badge-{{ badge.type }}\" *ngFor=\"let badge of menuitem.badge\">{{ badge.value }}</span>\n          </a>\n          <a class=\"navigation-link\" appAccordionToggle href=\"javascript:;\" *ngIf=\"menuitem.type === 'sub'\">\n            <i class=\"icon icon-{{ menuitem.icon }}\"></i>\n            <span>{{ menuitem.name | translate }}</span>\n            <span class=\"mr-auto\"></span>\n            <span class=\"badge badge-{{ badge.type }}\" *ngFor=\"let badge of menuitem.badge\">{{ badge.value }}</span>\n            <i class=\"menu-caret icon icon-arrows-right\"></i>\n          </a>\n          <ul class=\"navigation-submenu\" *ngIf=\"menuitem.type === 'sub'\">\n            <li class=\"navigation-item\" *ngFor=\"let childitem of menuitem.children\" routerLinkActive=\"open\">\n              <a [routerLink]=\"['/', menuitem.state, childitem.state ]\" class=\"navigation-link relative\">{{ childitem.name | translate }}</a>\n            </li>\n          </ul>\n        </li>\n        <li class=\"navigation-item\"><hr class=\"mt-0 mb-0\" /></li>\n        <!--<li class=\"navigation-item\">-->\n          <!--<a class=\"navigation-link\" (click)=\"addMenuItem()\">-->\n            <!--<i class=\"icon icon-basic-add\"></i>-->\n            <!--<span>Add</span>-->\n          <!--</a>-->\n        <!--</li>-->\n      </ul>\n    </nav>\n    <!-- /main navigation -->\n  </ng-sidebar>\n\n  <div ng-sidebar-content class=\"app-inner\">\n    <nav class=\"navbar custom-navbar bg-faded main-header\">\n      <ul class=\"navbar-nav\">\n        <li class=\"nav-item\">\n          <a href=\"javascript:;\" class=\"nav-link\" (click)=\"toogleSidebar()\">\n            <i class=\"hamburger-icon v2\" *ngIf=\"_mode !== 'dock'\">\n              <span></span>\n            </i>\n          </a>\n        </li>\n      </ul>\n      <span class=\"navbar-heading hidden-xs-down\">{{options?.heading}}</span>\n      <span class=\"mr-auto\"></span>\n      <ul class=\"navbar-nav\">\n        <li class=\"nav-item\" ngbDropdown placement=\"bottom-right\">\n          <a href=\"javascript:;\" class=\"nav-link\" ngbDropdownToggle>\n            <img src=\"assets/images/avatar.jpg\" class=\"navbar-avatar rounded-circle\" alt=\"user\" title=\"user\">\n          </a>\n          <div ngbDropdownMenu class=\"dropdown-menu dropdown-menu-right\">\n            <a class=\"dropdown-item\" href=\"javascript:;\">\n              <i class=\"icon icon-basic-settings mr-3\"></i>\n              <span>Settings</span>\n            </a>\n            <a class=\"dropdown-item\" href=\"javascript:;\">\n              <i class=\"icon icon-basic-postcard mr-3\"></i>\n              <span>Profile</span>\n            </a>\n            <a class=\"dropdown-item\" href=\"javascript:;\">\n              <i class=\"icon icon-basic-message-multiple mr-3\"></i>\n              <span>Notifications</span>\n            </a>\n            <div class=\"dropdown-divider\"></div>\n            <a class=\"dropdown-item\" href=\"#\" (click)=\"logout()\">\n              <i class=\"icon icon-arrows-switch-vertical mr-3\"></i>\n              <span>Signout</span>\n            </a>\n          </div>\n        </li> \n        <li class=\"nav-item\" ngbDropdown placement=\"bottom-right\">\n          <a href=\"javascript:;\" class=\"nav-link\" ngbDropdownToggle>\n            <i class=\"fi flaticon-notification\"></i>\n            <span class=\"badge badge-danger\">4</span>\n          </a>\n          <div ngbDropdownMenu class=\"dropdown-menu dropdown-menu-right notifications\">\n            <div class=\"notifications-wrapper\">\n              <a href=\"javascript:;\" class=\"dropdown-item\">\n                <span class=\"badge badge-warning\">NEW</span>\n                &nbsp;Sean launched a new application\n                <span class=\"time\">2 seconds ago</span>\n              </a>\n              <a href=\"javascript:;\" class=\"dropdown-item\">\n                Removed calendar from app list\n                <span class=\"time\">4 hours ago</span>\n              </a>\n              <a href=\"javascript:;\" class=\"dropdown-item\">\n                Jack Hunt has joined mailing list\n                <span class=\"time\">9 days ago</span>\n              </a>\n              <a href=\"javascript:;\" class=\"dropdown-item\">\n                <span class=\"text-muted\">Conan Johns created a new list</span>\n                <span class=\"time\">9 days ago</span>\n              </a>\n            </div>\n            <div class=\"notification-footer\">Notifications</div>\n          </div>\n        </li>\n        <li class=\"nav-item\">\n          <a href=\"javascript:;\" class=\"nav-link\" appToggleFullscreen>\n            <i class=\"fi flaticon-fullscreen\"></i>\n          </a>\n        </li>\n        \n        <li class=\"nav-item\">\n          <span class=\"nav-divider\"></span>\n        </li>\n        <li class=\"nav-item\">\n          <a href=\"javascript:;\" class=\"nav-link\" (click)=\"openSearch(search)\">\n            <i class=\"fi flaticon-search\"></i>\n          </a>\n        </li>\n              \n      </ul>\n    </nav>\n\n    <div class=\"main-content\">\n      <router-outlet></router-outlet>\n      <nav class=\"navbar custom-navbar navbar-light main-footer small\">\n        <ul class=\"navbar-nav mr-auto\">\n          <li class=\"nav-item\">\n            <a class=\"nav-link\" href=\"javascript:;\">\n              <span>Copyright &copy; 2017</span> <span class=\"ff-headers text-uppercase\">Decima</span>. All rights reserved\n            </a>\n          </li>\n        </ul>\n        <ul class=\"navbar-nav hidden-xs-down\">\n          <li class=\"nav-item\">\n            <a class=\"nav-link\" href=\"javascript:;\">Made with love on Earth</a>\n          </li>\n        </ul>\n      </nav>\n    </div>\n  </div>\n\n</ng-sidebar-container>\n\n<ng-template #search let-c=\"close\" let-d=\"dismiss\">\n  <form class=\"search__form\" action=\"\">\n    <input class=\"search-input\" name=\"search\" type=\"search\" placeholder=\"Search...\" autocomplete=\"off\" autocorrect=\"off\" autocapitalize=\"off\" spellcheck=\"false\" autofocus=\"true\" />\n    <p class=\"text-muted\"><small><strong>Hit enter to search or ESC to close</strong></small></p>\n  </form>\n  <div class=\"search-suggestions\">\n    <h6 class=\"text-uppercase\"><strong>Suggestions?</strong></h6>\n    <p class=\"text-primary\">#medical #analytics #fitness #transport #ui #dashboard #admin #bootstrap #angular #typescript</p>\n  </div>\n  <button type=\"button\" class=\"search-close\" aria-label=\"Close search form\" (click)=\"d('Cross click')\">\n    <i class=\"fi flaticon-close\"></i>\n  </button>\n</ng-template>\n\n\n<div class=\"configuration hidden-sm-down\" [ngClass]=\"{'active': showSettings}\" *ngIf=\"showSettings\">\n  <div class=\"configuration-cog\" (click)=\"showSettings = !showSettings\">\n    <i class=\"icon icon-basic-mixer2\"></i>\n  </div>\n  <div class=\"card\">\n    <div class=\"card-header\">\n      Template Options\n    </div>\n    <div class=\"card-body\">\n      <small class=\"ff-headers text-uppercase mb-3\"><strong>Explore Sidebar API</strong></small>\n      <div class=\"custom-controls-stacked mb-2\">\n        <label class=\"custom-control custom-checkbox\">\n          <input class=\"custom-control-input\" name=\"radio-stacked\" type=\"radio\" value=\"push\" [(ngModel)]=\"_mode\" (change)=\"isOpened = true; mode = _mode\">\n          <span class=\"custom-control-indicator\"></span>\n          <span class=\"custom-control-description\">Push mode</span>\n        </label>\n        <label class=\"custom-control custom-checkbox\">\n          <input class=\"custom-control-input\" name=\"radio-stacked\" type=\"radio\" value=\"dock\" [(ngModel)]=\"_mode\" (change)=\"isOpened = true; mode = _mode\">\n          <span class=\"custom-control-indicator\"></span>\n          <span class=\"custom-control-description\">Docked mode</span>\n        </label>\n        <label class=\"custom-control custom-checkbox\">\n          <input class=\"custom-control-input\" name=\"radio-stacked\" type=\"radio\" value=\"over\" [(ngModel)]=\"_mode\" (change)=\"isOpened = true; mode = _mode\">\n          <span class=\"custom-control-indicator\"></span>\n          <span class=\"custom-control-description\">Over content mode</span>\n        </label>\n        <label class=\"custom-control custom-checkbox\">\n          <input class=\"custom-control-input\" name=\"radio-stacked\" type=\"radio\" value=\"slide\" [(ngModel)]=\"_mode\" (change)=\"isOpened = true; mode = _mode\">\n          <span class=\"custom-control-indicator\"></span>\n          <span class=\"custom-control-description\">Slide mode</span>\n        </label>\n      </div>\n\n      <small class=\"ff-headers text-uppercase mb-3\"><strong>Select A Layout</strong></small>\n      <div class=\"d-flex align-items-center\">\n        <label class=\"custom-control custom-checkbox mb-2\">\n          <input type=\"checkbox\" class=\"custom-control-input\" [(ngModel)]=\"isBoxed\">\n          <span class=\"custom-control-indicator\"></span>\n          <span class=\"custom-control-description\">Boxed</span>\n        </label>\n      </div>\n      <div class=\"d-flex align-items-center mb-2\">\n        <label class=\"custom-control custom-checkbox mb-2\">\n          <input type=\"checkbox\" class=\"custom-control-input\" [ngModel]=\"options?.removeFooter\" (ngModelChange)=\"options.removeFooter=$event\">\n          <span class=\"custom-control-indicator\"></span>\n          <span class=\"custom-control-description\">Remove footer</span>\n        </label>\n      </div>\n\n      <small class=\"ff-headers text-uppercase mb-3\"><strong>Select A Theme</strong></small>\n      <div class=\"custom-controls-stacked mb-2\">\n        <label class=\"custom-control custom-checkbox\">\n          <input class=\"custom-control-input\" name=\"radio-stacked\" type=\"radio\" value=\"light\" [(ngModel)]=\"theme\">\n          <span class=\"custom-control-indicator\"></span>\n          <span class=\"custom-control-description\">Light theme</span>\n        </label>\n        <label class=\"custom-control custom-checkbox\">\n          <input class=\"custom-control-input\" name=\"radio-stacked\" type=\"radio\" value=\"dark\" [(ngModel)]=\"theme\">\n          <span class=\"custom-control-indicator\"></span>\n          <span class=\"custom-control-description\">Dark theme</span>\n        </label>\n      </div>\n\n      <small class=\"ff-headers text-uppercase mb-3\"><strong>Select A Language</strong></small>\n      <div class=\"d-flex align-items-center\">\n        <select class=\"custom-select\" [(ngModel)]=\"currentLang\" #langSelect=\"ngModel\" (ngModelChange)=\"translate.use(currentLang)\" placeholder=\"Select language\" style=\"min-width: 50%;\">\n          <option *ngFor=\"let lang of translate.getLangs()\" [value]=\"lang\">{{ lang }}</option>\n        </select>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/layouts/admin/admin-layout.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".configuration {\n  width: 240px;\n  position: fixed;\n  right: 0;\n  top: 150px;\n  margin-left: 0;\n  z-index: 99999999;\n  transition: transform .3s ease-in-out;\n  transform: translate(100%, 0); }\n  .configuration .card {\n    box-shadow: 0 0 5px rgba(0, 0, 0, 0.2);\n    margin: 0;\n    border-radius: 0; }\n\n.configuration.active {\n  transform: translate(0, 0); }\n\n.configuration-cog {\n  width: 50px;\n  height: 50px;\n  position: absolute;\n  left: -50px;\n  line-height: 50px;\n  font-size: 24px;\n  text-align: center;\n  background: #fff;\n  box-shadow: 0 0 1px rgba(0, 0, 0, 0.2);\n  border-top-left-radius: 2px;\n  border-bottom-left-radius: 2px;\n  cursor: pointer; }\n  .configuration-cog i:before {\n    line-height: 50px; }\n\n* {\n  overflow-y: auto !important; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/layouts/admin/admin-layout.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminLayoutComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_menu_items_menu_items__ = __webpack_require__("../../../../../src/app/shared/menu-items/menu-items.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_filter__ = __webpack_require__("../../../../rxjs/add/operator/filter.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_filter___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_filter__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__ = __webpack_require__("../../../../@ngx-translate/core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__settings_settings_service__ = __webpack_require__("../../../../../src/settings/settings.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var SMALL_WIDTH_BREAKPOINT = 991;
var AdminLayoutComponent = (function () {
    function AdminLayoutComponent(menuItems, router, route, translate, modalService, titleService, zone, settings, http) {
        var _this = this;
        this.menuItems = menuItems;
        this.router = router;
        this.route = route;
        this.translate = translate;
        this.modalService = modalService;
        this.titleService = titleService;
        this.zone = zone;
        this.http = http;
        this.mediaMatcher = matchMedia("(max-width: " + SMALL_WIDTH_BREAKPOINT + "px)");
        this.currentLang = 'en';
        this.theme = 'light';
        this.showSettings = false;
        this.isDocked = false;
        this.isBoxed = false;
        this.isOpened = true;
        this.mode = 'push';
        this._mode = this.mode;
        this._autoCollapseWidth = 991;
        this.width = window.innerWidth;
        this.DataItem = [];
        this.ServerUrl = "";
        this.PageTitle = '';
        var browserLang = translate.getBrowserLang();
        translate.use(browserLang.match(/en|fr/) ? browserLang : 'en');
        this.mediaMatcher.addListener(function (mql) { return zone.run(function () { return _this.mediaMatcher = mql; }); });
        this.ServerUrl = settings.ServerUrl;
    }
    AdminLayoutComponent.prototype.getItems = function (url) {
        var _this = this;
        var body = new FormData();
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { _this.DataItem = data; }).toPromise();
    };
    AdminLayoutComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.isOver()) {
            this._mode = 'over';
            this.isOpened = false;
        }
        this._router = this.router.events.filter(function (event) { return event instanceof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* NavigationEnd */]; }).subscribe(function (event) {
            // Scroll to top on view load
            document.querySelector('.main-content').scrollTop = 0;
            _this.runOnRouteChange();
        });
    };
    AdminLayoutComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.getItems('getAppSettings');
        setTimeout(function (_) { return _this.runOnRouteChange(); });
    };
    AdminLayoutComponent.prototype.ngOnDestroy = function () {
        this._router.unsubscribe();
    };
    AdminLayoutComponent.prototype.runOnRouteChange = function () {
        var _this = this;
        if (this.isOver() || this.router.url === '/maps/fullscreen') {
            this.isOpened = false;
        }
        this.route.children.forEach(function (route) {
            var activeRoute = route;
            while (activeRoute.firstChild) {
                activeRoute = activeRoute.firstChild;
            }
            _this.options = activeRoute.snapshot.data;
        });
        if (this.options) {
            if (this.options.hasOwnProperty('heading')) {
                this.setTitle(this.options.heading);
            }
        }
    };
    AdminLayoutComponent.prototype.setTitle = function (newTitle) {
        this.titleService.setTitle('ארנקי לימור | ' + newTitle);
    };
    AdminLayoutComponent.prototype.toogleSidebar = function () {
        if (this._mode !== 'dock') {
            this.isOpened = !this.isOpened;
        }
    };
    AdminLayoutComponent.prototype.isOver = function () {
        return window.matchMedia("(max-width: 991px)").matches;
    };
    AdminLayoutComponent.prototype.openSearch = function (search) {
        this.modalService.open(search, { windowClass: 'search', backdrop: false });
    };
    AdminLayoutComponent.prototype.addMenuItem = function () {
        this.menuItems.add({
            state: 'menu',
            name: 'MENU',
            type: 'sub',
            icon: 'basic-webpage-txt',
            children: [
                { state: 'menu', name: 'MENU' },
                { state: 'menu', name: 'MENU' }
            ]
        });
    };
    AdminLayoutComponent.prototype.logout = function () {
        localStorage.clear();
        this.router.navigate(['/authentication']);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('sidebar'),
        __metadata("design:type", Object)
    ], AdminLayoutComponent.prototype, "sidebar", void 0);
    AdminLayoutComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-layout',
            template: __webpack_require__("../../../../../src/app/layouts/admin/admin-layout.component.html"),
            styles: [__webpack_require__("../../../../../src/app/layouts/admin/admin-layout.component.scss")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_4__shared_menu_items_menu_items__["a" /* MenuItems */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__shared_menu_items_menu_items__["a" /* MenuItems */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__["c" /* TranslateService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__["c" /* TranslateService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3__ng_bootstrap_ng_bootstrap__["f" /* NgbModal */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__ng_bootstrap_ng_bootstrap__["f" /* NgbModal */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["Title"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["Title"]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_7__settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__settings_settings_service__["a" /* SettingsService */]) === "function" && _h || Object, typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_8__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8__angular_http__["b" /* Http */]) === "function" && _j || Object])
    ], AdminLayoutComponent);
    return AdminLayoutComponent;
    var _a, _b, _c, _d, _e, _f, _g, _h, _j;
}());

//# sourceMappingURL=admin-layout.component.js.map

/***/ }),

/***/ "../../../../../src/app/layouts/auth/auth-layout.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthLayoutComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AuthLayoutComponent = (function () {
    function AuthLayoutComponent() {
    }
    AuthLayoutComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-layout',
            template: '<router-outlet></router-outlet>'
        })
    ], AuthLayoutComponent);
    return AuthLayoutComponent;
}());

//# sourceMappingURL=auth-layout.component.js.map

/***/ }),

/***/ "../../../../../src/app/shared/accordion/accordion.directive.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AccordionDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AccordionDirective = (function () {
    function AccordionDirective(router) {
        var _this = this;
        this.router = router;
        this.navlinks = [];
        setTimeout(function () { return _this.checkOpenLinks(); });
    }
    AccordionDirective.prototype.closeOtherLinks = function (openLink) {
        this.navlinks.forEach(function (link) {
            if (link !== openLink) {
                link.open = false;
            }
        });
    };
    AccordionDirective.prototype.addLink = function (link) {
        this.navlinks.push(link);
    };
    AccordionDirective.prototype.removeGroup = function (link) {
        var index = this.navlinks.indexOf(link);
        if (index !== -1) {
            this.navlinks.splice(index, 1);
        }
    };
    AccordionDirective.prototype.checkOpenLinks = function () {
        var _this = this;
        this.navlinks.forEach(function (link) {
            if (link.group) {
                var routeUrl = _this.router.url;
                var currentUrl = routeUrl.split('/');
                if (currentUrl.indexOf(link.group) > 0) {
                    link.open = true;
                    _this.closeOtherLinks(link);
                }
            }
        });
    };
    AccordionDirective.prototype.ngAfterContentChecked = function () {
        var _this = this;
        this.router.events.filter(function (event) { return event instanceof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* NavigationEnd */]; }).subscribe(function (e) { return _this.checkOpenLinks(); });
    };
    AccordionDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: '[appAccordion]',
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _a || Object])
    ], AccordionDirective);
    return AccordionDirective;
    var _a;
}());

//# sourceMappingURL=accordion.directive.js.map

/***/ }),

/***/ "../../../../../src/app/shared/accordion/accordionanchor.directive.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AccordionAnchorDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__accordionlink_directive__ = __webpack_require__("../../../../../src/app/shared/accordion/accordionlink.directive.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var AccordionAnchorDirective = (function () {
    function AccordionAnchorDirective(navlink) {
        this.navlink = navlink;
    }
    AccordionAnchorDirective.prototype.onClick = function (e) {
        this.navlink.toggle();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])('click', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], AccordionAnchorDirective.prototype, "onClick", null);
    AccordionAnchorDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: '[appAccordionToggle]'
        }),
        __param(0, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_1__accordionlink_directive__["a" /* AccordionLinkDirective */])),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__accordionlink_directive__["a" /* AccordionLinkDirective */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__accordionlink_directive__["a" /* AccordionLinkDirective */]) === "function" && _a || Object])
    ], AccordionAnchorDirective);
    return AccordionAnchorDirective;
    var _a;
}());

//# sourceMappingURL=accordionanchor.directive.js.map

/***/ }),

/***/ "../../../../../src/app/shared/accordion/accordionlink.directive.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AccordionLinkDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__accordion_directive__ = __webpack_require__("../../../../../src/app/shared/accordion/accordion.directive.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var AccordionLinkDirective = (function () {
    function AccordionLinkDirective(nav) {
        this.nav = nav;
    }
    Object.defineProperty(AccordionLinkDirective.prototype, "open", {
        get: function () {
            return this._open;
        },
        set: function (value) {
            this._open = value;
            if (value) {
                this.nav.closeOtherLinks(this);
            }
        },
        enumerable: true,
        configurable: true
    });
    AccordionLinkDirective.prototype.ngOnInit = function () {
        this.nav.addLink(this);
    };
    AccordionLinkDirective.prototype.ngOnDestroy = function () {
        this.nav.removeGroup(this);
    };
    AccordionLinkDirective.prototype.toggle = function () {
        this.open = !this.open;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], AccordionLinkDirective.prototype, "group", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostBinding"])('class.open'),
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [Boolean])
    ], AccordionLinkDirective.prototype, "open", null);
    AccordionLinkDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: '[appAccordionLink]'
        }),
        __param(0, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_1__accordion_directive__["a" /* AccordionDirective */])),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__accordion_directive__["a" /* AccordionDirective */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__accordion_directive__["a" /* AccordionDirective */]) === "function" && _a || Object])
    ], AccordionLinkDirective);
    return AccordionLinkDirective;
    var _a;
}());

//# sourceMappingURL=accordionlink.directive.js.map

/***/ }),

/***/ "../../../../../src/app/shared/accordion/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__accordionanchor_directive__ = __webpack_require__("../../../../../src/app/shared/accordion/accordionanchor.directive.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__accordionanchor_directive__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__accordionlink_directive__ = __webpack_require__("../../../../../src/app/shared/accordion/accordionlink.directive.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_1__accordionlink_directive__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__accordion_directive__ = __webpack_require__("../../../../../src/app/shared/accordion/accordion.directive.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_2__accordion_directive__["a"]; });



//# sourceMappingURL=index.js.map

/***/ }),

/***/ "../../../../../src/app/shared/fullscreen/toggle-fullscreen.directive.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ToggleFullscreenDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_screenfull__ = __webpack_require__("../../../../screenfull/dist/screenfull.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_screenfull___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_screenfull__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ToggleFullscreenDirective = (function () {
    function ToggleFullscreenDirective() {
    }
    ToggleFullscreenDirective.prototype.onClick = function () {
        if (__WEBPACK_IMPORTED_MODULE_1_screenfull__["enabled"]) {
            __WEBPACK_IMPORTED_MODULE_1_screenfull__["toggle"]();
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])('click'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], ToggleFullscreenDirective.prototype, "onClick", null);
    ToggleFullscreenDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: '[appToggleFullscreen]'
        })
    ], ToggleFullscreenDirective);
    return ToggleFullscreenDirective;
}());

//# sourceMappingURL=toggle-fullscreen.directive.js.map

/***/ }),

/***/ "../../../../../src/app/shared/menu-items/menu-items.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuItems; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var MENUITEMS = [
    {
        state: '/',
        name: 'ראשי',
        type: 'link',
        icon: 'basic-accelerator'
    },
    {
        state: 'suppliers',
        child: 'index',
        name: 'ספקים',
        type: 'new',
        icon: 'basic-message-txt'
    }, {
        state: 'category',
        child: 'index',
        name: 'קטגוריות',
        type: 'new',
        icon: 'basic-message-txt'
    }, {
        state: 'subCategory',
        child: 'index',
        name: 'תת קטגוריות',
        type: 'new',
        icon: 'basic-message-txt'
    }, {
        state: 'productImages',
        child: 'index',
        name: 'תמונות',
        type: 'new',
        icon: 'basic-message-txt'
    }, {
        state: 'CampignGallery',
        child: 'index',
        name: 'קמפיינים פרסומיים',
        type: 'new',
        icon: 'basic-message-txt'
    }, {
        state: 'products',
        child: 'index',
        name: 'מוצרים',
        type: 'new',
        icon: 'basic-message-txt'
    },
    {
        state: 'orders_new',
        child: 'index',
        name: 'הזמנות',
        type: 'new',
        icon: 'basic-message-txt'
    },
    {
        state: 'users',
        child: 'index',
        name: 'משתמשים',
        type: 'new',
        icon: 'basic-message-txt'
    },
    {
        state: 'chat_messages',
        child: 'index',
        name: 'הודעות',
        type: 'new',
        icon: 'basic-message-txt'
    },
    {
        state: 'app_settings_page',
        child: 'index',
        name: 'הגדרות אפליקציה',
        type: 'new',
        icon: 'basic-message-txt'
    }
];
var MenuItems = (function () {
    function MenuItems() {
    }
    MenuItems.prototype.getAll = function () {
        return MENUITEMS;
    };
    MenuItems.prototype.add = function (menu) {
        MENUITEMS.push(menu);
    };
    MenuItems = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
    ], MenuItems);
    return MenuItems;
}());

//# sourceMappingURL=menu-items.js.map

/***/ }),

/***/ "../../../../../src/app/shared/shared.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SharedModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__menu_items_menu_items__ = __webpack_require__("../../../../../src/app/shared/menu-items/menu-items.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__accordion__ = __webpack_require__("../../../../../src/app/shared/accordion/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__fullscreen_toggle_fullscreen_directive__ = __webpack_require__("../../../../../src/app/shared/fullscreen/toggle-fullscreen.directive.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var SharedModule = (function () {
    function SharedModule() {
    }
    SharedModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [__WEBPACK_IMPORTED_MODULE_2__accordion__["a" /* AccordionAnchorDirective */], __WEBPACK_IMPORTED_MODULE_2__accordion__["c" /* AccordionLinkDirective */], __WEBPACK_IMPORTED_MODULE_2__accordion__["b" /* AccordionDirective */], __WEBPACK_IMPORTED_MODULE_3__fullscreen_toggle_fullscreen_directive__["a" /* ToggleFullscreenDirective */]],
            exports: [__WEBPACK_IMPORTED_MODULE_2__accordion__["a" /* AccordionAnchorDirective */], __WEBPACK_IMPORTED_MODULE_2__accordion__["c" /* AccordionLinkDirective */], __WEBPACK_IMPORTED_MODULE_2__accordion__["b" /* AccordionDirective */], __WEBPACK_IMPORTED_MODULE_3__fullscreen_toggle_fullscreen_directive__["a" /* ToggleFullscreenDirective */]],
            providers: [__WEBPACK_IMPORTED_MODULE_1__menu_items_menu_items__["a" /* MenuItems */]]
        })
    ], SharedModule);
    return SharedModule;
}());

//# sourceMappingURL=shared.module.js.map

/***/ }),

/***/ "../../../../../src/assets/fonts/font-awesome-4.7.0/fonts/fontawesome-webfont.eot":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "fontawesome-webfont.674f50d287a8c48dc19b.eot";

/***/ }),

/***/ "../../../../../src/assets/fonts/font-awesome-4.7.0/fonts/fontawesome-webfont.eot?v=4.7.0":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "fontawesome-webfont.674f50d287a8c48dc19b.eot";

/***/ }),

/***/ "../../../../../src/assets/fonts/font-awesome-4.7.0/fonts/fontawesome-webfont.svg?v=4.7.0":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "fontawesome-webfont.912ec66d7572ff821749.svg";

/***/ }),

/***/ "../../../../../src/assets/fonts/font-awesome-4.7.0/fonts/fontawesome-webfont.ttf?v=4.7.0":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "fontawesome-webfont.b06871f281fee6b241d6.ttf";

/***/ }),

/***/ "../../../../../src/assets/fonts/font-awesome-4.7.0/fonts/fontawesome-webfont.woff2?v=4.7.0":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "fontawesome-webfont.af7ae505a9eed503f8b8.woff2";

/***/ }),

/***/ "../../../../../src/assets/fonts/font-awesome-4.7.0/fonts/fontawesome-webfont.woff?v=4.7.0":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "fontawesome-webfont.fee66e712a8a08eef580.woff";

/***/ }),

/***/ "../../../../../src/assets/fonts/simple-line-icons/fonts/Simple-Line-Icons.eot?v=2.4.0":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "Simple-Line-Icons.f33df365d6d0255b586f.eot";

/***/ }),

/***/ "../../../../../src/assets/fonts/simple-line-icons/fonts/Simple-Line-Icons.svg?v=2.4.0":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "Simple-Line-Icons.2fe2efe63441d830b1ac.svg";

/***/ }),

/***/ "../../../../../src/assets/fonts/simple-line-icons/fonts/Simple-Line-Icons.ttf?v=2.4.0":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "Simple-Line-Icons.d2285965fe34b0546504.ttf";

/***/ }),

/***/ "../../../../../src/assets/fonts/simple-line-icons/fonts/Simple-Line-Icons.woff2?v=2.4.0":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "Simple-Line-Icons.0cb0b9c589c0624c9c78.woff2";

/***/ }),

/***/ "../../../../../src/assets/fonts/simple-line-icons/fonts/Simple-Line-Icons.woff?v=2.4.0":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "Simple-Line-Icons.78f07e2c2a535c26ef21.woff";

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });
//# sourceMappingURL=main.js.map

/***/ }),

/***/ "../../../../../src/settings/settings.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__("../../../../rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SettingsService = (function () {
    function SettingsService(http) {
        this.http = http;
        this.ServerUrl = "http://limor.tapper.org.il/laravel/public/api/";
        this.host = "http://limor.tapper.org.il/laravel/storage/app/public/";
        this.avatar = "http://limor.tapper.org.il/avatar.jpg";
    }
    ;
    SettingsService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */]) === "function" && _a || Object])
    ], SettingsService);
    return SettingsService;
    var _a;
}());

;
//# sourceMappingURL=settings.service.js.map

/***/ }),

/***/ "../../../../css-loader/index.js?{\"sourceMap\":false,\"importLoaders\":1}!../../../../postcss-loader/index.js?{\"ident\":\"postcss\"}!../../../../../src/assets/fonts/font-awesome-4.7.0/css/font-awesome.css":
/***/ (function(module, exports, __webpack_require__) {

var escape = __webpack_require__("../../../../css-loader/lib/url/escape.js");
exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*!\n *  Font Awesome 4.7.0 by @davegandy - http://fontawesome.io - @fontawesome\n *  License - http://fontawesome.io/license (Font: SIL OFL 1.1, CSS: MIT License)\n */\n/* FONT PATH\n * -------------------------- */\n@font-face {\n  font-family: 'FontAwesome';\n  src: url(" + escape(__webpack_require__("../../../../../src/assets/fonts/font-awesome-4.7.0/fonts/fontawesome-webfont.eot?v=4.7.0")) + ");\n  src: url(" + escape(__webpack_require__("../../../../../src/assets/fonts/font-awesome-4.7.0/fonts/fontawesome-webfont.eot")) + "?#iefix&v=4.7.0) format('embedded-opentype'), url(" + escape(__webpack_require__("../../../../../src/assets/fonts/font-awesome-4.7.0/fonts/fontawesome-webfont.woff2?v=4.7.0")) + ") format('woff2'), url(" + escape(__webpack_require__("../../../../../src/assets/fonts/font-awesome-4.7.0/fonts/fontawesome-webfont.woff?v=4.7.0")) + ") format('woff'), url(" + escape(__webpack_require__("../../../../../src/assets/fonts/font-awesome-4.7.0/fonts/fontawesome-webfont.ttf?v=4.7.0")) + ") format('truetype'), url(" + escape(__webpack_require__("../../../../../src/assets/fonts/font-awesome-4.7.0/fonts/fontawesome-webfont.svg?v=4.7.0")) + "#fontawesomeregular) format('svg');\n  font-weight: normal;\n  font-style: normal;\n}\n.fa {\n  display: inline-block;\n  font: normal normal normal 14px/1 FontAwesome;\n  font-size: inherit;\n  text-rendering: auto;\n  -webkit-font-smoothing: antialiased;\n  -moz-osx-font-smoothing: grayscale;\n}\n/* makes the font 33% larger relative to the icon container */\n.fa-lg {\n  font-size: 1.33333333em;\n  line-height: 0.75em;\n  vertical-align: -15%;\n}\n.fa-2x {\n  font-size: 2em;\n}\n.fa-3x {\n  font-size: 3em;\n}\n.fa-4x {\n  font-size: 4em;\n}\n.fa-5x {\n  font-size: 5em;\n}\n.fa-fw {\n  width: 1.28571429em;\n  text-align: center;\n}\n.fa-ul {\n  padding-left: 0;\n  margin-left: 2.14285714em;\n  list-style-type: none;\n}\n.fa-ul > li {\n  position: relative;\n}\n.fa-li {\n  position: absolute;\n  left: -2.14285714em;\n  width: 2.14285714em;\n  top: 0.14285714em;\n  text-align: center;\n}\n.fa-li.fa-lg {\n  left: -1.85714286em;\n}\n.fa-border {\n  padding: .2em .25em .15em;\n  border: solid 0.08em #eeeeee;\n  border-radius: .1em;\n}\n.fa-pull-left {\n  float: left;\n}\n.fa-pull-right {\n  float: right;\n}\n.fa.fa-pull-left {\n  margin-right: .3em;\n}\n.fa.fa-pull-right {\n  margin-left: .3em;\n}\n/* Deprecated as of 4.4.0 */\n.pull-right {\n  float: right;\n}\n.pull-left {\n  float: left;\n}\n.fa.pull-left {\n  margin-right: .3em;\n}\n.fa.pull-right {\n  margin-left: .3em;\n}\n.fa-spin {\n  animation: fa-spin 2s infinite linear;\n}\n.fa-pulse {\n  animation: fa-spin 1s infinite steps(8);\n}\n@keyframes fa-spin {\n  0% {\n    transform: rotate(0deg);\n  }\n  100% {\n    transform: rotate(359deg);\n  }\n}\n.fa-rotate-90 {\n  -ms-filter: \"progid:DXImageTransform.Microsoft.BasicImage(rotation=1)\";\n  transform: rotate(90deg);\n}\n.fa-rotate-180 {\n  -ms-filter: \"progid:DXImageTransform.Microsoft.BasicImage(rotation=2)\";\n  transform: rotate(180deg);\n}\n.fa-rotate-270 {\n  -ms-filter: \"progid:DXImageTransform.Microsoft.BasicImage(rotation=3)\";\n  transform: rotate(270deg);\n}\n.fa-flip-horizontal {\n  -ms-filter: \"progid:DXImageTransform.Microsoft.BasicImage(rotation=0, mirror=1)\";\n  transform: scale(-1, 1);\n}\n.fa-flip-vertical {\n  -ms-filter: \"progid:DXImageTransform.Microsoft.BasicImage(rotation=2, mirror=1)\";\n  transform: scale(1, -1);\n}\n:root .fa-rotate-90,\n:root .fa-rotate-180,\n:root .fa-rotate-270,\n:root .fa-flip-horizontal,\n:root .fa-flip-vertical {\n  -webkit-filter: none;\n          filter: none;\n}\n.fa-stack {\n  position: relative;\n  display: inline-block;\n  width: 2em;\n  height: 2em;\n  line-height: 2em;\n  vertical-align: middle;\n}\n.fa-stack-1x,\n.fa-stack-2x {\n  position: absolute;\n  left: 0;\n  width: 100%;\n  text-align: center;\n}\n.fa-stack-1x {\n  line-height: inherit;\n}\n.fa-stack-2x {\n  font-size: 2em;\n}\n.fa-inverse {\n  color: #ffffff;\n}\n/* Font Awesome uses the Unicode Private Use Area (PUA) to ensure screen\n   readers do not read off random characters that represent icons */\n.fa-glass:before {\n  content: \"\\F000\";\n}\n.fa-music:before {\n  content: \"\\F001\";\n}\n.fa-search:before {\n  content: \"\\F002\";\n}\n.fa-envelope-o:before {\n  content: \"\\F003\";\n}\n.fa-heart:before {\n  content: \"\\F004\";\n}\n.fa-star:before {\n  content: \"\\F005\";\n}\n.fa-star-o:before {\n  content: \"\\F006\";\n}\n.fa-user:before {\n  content: \"\\F007\";\n}\n.fa-film:before {\n  content: \"\\F008\";\n}\n.fa-th-large:before {\n  content: \"\\F009\";\n}\n.fa-th:before {\n  content: \"\\F00A\";\n}\n.fa-th-list:before {\n  content: \"\\F00B\";\n}\n.fa-check:before {\n  content: \"\\F00C\";\n}\n.fa-remove:before,\n.fa-close:before,\n.fa-times:before {\n  content: \"\\F00D\";\n}\n.fa-search-plus:before {\n  content: \"\\F00E\";\n}\n.fa-search-minus:before {\n  content: \"\\F010\";\n}\n.fa-power-off:before {\n  content: \"\\F011\";\n}\n.fa-signal:before {\n  content: \"\\F012\";\n}\n.fa-gear:before,\n.fa-cog:before {\n  content: \"\\F013\";\n}\n.fa-trash-o:before {\n  content: \"\\F014\";\n}\n.fa-home:before {\n  content: \"\\F015\";\n}\n.fa-file-o:before {\n  content: \"\\F016\";\n}\n.fa-clock-o:before {\n  content: \"\\F017\";\n}\n.fa-road:before {\n  content: \"\\F018\";\n}\n.fa-download:before {\n  content: \"\\F019\";\n}\n.fa-arrow-circle-o-down:before {\n  content: \"\\F01A\";\n}\n.fa-arrow-circle-o-up:before {\n  content: \"\\F01B\";\n}\n.fa-inbox:before {\n  content: \"\\F01C\";\n}\n.fa-play-circle-o:before {\n  content: \"\\F01D\";\n}\n.fa-rotate-right:before,\n.fa-repeat:before {\n  content: \"\\F01E\";\n}\n.fa-refresh:before {\n  content: \"\\F021\";\n}\n.fa-list-alt:before {\n  content: \"\\F022\";\n}\n.fa-lock:before {\n  content: \"\\F023\";\n}\n.fa-flag:before {\n  content: \"\\F024\";\n}\n.fa-headphones:before {\n  content: \"\\F025\";\n}\n.fa-volume-off:before {\n  content: \"\\F026\";\n}\n.fa-volume-down:before {\n  content: \"\\F027\";\n}\n.fa-volume-up:before {\n  content: \"\\F028\";\n}\n.fa-qrcode:before {\n  content: \"\\F029\";\n}\n.fa-barcode:before {\n  content: \"\\F02A\";\n}\n.fa-tag:before {\n  content: \"\\F02B\";\n}\n.fa-tags:before {\n  content: \"\\F02C\";\n}\n.fa-book:before {\n  content: \"\\F02D\";\n}\n.fa-bookmark:before {\n  content: \"\\F02E\";\n}\n.fa-print:before {\n  content: \"\\F02F\";\n}\n.fa-camera:before {\n  content: \"\\F030\";\n}\n.fa-font:before {\n  content: \"\\F031\";\n}\n.fa-bold:before {\n  content: \"\\F032\";\n}\n.fa-italic:before {\n  content: \"\\F033\";\n}\n.fa-text-height:before {\n  content: \"\\F034\";\n}\n.fa-text-width:before {\n  content: \"\\F035\";\n}\n.fa-align-left:before {\n  content: \"\\F036\";\n}\n.fa-align-center:before {\n  content: \"\\F037\";\n}\n.fa-align-right:before {\n  content: \"\\F038\";\n}\n.fa-align-justify:before {\n  content: \"\\F039\";\n}\n.fa-list:before {\n  content: \"\\F03A\";\n}\n.fa-dedent:before,\n.fa-outdent:before {\n  content: \"\\F03B\";\n}\n.fa-indent:before {\n  content: \"\\F03C\";\n}\n.fa-video-camera:before {\n  content: \"\\F03D\";\n}\n.fa-photo:before,\n.fa-image:before,\n.fa-picture-o:before {\n  content: \"\\F03E\";\n}\n.fa-pencil:before {\n  content: \"\\F040\";\n}\n.fa-map-marker:before {\n  content: \"\\F041\";\n}\n.fa-adjust:before {\n  content: \"\\F042\";\n}\n.fa-tint:before {\n  content: \"\\F043\";\n}\n.fa-edit:before,\n.fa-pencil-square-o:before {\n  content: \"\\F044\";\n}\n.fa-share-square-o:before {\n  content: \"\\F045\";\n}\n.fa-check-square-o:before {\n  content: \"\\F046\";\n}\n.fa-arrows:before {\n  content: \"\\F047\";\n}\n.fa-step-backward:before {\n  content: \"\\F048\";\n}\n.fa-fast-backward:before {\n  content: \"\\F049\";\n}\n.fa-backward:before {\n  content: \"\\F04A\";\n}\n.fa-play:before {\n  content: \"\\F04B\";\n}\n.fa-pause:before {\n  content: \"\\F04C\";\n}\n.fa-stop:before {\n  content: \"\\F04D\";\n}\n.fa-forward:before {\n  content: \"\\F04E\";\n}\n.fa-fast-forward:before {\n  content: \"\\F050\";\n}\n.fa-step-forward:before {\n  content: \"\\F051\";\n}\n.fa-eject:before {\n  content: \"\\F052\";\n}\n.fa-chevron-left:before {\n  content: \"\\F053\";\n}\n.fa-chevron-right:before {\n  content: \"\\F054\";\n}\n.fa-plus-circle:before {\n  content: \"\\F055\";\n}\n.fa-minus-circle:before {\n  content: \"\\F056\";\n}\n.fa-times-circle:before {\n  content: \"\\F057\";\n}\n.fa-check-circle:before {\n  content: \"\\F058\";\n}\n.fa-question-circle:before {\n  content: \"\\F059\";\n}\n.fa-info-circle:before {\n  content: \"\\F05A\";\n}\n.fa-crosshairs:before {\n  content: \"\\F05B\";\n}\n.fa-times-circle-o:before {\n  content: \"\\F05C\";\n}\n.fa-check-circle-o:before {\n  content: \"\\F05D\";\n}\n.fa-ban:before {\n  content: \"\\F05E\";\n}\n.fa-arrow-left:before {\n  content: \"\\F060\";\n}\n.fa-arrow-right:before {\n  content: \"\\F061\";\n}\n.fa-arrow-up:before {\n  content: \"\\F062\";\n}\n.fa-arrow-down:before {\n  content: \"\\F063\";\n}\n.fa-mail-forward:before,\n.fa-share:before {\n  content: \"\\F064\";\n}\n.fa-expand:before {\n  content: \"\\F065\";\n}\n.fa-compress:before {\n  content: \"\\F066\";\n}\n.fa-plus:before {\n  content: \"\\F067\";\n}\n.fa-minus:before {\n  content: \"\\F068\";\n}\n.fa-asterisk:before {\n  content: \"\\F069\";\n}\n.fa-exclamation-circle:before {\n  content: \"\\F06A\";\n}\n.fa-gift:before {\n  content: \"\\F06B\";\n}\n.fa-leaf:before {\n  content: \"\\F06C\";\n}\n.fa-fire:before {\n  content: \"\\F06D\";\n}\n.fa-eye:before {\n  content: \"\\F06E\";\n}\n.fa-eye-slash:before {\n  content: \"\\F070\";\n}\n.fa-warning:before,\n.fa-exclamation-triangle:before {\n  content: \"\\F071\";\n}\n.fa-plane:before {\n  content: \"\\F072\";\n}\n.fa-calendar:before {\n  content: \"\\F073\";\n}\n.fa-random:before {\n  content: \"\\F074\";\n}\n.fa-comment:before {\n  content: \"\\F075\";\n}\n.fa-magnet:before {\n  content: \"\\F076\";\n}\n.fa-chevron-up:before {\n  content: \"\\F077\";\n}\n.fa-chevron-down:before {\n  content: \"\\F078\";\n}\n.fa-retweet:before {\n  content: \"\\F079\";\n}\n.fa-shopping-cart:before {\n  content: \"\\F07A\";\n}\n.fa-folder:before {\n  content: \"\\F07B\";\n}\n.fa-folder-open:before {\n  content: \"\\F07C\";\n}\n.fa-arrows-v:before {\n  content: \"\\F07D\";\n}\n.fa-arrows-h:before {\n  content: \"\\F07E\";\n}\n.fa-bar-chart-o:before,\n.fa-bar-chart:before {\n  content: \"\\F080\";\n}\n.fa-twitter-square:before {\n  content: \"\\F081\";\n}\n.fa-facebook-square:before {\n  content: \"\\F082\";\n}\n.fa-camera-retro:before {\n  content: \"\\F083\";\n}\n.fa-key:before {\n  content: \"\\F084\";\n}\n.fa-gears:before,\n.fa-cogs:before {\n  content: \"\\F085\";\n}\n.fa-comments:before {\n  content: \"\\F086\";\n}\n.fa-thumbs-o-up:before {\n  content: \"\\F087\";\n}\n.fa-thumbs-o-down:before {\n  content: \"\\F088\";\n}\n.fa-star-half:before {\n  content: \"\\F089\";\n}\n.fa-heart-o:before {\n  content: \"\\F08A\";\n}\n.fa-sign-out:before {\n  content: \"\\F08B\";\n}\n.fa-linkedin-square:before {\n  content: \"\\F08C\";\n}\n.fa-thumb-tack:before {\n  content: \"\\F08D\";\n}\n.fa-external-link:before {\n  content: \"\\F08E\";\n}\n.fa-sign-in:before {\n  content: \"\\F090\";\n}\n.fa-trophy:before {\n  content: \"\\F091\";\n}\n.fa-github-square:before {\n  content: \"\\F092\";\n}\n.fa-upload:before {\n  content: \"\\F093\";\n}\n.fa-lemon-o:before {\n  content: \"\\F094\";\n}\n.fa-phone:before {\n  content: \"\\F095\";\n}\n.fa-square-o:before {\n  content: \"\\F096\";\n}\n.fa-bookmark-o:before {\n  content: \"\\F097\";\n}\n.fa-phone-square:before {\n  content: \"\\F098\";\n}\n.fa-twitter:before {\n  content: \"\\F099\";\n}\n.fa-facebook-f:before,\n.fa-facebook:before {\n  content: \"\\F09A\";\n}\n.fa-github:before {\n  content: \"\\F09B\";\n}\n.fa-unlock:before {\n  content: \"\\F09C\";\n}\n.fa-credit-card:before {\n  content: \"\\F09D\";\n}\n.fa-feed:before,\n.fa-rss:before {\n  content: \"\\F09E\";\n}\n.fa-hdd-o:before {\n  content: \"\\F0A0\";\n}\n.fa-bullhorn:before {\n  content: \"\\F0A1\";\n}\n.fa-bell:before {\n  content: \"\\F0F3\";\n}\n.fa-certificate:before {\n  content: \"\\F0A3\";\n}\n.fa-hand-o-right:before {\n  content: \"\\F0A4\";\n}\n.fa-hand-o-left:before {\n  content: \"\\F0A5\";\n}\n.fa-hand-o-up:before {\n  content: \"\\F0A6\";\n}\n.fa-hand-o-down:before {\n  content: \"\\F0A7\";\n}\n.fa-arrow-circle-left:before {\n  content: \"\\F0A8\";\n}\n.fa-arrow-circle-right:before {\n  content: \"\\F0A9\";\n}\n.fa-arrow-circle-up:before {\n  content: \"\\F0AA\";\n}\n.fa-arrow-circle-down:before {\n  content: \"\\F0AB\";\n}\n.fa-globe:before {\n  content: \"\\F0AC\";\n}\n.fa-wrench:before {\n  content: \"\\F0AD\";\n}\n.fa-tasks:before {\n  content: \"\\F0AE\";\n}\n.fa-filter:before {\n  content: \"\\F0B0\";\n}\n.fa-briefcase:before {\n  content: \"\\F0B1\";\n}\n.fa-arrows-alt:before {\n  content: \"\\F0B2\";\n}\n.fa-group:before,\n.fa-users:before {\n  content: \"\\F0C0\";\n}\n.fa-chain:before,\n.fa-link:before {\n  content: \"\\F0C1\";\n}\n.fa-cloud:before {\n  content: \"\\F0C2\";\n}\n.fa-flask:before {\n  content: \"\\F0C3\";\n}\n.fa-cut:before,\n.fa-scissors:before {\n  content: \"\\F0C4\";\n}\n.fa-copy:before,\n.fa-files-o:before {\n  content: \"\\F0C5\";\n}\n.fa-paperclip:before {\n  content: \"\\F0C6\";\n}\n.fa-save:before,\n.fa-floppy-o:before {\n  content: \"\\F0C7\";\n}\n.fa-square:before {\n  content: \"\\F0C8\";\n}\n.fa-navicon:before,\n.fa-reorder:before,\n.fa-bars:before {\n  content: \"\\F0C9\";\n}\n.fa-list-ul:before {\n  content: \"\\F0CA\";\n}\n.fa-list-ol:before {\n  content: \"\\F0CB\";\n}\n.fa-strikethrough:before {\n  content: \"\\F0CC\";\n}\n.fa-underline:before {\n  content: \"\\F0CD\";\n}\n.fa-table:before {\n  content: \"\\F0CE\";\n}\n.fa-magic:before {\n  content: \"\\F0D0\";\n}\n.fa-truck:before {\n  content: \"\\F0D1\";\n}\n.fa-pinterest:before {\n  content: \"\\F0D2\";\n}\n.fa-pinterest-square:before {\n  content: \"\\F0D3\";\n}\n.fa-google-plus-square:before {\n  content: \"\\F0D4\";\n}\n.fa-google-plus:before {\n  content: \"\\F0D5\";\n}\n.fa-money:before {\n  content: \"\\F0D6\";\n}\n.fa-caret-down:before {\n  content: \"\\F0D7\";\n}\n.fa-caret-up:before {\n  content: \"\\F0D8\";\n}\n.fa-caret-left:before {\n  content: \"\\F0D9\";\n}\n.fa-caret-right:before {\n  content: \"\\F0DA\";\n}\n.fa-columns:before {\n  content: \"\\F0DB\";\n}\n.fa-unsorted:before,\n.fa-sort:before {\n  content: \"\\F0DC\";\n}\n.fa-sort-down:before,\n.fa-sort-desc:before {\n  content: \"\\F0DD\";\n}\n.fa-sort-up:before,\n.fa-sort-asc:before {\n  content: \"\\F0DE\";\n}\n.fa-envelope:before {\n  content: \"\\F0E0\";\n}\n.fa-linkedin:before {\n  content: \"\\F0E1\";\n}\n.fa-rotate-left:before,\n.fa-undo:before {\n  content: \"\\F0E2\";\n}\n.fa-legal:before,\n.fa-gavel:before {\n  content: \"\\F0E3\";\n}\n.fa-dashboard:before,\n.fa-tachometer:before {\n  content: \"\\F0E4\";\n}\n.fa-comment-o:before {\n  content: \"\\F0E5\";\n}\n.fa-comments-o:before {\n  content: \"\\F0E6\";\n}\n.fa-flash:before,\n.fa-bolt:before {\n  content: \"\\F0E7\";\n}\n.fa-sitemap:before {\n  content: \"\\F0E8\";\n}\n.fa-umbrella:before {\n  content: \"\\F0E9\";\n}\n.fa-paste:before,\n.fa-clipboard:before {\n  content: \"\\F0EA\";\n}\n.fa-lightbulb-o:before {\n  content: \"\\F0EB\";\n}\n.fa-exchange:before {\n  content: \"\\F0EC\";\n}\n.fa-cloud-download:before {\n  content: \"\\F0ED\";\n}\n.fa-cloud-upload:before {\n  content: \"\\F0EE\";\n}\n.fa-user-md:before {\n  content: \"\\F0F0\";\n}\n.fa-stethoscope:before {\n  content: \"\\F0F1\";\n}\n.fa-suitcase:before {\n  content: \"\\F0F2\";\n}\n.fa-bell-o:before {\n  content: \"\\F0A2\";\n}\n.fa-coffee:before {\n  content: \"\\F0F4\";\n}\n.fa-cutlery:before {\n  content: \"\\F0F5\";\n}\n.fa-file-text-o:before {\n  content: \"\\F0F6\";\n}\n.fa-building-o:before {\n  content: \"\\F0F7\";\n}\n.fa-hospital-o:before {\n  content: \"\\F0F8\";\n}\n.fa-ambulance:before {\n  content: \"\\F0F9\";\n}\n.fa-medkit:before {\n  content: \"\\F0FA\";\n}\n.fa-fighter-jet:before {\n  content: \"\\F0FB\";\n}\n.fa-beer:before {\n  content: \"\\F0FC\";\n}\n.fa-h-square:before {\n  content: \"\\F0FD\";\n}\n.fa-plus-square:before {\n  content: \"\\F0FE\";\n}\n.fa-angle-double-left:before {\n  content: \"\\F100\";\n}\n.fa-angle-double-right:before {\n  content: \"\\F101\";\n}\n.fa-angle-double-up:before {\n  content: \"\\F102\";\n}\n.fa-angle-double-down:before {\n  content: \"\\F103\";\n}\n.fa-angle-left:before {\n  content: \"\\F104\";\n}\n.fa-angle-right:before {\n  content: \"\\F105\";\n}\n.fa-angle-up:before {\n  content: \"\\F106\";\n}\n.fa-angle-down:before {\n  content: \"\\F107\";\n}\n.fa-desktop:before {\n  content: \"\\F108\";\n}\n.fa-laptop:before {\n  content: \"\\F109\";\n}\n.fa-tablet:before {\n  content: \"\\F10A\";\n}\n.fa-mobile-phone:before,\n.fa-mobile:before {\n  content: \"\\F10B\";\n}\n.fa-circle-o:before {\n  content: \"\\F10C\";\n}\n.fa-quote-left:before {\n  content: \"\\F10D\";\n}\n.fa-quote-right:before {\n  content: \"\\F10E\";\n}\n.fa-spinner:before {\n  content: \"\\F110\";\n}\n.fa-circle:before {\n  content: \"\\F111\";\n}\n.fa-mail-reply:before,\n.fa-reply:before {\n  content: \"\\F112\";\n}\n.fa-github-alt:before {\n  content: \"\\F113\";\n}\n.fa-folder-o:before {\n  content: \"\\F114\";\n}\n.fa-folder-open-o:before {\n  content: \"\\F115\";\n}\n.fa-smile-o:before {\n  content: \"\\F118\";\n}\n.fa-frown-o:before {\n  content: \"\\F119\";\n}\n.fa-meh-o:before {\n  content: \"\\F11A\";\n}\n.fa-gamepad:before {\n  content: \"\\F11B\";\n}\n.fa-keyboard-o:before {\n  content: \"\\F11C\";\n}\n.fa-flag-o:before {\n  content: \"\\F11D\";\n}\n.fa-flag-checkered:before {\n  content: \"\\F11E\";\n}\n.fa-terminal:before {\n  content: \"\\F120\";\n}\n.fa-code:before {\n  content: \"\\F121\";\n}\n.fa-mail-reply-all:before,\n.fa-reply-all:before {\n  content: \"\\F122\";\n}\n.fa-star-half-empty:before,\n.fa-star-half-full:before,\n.fa-star-half-o:before {\n  content: \"\\F123\";\n}\n.fa-location-arrow:before {\n  content: \"\\F124\";\n}\n.fa-crop:before {\n  content: \"\\F125\";\n}\n.fa-code-fork:before {\n  content: \"\\F126\";\n}\n.fa-unlink:before,\n.fa-chain-broken:before {\n  content: \"\\F127\";\n}\n.fa-question:before {\n  content: \"\\F128\";\n}\n.fa-info:before {\n  content: \"\\F129\";\n}\n.fa-exclamation:before {\n  content: \"\\F12A\";\n}\n.fa-superscript:before {\n  content: \"\\F12B\";\n}\n.fa-subscript:before {\n  content: \"\\F12C\";\n}\n.fa-eraser:before {\n  content: \"\\F12D\";\n}\n.fa-puzzle-piece:before {\n  content: \"\\F12E\";\n}\n.fa-microphone:before {\n  content: \"\\F130\";\n}\n.fa-microphone-slash:before {\n  content: \"\\F131\";\n}\n.fa-shield:before {\n  content: \"\\F132\";\n}\n.fa-calendar-o:before {\n  content: \"\\F133\";\n}\n.fa-fire-extinguisher:before {\n  content: \"\\F134\";\n}\n.fa-rocket:before {\n  content: \"\\F135\";\n}\n.fa-maxcdn:before {\n  content: \"\\F136\";\n}\n.fa-chevron-circle-left:before {\n  content: \"\\F137\";\n}\n.fa-chevron-circle-right:before {\n  content: \"\\F138\";\n}\n.fa-chevron-circle-up:before {\n  content: \"\\F139\";\n}\n.fa-chevron-circle-down:before {\n  content: \"\\F13A\";\n}\n.fa-html5:before {\n  content: \"\\F13B\";\n}\n.fa-css3:before {\n  content: \"\\F13C\";\n}\n.fa-anchor:before {\n  content: \"\\F13D\";\n}\n.fa-unlock-alt:before {\n  content: \"\\F13E\";\n}\n.fa-bullseye:before {\n  content: \"\\F140\";\n}\n.fa-ellipsis-h:before {\n  content: \"\\F141\";\n}\n.fa-ellipsis-v:before {\n  content: \"\\F142\";\n}\n.fa-rss-square:before {\n  content: \"\\F143\";\n}\n.fa-play-circle:before {\n  content: \"\\F144\";\n}\n.fa-ticket:before {\n  content: \"\\F145\";\n}\n.fa-minus-square:before {\n  content: \"\\F146\";\n}\n.fa-minus-square-o:before {\n  content: \"\\F147\";\n}\n.fa-level-up:before {\n  content: \"\\F148\";\n}\n.fa-level-down:before {\n  content: \"\\F149\";\n}\n.fa-check-square:before {\n  content: \"\\F14A\";\n}\n.fa-pencil-square:before {\n  content: \"\\F14B\";\n}\n.fa-external-link-square:before {\n  content: \"\\F14C\";\n}\n.fa-share-square:before {\n  content: \"\\F14D\";\n}\n.fa-compass:before {\n  content: \"\\F14E\";\n}\n.fa-toggle-down:before,\n.fa-caret-square-o-down:before {\n  content: \"\\F150\";\n}\n.fa-toggle-up:before,\n.fa-caret-square-o-up:before {\n  content: \"\\F151\";\n}\n.fa-toggle-right:before,\n.fa-caret-square-o-right:before {\n  content: \"\\F152\";\n}\n.fa-euro:before,\n.fa-eur:before {\n  content: \"\\F153\";\n}\n.fa-gbp:before {\n  content: \"\\F154\";\n}\n.fa-dollar:before,\n.fa-usd:before {\n  content: \"\\F155\";\n}\n.fa-rupee:before,\n.fa-inr:before {\n  content: \"\\F156\";\n}\n.fa-cny:before,\n.fa-rmb:before,\n.fa-yen:before,\n.fa-jpy:before {\n  content: \"\\F157\";\n}\n.fa-ruble:before,\n.fa-rouble:before,\n.fa-rub:before {\n  content: \"\\F158\";\n}\n.fa-won:before,\n.fa-krw:before {\n  content: \"\\F159\";\n}\n.fa-bitcoin:before,\n.fa-btc:before {\n  content: \"\\F15A\";\n}\n.fa-file:before {\n  content: \"\\F15B\";\n}\n.fa-file-text:before {\n  content: \"\\F15C\";\n}\n.fa-sort-alpha-asc:before {\n  content: \"\\F15D\";\n}\n.fa-sort-alpha-desc:before {\n  content: \"\\F15E\";\n}\n.fa-sort-amount-asc:before {\n  content: \"\\F160\";\n}\n.fa-sort-amount-desc:before {\n  content: \"\\F161\";\n}\n.fa-sort-numeric-asc:before {\n  content: \"\\F162\";\n}\n.fa-sort-numeric-desc:before {\n  content: \"\\F163\";\n}\n.fa-thumbs-up:before {\n  content: \"\\F164\";\n}\n.fa-thumbs-down:before {\n  content: \"\\F165\";\n}\n.fa-youtube-square:before {\n  content: \"\\F166\";\n}\n.fa-youtube:before {\n  content: \"\\F167\";\n}\n.fa-xing:before {\n  content: \"\\F168\";\n}\n.fa-xing-square:before {\n  content: \"\\F169\";\n}\n.fa-youtube-play:before {\n  content: \"\\F16A\";\n}\n.fa-dropbox:before {\n  content: \"\\F16B\";\n}\n.fa-stack-overflow:before {\n  content: \"\\F16C\";\n}\n.fa-instagram:before {\n  content: \"\\F16D\";\n}\n.fa-flickr:before {\n  content: \"\\F16E\";\n}\n.fa-adn:before {\n  content: \"\\F170\";\n}\n.fa-bitbucket:before {\n  content: \"\\F171\";\n}\n.fa-bitbucket-square:before {\n  content: \"\\F172\";\n}\n.fa-tumblr:before {\n  content: \"\\F173\";\n}\n.fa-tumblr-square:before {\n  content: \"\\F174\";\n}\n.fa-long-arrow-down:before {\n  content: \"\\F175\";\n}\n.fa-long-arrow-up:before {\n  content: \"\\F176\";\n}\n.fa-long-arrow-left:before {\n  content: \"\\F177\";\n}\n.fa-long-arrow-right:before {\n  content: \"\\F178\";\n}\n.fa-apple:before {\n  content: \"\\F179\";\n}\n.fa-windows:before {\n  content: \"\\F17A\";\n}\n.fa-android:before {\n  content: \"\\F17B\";\n}\n.fa-linux:before {\n  content: \"\\F17C\";\n}\n.fa-dribbble:before {\n  content: \"\\F17D\";\n}\n.fa-skype:before {\n  content: \"\\F17E\";\n}\n.fa-foursquare:before {\n  content: \"\\F180\";\n}\n.fa-trello:before {\n  content: \"\\F181\";\n}\n.fa-female:before {\n  content: \"\\F182\";\n}\n.fa-male:before {\n  content: \"\\F183\";\n}\n.fa-gittip:before,\n.fa-gratipay:before {\n  content: \"\\F184\";\n}\n.fa-sun-o:before {\n  content: \"\\F185\";\n}\n.fa-moon-o:before {\n  content: \"\\F186\";\n}\n.fa-archive:before {\n  content: \"\\F187\";\n}\n.fa-bug:before {\n  content: \"\\F188\";\n}\n.fa-vk:before {\n  content: \"\\F189\";\n}\n.fa-weibo:before {\n  content: \"\\F18A\";\n}\n.fa-renren:before {\n  content: \"\\F18B\";\n}\n.fa-pagelines:before {\n  content: \"\\F18C\";\n}\n.fa-stack-exchange:before {\n  content: \"\\F18D\";\n}\n.fa-arrow-circle-o-right:before {\n  content: \"\\F18E\";\n}\n.fa-arrow-circle-o-left:before {\n  content: \"\\F190\";\n}\n.fa-toggle-left:before,\n.fa-caret-square-o-left:before {\n  content: \"\\F191\";\n}\n.fa-dot-circle-o:before {\n  content: \"\\F192\";\n}\n.fa-wheelchair:before {\n  content: \"\\F193\";\n}\n.fa-vimeo-square:before {\n  content: \"\\F194\";\n}\n.fa-turkish-lira:before,\n.fa-try:before {\n  content: \"\\F195\";\n}\n.fa-plus-square-o:before {\n  content: \"\\F196\";\n}\n.fa-space-shuttle:before {\n  content: \"\\F197\";\n}\n.fa-slack:before {\n  content: \"\\F198\";\n}\n.fa-envelope-square:before {\n  content: \"\\F199\";\n}\n.fa-wordpress:before {\n  content: \"\\F19A\";\n}\n.fa-openid:before {\n  content: \"\\F19B\";\n}\n.fa-institution:before,\n.fa-bank:before,\n.fa-university:before {\n  content: \"\\F19C\";\n}\n.fa-mortar-board:before,\n.fa-graduation-cap:before {\n  content: \"\\F19D\";\n}\n.fa-yahoo:before {\n  content: \"\\F19E\";\n}\n.fa-google:before {\n  content: \"\\F1A0\";\n}\n.fa-reddit:before {\n  content: \"\\F1A1\";\n}\n.fa-reddit-square:before {\n  content: \"\\F1A2\";\n}\n.fa-stumbleupon-circle:before {\n  content: \"\\F1A3\";\n}\n.fa-stumbleupon:before {\n  content: \"\\F1A4\";\n}\n.fa-delicious:before {\n  content: \"\\F1A5\";\n}\n.fa-digg:before {\n  content: \"\\F1A6\";\n}\n.fa-pied-piper-pp:before {\n  content: \"\\F1A7\";\n}\n.fa-pied-piper-alt:before {\n  content: \"\\F1A8\";\n}\n.fa-drupal:before {\n  content: \"\\F1A9\";\n}\n.fa-joomla:before {\n  content: \"\\F1AA\";\n}\n.fa-language:before {\n  content: \"\\F1AB\";\n}\n.fa-fax:before {\n  content: \"\\F1AC\";\n}\n.fa-building:before {\n  content: \"\\F1AD\";\n}\n.fa-child:before {\n  content: \"\\F1AE\";\n}\n.fa-paw:before {\n  content: \"\\F1B0\";\n}\n.fa-spoon:before {\n  content: \"\\F1B1\";\n}\n.fa-cube:before {\n  content: \"\\F1B2\";\n}\n.fa-cubes:before {\n  content: \"\\F1B3\";\n}\n.fa-behance:before {\n  content: \"\\F1B4\";\n}\n.fa-behance-square:before {\n  content: \"\\F1B5\";\n}\n.fa-steam:before {\n  content: \"\\F1B6\";\n}\n.fa-steam-square:before {\n  content: \"\\F1B7\";\n}\n.fa-recycle:before {\n  content: \"\\F1B8\";\n}\n.fa-automobile:before,\n.fa-car:before {\n  content: \"\\F1B9\";\n}\n.fa-cab:before,\n.fa-taxi:before {\n  content: \"\\F1BA\";\n}\n.fa-tree:before {\n  content: \"\\F1BB\";\n}\n.fa-spotify:before {\n  content: \"\\F1BC\";\n}\n.fa-deviantart:before {\n  content: \"\\F1BD\";\n}\n.fa-soundcloud:before {\n  content: \"\\F1BE\";\n}\n.fa-database:before {\n  content: \"\\F1C0\";\n}\n.fa-file-pdf-o:before {\n  content: \"\\F1C1\";\n}\n.fa-file-word-o:before {\n  content: \"\\F1C2\";\n}\n.fa-file-excel-o:before {\n  content: \"\\F1C3\";\n}\n.fa-file-powerpoint-o:before {\n  content: \"\\F1C4\";\n}\n.fa-file-photo-o:before,\n.fa-file-picture-o:before,\n.fa-file-image-o:before {\n  content: \"\\F1C5\";\n}\n.fa-file-zip-o:before,\n.fa-file-archive-o:before {\n  content: \"\\F1C6\";\n}\n.fa-file-sound-o:before,\n.fa-file-audio-o:before {\n  content: \"\\F1C7\";\n}\n.fa-file-movie-o:before,\n.fa-file-video-o:before {\n  content: \"\\F1C8\";\n}\n.fa-file-code-o:before {\n  content: \"\\F1C9\";\n}\n.fa-vine:before {\n  content: \"\\F1CA\";\n}\n.fa-codepen:before {\n  content: \"\\F1CB\";\n}\n.fa-jsfiddle:before {\n  content: \"\\F1CC\";\n}\n.fa-life-bouy:before,\n.fa-life-buoy:before,\n.fa-life-saver:before,\n.fa-support:before,\n.fa-life-ring:before {\n  content: \"\\F1CD\";\n}\n.fa-circle-o-notch:before {\n  content: \"\\F1CE\";\n}\n.fa-ra:before,\n.fa-resistance:before,\n.fa-rebel:before {\n  content: \"\\F1D0\";\n}\n.fa-ge:before,\n.fa-empire:before {\n  content: \"\\F1D1\";\n}\n.fa-git-square:before {\n  content: \"\\F1D2\";\n}\n.fa-git:before {\n  content: \"\\F1D3\";\n}\n.fa-y-combinator-square:before,\n.fa-yc-square:before,\n.fa-hacker-news:before {\n  content: \"\\F1D4\";\n}\n.fa-tencent-weibo:before {\n  content: \"\\F1D5\";\n}\n.fa-qq:before {\n  content: \"\\F1D6\";\n}\n.fa-wechat:before,\n.fa-weixin:before {\n  content: \"\\F1D7\";\n}\n.fa-send:before,\n.fa-paper-plane:before {\n  content: \"\\F1D8\";\n}\n.fa-send-o:before,\n.fa-paper-plane-o:before {\n  content: \"\\F1D9\";\n}\n.fa-history:before {\n  content: \"\\F1DA\";\n}\n.fa-circle-thin:before {\n  content: \"\\F1DB\";\n}\n.fa-header:before {\n  content: \"\\F1DC\";\n}\n.fa-paragraph:before {\n  content: \"\\F1DD\";\n}\n.fa-sliders:before {\n  content: \"\\F1DE\";\n}\n.fa-share-alt:before {\n  content: \"\\F1E0\";\n}\n.fa-share-alt-square:before {\n  content: \"\\F1E1\";\n}\n.fa-bomb:before {\n  content: \"\\F1E2\";\n}\n.fa-soccer-ball-o:before,\n.fa-futbol-o:before {\n  content: \"\\F1E3\";\n}\n.fa-tty:before {\n  content: \"\\F1E4\";\n}\n.fa-binoculars:before {\n  content: \"\\F1E5\";\n}\n.fa-plug:before {\n  content: \"\\F1E6\";\n}\n.fa-slideshare:before {\n  content: \"\\F1E7\";\n}\n.fa-twitch:before {\n  content: \"\\F1E8\";\n}\n.fa-yelp:before {\n  content: \"\\F1E9\";\n}\n.fa-newspaper-o:before {\n  content: \"\\F1EA\";\n}\n.fa-wifi:before {\n  content: \"\\F1EB\";\n}\n.fa-calculator:before {\n  content: \"\\F1EC\";\n}\n.fa-paypal:before {\n  content: \"\\F1ED\";\n}\n.fa-google-wallet:before {\n  content: \"\\F1EE\";\n}\n.fa-cc-visa:before {\n  content: \"\\F1F0\";\n}\n.fa-cc-mastercard:before {\n  content: \"\\F1F1\";\n}\n.fa-cc-discover:before {\n  content: \"\\F1F2\";\n}\n.fa-cc-amex:before {\n  content: \"\\F1F3\";\n}\n.fa-cc-paypal:before {\n  content: \"\\F1F4\";\n}\n.fa-cc-stripe:before {\n  content: \"\\F1F5\";\n}\n.fa-bell-slash:before {\n  content: \"\\F1F6\";\n}\n.fa-bell-slash-o:before {\n  content: \"\\F1F7\";\n}\n.fa-trash:before {\n  content: \"\\F1F8\";\n}\n.fa-copyright:before {\n  content: \"\\F1F9\";\n}\n.fa-at:before {\n  content: \"\\F1FA\";\n}\n.fa-eyedropper:before {\n  content: \"\\F1FB\";\n}\n.fa-paint-brush:before {\n  content: \"\\F1FC\";\n}\n.fa-birthday-cake:before {\n  content: \"\\F1FD\";\n}\n.fa-area-chart:before {\n  content: \"\\F1FE\";\n}\n.fa-pie-chart:before {\n  content: \"\\F200\";\n}\n.fa-line-chart:before {\n  content: \"\\F201\";\n}\n.fa-lastfm:before {\n  content: \"\\F202\";\n}\n.fa-lastfm-square:before {\n  content: \"\\F203\";\n}\n.fa-toggle-off:before {\n  content: \"\\F204\";\n}\n.fa-toggle-on:before {\n  content: \"\\F205\";\n}\n.fa-bicycle:before {\n  content: \"\\F206\";\n}\n.fa-bus:before {\n  content: \"\\F207\";\n}\n.fa-ioxhost:before {\n  content: \"\\F208\";\n}\n.fa-angellist:before {\n  content: \"\\F209\";\n}\n.fa-cc:before {\n  content: \"\\F20A\";\n}\n.fa-shekel:before,\n.fa-sheqel:before,\n.fa-ils:before {\n  content: \"\\F20B\";\n}\n.fa-meanpath:before {\n  content: \"\\F20C\";\n}\n.fa-buysellads:before {\n  content: \"\\F20D\";\n}\n.fa-connectdevelop:before {\n  content: \"\\F20E\";\n}\n.fa-dashcube:before {\n  content: \"\\F210\";\n}\n.fa-forumbee:before {\n  content: \"\\F211\";\n}\n.fa-leanpub:before {\n  content: \"\\F212\";\n}\n.fa-sellsy:before {\n  content: \"\\F213\";\n}\n.fa-shirtsinbulk:before {\n  content: \"\\F214\";\n}\n.fa-simplybuilt:before {\n  content: \"\\F215\";\n}\n.fa-skyatlas:before {\n  content: \"\\F216\";\n}\n.fa-cart-plus:before {\n  content: \"\\F217\";\n}\n.fa-cart-arrow-down:before {\n  content: \"\\F218\";\n}\n.fa-diamond:before {\n  content: \"\\F219\";\n}\n.fa-ship:before {\n  content: \"\\F21A\";\n}\n.fa-user-secret:before {\n  content: \"\\F21B\";\n}\n.fa-motorcycle:before {\n  content: \"\\F21C\";\n}\n.fa-street-view:before {\n  content: \"\\F21D\";\n}\n.fa-heartbeat:before {\n  content: \"\\F21E\";\n}\n.fa-venus:before {\n  content: \"\\F221\";\n}\n.fa-mars:before {\n  content: \"\\F222\";\n}\n.fa-mercury:before {\n  content: \"\\F223\";\n}\n.fa-intersex:before,\n.fa-transgender:before {\n  content: \"\\F224\";\n}\n.fa-transgender-alt:before {\n  content: \"\\F225\";\n}\n.fa-venus-double:before {\n  content: \"\\F226\";\n}\n.fa-mars-double:before {\n  content: \"\\F227\";\n}\n.fa-venus-mars:before {\n  content: \"\\F228\";\n}\n.fa-mars-stroke:before {\n  content: \"\\F229\";\n}\n.fa-mars-stroke-v:before {\n  content: \"\\F22A\";\n}\n.fa-mars-stroke-h:before {\n  content: \"\\F22B\";\n}\n.fa-neuter:before {\n  content: \"\\F22C\";\n}\n.fa-genderless:before {\n  content: \"\\F22D\";\n}\n.fa-facebook-official:before {\n  content: \"\\F230\";\n}\n.fa-pinterest-p:before {\n  content: \"\\F231\";\n}\n.fa-whatsapp:before {\n  content: \"\\F232\";\n}\n.fa-server:before {\n  content: \"\\F233\";\n}\n.fa-user-plus:before {\n  content: \"\\F234\";\n}\n.fa-user-times:before {\n  content: \"\\F235\";\n}\n.fa-hotel:before,\n.fa-bed:before {\n  content: \"\\F236\";\n}\n.fa-viacoin:before {\n  content: \"\\F237\";\n}\n.fa-train:before {\n  content: \"\\F238\";\n}\n.fa-subway:before {\n  content: \"\\F239\";\n}\n.fa-medium:before {\n  content: \"\\F23A\";\n}\n.fa-yc:before,\n.fa-y-combinator:before {\n  content: \"\\F23B\";\n}\n.fa-optin-monster:before {\n  content: \"\\F23C\";\n}\n.fa-opencart:before {\n  content: \"\\F23D\";\n}\n.fa-expeditedssl:before {\n  content: \"\\F23E\";\n}\n.fa-battery-4:before,\n.fa-battery:before,\n.fa-battery-full:before {\n  content: \"\\F240\";\n}\n.fa-battery-3:before,\n.fa-battery-three-quarters:before {\n  content: \"\\F241\";\n}\n.fa-battery-2:before,\n.fa-battery-half:before {\n  content: \"\\F242\";\n}\n.fa-battery-1:before,\n.fa-battery-quarter:before {\n  content: \"\\F243\";\n}\n.fa-battery-0:before,\n.fa-battery-empty:before {\n  content: \"\\F244\";\n}\n.fa-mouse-pointer:before {\n  content: \"\\F245\";\n}\n.fa-i-cursor:before {\n  content: \"\\F246\";\n}\n.fa-object-group:before {\n  content: \"\\F247\";\n}\n.fa-object-ungroup:before {\n  content: \"\\F248\";\n}\n.fa-sticky-note:before {\n  content: \"\\F249\";\n}\n.fa-sticky-note-o:before {\n  content: \"\\F24A\";\n}\n.fa-cc-jcb:before {\n  content: \"\\F24B\";\n}\n.fa-cc-diners-club:before {\n  content: \"\\F24C\";\n}\n.fa-clone:before {\n  content: \"\\F24D\";\n}\n.fa-balance-scale:before {\n  content: \"\\F24E\";\n}\n.fa-hourglass-o:before {\n  content: \"\\F250\";\n}\n.fa-hourglass-1:before,\n.fa-hourglass-start:before {\n  content: \"\\F251\";\n}\n.fa-hourglass-2:before,\n.fa-hourglass-half:before {\n  content: \"\\F252\";\n}\n.fa-hourglass-3:before,\n.fa-hourglass-end:before {\n  content: \"\\F253\";\n}\n.fa-hourglass:before {\n  content: \"\\F254\";\n}\n.fa-hand-grab-o:before,\n.fa-hand-rock-o:before {\n  content: \"\\F255\";\n}\n.fa-hand-stop-o:before,\n.fa-hand-paper-o:before {\n  content: \"\\F256\";\n}\n.fa-hand-scissors-o:before {\n  content: \"\\F257\";\n}\n.fa-hand-lizard-o:before {\n  content: \"\\F258\";\n}\n.fa-hand-spock-o:before {\n  content: \"\\F259\";\n}\n.fa-hand-pointer-o:before {\n  content: \"\\F25A\";\n}\n.fa-hand-peace-o:before {\n  content: \"\\F25B\";\n}\n.fa-trademark:before {\n  content: \"\\F25C\";\n}\n.fa-registered:before {\n  content: \"\\F25D\";\n}\n.fa-creative-commons:before {\n  content: \"\\F25E\";\n}\n.fa-gg:before {\n  content: \"\\F260\";\n}\n.fa-gg-circle:before {\n  content: \"\\F261\";\n}\n.fa-tripadvisor:before {\n  content: \"\\F262\";\n}\n.fa-odnoklassniki:before {\n  content: \"\\F263\";\n}\n.fa-odnoklassniki-square:before {\n  content: \"\\F264\";\n}\n.fa-get-pocket:before {\n  content: \"\\F265\";\n}\n.fa-wikipedia-w:before {\n  content: \"\\F266\";\n}\n.fa-safari:before {\n  content: \"\\F267\";\n}\n.fa-chrome:before {\n  content: \"\\F268\";\n}\n.fa-firefox:before {\n  content: \"\\F269\";\n}\n.fa-opera:before {\n  content: \"\\F26A\";\n}\n.fa-internet-explorer:before {\n  content: \"\\F26B\";\n}\n.fa-tv:before,\n.fa-television:before {\n  content: \"\\F26C\";\n}\n.fa-contao:before {\n  content: \"\\F26D\";\n}\n.fa-500px:before {\n  content: \"\\F26E\";\n}\n.fa-amazon:before {\n  content: \"\\F270\";\n}\n.fa-calendar-plus-o:before {\n  content: \"\\F271\";\n}\n.fa-calendar-minus-o:before {\n  content: \"\\F272\";\n}\n.fa-calendar-times-o:before {\n  content: \"\\F273\";\n}\n.fa-calendar-check-o:before {\n  content: \"\\F274\";\n}\n.fa-industry:before {\n  content: \"\\F275\";\n}\n.fa-map-pin:before {\n  content: \"\\F276\";\n}\n.fa-map-signs:before {\n  content: \"\\F277\";\n}\n.fa-map-o:before {\n  content: \"\\F278\";\n}\n.fa-map:before {\n  content: \"\\F279\";\n}\n.fa-commenting:before {\n  content: \"\\F27A\";\n}\n.fa-commenting-o:before {\n  content: \"\\F27B\";\n}\n.fa-houzz:before {\n  content: \"\\F27C\";\n}\n.fa-vimeo:before {\n  content: \"\\F27D\";\n}\n.fa-black-tie:before {\n  content: \"\\F27E\";\n}\n.fa-fonticons:before {\n  content: \"\\F280\";\n}\n.fa-reddit-alien:before {\n  content: \"\\F281\";\n}\n.fa-edge:before {\n  content: \"\\F282\";\n}\n.fa-credit-card-alt:before {\n  content: \"\\F283\";\n}\n.fa-codiepie:before {\n  content: \"\\F284\";\n}\n.fa-modx:before {\n  content: \"\\F285\";\n}\n.fa-fort-awesome:before {\n  content: \"\\F286\";\n}\n.fa-usb:before {\n  content: \"\\F287\";\n}\n.fa-product-hunt:before {\n  content: \"\\F288\";\n}\n.fa-mixcloud:before {\n  content: \"\\F289\";\n}\n.fa-scribd:before {\n  content: \"\\F28A\";\n}\n.fa-pause-circle:before {\n  content: \"\\F28B\";\n}\n.fa-pause-circle-o:before {\n  content: \"\\F28C\";\n}\n.fa-stop-circle:before {\n  content: \"\\F28D\";\n}\n.fa-stop-circle-o:before {\n  content: \"\\F28E\";\n}\n.fa-shopping-bag:before {\n  content: \"\\F290\";\n}\n.fa-shopping-basket:before {\n  content: \"\\F291\";\n}\n.fa-hashtag:before {\n  content: \"\\F292\";\n}\n.fa-bluetooth:before {\n  content: \"\\F293\";\n}\n.fa-bluetooth-b:before {\n  content: \"\\F294\";\n}\n.fa-percent:before {\n  content: \"\\F295\";\n}\n.fa-gitlab:before {\n  content: \"\\F296\";\n}\n.fa-wpbeginner:before {\n  content: \"\\F297\";\n}\n.fa-wpforms:before {\n  content: \"\\F298\";\n}\n.fa-envira:before {\n  content: \"\\F299\";\n}\n.fa-universal-access:before {\n  content: \"\\F29A\";\n}\n.fa-wheelchair-alt:before {\n  content: \"\\F29B\";\n}\n.fa-question-circle-o:before {\n  content: \"\\F29C\";\n}\n.fa-blind:before {\n  content: \"\\F29D\";\n}\n.fa-audio-description:before {\n  content: \"\\F29E\";\n}\n.fa-volume-control-phone:before {\n  content: \"\\F2A0\";\n}\n.fa-braille:before {\n  content: \"\\F2A1\";\n}\n.fa-assistive-listening-systems:before {\n  content: \"\\F2A2\";\n}\n.fa-asl-interpreting:before,\n.fa-american-sign-language-interpreting:before {\n  content: \"\\F2A3\";\n}\n.fa-deafness:before,\n.fa-hard-of-hearing:before,\n.fa-deaf:before {\n  content: \"\\F2A4\";\n}\n.fa-glide:before {\n  content: \"\\F2A5\";\n}\n.fa-glide-g:before {\n  content: \"\\F2A6\";\n}\n.fa-signing:before,\n.fa-sign-language:before {\n  content: \"\\F2A7\";\n}\n.fa-low-vision:before {\n  content: \"\\F2A8\";\n}\n.fa-viadeo:before {\n  content: \"\\F2A9\";\n}\n.fa-viadeo-square:before {\n  content: \"\\F2AA\";\n}\n.fa-snapchat:before {\n  content: \"\\F2AB\";\n}\n.fa-snapchat-ghost:before {\n  content: \"\\F2AC\";\n}\n.fa-snapchat-square:before {\n  content: \"\\F2AD\";\n}\n.fa-pied-piper:before {\n  content: \"\\F2AE\";\n}\n.fa-first-order:before {\n  content: \"\\F2B0\";\n}\n.fa-yoast:before {\n  content: \"\\F2B1\";\n}\n.fa-themeisle:before {\n  content: \"\\F2B2\";\n}\n.fa-google-plus-circle:before,\n.fa-google-plus-official:before {\n  content: \"\\F2B3\";\n}\n.fa-fa:before,\n.fa-font-awesome:before {\n  content: \"\\F2B4\";\n}\n.fa-handshake-o:before {\n  content: \"\\F2B5\";\n}\n.fa-envelope-open:before {\n  content: \"\\F2B6\";\n}\n.fa-envelope-open-o:before {\n  content: \"\\F2B7\";\n}\n.fa-linode:before {\n  content: \"\\F2B8\";\n}\n.fa-address-book:before {\n  content: \"\\F2B9\";\n}\n.fa-address-book-o:before {\n  content: \"\\F2BA\";\n}\n.fa-vcard:before,\n.fa-address-card:before {\n  content: \"\\F2BB\";\n}\n.fa-vcard-o:before,\n.fa-address-card-o:before {\n  content: \"\\F2BC\";\n}\n.fa-user-circle:before {\n  content: \"\\F2BD\";\n}\n.fa-user-circle-o:before {\n  content: \"\\F2BE\";\n}\n.fa-user-o:before {\n  content: \"\\F2C0\";\n}\n.fa-id-badge:before {\n  content: \"\\F2C1\";\n}\n.fa-drivers-license:before,\n.fa-id-card:before {\n  content: \"\\F2C2\";\n}\n.fa-drivers-license-o:before,\n.fa-id-card-o:before {\n  content: \"\\F2C3\";\n}\n.fa-quora:before {\n  content: \"\\F2C4\";\n}\n.fa-free-code-camp:before {\n  content: \"\\F2C5\";\n}\n.fa-telegram:before {\n  content: \"\\F2C6\";\n}\n.fa-thermometer-4:before,\n.fa-thermometer:before,\n.fa-thermometer-full:before {\n  content: \"\\F2C7\";\n}\n.fa-thermometer-3:before,\n.fa-thermometer-three-quarters:before {\n  content: \"\\F2C8\";\n}\n.fa-thermometer-2:before,\n.fa-thermometer-half:before {\n  content: \"\\F2C9\";\n}\n.fa-thermometer-1:before,\n.fa-thermometer-quarter:before {\n  content: \"\\F2CA\";\n}\n.fa-thermometer-0:before,\n.fa-thermometer-empty:before {\n  content: \"\\F2CB\";\n}\n.fa-shower:before {\n  content: \"\\F2CC\";\n}\n.fa-bathtub:before,\n.fa-s15:before,\n.fa-bath:before {\n  content: \"\\F2CD\";\n}\n.fa-podcast:before {\n  content: \"\\F2CE\";\n}\n.fa-window-maximize:before {\n  content: \"\\F2D0\";\n}\n.fa-window-minimize:before {\n  content: \"\\F2D1\";\n}\n.fa-window-restore:before {\n  content: \"\\F2D2\";\n}\n.fa-times-rectangle:before,\n.fa-window-close:before {\n  content: \"\\F2D3\";\n}\n.fa-times-rectangle-o:before,\n.fa-window-close-o:before {\n  content: \"\\F2D4\";\n}\n.fa-bandcamp:before {\n  content: \"\\F2D5\";\n}\n.fa-grav:before {\n  content: \"\\F2D6\";\n}\n.fa-etsy:before {\n  content: \"\\F2D7\";\n}\n.fa-imdb:before {\n  content: \"\\F2D8\";\n}\n.fa-ravelry:before {\n  content: \"\\F2D9\";\n}\n.fa-eercast:before {\n  content: \"\\F2DA\";\n}\n.fa-microchip:before {\n  content: \"\\F2DB\";\n}\n.fa-snowflake-o:before {\n  content: \"\\F2DC\";\n}\n.fa-superpowers:before {\n  content: \"\\F2DD\";\n}\n.fa-wpexplorer:before {\n  content: \"\\F2DE\";\n}\n.fa-meetup:before {\n  content: \"\\F2E0\";\n}\n.sr-only {\n  position: absolute;\n  width: 1px;\n  height: 1px;\n  padding: 0;\n  margin: -1px;\n  overflow: hidden;\n  clip: rect(0, 0, 0, 0);\n  border: 0;\n}\n.sr-only-focusable:active,\n.sr-only-focusable:focus {\n  position: static;\n  width: auto;\n  height: auto;\n  margin: 0;\n  overflow: visible;\n  clip: auto;\n}\n", ""]);

// exports


/***/ }),

/***/ "../../../../css-loader/index.js?{\"sourceMap\":false,\"importLoaders\":1}!../../../../postcss-loader/index.js?{\"ident\":\"postcss\"}!../../../../../src/assets/fonts/simple-line-icons/simple-line-icons.css":
/***/ (function(module, exports, __webpack_require__) {

var escape = __webpack_require__("../../../../css-loader/lib/url/escape.js");
exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "@font-face {\n  font-family: 'simple-line-icons';\n  src: url(" + escape(__webpack_require__("../../../../../src/assets/fonts/simple-line-icons/fonts/Simple-Line-Icons.eot?v=2.4.0")) + ");\n  src: url(" + escape(__webpack_require__("../../../../../src/assets/fonts/simple-line-icons/fonts/Simple-Line-Icons.eot?v=2.4.0")) + "#iefix) format('embedded-opentype'), url(" + escape(__webpack_require__("../../../../../src/assets/fonts/simple-line-icons/fonts/Simple-Line-Icons.woff2?v=2.4.0")) + ") format('woff2'), url(" + escape(__webpack_require__("../../../../../src/assets/fonts/simple-line-icons/fonts/Simple-Line-Icons.ttf?v=2.4.0")) + ") format('truetype'), url(" + escape(__webpack_require__("../../../../../src/assets/fonts/simple-line-icons/fonts/Simple-Line-Icons.woff?v=2.4.0")) + ") format('woff'), url(" + escape(__webpack_require__("../../../../../src/assets/fonts/simple-line-icons/fonts/Simple-Line-Icons.svg?v=2.4.0")) + "#simple-line-icons) format('svg');\n  font-weight: normal;\n  font-style: normal;\n}\n/*\n Use the following CSS code if you want to have a class per icon.\n Instead of a list of all class selectors, you can use the generic [class*=\"icon-\"] selector, but it's slower:\n*/\n.icon-user,\n.icon-people,\n.icon-user-female,\n.icon-user-follow,\n.icon-user-following,\n.icon-user-unfollow,\n.icon-login,\n.icon-logout,\n.icon-emotsmile,\n.icon-phone,\n.icon-call-end,\n.icon-call-in,\n.icon-call-out,\n.icon-map,\n.icon-location-pin,\n.icon-direction,\n.icon-directions,\n.icon-compass,\n.icon-layers,\n.icon-menu,\n.icon-list,\n.icon-options-vertical,\n.icon-options,\n.icon-arrow-down,\n.icon-arrow-left,\n.icon-arrow-right,\n.icon-arrow-up,\n.icon-arrow-up-circle,\n.icon-arrow-left-circle,\n.icon-arrow-right-circle,\n.icon-arrow-down-circle,\n.icon-check,\n.icon-clock,\n.icon-plus,\n.icon-minus,\n.icon-close,\n.icon-event,\n.icon-exclamation,\n.icon-organization,\n.icon-trophy,\n.icon-screen-smartphone,\n.icon-screen-desktop,\n.icon-plane,\n.icon-notebook,\n.icon-mustache,\n.icon-mouse,\n.icon-magnet,\n.icon-energy,\n.icon-disc,\n.icon-cursor,\n.icon-cursor-move,\n.icon-crop,\n.icon-chemistry,\n.icon-speedometer,\n.icon-shield,\n.icon-screen-tablet,\n.icon-magic-wand,\n.icon-hourglass,\n.icon-graduation,\n.icon-ghost,\n.icon-game-controller,\n.icon-fire,\n.icon-eyeglass,\n.icon-envelope-open,\n.icon-envelope-letter,\n.icon-bell,\n.icon-badge,\n.icon-anchor,\n.icon-wallet,\n.icon-vector,\n.icon-speech,\n.icon-puzzle,\n.icon-printer,\n.icon-present,\n.icon-playlist,\n.icon-pin,\n.icon-picture,\n.icon-handbag,\n.icon-globe-alt,\n.icon-globe,\n.icon-folder-alt,\n.icon-folder,\n.icon-film,\n.icon-feed,\n.icon-drop,\n.icon-drawer,\n.icon-docs,\n.icon-doc,\n.icon-diamond,\n.icon-cup,\n.icon-calculator,\n.icon-bubbles,\n.icon-briefcase,\n.icon-book-open,\n.icon-basket-loaded,\n.icon-basket,\n.icon-bag,\n.icon-action-undo,\n.icon-action-redo,\n.icon-wrench,\n.icon-umbrella,\n.icon-trash,\n.icon-tag,\n.icon-support,\n.icon-frame,\n.icon-size-fullscreen,\n.icon-size-actual,\n.icon-shuffle,\n.icon-share-alt,\n.icon-share,\n.icon-rocket,\n.icon-question,\n.icon-pie-chart,\n.icon-pencil,\n.icon-note,\n.icon-loop,\n.icon-home,\n.icon-grid,\n.icon-graph,\n.icon-microphone,\n.icon-music-tone-alt,\n.icon-music-tone,\n.icon-earphones-alt,\n.icon-earphones,\n.icon-equalizer,\n.icon-like,\n.icon-dislike,\n.icon-control-start,\n.icon-control-rewind,\n.icon-control-play,\n.icon-control-pause,\n.icon-control-forward,\n.icon-control-end,\n.icon-volume-1,\n.icon-volume-2,\n.icon-volume-off,\n.icon-calendar,\n.icon-bulb,\n.icon-chart,\n.icon-ban,\n.icon-bubble,\n.icon-camrecorder,\n.icon-camera,\n.icon-cloud-download,\n.icon-cloud-upload,\n.icon-envelope,\n.icon-eye,\n.icon-flag,\n.icon-heart,\n.icon-info,\n.icon-key,\n.icon-link,\n.icon-lock,\n.icon-lock-open,\n.icon-magnifier,\n.icon-magnifier-add,\n.icon-magnifier-remove,\n.icon-paper-clip,\n.icon-paper-plane,\n.icon-power,\n.icon-refresh,\n.icon-reload,\n.icon-settings,\n.icon-star,\n.icon-symbol-female,\n.icon-symbol-male,\n.icon-target,\n.icon-credit-card,\n.icon-paypal,\n.icon-social-tumblr,\n.icon-social-twitter,\n.icon-social-facebook,\n.icon-social-instagram,\n.icon-social-linkedin,\n.icon-social-pinterest,\n.icon-social-github,\n.icon-social-google,\n.icon-social-reddit,\n.icon-social-skype,\n.icon-social-dribbble,\n.icon-social-behance,\n.icon-social-foursqare,\n.icon-social-soundcloud,\n.icon-social-spotify,\n.icon-social-stumbleupon,\n.icon-social-youtube,\n.icon-social-dropbox,\n.icon-social-vkontakte,\n.icon-social-steam {\n  font-family: 'simple-line-icons';\n  speak: none;\n  font-style: normal;\n  font-weight: normal;\n  font-variant: normal;\n  text-transform: none;\n  line-height: 1;\n  /* Better Font Rendering =========== */\n  -webkit-font-smoothing: antialiased;\n  -moz-osx-font-smoothing: grayscale;\n}\n.icon-user:before {\n  content: \"\\E005\";\n}\n.icon-people:before {\n  content: \"\\E001\";\n}\n.icon-user-female:before {\n  content: \"\\E000\";\n}\n.icon-user-follow:before {\n  content: \"\\E002\";\n}\n.icon-user-following:before {\n  content: \"\\E003\";\n}\n.icon-user-unfollow:before {\n  content: \"\\E004\";\n}\n.icon-login:before {\n  content: \"\\E066\";\n}\n.icon-logout:before {\n  content: \"\\E065\";\n}\n.icon-emotsmile:before {\n  content: \"\\E021\";\n}\n.icon-phone:before {\n  content: \"\\E600\";\n}\n.icon-call-end:before {\n  content: \"\\E048\";\n}\n.icon-call-in:before {\n  content: \"\\E047\";\n}\n.icon-call-out:before {\n  content: \"\\E046\";\n}\n.icon-map:before {\n  content: \"\\E033\";\n}\n.icon-location-pin:before {\n  content: \"\\E096\";\n}\n.icon-direction:before {\n  content: \"\\E042\";\n}\n.icon-directions:before {\n  content: \"\\E041\";\n}\n.icon-compass:before {\n  content: \"\\E045\";\n}\n.icon-layers:before {\n  content: \"\\E034\";\n}\n.icon-menu:before {\n  content: \"\\E601\";\n}\n.icon-list:before {\n  content: \"\\E067\";\n}\n.icon-options-vertical:before {\n  content: \"\\E602\";\n}\n.icon-options:before {\n  content: \"\\E603\";\n}\n.icon-arrow-down:before {\n  content: \"\\E604\";\n}\n.icon-arrow-left:before {\n  content: \"\\E605\";\n}\n.icon-arrow-right:before {\n  content: \"\\E606\";\n}\n.icon-arrow-up:before {\n  content: \"\\E607\";\n}\n.icon-arrow-up-circle:before {\n  content: \"\\E078\";\n}\n.icon-arrow-left-circle:before {\n  content: \"\\E07A\";\n}\n.icon-arrow-right-circle:before {\n  content: \"\\E079\";\n}\n.icon-arrow-down-circle:before {\n  content: \"\\E07B\";\n}\n.icon-check:before {\n  content: \"\\E080\";\n}\n.icon-clock:before {\n  content: \"\\E081\";\n}\n.icon-plus:before {\n  content: \"\\E095\";\n}\n.icon-minus:before {\n  content: \"\\E615\";\n}\n.icon-close:before {\n  content: \"\\E082\";\n}\n.icon-event:before {\n  content: \"\\E619\";\n}\n.icon-exclamation:before {\n  content: \"\\E617\";\n}\n.icon-organization:before {\n  content: \"\\E616\";\n}\n.icon-trophy:before {\n  content: \"\\E006\";\n}\n.icon-screen-smartphone:before {\n  content: \"\\E010\";\n}\n.icon-screen-desktop:before {\n  content: \"\\E011\";\n}\n.icon-plane:before {\n  content: \"\\E012\";\n}\n.icon-notebook:before {\n  content: \"\\E013\";\n}\n.icon-mustache:before {\n  content: \"\\E014\";\n}\n.icon-mouse:before {\n  content: \"\\E015\";\n}\n.icon-magnet:before {\n  content: \"\\E016\";\n}\n.icon-energy:before {\n  content: \"\\E020\";\n}\n.icon-disc:before {\n  content: \"\\E022\";\n}\n.icon-cursor:before {\n  content: \"\\E06E\";\n}\n.icon-cursor-move:before {\n  content: \"\\E023\";\n}\n.icon-crop:before {\n  content: \"\\E024\";\n}\n.icon-chemistry:before {\n  content: \"\\E026\";\n}\n.icon-speedometer:before {\n  content: \"\\E007\";\n}\n.icon-shield:before {\n  content: \"\\E00E\";\n}\n.icon-screen-tablet:before {\n  content: \"\\E00F\";\n}\n.icon-magic-wand:before {\n  content: \"\\E017\";\n}\n.icon-hourglass:before {\n  content: \"\\E018\";\n}\n.icon-graduation:before {\n  content: \"\\E019\";\n}\n.icon-ghost:before {\n  content: \"\\E01A\";\n}\n.icon-game-controller:before {\n  content: \"\\E01B\";\n}\n.icon-fire:before {\n  content: \"\\E01C\";\n}\n.icon-eyeglass:before {\n  content: \"\\E01D\";\n}\n.icon-envelope-open:before {\n  content: \"\\E01E\";\n}\n.icon-envelope-letter:before {\n  content: \"\\E01F\";\n}\n.icon-bell:before {\n  content: \"\\E027\";\n}\n.icon-badge:before {\n  content: \"\\E028\";\n}\n.icon-anchor:before {\n  content: \"\\E029\";\n}\n.icon-wallet:before {\n  content: \"\\E02A\";\n}\n.icon-vector:before {\n  content: \"\\E02B\";\n}\n.icon-speech:before {\n  content: \"\\E02C\";\n}\n.icon-puzzle:before {\n  content: \"\\E02D\";\n}\n.icon-printer:before {\n  content: \"\\E02E\";\n}\n.icon-present:before {\n  content: \"\\E02F\";\n}\n.icon-playlist:before {\n  content: \"\\E030\";\n}\n.icon-pin:before {\n  content: \"\\E031\";\n}\n.icon-picture:before {\n  content: \"\\E032\";\n}\n.icon-handbag:before {\n  content: \"\\E035\";\n}\n.icon-globe-alt:before {\n  content: \"\\E036\";\n}\n.icon-globe:before {\n  content: \"\\E037\";\n}\n.icon-folder-alt:before {\n  content: \"\\E039\";\n}\n.icon-folder:before {\n  content: \"\\E089\";\n}\n.icon-film:before {\n  content: \"\\E03A\";\n}\n.icon-feed:before {\n  content: \"\\E03B\";\n}\n.icon-drop:before {\n  content: \"\\E03E\";\n}\n.icon-drawer:before {\n  content: \"\\E03F\";\n}\n.icon-docs:before {\n  content: \"\\E040\";\n}\n.icon-doc:before {\n  content: \"\\E085\";\n}\n.icon-diamond:before {\n  content: \"\\E043\";\n}\n.icon-cup:before {\n  content: \"\\E044\";\n}\n.icon-calculator:before {\n  content: \"\\E049\";\n}\n.icon-bubbles:before {\n  content: \"\\E04A\";\n}\n.icon-briefcase:before {\n  content: \"\\E04B\";\n}\n.icon-book-open:before {\n  content: \"\\E04C\";\n}\n.icon-basket-loaded:before {\n  content: \"\\E04D\";\n}\n.icon-basket:before {\n  content: \"\\E04E\";\n}\n.icon-bag:before {\n  content: \"\\E04F\";\n}\n.icon-action-undo:before {\n  content: \"\\E050\";\n}\n.icon-action-redo:before {\n  content: \"\\E051\";\n}\n.icon-wrench:before {\n  content: \"\\E052\";\n}\n.icon-umbrella:before {\n  content: \"\\E053\";\n}\n.icon-trash:before {\n  content: \"\\E054\";\n}\n.icon-tag:before {\n  content: \"\\E055\";\n}\n.icon-support:before {\n  content: \"\\E056\";\n}\n.icon-frame:before {\n  content: \"\\E038\";\n}\n.icon-size-fullscreen:before {\n  content: \"\\E057\";\n}\n.icon-size-actual:before {\n  content: \"\\E058\";\n}\n.icon-shuffle:before {\n  content: \"\\E059\";\n}\n.icon-share-alt:before {\n  content: \"\\E05A\";\n}\n.icon-share:before {\n  content: \"\\E05B\";\n}\n.icon-rocket:before {\n  content: \"\\E05C\";\n}\n.icon-question:before {\n  content: \"\\E05D\";\n}\n.icon-pie-chart:before {\n  content: \"\\E05E\";\n}\n.icon-pencil:before {\n  content: \"\\E05F\";\n}\n.icon-note:before {\n  content: \"\\E060\";\n}\n.icon-loop:before {\n  content: \"\\E064\";\n}\n.icon-home:before {\n  content: \"\\E069\";\n}\n.icon-grid:before {\n  content: \"\\E06A\";\n}\n.icon-graph:before {\n  content: \"\\E06B\";\n}\n.icon-microphone:before {\n  content: \"\\E063\";\n}\n.icon-music-tone-alt:before {\n  content: \"\\E061\";\n}\n.icon-music-tone:before {\n  content: \"\\E062\";\n}\n.icon-earphones-alt:before {\n  content: \"\\E03C\";\n}\n.icon-earphones:before {\n  content: \"\\E03D\";\n}\n.icon-equalizer:before {\n  content: \"\\E06C\";\n}\n.icon-like:before {\n  content: \"\\E068\";\n}\n.icon-dislike:before {\n  content: \"\\E06D\";\n}\n.icon-control-start:before {\n  content: \"\\E06F\";\n}\n.icon-control-rewind:before {\n  content: \"\\E070\";\n}\n.icon-control-play:before {\n  content: \"\\E071\";\n}\n.icon-control-pause:before {\n  content: \"\\E072\";\n}\n.icon-control-forward:before {\n  content: \"\\E073\";\n}\n.icon-control-end:before {\n  content: \"\\E074\";\n}\n.icon-volume-1:before {\n  content: \"\\E09F\";\n}\n.icon-volume-2:before {\n  content: \"\\E0A0\";\n}\n.icon-volume-off:before {\n  content: \"\\E0A1\";\n}\n.icon-calendar:before {\n  content: \"\\E075\";\n}\n.icon-bulb:before {\n  content: \"\\E076\";\n}\n.icon-chart:before {\n  content: \"\\E077\";\n}\n.icon-ban:before {\n  content: \"\\E07C\";\n}\n.icon-bubble:before {\n  content: \"\\E07D\";\n}\n.icon-camrecorder:before {\n  content: \"\\E07E\";\n}\n.icon-camera:before {\n  content: \"\\E07F\";\n}\n.icon-cloud-download:before {\n  content: \"\\E083\";\n}\n.icon-cloud-upload:before {\n  content: \"\\E084\";\n}\n.icon-envelope:before {\n  content: \"\\E086\";\n}\n.icon-eye:before {\n  content: \"\\E087\";\n}\n.icon-flag:before {\n  content: \"\\E088\";\n}\n.icon-heart:before {\n  content: \"\\E08A\";\n}\n.icon-info:before {\n  content: \"\\E08B\";\n}\n.icon-key:before {\n  content: \"\\E08C\";\n}\n.icon-link:before {\n  content: \"\\E08D\";\n}\n.icon-lock:before {\n  content: \"\\E08E\";\n}\n.icon-lock-open:before {\n  content: \"\\E08F\";\n}\n.icon-magnifier:before {\n  content: \"\\E090\";\n}\n.icon-magnifier-add:before {\n  content: \"\\E091\";\n}\n.icon-magnifier-remove:before {\n  content: \"\\E092\";\n}\n.icon-paper-clip:before {\n  content: \"\\E093\";\n}\n.icon-paper-plane:before {\n  content: \"\\E094\";\n}\n.icon-power:before {\n  content: \"\\E097\";\n}\n.icon-refresh:before {\n  content: \"\\E098\";\n}\n.icon-reload:before {\n  content: \"\\E099\";\n}\n.icon-settings:before {\n  content: \"\\E09A\";\n}\n.icon-star:before {\n  content: \"\\E09B\";\n}\n.icon-symbol-female:before {\n  content: \"\\E09C\";\n}\n.icon-symbol-male:before {\n  content: \"\\E09D\";\n}\n.icon-target:before {\n  content: \"\\E09E\";\n}\n.icon-credit-card:before {\n  content: \"\\E025\";\n}\n.icon-paypal:before {\n  content: \"\\E608\";\n}\n.icon-social-tumblr:before {\n  content: \"\\E00A\";\n}\n.icon-social-twitter:before {\n  content: \"\\E009\";\n}\n.icon-social-facebook:before {\n  content: \"\\E00B\";\n}\n.icon-social-instagram:before {\n  content: \"\\E609\";\n}\n.icon-social-linkedin:before {\n  content: \"\\E60A\";\n}\n.icon-social-pinterest:before {\n  content: \"\\E60B\";\n}\n.icon-social-github:before {\n  content: \"\\E60C\";\n}\n.icon-social-google:before {\n  content: \"\\E60D\";\n}\n.icon-social-reddit:before {\n  content: \"\\E60E\";\n}\n.icon-social-skype:before {\n  content: \"\\E60F\";\n}\n.icon-social-dribbble:before {\n  content: \"\\E00D\";\n}\n.icon-social-behance:before {\n  content: \"\\E610\";\n}\n.icon-social-foursqare:before {\n  content: \"\\E611\";\n}\n.icon-social-soundcloud:before {\n  content: \"\\E612\";\n}\n.icon-social-spotify:before {\n  content: \"\\E613\";\n}\n.icon-social-stumbleupon:before {\n  content: \"\\E614\";\n}\n.icon-social-youtube:before {\n  content: \"\\E008\";\n}\n.icon-social-dropbox:before {\n  content: \"\\E00C\";\n}\n.icon-social-vkontakte:before {\n  content: \"\\E618\";\n}\n.icon-social-steam:before {\n  content: \"\\E620\";\n}\n", ""]);

// exports


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map