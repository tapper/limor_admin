webpackJsonp(["Main.module.3"],{

/***/ "../../../../../src/app/productImages/Main.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainModule", function() { return MainModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Main_routing__ = __webpack_require__("../../../../../src/app/productImages/Main.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_datatable__ = __webpack_require__("../../../../@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_datatable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_datatable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__MainService_service__ = __webpack_require__("../../../../../src/app/productImages/MainService.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__edit_edit_component__ = __webpack_require__("../../../../../src/app/productImages/edit/edit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__index_index_component__ = __webpack_require__("../../../../../src/app/productImages/index/index.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__add_add_component__ = __webpack_require__("../../../../../src/app/productImages/add/add.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ng2_file_upload__ = __webpack_require__("../../../../ng2-file-upload/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













var MainModule = (function () {
    function MainModule() {
    }
    MainModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__Main_routing__["a" /* MaindRoutes */]),
                __WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_datatable__["NgxDatatableModule"],
                __WEBPACK_IMPORTED_MODULE_6__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_10__ng_bootstrap_ng_bootstrap__["g" /* NgbModule */],
                __WEBPACK_IMPORTED_MODULE_11_ng2_file_upload__["FileUploadModule"],
                __WEBPACK_IMPORTED_MODULE_12__angular_forms__["FormsModule"]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_7__edit_edit_component__["a" /* EditComponent */],
                __WEBPACK_IMPORTED_MODULE_8__index_index_component__["a" /* IndexComponent */],
                __WEBPACK_IMPORTED_MODULE_9__add_add_component__["a" /* AddComponent */]
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_5__MainService_service__["a" /* MainService */]]
        })
    ], MainModule);
    return MainModule;
}());

//# sourceMappingURL=Main.module.js.map

/***/ }),

/***/ "../../../../../src/app/productImages/Main.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MaindRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__edit_edit_component__ = __webpack_require__("../../../../../src/app/productImages/edit/edit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__add_add_component__ = __webpack_require__("../../../../../src/app/productImages/add/add.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__index_index_component__ = __webpack_require__("../../../../../src/app/productImages/index/index.component.ts");



var MaindRoutes = [{
        path: '',
        children: [{
                path: 'index',
                component: __WEBPACK_IMPORTED_MODULE_2__index_index_component__["a" /* IndexComponent */],
                data: {
                    heading: 'תמונות מוצרים'
                }
            }, {
                path: 'index',
                component: __WEBPACK_IMPORTED_MODULE_0__edit_edit_component__["a" /* EditComponent */],
                data: {
                    heading: 'Edit Company'
                }
            }, {
                path: 'add',
                component: __WEBPACK_IMPORTED_MODULE_1__add_add_component__["a" /* AddComponent */],
                data: {
                    heading: 'Edit Company'
                }
            }]
    }];
//# sourceMappingURL=Main.routing.js.map

/***/ }),

/***/ "../../../../../src/app/productImages/MainService.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MainService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__("../../../../rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__settings_settings_service__ = __webpack_require__("../../../../../src/settings/settings.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//public this.ServerUrl = "";//http://www.tapper.co.il/salecar/laravel/public/api/";
var MainService = (function () {
    function MainService(http, settings) {
        this.http = http;
        this.ServerUrl = "";
        this.headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/x-www-form-urlencoded' });
        this.options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["f" /* RequestOptions */]({ headers: this.headers });
        this.Items = [];
        this.ServerUrl = settings.ServerUrl;
    }
    ;
    MainService.prototype.GetItems = function (url, Id) {
        var _this = this;
        var body = new FormData();
        body.append('id', Id);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { _this.Items = data; }).toPromise();
    };
    MainService.prototype.AddItem = function (url, Id, File) {
        var body = new FormData();
        body.append("file", File);
        body.append('id', Id);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { }).toPromise();
    };
    MainService.prototype.EditItem = function (url, Item, File) {
        console.log("IT : ", File, Item);
        this.Items = Item;
        var body = new FormData();
        body.append("file", File);
        body.append("category", JSON.stringify(this.Items));
        //let body = 'category=' + JSON.stringify(this.Items);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { }).toPromise();
    };
    MainService.prototype.DeleteItem = function (url, Id, Pid) {
        var body = new FormData();
        body.append("id", Id);
        body.append("pid", Pid);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { }).toPromise();
    };
    MainService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__settings_settings_service__["a" /* SettingsService */]) === "function" && _b || Object])
    ], MainService);
    return MainService;
    var _a, _b;
}());

;
//# sourceMappingURL=MainService.service.js.map

/***/ }),

/***/ "../../../../../src/app/productImages/add/add.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".FormClass\n{\n    text-align: right;\n    direction: rtl;\n}\n\n.row\n{\n    margin-top: 20px;\n}\n\n.textWhite\n{\n    background-color: white;\n}\n\ninput.ng-invalid.ng-touched\n{\n    border:1px solid red;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/productImages/add/add.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row FormClass\">\n  <div class=\"col-lg-12\">\n    <div class=\"card\">\n      <div class=\"card-header\">\n        ערוך חברה\n      </div>\n      <div class=\"card-body\">\n        <form (ngSubmit)=\"onSubmit(f)\" #f=\"ngForm\" >\n          <div class=\"row\" >\n            <div class=\"form-group\" class=\"col-lg-6\" *ngFor=\"let row of rows let i=index\" style=\"margin-top: 15px;\">\n              <label for=\"formGroupExampleInput\">הכנס {{rowsNames[i]}} </label>\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" name={{row}} [(ngModel)]=\"Item[row]\"  ngModel required>\n            </div>\n          </div>\n          <div class=\"row\">\n            <div class=\"form-group\" class=\"col-lg-6\">\n              <label for=\"formGroupExampleInput\">תיאור בעיברית</label>\n              <textarea rows=\"4\" cols=\"50\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" [(ngModel)]=\"Item.description\" name=\"description\" ngModel required> </textarea>\n            </div>\n            <div class=\"form-group\" class=\"col-lg-6\">\n              <label for=\"formGroupExampleInput\">תיאור באנגלית</label>\n              <textarea rows=\"4\" cols=\"50\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" [(ngModel)]=\"Item.description_english\" name=\"description_english\" ngModel required> </textarea>\n            </div>\n          </div>\n          <div class=\"row\">\n            <div class=\"form-group\" class=\"col-lg-6\">\n              <label for=\"formGroupExampleInput\">תת קטגורייה </label>\n              <select class=\"form-control textWhite\" [(ngModel)]=\"Item.sub_category_id\" name=\"sub_category_id\">\n                <option *ngFor=\"let item of SubCategories let i=index\" [value]=\"item.id\">{{item.title}}</option>\n              </select><br/>\n            </div>\n          </div>\n          <!--<div class=\"row\">-->\n          <!--<img src=\"{{Image}}\" style=\"width:50px; height: 50px\" />-->\n          <!--<input  #fileInput   type=\"file\"(change)=\"onChange($event)\" style=\"margin-right: 20px; margin-top: 10px\"/>-->\n          <!--</div>-->\n          <div class=\"row\">\n            <!--<div class=\"form-group\" class=\"col-lg-6\">-->\n              <!--<div class=\"row\">-->\n                <!--<label class=\"uploader\">-->\n\n                  <!--<img *ngIf=\"changeImage\" src=\"{{changeImage}}\" [class.loaded]=\"imageLoaded\" style=\"width: 100px;\"/>-->\n                  <!--<img *ngIf=\"!changeImage\" src=\"{{host}}{{Item.image}}\" [class.loaded]=\"imageLoaded\" style=\"width: 100px;\"/>-->\n                  <!--<input #fileInput type=\"file\" name=\"file\" accept=\"image/*\" (change)=\"onChange($event)\">-->\n                <!--</label>-->\n\n\n              <!--</div>-->\n            <!--</div>-->\n            <div class=\"form-group\" class=\"col-lg-6\">\n              <div class=\"row\">\n                <input #fileInput type=\"file\"/>\n              </div>\n            </div>\n          </div>\n\n          <div class=\"row\" align=\"left\" style=\"float: left; margin-left: 5px\">\n            <button [disabled]=\"!f.valid\" type=\"submit\"\n                    class=\"btn btn-primary btn-icon loading-demo mr-1 mb-1\"\n                    style=\"padding:0 20px !important; cursor: pointer; width: 205px; text-align: center; font-weight: bold; font-size: 16px\">\n              <span style=\"text-align: center; padding: 10px;  width: 100% !important;\">שלח טופס</span>\n            </button>\n          </div>\n        </form>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/productImages/add/add.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__MainService_service__ = __webpack_require__("../../../../../src/app/productImages/MainService.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AddComponent = (function () {
    function AddComponent(route, http, service, router) {
        var _this = this;
        this.route = route;
        this.http = http;
        this.service = service;
        this.router = router;
        this.navigateTo = '/category/index';
        this.imageSrc = '';
        this.folderName = 'products';
        this.rowsNames = ['שם בעיברית', 'שם באנגלית', 'מחיר נמוך', 'מחיר גבוה'];
        this.rows = ['title', 'title_english', 'low_price', 'high_price'];
        this.Item = {
            'title': '',
            'title_english': '',
            'low_price': '',
            'high_price': '',
            'description': '',
            'description_english': '',
            'image': '',
        };
        console.log("Row : ", this.rows);
        this.service.GetItems('GetSubCategoriesById', -1).then(function (data) {
            _this.SubCategories = data;
            _this.isReady = true;
            console.log("Sub : ", data);
        });
    }
    AddComponent.prototype.onSubmit = function (form) {
        var _this = this;
        console.log(form.value);
        var fi = this.fileInput.nativeElement;
        var fileToUpload;
        if (fi.files && fi.files[0]) {
            fileToUpload = fi.files[0];
        }
        this.service.AddItem('AddProduct', form.value, fileToUpload).then(function (data) {
            console.log("AddCompany : ", data);
            _this.router.navigate([_this.navigateTo]);
        });
    };
    AddComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.paramsSub = this.route.params.subscribe(function (params) { return _this.id = params['id']; });
    };
    AddComponent.prototype.ngOnDestroy = function () {
        this.paramsSub.unsubscribe();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("fileInput"),
        __metadata("design:type", Object)
    ], AddComponent.prototype, "fileInput", void 0);
    AddComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-add',
            template: __webpack_require__("../../../../../src/app/productImages/add/add.component.html"),
            styles: [__webpack_require__("../../../../../src/app/productImages/add/add.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__MainService_service__["a" /* MainService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__MainService_service__["a" /* MainService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _d || Object])
    ], AddComponent);
    return AddComponent;
    var _a, _b, _c, _d;
}());

//# sourceMappingURL=add.component.js.map

/***/ }),

/***/ "../../../../../src/app/productImages/edit/edit.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".FormClass\n{\n    text-align: right;\n    direction: rtl;\n}\n\n.row\n{\n    margin-top: 20px;\n}\n\n.textWhite\n{\n    background-color: white;\n}\n\ninput.ng-invalid.ng-touched\n{\n    border:1px solid red;\n}\n\n.SearchInput\n{\n    background-color: white;\n    text-align: right;\n}\n\n.p-3\n{\n    padding: 0px;\n    background-color: red;\n}\n\n.KitchensForm\n{\n    direction: rtl;\n    text-align: right;\n}\n\n.formCheck\n{\n    position: relative;\n    left:20px;\n    text-align: right;\n    width: 50px;\n    background-color: red;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/productImages/edit/edit.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row FormClass\">\n    <div class=\"col-lg-12\">\n        <div class=\"card\">\n            <div class=\"card-header\">\n                ערוך חברה\n            </div>\n            <div class=\"card-body\">\n                <form (ngSubmit)=\"onSubmit(f)\" #f=\"ngForm\" >\n                    <div class=\"row\" >\n                        <div class=\"form-group\" class=\"col-lg-6\" *ngFor=\"let row of rows let i=index\" style=\"margin-top: 15px;\">\n                            <label for=\"formGroupExampleInput\">הכנס {{rowsNames[i]}} </label>\n                            <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" name={{row}} [(ngModel)]=\"Item[row]\"  ngModel required>\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                        <div class=\"form-group\" class=\"col-lg-6\">\n                            <label for=\"formGroupExampleInput\">תיאור בעיברית</label>\n                            <textarea rows=\"4\" cols=\"50\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" [(ngModel)]=\"Item.description\" name=\"description\" ngModel required> </textarea>\n                        </div>\n                        <div class=\"form-group\" class=\"col-lg-6\">\n                            <label for=\"formGroupExampleInput\">תיאור באנגלית</label>\n                            <textarea rows=\"4\" cols=\"50\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" [(ngModel)]=\"Item.description_english\" name=\"description\" ngModel required> </textarea>\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                        <div class=\"form-group\" class=\"col-lg-6\">\n                            <label for=\"formGroupExampleInput\">תת קטגורייה </label>\n                            <select class=\"form-control textWhite\" [(ngModel)]=\"Item.sub_category_id\" name=\"category\">\n                                <option *ngFor=\"let item of SubCategories let i=index\" [value]=\"item.id\">{{item.title}}</option>\n                            </select><br/>\n                        </div>\n                    </div>\n                    <!--<div class=\"row\">-->\n                        <!--<img src=\"{{Image}}\" style=\"width:50px; height: 50px\" />-->\n                        <!--<input  #fileInput   type=\"file\"(change)=\"onChange($event)\" style=\"margin-right: 20px; margin-top: 10px\"/>-->\n                    <!--</div>-->\n                    <div class=\"row\">\n                    <div class=\"form-group\" class=\"col-lg-6\">\n                        <div class=\"row\">\n                            <label class=\"uploader\">\n\n                                <img *ngIf=\"changeImage\" src=\"{{changeImage}}\" [class.loaded]=\"imageLoaded\" style=\"width: 100px;\"/>\n                                <img *ngIf=\"!changeImage\" src=\"{{host}}{{Item.image}}\" [class.loaded]=\"imageLoaded\" style=\"width: 100px;\"/>\n                                <input #fileInput type=\"file\" name=\"file\" accept=\"image/*\" (change)=\"onChange($event)\">\n                            </label>\n\n\n                        </div>\n                    </div></div>\n\n                    <div class=\"row\" align=\"left\" style=\"float: left; margin-left: 5px\">\n                        <button [disabled]=\"!f.valid\" type=\"submit\"\n                                class=\"btn btn-primary btn-icon loading-demo mr-1 mb-1\"\n                                style=\"padding:0 20px !important; cursor: pointer; width: 205px; text-align: center; font-weight: bold; font-size: 16px\">\n                            <span style=\"text-align: center; padding: 10px;  width: 100% !important;\">שלח טופס</span>\n                        </button>\n                    </div>\n                </form>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/productImages/edit/edit.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__MainService_service__ = __webpack_require__("../../../../../src/app/productImages/MainService.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__settings_settings_service__ = __webpack_require__("../../../../../src/settings/settings.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var URL = 'https://evening-anchorage-3159.herokuapp.com/api/';
var EditComponent = (function () {
    function EditComponent(route, http, service, router, settings) {
        var _this = this;
        this.route = route;
        this.http = http;
        this.service = service;
        this.router = router;
        this.settings = settings;
        this.navigateTo = '/products/index';
        this.imageSrc = '';
        this.Items = [];
        this.rowsNames = ['שם בעיברית', 'שם באנגלית', 'מחיר נמוך', 'מחיר גבוה'];
        this.rows = ['title', 'title_english', 'low_price', 'high_price'];
        this.Change = false;
        this.route.params.subscribe(function (params) {
            _this.Id = params['id'];
            _this.Item = _this.service.Items[_this.Id];
            _this.host = settings.host;
            _this.service.GetItems('GetSubCategoriesById', -1).then(function (data) {
                _this.SubCategories = data;
                _this.isReady = true;
                console.log("Sub : ", data);
            });
            console.log("Product", _this.Item);
            console.log("Item : ", _this.Item);
        });
    }
    EditComponent.prototype.onSubmit = function (form) {
        var _this = this;
        console.log("Edit1 : ", this.Item);
        var fi = this.fileInput.nativeElement;
        var fileToUpload;
        if (fi.files && fi.files[0]) {
            fileToUpload = fi.files[0];
            console.log("fff : ", fileToUpload);
        }
        this.Item.change = this.Change.toString();
        //this.Item.change = this.Change;
        this.service.EditItem('EditProduct', this.Item, fileToUpload).then(function (data) {
            console.log("AddCompany : ", data);
            _this.router.navigate([_this.navigateTo]);
        });
    };
    EditComponent.prototype.onChange = function (event) {
        var _this = this;
        this.Change = true;
        var files = event.srcElement.files;
        this.changeImage = event.target.files[0];
        var reader = new FileReader();
        reader.onload = function (e) {
            _this.changeImage = e.target.result;
        };
        reader.readAsDataURL(event.target.files[0]);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("fileInput"),
        __metadata("design:type", Object)
    ], EditComponent.prototype, "fileInput", void 0);
    EditComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-edit',
            template: __webpack_require__("../../../../../src/app/productImages/edit/edit.component.html"),
            styles: [__webpack_require__("../../../../../src/app/icons/fontawesome/fontawesome.component.scss"), __webpack_require__("../../../../../src/app/components/buttons/buttons.component.scss"), __webpack_require__("../../../../../src/app/productImages/edit/edit.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__MainService_service__["a" /* MainService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__MainService_service__["a" /* MainService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__settings_settings_service__["a" /* SettingsService */]) === "function" && _e || Object])
    ], EditComponent);
    return EditComponent;
    var _a, _b, _c, _d, _e;
}());

//# sourceMappingURL=edit.component.js.map

/***/ }),

/***/ "../../../../../src/app/productImages/index/index.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".card\n{\n    text-align: right;\n    direction: rtl;\n}\n\n.card-body\n{\n    border-bottom: 1px solid #f2f1f2;\n}\n\n.mr-auto\n{\n    text-align: right;\n    direction: rtl;\n    background-color: red;\n    float: right;\n}\n\n.mr-3\n{\n    background-color: green;\n    float: right;\n}\n\n.IconClass\n{\n    margin-top: 6px;\n    text-align: center;\n    padding-left: -13px !important;\n    background-color: red;\n}\n\n.d-icon{\n    margin-top: -20px;\n}\n\n.titleImage\n{\n    width: 80px;\n    border-radius: 70%;\n    height:80px;\n    margin-top:3px;\n    border: 1px solid #f1f1f1;\n}\n\n.textHeader\n{\n    color: #337ab7;;\n    font-size: 15px;\n    font-weight: bold;\n}\n\n.SearchInput{\n    background-color: white;\n    text-align: right;\n    paddding:5px;\n    margin-bottom: -15px;\n    margin-top: -15px;\n}\n\n.p-3{\n    margin-top: 2px;\n    margin-bottom: 2px;\n}\n\n.sideButton\n{\n    width:90%;\n    cursor: pointer;\n    background-color: #3b5998;\n    color: white;\n    text-align: right;\n    padding: 3px;\n    overflow: hidden;\n}\n\n.sideButtonText\n{\n    margin-right: 10px;\n    font-size: 14px;\n    font-weight: bold;\n    top: 7px !important;\n    position: relative;\n}\n\n.sideButtonTextEmpty{\n    margin-right: 10px;\n    font-size: 14px;\n    font-weight: bold;\n    top: 0px !important;\n    position: relative;\n}\n\n.sideButtonBadge\n{\n    background-color: red;\n    border-radius:50%;\n    font-size: 12px;\n    margin-top: 5px;\n    padding: 3px;\n    width: 25px;\n    height: 25px;\n}\n\n.buttonDivBadge\n{\n    float: right;\n    width: 12%;\n}\n\n.buttonDivText\n{\n    float: right;\n    width: 90%;\n    text-align: right !important;\n}\n\n.SearchInput{\n    background-color: white;\n    text-align: right;\n    paddding:5px;\n    margin-bottom: -15px;\n    margin-top: -15px;\n}\n\n.buttonDivIcon\n{\n    float: left;\n    width: 20%;\n}\n\n.badgeText\n{\n    top: 4px;\n    position: relative;\n}\n\n\nngx-datatable {\n    direction: rtl !important;\n    text-align: right !important;\n}\n\n.yellow-star {\n    color: #ffbd53;\n}\n\n.grey-star {\n    color: grey;\n}\n\n\n\n.sideButtonTextEmpty{\n    margin-right: 10px;\n    font-size: 14px;\n    font-weight: bold;\n    top: 0px !important;\n    position: relative;\n    text-align: right !important;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/productImages/index/index.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row nopadding\" style=\"padding: 0px; background-color: white; margin-top: -15px; margin-left: -17px\">\n            <div class=\"col-lg-12 nopadding\" style=\"margin-top: 10px;\">\n                        <div class=\"row\" style=\"direction: rtl; margin-left:0%;\">\n                            <div class=\"col-lg-9 nopadding\" style=\"margin-top: 10px;\">\n                                <form (ngSubmit)=\"onSubmit(f)\" #f=\"ngForm\" >\n                                    <div class=\"row\">\n                                        <label class=\"uploader\" >\n                                            <!--<img *ngIf=\"changeImage\" src=\"{{changeImage}}\" [class.loaded]=\"imageLoaded\" style=\"width: 100px;\"/>-->\n                                            <!--<img *ngIf=\"!changeImage\" src=\"{{host}}{{Item.image}}\" [class.loaded]=\"imageLoaded\" style=\"width: 100px;\"/>-->\n                                            <input #fileInput type=\"file\" name=\"file\" accept=\"image/*\" (change)=\"onChange($event)\" *ngIf=\"ProductId != -1\">\n                                        </label>\n                                    </div>\n                                </form>\n                            </div>\n\n                            <div class=\"col-lg-1 nopadding\" style=\"margin-top: 5px;\">\n                                <button [routerLink]=\"['/', 'products' , 'index' , { id:subCat}]\"  class=\"btn btn-icon btn-facebook mb-1 mr-1 sideButton\" >\n                                    <div class=\"buttonDivText\">\n                                        <span class=\"sideButtonTextEmpty\">חזרה</span>\n                                    </div>\n                                    <div class=\"buttonDivIcon\">\n                                        <i class=\"fa fa-chevron-left\"></i>\n                                    </div>\n                                </button>\n                            </div>\n\n                        </div>\n            </div>\n\n</div>\n\n<div class=\"row\">\n    <div class=\"media-grid col-lg-3 col-sm-6\" *ngFor=\"let Item of ItemsArray\">\n        <figure>\n            <img src=\"{{host}}{{Item.url}}\" alt=\"Card image cap\" class=\"img-fluid\" />\n            <figcaption>\n                <!--<h5 class=\"media-title fw-300\">Media <strong>{{ id }}</strong></h5>-->\n                <p>\n                    <a href=\"javascript:;\" placement=\"left\" ngbTooltip=\"Delete\" (click)=\"deleteItem(Item.id)\"><i class=\"icon-arrows-remove\"></i></a>\n                    <!--<a href=\"javascript:;\" placement=\"left\" ngbTooltip=\"Like\"><i class=\"icon icon-basic-heart\"></i></a>-->\n                    <!--<a href=\"javascript:;\" placement=\"left\" ngbTooltip=\"Share\"><i class=\"icon icon-basic-share\"></i></a>-->\n                    <!--<a href=\"javascript:;\" placement=\"left\" ngbTooltip=\"Bookmark\"><i class=\"icon icon-basic-bookmark\"></i></a>-->\n                </p>\n            </figcaption>\n        </figure>\n    </div>\n</div>\n\n\n\n\n\n\n\n\n"

/***/ }),

/***/ "../../../../../src/app/productImages/index/index.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IndexComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__MainService_service__ = __webpack_require__("../../../../../src/app/productImages/MainService.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__settings_settings_service__ = __webpack_require__("../../../../../src/settings/settings.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var IndexComponent = (function () {
    function IndexComponent(MainService, service, settings, modalService, route) {
        var _this = this;
        this.MainService = MainService;
        this.service = service;
        this.modalService = modalService;
        this.route = route;
        this.ItemsArray = [];
        this.ItemsArray1 = [];
        this.host = '';
        this.route.params.subscribe(function (params) {
            _this.ProductId = params['id'];
            if (!_this.ProductId)
                _this.ProductId = "-1";
            _this.subCat = params['subCat'];
            console.log("11 : ", _this.ProductId);
            _this.MainService.GetItems('getImages', _this.ProductId).then(function (data) {
                console.log("GetCategories12 : ", data);
                _this.ItemsArray = data;
                _this.ItemsArray1 = data;
                _this.host = settings.host;
            });
        });
    }
    IndexComponent.prototype.ngOnInit = function () {
    };
    IndexComponent.prototype.onChange = function (event) {
        var _this = this;
        console.log(event);
        var files = event.target.files[0]; //event.srcElement.files;
        // let reader = new FileReader();
        // reader.onload = (e: any) => {
        //     this.changeImage = e.target.result;
        // }
        //
        // reader.readAsDataURL(event.target.files[0]);
        var fi = this.fileInput.nativeElement;
        var fileToUpload;
        if (fi.files && fi.files[0]) {
            fileToUpload = fi.files[0];
            console.log("fff : ", fileToUpload);
        }
        //this.Item.change = this.Change;
        this.service.AddItem('AddImage', this.ProductId, fileToUpload).then(function (data) {
            console.log("GetCategories11 : ", data);
            _this.ItemsArray = data;
        });
    };
    IndexComponent.prototype.deleteItem = function (i) {
        var _this = this;
        //console.log("I : " , i ,  this.ProductId)
        this.service.DeleteItem('DeleteImage', i, this.ProductId).then(function (data) {
            console.log("GetCategories11 : ", data);
            _this.ItemsArray = data;
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("fileInput"),
        __metadata("design:type", Object)
    ], IndexComponent.prototype, "fileInput", void 0);
    IndexComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-index',
            template: __webpack_require__("../../../../../src/app/productImages/index/index.component.html"),
            styles: [__webpack_require__("../../../../../src/app/icons/fontawesome/fontawesome.component.scss"), __webpack_require__("../../../../../src/app/media/list/list.component.scss"), __webpack_require__("../../../../../src/app/productImages/index/index.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__MainService_service__["a" /* MainService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__MainService_service__["a" /* MainService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__MainService_service__["a" /* MainService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__MainService_service__["a" /* MainService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__settings_settings_service__["a" /* SettingsService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__ng_bootstrap_ng_bootstrap__["f" /* NgbModal */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__ng_bootstrap_ng_bootstrap__["f" /* NgbModal */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* ActivatedRoute */]) === "function" && _e || Object])
    ], IndexComponent);
    return IndexComponent;
    var _a, _b, _c, _d, _e;
}());

//# sourceMappingURL=index.component.js.map

/***/ })

});
//# sourceMappingURL=Main.module.3.chunk.js.map