webpackJsonp(["employee.module.1"],{

/***/ "../../../../../src/app/chat/employee.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeModule", function() { return EmployeeModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__employee_routing__ = __webpack_require__("../../../../../src/app/chat/employee.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_datatable__ = __webpack_require__("../../../../@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_datatable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_datatable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__employee_service__ = __webpack_require__("../../../../../src/app/chat/employee.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__index_index_component__ = __webpack_require__("../../../../../src/app/chat/index/index.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ng2_file_upload__ = __webpack_require__("../../../../ng2-file-upload/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ng_sidebar__ = __webpack_require__("../../../../ng-sidebar/lib/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ng_sidebar___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_ng_sidebar__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var EmployeeModule = (function () {
    function EmployeeModule() {
    }
    EmployeeModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__employee_routing__["a" /* EmployeeRoutes */]),
                __WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_datatable__["NgxDatatableModule"],
                __WEBPACK_IMPORTED_MODULE_6__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_8__ng_bootstrap_ng_bootstrap__["g" /* NgbModule */],
                __WEBPACK_IMPORTED_MODULE_9_ng2_file_upload__["FileUploadModule"],
                __WEBPACK_IMPORTED_MODULE_10__angular_forms__["FormsModule"], __WEBPACK_IMPORTED_MODULE_11_ng_sidebar__["SidebarModule"]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_7__index_index_component__["a" /* IndexComponent */],
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_5__employee_service__["a" /* EmployeeService */]]
        })
    ], EmployeeModule);
    return EmployeeModule;
}());

//# sourceMappingURL=employee.module.js.map

/***/ }),

/***/ "../../../../../src/app/chat/employee.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EmployeeRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__index_index_component__ = __webpack_require__("../../../../../src/app/chat/index/index.component.ts");

var EmployeeRoutes = [{
        path: '',
        children: [{
                path: 'index',
                component: __WEBPACK_IMPORTED_MODULE_0__index_index_component__["a" /* IndexComponent */],
                data: {
                    heading: 'הודעות'
                }
            }]
    }];
//# sourceMappingURL=employee.routing.js.map

/***/ }),

/***/ "../../../../../src/app/chat/employee.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EmployeeService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__("../../../../rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__settings_settings_service__ = __webpack_require__("../../../../../src/settings/settings.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//public this.ServerUrl = "";//http://www.tapper.co.il/salecar/laravel/public/api/";
var EmployeeService = (function () {
    function EmployeeService(http, settings) {
        this.http = http;
        this.ServerUrl = "";
        this.headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/x-www-form-urlencoded' });
        this.options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["f" /* RequestOptions */]({ headers: this.headers });
        this.Items = [];
        this.Companies = [];
        this.CompanyArray = [];
        this.Kitchens = [];
        this.ServerUrl = settings.ServerUrl;
    }
    ;
    EmployeeService.prototype.GetItems = function (url, CompanyId) {
        var _this = this;
        var body = new FormData();
        body.append('uid', window.localStorage.identify);
        body.append('cid', CompanyId);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { _this.Items = data; }).toPromise();
    };
    EmployeeService.prototype.getChatDetails = function (url, user_id, product_id) {
        var _this = this;
        var body = new FormData();
        body.append('user_id', user_id);
        body.append('product_id', product_id);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { _this.Items = data.reverse(); }).toPromise();
    };
    EmployeeService.prototype.AddItem = function (url, Items) {
        this.Items = Items;
        var body = 'items=' + JSON.stringify(this.Items);
        return this.http.post(this.ServerUrl + '' + url, body, this.options).map(function (res) { return res; }).do(function (data) { }).toPromise();
    };
    EmployeeService.prototype.addTitle = function (url, title, datetime, time, name, image, chatType, product_id, is_Admin, recipent, uid) {
        var body = new FormData();
        body.append('uid', uid);
        body.append('title', title);
        body.append('type', chatType);
        body.append('product_id', product_id);
        body.append('is_Admin', is_Admin);
        body.append('recipent', recipent);
        body.append('date', datetime);
        body.append('time', time);
        body.append('name', name);
        //body.append('image', image );
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return ''; }).do(function (data) { console.log("Chat : ", data); }).toPromise();
    };
    EmployeeService.prototype.EditCompany = function (url, Company) {
        this.CompanyArray = Company;
        var body = 'company=' + JSON.stringify(this.CompanyArray);
        return this.http.post(this.ServerUrl + '' + url, body, this.options).map(function (res) { return res; }).do(function (data) { }).toPromise();
    };
    EmployeeService.prototype.DeleteItem = function (url, Id, Cid) {
        var _this = this;
        var body = new FormData();
        body.append('id', Id);
        body.append('cid', Cid);
        console.log("Del 3 : ", Cid + " : " + Id);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { _this.Items = data; }).toPromise();
    };
    EmployeeService.prototype.DeleteMessage = function (url, id) {
        var _this = this;
        var body = new FormData();
        body.append('id', id.toString());
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { _this.Items = data; }).toPromise();
    };
    EmployeeService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__settings_settings_service__["a" /* SettingsService */]) === "function" && _b || Object])
    ], EmployeeService);
    return EmployeeService;
    var _a, _b;
}());

;
//# sourceMappingURL=employee.service.js.map

/***/ }),

/***/ "../../../../../src/app/chat/index/index.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/chat/index/index.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"footer\">\n    <div class=\"row\">\n        <div class=\"col-6\">\n            <textarea style=\"text-align: left;\" [(ngModel)]=\"chatText\" (keyup)=\"$event.keyCode == 13 && sendMessage()\"></textarea>\n        </div>\n        <div class=\"col-6\">\n            <button  type=\"button\" (click)=\"sendMessage()\"\n                    [disabled]=\"chatText.length === 0\"\n                    class=\"btn btn-primary btn-icon loading-demo mr-1 mb-1\"\n                    style=\"padding:0 20px !important; cursor: pointer; text-align: center; font-weight: bold; font-size: 16px\">\n                <span style=\"text-align: center; padding: 10px;  width: 100% !important;\">שלח</span>\n            </button>\n    \n        </div>\n    </div>\n\n</div>\n\n<div class=\"row chat-wrap\">\n    <div class=\"col-sm-12\">\n        <div class=\"card\" [ngStyle]=\"{'background-color': item.is_Admin === '1' ? '#d2d2f7' : '#fff'}\" *ngFor=\"let item of ItemsArray; let i = index\">\n            <div class=\"card-body\">\n                <p class=\"card-text\">{{item.username}}</p>\n                <p class=\"card-text\">{{item.title}}</p>\n                <p class=\"card-text\">{{item.date}}</p>\n                <button  type=\"button\" (click)=\"DeleteItem(i)\"\n                class=\"btn btn-danger btn-icon loading-demo mr-1 mb-1\"\n                style=\"padding:0 20px !important; cursor: pointer; text-align: center; float: right; font-weight: bold; font-size: 16px\">\n                <span style=\"text-align: center; padding: 10px;  width: 100% !important;\">מחק</span>\n                </button>\n\n            </div>\n        </div>\n    </div>\n\n</div>\n\n"

/***/ }),

/***/ "../../../../../src/app/chat/index/index.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".footer {\n  direction: rtl;\n  text-align: right;\n  min-height: 80px;\n  max-height: 100px;\n  margin-bottom: 20px;\n  width: 100%;\n  position: fixed;\n  z-index: 50;\n  padding-right: 20%;\n  background-color: rgba(255, 255, 255, 0.3);\n  margin-top: 5px; }\n  .footer textarea {\n    width: 100%;\n    min-height: 50px;\n    padding: 5px; }\n  .footer button {\n    vertical-align: -25px; }\n\n.chat-wrap {\n  margin-top: 80px; }\n\nfooter {\n  background: #42A5F5;\n  color: white;\n  line-height: 50px;\n  padding: 0 20px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/chat/index/index.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IndexComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__employee_service__ = __webpack_require__("../../../../../src/app/chat/employee.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__settings_settings_service__ = __webpack_require__("../../../../../src/settings/settings.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var IndexComponent = (function () {
    function IndexComponent(EmployeeService, settings, route) {
        var _this = this;
        this.EmployeeService = EmployeeService;
        this.settings = settings;
        this.route = route;
        this.ItemsArray = [];
        this.ItemsArray1 = [];
        this.host = '';
        this.chatText = '';
        this.ChatArray = [];
        this.screenHeight = screen.height - 100 + 'px';
        this.route.params.subscribe(function (params) {
            _this.Id = params['id'];
            console.log("ssss : ", _this.Id);
            _this.host = settings.host;
            _this.getItems(_this.Id);
        });
    }
    IndexComponent.prototype.ngOnInit = function () {
    };
    IndexComponent.prototype.getItems = function (id) {
        var _this = this;
        this.EmployeeService.getChatDetails('getChatDetails', this.Id, 0).then(function (data) {
            console.log("getChatDetails : ", data),
                _this.ItemsArray = data,
                _this.ItemsArray1 = data;
        });
    };
    IndexComponent.prototype.sendMessage = function () {
        var _this = this;
        this.date = new Date();
        this.hours = this.date.getHours();
        this.minutes = this.date.getMinutes();
        this.seconds = this.date.getSeconds();
        if (this.hours < 10)
            this.hours = "0" + this.hours;
        if (this.minutes < 10)
            this.minutes = "0" + this.minutes;
        this.time = this.hours + ':' + this.minutes;
        this.today = new Date();
        this.dd = this.today.getDate();
        this.mm = this.today.getMonth() + 1; //January is 0!
        this.yyyy = this.today.getFullYear();
        if (this.dd < 10) {
            this.dd = '0' + this.dd;
        }
        if (this.mm < 10) {
            this.mm = '0' + this.mm;
        }
        this.today = this.dd + '/' + this.mm + '/' + this.yyyy;
        //this.newdate = this.today + ' ' + this.time;
        this.Obj = {
            id: this.Id,
            uid: 0,
            username: 'admin',
            product_id: 0,
            is_Admin: 0,
            title: this.chatText,
            date: this.today,
            time: this.time,
            type: '1',
        };
        this.ItemsArray.push(this.Obj);
        this.ItemsArray.reverse();
        this.EmployeeService.addTitle('addChatTitle', this.chatText, this.today, this.time, 'admin', '', '1', '0', '1', '1', this.Id).then(function (data) {
            console.log("Weights : ", data), _this.chatText = '';
        });
        console.log("this.Obj", this.Obj);
    };
    IndexComponent.prototype.DeleteItem = function (i) {
        var _this = this;
        console.log("Del 00 : ", this.ItemsArray[i].id + " : " + this.Id);
        this.EmployeeService.DeleteMessage('deleteMessage', this.ItemsArray[i].id).then(function (data) {
            console.log(data);
            _this.ItemsArray.splice(i, 1);
        });
    };
    IndexComponent.prototype.updateFilter = function (event) {
        var val = event.target.value;
        // filter our data
        var temp = this.ItemsArray1.filter(function (d) {
            return d.username.toLowerCase().indexOf(val) !== -1 || !val;
        });
        // update the rows
        this.ItemsArray = temp;
    };
    IndexComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-index',
            template: __webpack_require__("../../../../../src/app/chat/index/index.component.html"),
            styles: [__webpack_require__("../../../../../src/app/icons/fontawesome/fontawesome.component.scss"), __webpack_require__("../../../../../src/app/media/list/list.component.scss"), __webpack_require__("../../../../../src/app/chat/index/index.component.css"), __webpack_require__("../../../../../src/app/chat/index/index.component.scss")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__employee_service__["a" /* EmployeeService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__employee_service__["a" /* EmployeeService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__settings_settings_service__["a" /* SettingsService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* ActivatedRoute */]) === "function" && _c || Object])
    ], IndexComponent);
    return IndexComponent;
    var _a, _b, _c;
}());

//# sourceMappingURL=index.component.js.map

/***/ })

});
//# sourceMappingURL=employee.module.1.chunk.js.map