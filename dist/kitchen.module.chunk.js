webpackJsonp(["kitchen.module"],{

/***/ "../../../../../src/app/kitchens/add/add.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".FormClass\n{\n    text-align: right;\n    direction: rtl;\n}\n\n.row\n{\n    margin-top: 20px;\n}\n\n.textWhite\n{\n    background-color: white;\n}\n\ninput.ng-invalid.ng-touched\n{\n    border:1px solid red;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/kitchens/add/add.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row FormClass\">\n  <div class=\"col-lg-12\">\n    <div class=\"card\">\n      <div class=\"card-header\">\n        הוסף מטבח\n      </div>\n      <div class=\"card-body\">\n          <!-- name , username , password , email , phone. , address , desc , company_price , restaurant_price  -->\n        <form (ngSubmit)=\"onSubmit(f)\" #f=\"ngForm\">\n          <div class=\"row\">\n            <div class=\"form-group\" class=\"col-lg-6\">\n              <label for=\"formGroupExampleInput\">הכנס שם מטבח </label>\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" name=\"name\" ngModel required>\n            </div>\n              <div class=\"form-group\" class=\"col-lg-6\">\n                  <label for=\"formGroupExampleInput\">הכנס אימייל </label>\n                  <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" name=\"email\" ngModel required>\n              </div>\n          </div>\n\n          <div class=\"row\">\n            <div class=\"form-group\" class=\"col-lg-6\">\n              <label for=\"formGroupExampleInput\">הכנס כתובת </label>\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" name=\"address\"\n                     ngModel required>\n            </div>\n            <div class=\"form-group\" class=\"col-lg-6\">\n              <label for=\"formGroupExampleInput2\">הכנס טלפון</label>\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput2\" name=\"phone\"\n                     ngModel required>\n            </div>\n          </div>\n\n          <div class=\"row\">\n            <div class=\"form-group\" class=\"col-lg-6\">\n              <label for=\"formGroupExampleInput\">הזן שם משתמש</label>\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" name=\"username\" ngModel required>\n            </div>\n            <div class=\"form-group\" class=\"col-lg-6\">\n              <label for=\"formGroupExampleInput2\">הכנס סיסמה</label>\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput2\" name=\"password\" ngModel required >\n            </div>\n          </div>\n\n          <div class=\"row\">\n            <div class=\"form-group\" class=\"col-lg-6\">\n              <label for=\"formGroupExampleInput\">הכנס מחיר חברה</label>\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" name=\"company_price\"\n                     ngModel>\n            </div>\n            <div class=\"form-group\" class=\"col-lg-6\">\n              <label for=\"formGroupExampleInput2\">הכנס מחיר מסעדה</label>\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput2\" name=\"resturant_price\"\n                     ngModel>\n            </div>\n          </div>\n\n          <div class=\"row\">\n            <div class=\"form-group\" class=\"col-lg-12\">\n              <label for=\"formGroupExampleInput\">פרטים נוספים</label>\n              <textarea  rows=\"4\" cols=\"50\"  class=\"form-control textWhite\" id=\"formGroupExampleInput\" name=\"desc\" ngModel required> </textarea>\n            </div>\n          </div>\n          <div class=\"row\">\n            <label class=\"uploader\">\n              <img [src]=\"imageSrc\"  [class.loaded]=\"imageLoaded\" style=\"width: 100px;\"/>\n              <input type=\"file\" name=\"file\" accept=\"image/*\" (change)=\"handleInputChange($event)\">\n            </label>\n          </div>\n          <div class=\"row\" align=\"left\" style=\"float: left; margin-left: 5px\">\n            <button [disabled]=\"!f.valid\" type=\"submit\" class=\"btn btn-primary btn-icon loading-demo mr-1 mb-1\"\n                    style=\"padding:0 20px !important; cursor: pointer; width: 205px; text-align: center; font-weight: bold; font-size: 16px\">\n              <span style=\"text-align: center; padding: 10px;  width: 100% !important;\">שלח טופס</span>\n            </button>\n          </div>\n\n        </form>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/kitchens/add/add.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__kitchen_service__ = __webpack_require__("../../../../../src/app/kitchens/kitchen.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_file_upload_ng2_file_upload__ = __webpack_require__("../../../../ng2-file-upload/ng2-file-upload.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_file_upload_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ng2_file_upload_ng2_file_upload__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AddComponent = (function () {
    function AddComponent(route, http, service, router) {
        this.route = route;
        this.http = http;
        this.service = service;
        this.router = router;
        this.navigateTo = '/kitchen/index';
        this.imageSrc = '';
        this.uploader = new __WEBPACK_IMPORTED_MODULE_4_ng2_file_upload_ng2_file_upload__["FileUploader"]({
            itemAlias: 'file',
            url: 'http://www.tapper.co.il/foodcloud/laravel1/public/api/GetFile',
            isHTML5: true,
            additionalParameter: { entity_id: 1, entity_type: 'file', media_key: 'file' }
        });
    }
    AddComponent.prototype.onSubmit = function (form) {
        var _this = this;
        console.log(form.value);
        form.value.type = 2;
        this.service.AddItem('AddItem', form.value).then(function (data) {
            console.log("AddCompany : ", data);
            _this.router.navigate([_this.navigateTo]);
        });
    };
    AddComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.paramsSub = this.route.params.subscribe(function (params) { return _this.id = params['id']; });
    };
    AddComponent.prototype.ngOnDestroy = function () {
        this.paramsSub.unsubscribe();
    };
    AddComponent.prototype.handleInputChange = function (e) {
        var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
        var pattern = /image-*/;
        var reader = new FileReader();
        if (!file.type.match(pattern)) {
            alert('invalid format');
            return;
        }
        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsDataURL(file);
        console.log("2 : ", reader);
    };
    AddComponent.prototype._handleReaderLoaded = function (e) {
        var reader = e.target;
        this.imageSrc = reader.result;
        console.log("3 : ", this.imageSrc);
        /* this.service.AddCompany('AddCompany1',form.value).then((data: any) => {
             console.log("AddCompany : " , data);
             this.router.navigate([this.navigateTo]);
         });*/
    };
    AddComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-add',
            template: __webpack_require__("../../../../../src/app/kitchens/add/add.component.html"),
            styles: [__webpack_require__("../../../../../src/app/kitchens/add/add.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__kitchen_service__["a" /* KitchenService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__kitchen_service__["a" /* KitchenService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _d || Object])
    ], AddComponent);
    return AddComponent;
    var _a, _b, _c, _d;
}());

//# sourceMappingURL=add.component.js.map

/***/ }),

/***/ "../../../../../src/app/kitchens/edit/edit.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".FormClass\n{\n    text-align: right;\n    direction: rtl;\n}\n\n.row\n{\n    margin-top: 20px;\n}\n\n.textWhite\n{\n    background-color: white;\n}\n\ninput.ng-invalid.ng-touched\n{\n    border:1px solid red;\n}\n\n.SearchInput\n{\n    background-color: white;\n    text-align: right;\n}\n\n.p-3\n{\n    padding: 0px;\n    background-color: red;\n}\n\n.KitchensForm\n{\n    direction: rtl;\n    text-align: right;\n}\n\n.formCheck\n{\n    position: relative;\n    left:20px;\n    text-align: right;\n    width: 50px;\n    background-color: red;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/kitchens/edit/edit.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row FormClass\">\n    <div class=\"col-lg-12\">\n        <div class=\"card\">\n            <div class=\"card-header\">\n                ערוך חברה\n            </div>\n            <div class=\"card-body\">\n                <form (ngSubmit)=\"onSubmit(f)\" #f=\"ngForm\" >\n                    <div class=\"row\">\n                        <div class=\"form-group\" class=\"col-lg-6\">\n                            <label for=\"formGroupExampleInput\">הכנס שם מטבח </label>\n                            <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" name=\"name\" [(ngModel)]=\"Item.name\" ngModel required>\n                        </div>\n                        <div class=\"form-group\" class=\"col-lg-6\">\n                            <label for=\"formGroupExampleInput\">הכנס אימייל </label>\n                            <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" name=\"email\" [(ngModel)]=\"Item.email\" ngModel required>\n                        </div>\n                    </div>\n\n                    <div class=\"row\">\n                        <div class=\"form-group\" class=\"col-lg-6\">\n                            <label for=\"formGroupExampleInput\">הכנס כתובת </label>\n                            <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" name=\"address\" [(ngModel)]=\"Item.address\" ngModel required>\n                        </div>\n                        <div class=\"form-group\" class=\"col-lg-6\">\n                            <label for=\"formGroupExampleInput2\">הכנס טלפון</label>\n                            <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput2\" [(ngModel)]=\"Item.phone\" name=\"phone\"\n                                   ngModel required>\n                        </div>\n                    </div>\n\n                    <div class=\"row\">\n                        <div class=\"form-group\" class=\"col-lg-6\">\n                            <label for=\"formGroupExampleInput\">הזן שם משתמש</label>\n                            <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" [(ngModel)]=\"Item.username\" name=\"username\" ngModel required>\n                        </div>\n                        <div class=\"form-group\" class=\"col-lg-6\">\n                            <label for=\"formGroupExampleInput2\">הכנס סיסמה</label>\n                            <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput2\" [(ngModel)]=\"Item.password\" name=\"password\" ngModel required >\n                        </div>\n                    </div>\n\n                    <div class=\"row\">\n                        <div class=\"form-group\" class=\"col-lg-6\">\n                            <label for=\"formGroupExampleInput\">הכנס מחיר חברה</label>\n                            <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" [(ngModel)]=\"Item.company_price\" name=\"company_price\"\n                                   ngModel>\n                        </div>\n                        <div class=\"form-group\" class=\"col-lg-6\">\n                            <label for=\"formGroupExampleInput2\">הכנס מחיר מסעדה</label>\n                            <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput2\" [(ngModel)]=\"Item.resturant_price\" name=\"resturant_price\"\n                                   ngModel>\n                        </div>\n                    </div>\n\n                    <div class=\"row\">\n                        <div class=\"form-group\" class=\"col-lg-12\">\n                            <label for=\"formGroupExampleInput\">פרטים נוספים</label>\n                            <textarea  rows=\"4\" cols=\"50\"  class=\"form-control textWhite\" id=\"formGroupExampleInput\" [(ngModel)]=\"Item.desc\" name=\"desc\" ngModel required> </textarea>\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                        <label class=\"uploader\">\n                            <img [src]=\"imageSrc\"  [class.loaded]=\"imageLoaded\" style=\"width: 100px;\"/>\n                            <input type=\"file\" name=\"file\" accept=\"image/*\" (change)=\"handleInputChange($event)\">\n                        </label>\n                    </div>\n                    <div class=\"row\" align=\"left\" style=\"float: left; margin-left: 5px\">\n                        <button [disabled]=\"!f.valid\" type=\"submit\" class=\"btn btn-primary btn-icon loading-demo mr-1 mb-1\"\n                                style=\"padding:0 20px !important; cursor: pointer; width: 205px; text-align: center; font-weight: bold; font-size: 16px\">\n                            <span style=\"text-align: center; padding: 10px;  width: 100% !important;\">שלח טופס</span>\n                        </button>\n                    </div>\n\n                </form>\n\n\n                <!-- <img src=\"{{imageSrc}}\" style=\"width: 100%\" /> <div class=\"row\" align=\"left\"  style=\"float: left; margin-left: 5px\">\n                     <button type=\"button\"  class=\"btn btn-primary btn-icon loading-demo mr-1 mb-1\" style=\"padding:0 20px !important; cursor: pointer; width: 205px; text-align: center; font-weight: bold; font-size: 16px\">\n                         <span style=\"text-align: center; padding: 10px;  width: 100% !important;\">שלח טופס</span>\n                     </button>\n                 </div>-->\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/kitchens/edit/edit.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__kitchen_service__ = __webpack_require__("../../../../../src/app/kitchens/kitchen.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var URL = 'https://evening-anchorage-3159.herokuapp.com/api/';
var EditComponent = (function () {
    function EditComponent(route, http, service, router) {
        var _this = this;
        this.route = route;
        this.http = http;
        this.service = service;
        this.router = router;
        this.navigateTo = '/kitchen/index';
        this.imageSrc = '';
        this.Item = [];
        this.route.params.subscribe(function (params) {
            console.log("Edit : ", params['id'] + " : ", _this.service.Items[_this.Id]);
            _this.Id = params['id'];
            _this.Item = _this.service.Items[_this.Id];
        });
    }
    EditComponent.prototype.onSubmit = function (form) {
        console.log("Edit : ", this.Item);
        this.service.EditItem('EditItem', this.Item).then(function (data) {
            console.log("EDDITCompany : ", data);
            // this.router.navigate([this.navigateTo]);
        });
    };
    EditComponent.prototype.handleInputChange = function (e) {
        var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
        var pattern = /image-*/;
        var reader = new FileReader();
        if (!file.type.match(pattern)) {
            alert('invalid format');
            return;
        }
        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsDataURL(file);
        console.log("2 : ", reader);
    };
    EditComponent.prototype._handleReaderLoaded = function (e) {
        var reader = e.target;
        this.imageSrc = reader.result;
        console.log("3 : ", this.imageSrc);
    };
    EditComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-edit',
            template: __webpack_require__("../../../../../src/app/kitchens/edit/edit.component.html"),
            styles: [__webpack_require__("../../../../../src/app/icons/fontawesome/fontawesome.component.scss"), __webpack_require__("../../../../../src/app/components/buttons/buttons.component.scss"), __webpack_require__("../../../../../src/app/kitchens/edit/edit.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__kitchen_service__["a" /* KitchenService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__kitchen_service__["a" /* KitchenService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _d || Object])
    ], EditComponent);
    return EditComponent;
    var _a, _b, _c, _d;
}());

//# sourceMappingURL=edit.component.js.map

/***/ }),

/***/ "../../../../../src/app/kitchens/index/index.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".card\n{\n    text-align: right;\n    direction: rtl;\n}\n\n.card-body\n{\n    border-bottom: 1px solid #f2f1f2;\n}\n\n.mr-auto\n{\n    text-align: right;\n    direction: rtl;\n    background-color: red;\n    float: right;\n}\n\n.mr-3\n{\n    background-color: green;\n    float: right;\n}\n\n.IconClass\n{\n    margin-top: 6px;\n    text-align: center;\n    padding-left: -13px !important;\n    background-color: red;\n}\n\n.d-icon{\n    margin-top: -20px;\n}\n\n.titleImage\n{\n    width: 80px;\n    border-radius: 70%;\n    height:80px;\n    margin-top:3px;\n    border: 1px solid #f1f1f1;\n}\n\n.textHeader\n{\n    color: #337ab7;;\n    font-size: 15px;\n    font-weight: bold;\n}\n\n.SearchInput{\n    background-color: white;\n    text-align: right;\n    paddding:5px;\n    margin-bottom: -15px;\n    margin-top: -15px;\n}\n\n.p-3{\n    margin-top: 2px;\n    margin-bottom: 2px;\n}\n\n.sideButton\n{\n    width:90%;\n    cursor: pointer;\n    background-color: #3b5998;\n    color: white;\n    text-align: right;\n    padding: 3px;\n    overflow: hidden;\n}\n\n.sideButtonText\n{\n    margin-right: 10px;\n    font-size: 14px;\n    font-weight: bold;\n    top: 7px !important;\n    position: relative;\n}\n\n.sideButtonTextEmpty{\n    margin-right: 10px;\n    font-size: 14px;\n    font-weight: bold;\n    top: 0px !important;\n    position: relative;\n}\n\n.sideButtonBadge\n{\n    background-color: red;\n    border-radius:50%;\n    font-size: 12px;\n    margin-top: 5px;\n    padding: 3px;\n    width: 25px;\n    height: 25px;\n}\n\n.buttonDivBadge\n{\n    float: right;\n    width: 12%;\n}\n\n.buttonDivText\n{\n    float: right;\n    width: 60%;\n}\n\n\n\n.buttonDivIcon\n{\n    float: left;\n    width: 20%;\n}\n\n.badgeText\n{\n    top: 4px;\n    position: relative;\n}\n\n\n\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/kitchens/index/index.component.html":
/***/ (function(module, exports) {

module.exports = "\n<div class=\"row nopadding\" style=\"padding: 0px; background-color: white; margin-top: -15px; margin-left: -17px\">\n    <div class=\"col-lg-2 nopadding\" style=\"background-color: #eff1f1; padding: 0px\" >\n        <div style=\"margin-top: 20px; padding: 10px;\" align=\"center\">\n            <button [routerLink]=\"['/', 'kitchen' , 'add' , { id: i}]\"  class=\"btn btn-icon btn-facebook mb-1 mr-1 sideButton\" style=\"background-color: #666\">\n                <div class=\"buttonDivBadge\">\n\n                </div>\n                <div class=\"buttonDivText\">\n                    <span class=\"sideButtonTextEmpty\">הוסף מטבח</span>\n                </div>\n                <div class=\"buttonDivIcon\">\n                    <i class=\"fa fa-chevron-left\"></i>\n                </div>\n            </button>\n\n            <hr>\n        </div>\n    </div>\n    <div class=\"col-lg-10 nopadding\" style=\"margin-top: 10px;\">\n        <div class=\"p-3\" style=\"\">\n            <div style=\"width: 99%; float: right\">\n                <input type=\"text\" (keyup)='updateFilter($event)' class=\"form-control SearchInput\" placeholder=\"חפש מטבח\"  id=\"formGroupExampleInput\" name=\"name\">\n            </div>\n        </div>\n\n        <div class=\"p-3\">\n            <div class=\"card\">\n\n                <div class=\"card-header\">\n                    <div class=\"card-header-text w-100\">\n                        <div class=\"card-title\">\n                            עמוד מטבחים\n                        </div>\n                        <div class=\"card-subtitle text-capitalize ff-sans\">\n                            כרגע יש {{ItemsArray.length}} מטבחים במערכת\n                        </div>\n                    </div>\n                    <button [routerLink]=\"['/', 'kitchen' , 'add' ]\" class=\"btn btn-icon btn-facebook mb-1 mr-1 \" style=\"width: 130px; background-color:#3b5998; cursor: pointer; color: white;  float: left\">\n                        <i class=\"fa fa-plus-circle\"></i>\n                        הוסף מטבח\n                    </button>\n                </div>\n\n                <div class=\"card-body\" *ngFor=\"let item of ItemsArray let i=index\">\n                    <div class=\"row d-flex\">\n                        <div class=\"col-lg-1 \"  align=\"center\">\n                            <img *ngIf=\"item.image != ''\" class=\"titleImage\" src=\"{{host}}{{item.image}}\" style=\"\" />\n                            <img *ngIf=\"item.image == ''\" class=\"titleImage\" src=\"{{avatar}}\" style=\"\" />\n                        </div>\n                        <div class=\"col-lg-7 col-md-9\">\n                            <a href=\"javascript:;\" class=\"textHeader\">{{item.name}}</a>\n                            <div>{{item.phone}}</div>\n                            <div>{{item.address}}</div>\n                        </div>\n                        <div class=\"col-lg-4 col-md-2 d-flex\" align=\"left\" >\n                            <div class=\"col-lg-8 col-md-3\" style=\"margin-top: 15px; cursor: pointer; text-align: center\" align=\"left\">\n                                <div style=\"float: left\"  [routerLink]=\"['/', 'employee' , 'index' , { id:item.index}]\">\n                                    <h6>{{item.EmployeesNumber}}</h6>\n                                    <small class=\"d-block\">מספר עובדים</small>\n                                </div>\n                                <div style=\"float: left; margin-left: 20px;\" [routerLink]=\"['/', 'order' , 'index' , { id:item.index}]\">\n                                    <h6>{{item.Orders}}</h6>\n                                    <small class=\"d-block\">הזמנות</small>\n                                </div>\n                            </div>\n                            <div class=\"col-lg-4 col-md-6\" style=\"padding-left:0px !important; float: left; cursor: pointer;\" align=\"left\">\n                                <button [routerLink]=\"['/', 'kitchen' , 'edit' , { id: i}]\"  class=\"btn btn-icon btn-facebook mb-1 mr-1 \" style=\"width: 100px; cursor: pointer; background-color: #3b5998; color: white\">\n                                    <i class=\"fa fa-edit\"></i>\n                                    Edit\n                                </button>\n                                <button class=\"btn btn-icon btn-instagram mb-1 mr-1 \" style=\"width: 100px; background-color:lightgrey; cursor: pointer;\" (click)=\"DeleteItem(i)\">\n                                    <i class=\"fa fa-close\"></i>\n                                    Delete\n                                </button>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n\n            </div>\n</div>\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n<!--<ngx-datatable\n        class=\"fullscreen\"\n        [columnMode]=\"'force'\"\n        [headerHeight]=\"40\"\n        [footerHeight]=\"0\"\n        [rowHeight]=\"70\"\n        [scrollbarV]=\"true\"\n        [scrollbarH]=\"true\"\n        [rows]=\"ItemsArray\">\n\n  <ngx-datatable-column name=\"icons\" >\n    <ng-template let-column=\"column\" ngx-datatable-header-template>\n      actions\n    </ng-template>\n    <ng-template let-rowIndex=\"rowIndex\" let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\n      <i class=\"fa fa-close IconClass\" style=\"\" (click)=\"DeleteItem()\"></i>\n      <a [routerLink]=\"['/', 'company' , 'index' , { id: rowIndex, foo: 'foo' }]\" class=\"navigation-link relative\"><i class=\"fa fa-index IconClassMt3\" ></i></a>\n    </ng-template>\n  </ngx-datatable-column>\n\n  <ngx-datatable-column name=\"Subdomain\">\n    <ng-template   ngx-datatable-header-template><span >Subdomain</span></ng-template>\n  </ngx-datatable-column>\n  <ngx-datatable-column name=\"Phone\">\n    <ng-template   ngx-datatable-header-template><span >מספר טלפון</span></ng-template>\n  </ngx-datatable-column>\n  <ngx-datatable-column name=\"email\" [width]=\"300\" >\n    <ng-template   ngx-datatable-header-template><span >אימייל</span></ng-template>\n  </ngx-datatable-column>\n  <ngx-datatable-column name=\"Address\" [width]=\"300\" style=\"padding-top:30px !important;\" >\n    <ng-template   ngx-datatable-header-template><span >כתובת</span></ng-template>\n  </ngx-datatable-column>\n  <ngx-datatable-column  [width]=\"120\" >\n    <template let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\n      <img src=\"{{host}}{{row.logo}}\" style=\"width:60px; height: 60px; margin-top: -13px;\" />\n    </template>\n  </ngx-datatable-column>\n\n</ngx-datatable> -->"

/***/ }),

/***/ "../../../../../src/app/kitchens/index/index.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IndexComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__kitchen_service__ = __webpack_require__("../../../../../src/app/kitchens/kitchen.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__settings_settings_service__ = __webpack_require__("../../../../../src/settings/settings.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var IndexComponent = (function () {
    function IndexComponent(KitchenService, settings) {
        var _this = this;
        this.KitchenService = KitchenService;
        this.ItemsArray = [];
        this.ItemsArray1 = [];
        this.host = '';
        this.settings = '';
        this.avatar = '';
        this.KitchenService.GetAllItems('GetAllKitchens').then(function (data) {
            console.log("GetAllKitchens : ", data),
                _this.ItemsArray = data,
                _this.ItemsArray1 = data,
                _this.host = settings.host,
                _this.avatar = settings.avatar,
                console.log(_this.ItemsArray[0].logo);
        });
        this.KitchenService.GetAllKitchens('GetAllKitchens').then(function (data) {
            console.log("GetAllKitchens : ", data);
        });
    }
    IndexComponent.prototype.ngOnInit = function () {
    };
    IndexComponent.prototype.DeleteItem = function (i) {
        var _this = this;
        console.log("Del 1 : ", this.ItemsArray[i].id);
        this.KitchenService.DeleteCompany('DeleteCompany', this.ItemsArray[i].id).then(function (data) {
            _this.ItemsArray = data, console.log("Del 2 : ", data);
        });
    };
    IndexComponent.prototype.updateFilter = function (event) {
        var val = event.target.value;
        // filter our data
        var temp = this.ItemsArray1.filter(function (d) {
            return d.name.toLowerCase().indexOf(val) !== -1 || !val;
        });
        // update the rows
        this.ItemsArray = temp;
    };
    IndexComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-index',
            template: __webpack_require__("../../../../../src/app/kitchens/index/index.component.html"),
            styles: [__webpack_require__("../../../../../src/app/icons/fontawesome/fontawesome.component.scss"), __webpack_require__("../../../../../src/app/media/list/list.component.scss"), __webpack_require__("../../../../../src/app/kitchens/index/index.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__kitchen_service__["a" /* KitchenService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__kitchen_service__["a" /* KitchenService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__settings_settings_service__["a" /* SettingsService */]) === "function" && _b || Object])
    ], IndexComponent);
    return IndexComponent;
    var _a, _b;
}());

//# sourceMappingURL=index.component.js.map

/***/ }),

/***/ "../../../../../src/app/kitchens/kitchen.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "KitchenModule", function() { return KitchenModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__kitchen_routing__ = __webpack_require__("../../../../../src/app/kitchens/kitchen.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_datatable__ = __webpack_require__("../../../../@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_datatable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_datatable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__kitchen_service__ = __webpack_require__("../../../../../src/app/kitchens/kitchen.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__edit_edit_component__ = __webpack_require__("../../../../../src/app/kitchens/edit/edit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__index_index_component__ = __webpack_require__("../../../../../src/app/kitchens/index/index.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__add_add_component__ = __webpack_require__("../../../../../src/app/kitchens/add/add.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ng2_file_upload__ = __webpack_require__("../../../../ng2-file-upload/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













var KitchenModule = (function () {
    function KitchenModule() {
    }
    KitchenModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__kitchen_routing__["a" /* CompanydRoutes */]),
                __WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_datatable__["NgxDatatableModule"],
                __WEBPACK_IMPORTED_MODULE_6__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_10__ng_bootstrap_ng_bootstrap__["g" /* NgbModule */],
                __WEBPACK_IMPORTED_MODULE_11_ng2_file_upload__["FileUploadModule"],
                __WEBPACK_IMPORTED_MODULE_12__angular_forms__["FormsModule"]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_7__edit_edit_component__["a" /* EditComponent */],
                __WEBPACK_IMPORTED_MODULE_8__index_index_component__["a" /* IndexComponent */],
                __WEBPACK_IMPORTED_MODULE_9__add_add_component__["a" /* AddComponent */]
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_5__kitchen_service__["a" /* KitchenService */]]
        })
    ], KitchenModule);
    return KitchenModule;
}());

//# sourceMappingURL=kitchen.module.js.map

/***/ }),

/***/ "../../../../../src/app/kitchens/kitchen.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CompanydRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__edit_edit_component__ = __webpack_require__("../../../../../src/app/kitchens/edit/edit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__add_add_component__ = __webpack_require__("../../../../../src/app/kitchens/add/add.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__index_index_component__ = __webpack_require__("../../../../../src/app/kitchens/index/index.component.ts");



var CompanydRoutes = [{
        path: '',
        children: [{
                path: 'index',
                component: __WEBPACK_IMPORTED_MODULE_2__index_index_component__["a" /* IndexComponent */],
                data: {
                    heading: 'Company'
                }
            }, {
                path: 'index',
                component: __WEBPACK_IMPORTED_MODULE_0__edit_edit_component__["a" /* EditComponent */],
                data: {
                    heading: 'Edit Company'
                }
            }, {
                path: 'add',
                component: __WEBPACK_IMPORTED_MODULE_1__add_add_component__["a" /* AddComponent */],
                data: {
                    heading: 'Edit Company'
                }
            }]
    }];
/*

component: CompanyComponent,
    data: {
        heading: 'Company',
        removeFooter: true
    },



*/ 
//# sourceMappingURL=kitchen.routing.js.map

/***/ }),

/***/ "../../../../../src/app/kitchens/kitchen.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return KitchenService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__("../../../../rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__settings_settings_service__ = __webpack_require__("../../../../../src/settings/settings.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//public this.ServerUrl = "";//http://www.tapper.co.il/salecar/laravel/public/api/";
var KitchenService = (function () {
    function KitchenService(http, settings) {
        this.http = http;
        this.ServerUrl = "";
        this.headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/x-www-form-urlencoded' });
        this.options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["f" /* RequestOptions */]({ headers: this.headers });
        this.Items = [];
        this.Kitchens = [];
        this.ServerUrl = settings.ServerUrl;
    }
    ;
    KitchenService.prototype.GetAllItems = function (url) {
        var _this = this;
        var body = new FormData();
        body.append('uid', window.localStorage.identify);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { _this.Items = data; }).toPromise();
    };
    KitchenService.prototype.GetAllKitchens = function (url) {
        var _this = this;
        var body = new FormData();
        body.append('uid', window.localStorage.identify);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { _this.Kitchens = data; }).toPromise();
    };
    KitchenService.prototype.AddItem = function (url, Items) {
        this.Items = Items;
        var body = 'items=' + JSON.stringify(this.Items);
        return this.http.post(this.ServerUrl + '' + url, body, this.options).map(function (res) { return res; }).do(function (data) { }).toPromise();
    };
    KitchenService.prototype.EditItem = function (url, Items) {
        this.Items = Items;
        var body = 'items=' + JSON.stringify(this.Items);
        return this.http.post(this.ServerUrl + '' + url, body, this.options).map(function (res) { return res.json(); }).do(function (data) { }).toPromise();
    };
    KitchenService.prototype.DeleteCompany = function (url, Id) {
        var body = 'id=' + Id;
        return this.http.post(this.ServerUrl + '' + url, body, this.options).map(function (res) { return res.json(); }).do(function (data) { }).toPromise();
    };
    KitchenService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__settings_settings_service__["a" /* SettingsService */]) === "function" && _b || Object])
    ], KitchenService);
    return KitchenService;
    var _a, _b;
}());

;
//# sourceMappingURL=kitchen.service.js.map

/***/ })

});
//# sourceMappingURL=kitchen.module.chunk.js.map