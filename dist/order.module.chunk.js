webpackJsonp(["order.module"],{

/***/ "../../../../../src/app/orders/add/add.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".FormClass\n{\n    text-align: right;\n    direction: rtl;\n}\n\n.row\n{\n    margin-top: 20px;\n}\n\n.textWhite\n{\n    background-color: white;\n}\n\ninput.ng-invalid.ng-touched\n{\n    border:1px solid red;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/orders/add/add.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row FormClass\">\n  <div class=\"col-lg-12\">\n    <div class=\"card\">\n      <div class=\"card-header\">\n        הוסף חברה\n      </div>\n      <div class=\"card-body\">\n        <form (ngSubmit)=\"onSubmit(f)\" #f=\"ngForm\">\n          <div class=\"row\">\n            <div class=\"form-group\" class=\"col-lg-12\">\n              <label for=\"formGroupExampleInput\">הכנס שם חברה </label>\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" name=\"name\" ngModel required>\n            </div>\n          </div>\n\n          <div class=\"row\">\n            <div class=\"form-group\" class=\"col-lg-6\">\n              <label for=\"formGroupExampleInput\">הכנס כתובת </label>\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" name=\"address\"\n                     ngModel required>\n            </div>\n            <div class=\"form-group\" class=\"col-lg-6\">\n              <label for=\"formGroupExampleInput2\">הכנס טלפון</label>\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput2\" name=\"phone\"\n                     ngModel required>\n            </div>\n          </div>\n\n          <div class=\"row\">\n            <div class=\"form-group\" class=\"col-lg-6\">\n              <label for=\"formGroupExampleInput\">הזן סיסמה</label>\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" name=\"password\" ngModel required>\n            </div>\n            <div class=\"form-group\" class=\"col-lg-6\">\n              <label for=\"formGroupExampleInput2\">הכנס אימייל</label>\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput2\" name=\"email\" ngModel required email>\n            </div>\n          </div>\n\n          <div class=\"row\">\n            <div class=\"form-group\" class=\"col-lg-6\">\n              <label for=\"formGroupExampleInput\">הכנס אתר אינטרנט</label>\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" name=\"website\"\n                     ngModel>\n            </div>\n            <div class=\"form-group\" class=\"col-lg-6\">\n              <label for=\"formGroupExampleInput2\">הכנס WAZE</label>\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput2\" name=\"waze\"\n                     ngModel>\n            </div>\n          </div>\n          <div class=\"row\">\n            <div class=\"form-group\" class=\"col-lg-6\">\n              <label for=\"formGroupExampleInput\">הכנס פייסבוק</label>\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" name=\"facebook\"\n                     ngModel>\n            </div>\n            <div class=\"form-group\" class=\"col-lg-6\">\n              <label for=\"formGroupExampleInput2\">הכנס subDomain</label>\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput2\" name=\"subdomain\"\n                     ngModel required>\n            </div>\n          </div>\n          <div class=\"row\">\n            <div class=\"form-group\" class=\"col-lg-12\">\n              <label for=\"formGroupExampleInput\">פרטים נוספים</label>\n              <textarea  rows=\"4\" cols=\"50\"  class=\"form-control textWhite\" id=\"formGroupExampleInput\" name=\"description\" ngModel required> </textarea>\n            </div>\n          </div>\n          <div class=\"row\">\n            <label class=\"uploader\">\n              <img [src]=\"imageSrc\"  [class.loaded]=\"imageLoaded\" style=\"width: 100px;\"/>\n              <input type=\"file\" name=\"file\" accept=\"image/*\" (change)=\"handleInputChange($event)\">\n            </label>\n          </div>\n          <div class=\"row\" align=\"left\" style=\"float: left; margin-left: 5px\">\n            <button [disabled]=\"!f.valid\" type=\"submit\" class=\"btn btn-primary btn-icon loading-demo mr-1 mb-1\"\n                    style=\"padding:0 20px !important; cursor: pointer; width: 205px; text-align: center; font-weight: bold; font-size: 16px\">\n              <span style=\"text-align: center; padding: 10px;  width: 100% !important;\">שלח טופס</span>\n            </button>\n          </div>\n        </form>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/orders/add/add.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__order_service__ = __webpack_require__("../../../../../src/app/orders/order.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AddComponent = (function () {
    function AddComponent(route, http, service, router) {
        this.route = route;
        this.http = http;
        this.service = service;
        this.router = router;
        this.navigateTo = '/company/index';
        this.imageSrc = '';
    }
    AddComponent.prototype.onSubmit = function (form) {
        var _this = this;
        console.log(form.value);
        this.service.AddCompany('AddCompany1', form.value).then(function (data) {
            console.log("AddCompany : ", data);
            _this.router.navigate([_this.navigateTo]);
        });
    };
    AddComponent.prototype.handleInputChange = function (e) {
        var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
        var pattern = /image-*/;
        var reader = new FileReader();
        if (!file.type.match(pattern)) {
            alert('invalid format');
            return;
        }
        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsDataURL(file);
        console.log("2 : ", reader);
    };
    AddComponent.prototype._handleReaderLoaded = function (e) {
        var reader = e.target;
        this.imageSrc = reader.result;
        console.log("3 : ", this.imageSrc);
        /* this.service.AddCompany('AddCompany1',form.value).then((data: any) => {
             console.log("AddCompany : " , data);
             this.router.navigate([this.navigateTo]);
         });*/
    };
    AddComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-add',
            template: __webpack_require__("../../../../../src/app/orders/add/add.component.html"),
            styles: [__webpack_require__("../../../../../src/app/orders/add/add.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__order_service__["a" /* OrderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__order_service__["a" /* OrderService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _d || Object])
    ], AddComponent);
    return AddComponent;
    var _a, _b, _c, _d;
}());

//# sourceMappingURL=add.component.js.map

/***/ }),

/***/ "../../../../../src/app/orders/edit/edit.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".FormClass\n{\n    text-align: right;\n    direction: rtl;\n}\n\n.row\n{\n    margin-top: 20px;\n}\n\n.textWhite\n{\n    background-color: white;\n}\n\ninput.ng-invalid.ng-touched\n{\n    border:1px solid red;\n}\n\n.SearchInput\n{\n    background-color: white;\n    text-align: right;\n}\n\n.p-3\n{\n    padding: 0px;\n    background-color: red;\n}\n\n.KitchensForm\n{\n    direction: rtl;\n    text-align: right;\n}\n\n.formCheck\n{\n    position: relative;\n    left:20px;\n    text-align: right;\n    width: 50px;\n    background-color: red;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/orders/edit/edit.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row FormClass\">\n    <div class=\"col-lg-12\">\n        <div class=\"card\">\n            <div class=\"card-header\">\n                ערוך חברה\n            </div>\n            <div class=\"card-body\">\n                <form (ngSubmit)=\"onSubmit(f)\" #f=\"ngForm\" >\n                    <div class=\"row\">\n                        <div class=\"form-group\" class=\"col-lg-12\">\n                            <label for=\"formGroupExampleInput\">הכנס שם חברה </label>\n                            <input type=\"text\" class=\"form-control textWhite\" [(ngModel)]=\"Company.name\"\n                                   id=\"formGroupExampleInput\" name=\"name\" ngModel required>\n                        </div>\n\n                    </div>\n\n                    <div class=\"row\">\n                        <div class=\"form-group\" class=\"col-lg-6\">\n                            <label for=\"formGroupExampleInput\">הכנס כתובת </label>\n                            <input type=\"text\" class=\"form-control textWhite\" [(ngModel)]=\"Company.address\"\n                                   id=\"formGroupExampleInput\" name=\"address\"\n                                   ngModel>\n                        </div>\n                        <div class=\"form-group\" class=\"col-lg-6\">\n                            <label for=\"formGroupExampleInput2\">הכנס טלפון</label>\n                            <input type=\"text\" class=\"form-control textWhite\" [(ngModel)]=\"Company.phone\"\n                                   id=\"formGroupExampleInput2\" name=\"phone\"\n                                   ngModel>\n                        </div>\n                    </div>\n\n\n                    <div class=\"row\">\n                        <div class=\"form-group\" class=\"col-lg-6\">\n                            <label for=\"formGroupExampleInput\">הזן סיסמה</label>\n                            <input type=\"text\" class=\"form-control textWhite\" [(ngModel)]=\"Company.password\"\n                                   id=\"formGroupExampleInput\" name=\"pass\" ngModel>\n                        </div>\n                        <div class=\"form-group\" class=\"col-lg-6\">\n                            <label for=\"formGroupExampleInput2\">הכנס אימייל</label>\n                            <input type=\"text\" class=\"form-control textWhite\" [(ngModel)]=\"Company.email\"\n                                   id=\"formGroupExampleInput2\" name=\"email\" ngModel required email>\n                        </div>\n                    </div>\n\n                    <div class=\"row\">\n                        <div class=\"form-group\" class=\"col-lg-6\">\n                            <label for=\"formGroupExampleInput\">מחיר מנה</label>\n                            <input type=\"text\" class=\"form-control textWhite\" [(ngModel)]=\"Company.company_price\"\n                                   id=\"formGroupExampleInput\" name=\"web\"\n                                   ngModel>\n                        </div>\n                        <div class=\"form-group\" class=\"col-lg-6\">\n\n                        </div>\n                    </div>\n\n                    <hr>\n                    <div>\n                        <p>מטבחים השייכים לחברה</p>\n                        <label class=\"KitchensForm custom-control custom-radio\" style=\"overflow: hidden; margin-right: 50px;\">\n                            <label class=\"form-check-label\" *ngFor=\"let kitchen of Kitchens let i=index\"  style=\"margin-right: 20px;\">\n                                <input type=\"checkbox\" name=\"kitchen\"  (change)=\"changeKitchen(kitchen.index)\" [checked]=\"checkKitchen(i) == 1\" class=\"formCheck\"  style=\"background-color: red !important;\">\n                                <span style=\"position: relative; left: 30px; margin-top: -5px;\">{{kitchen.name}}</span>\n                            </label>\n                        </label>\n                    </div>\n                    <hr>\n\n                    <div class=\"row\">\n                        <div class=\"form-group\" class=\"col-lg-12\">\n                            <label for=\"formGroupExampleInput\">פרטים נוספים</label>\n                            <textarea rows=\"4\" cols=\"50\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" [(ngModel)]=\"Company.desc\" name=\"description\" ngModel required> </textarea>\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                        <label class=\"uploader\">\n                            <img [src]=\"imageSrc\" [class.loaded]=\"imageLoaded\" style=\"width: 100px;\"/>\n                            <input type=\"file\" name=\"file\" accept=\"image/*\" (change)=\"handleInputChange($event)\">\n                        </label>\n                    </div>\n                    <div class=\"row\" align=\"left\" style=\"float: left; margin-left: 5px\">\n                        <button [disabled]=\"!f.valid\" type=\"submit\"\n                                class=\"btn btn-primary btn-icon loading-demo mr-1 mb-1\"\n                                style=\"padding:0 20px !important; cursor: pointer; width: 205px; text-align: center; font-weight: bold; font-size: 16px\">\n                            <span style=\"text-align: center; padding: 10px;  width: 100% !important;\">שלח טופס</span>\n                        </button>\n                    </div>\n                </form>\n\n\n                <!-- <img src=\"{{imageSrc}}\" style=\"width: 100%\" /> <div class=\"row\" align=\"left\"  style=\"float: left; margin-left: 5px\">\n                     <button type=\"button\"  class=\"btn btn-primary btn-icon loading-demo mr-1 mb-1\" style=\"padding:0 20px !important; cursor: pointer; width: 205px; text-align: center; font-weight: bold; font-size: 16px\">\n                         <span style=\"text-align: center; padding: 10px;  width: 100% !important;\">שלח טופס</span>\n                     </button>\n                 </div>-->\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/orders/edit/edit.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__order_service__ = __webpack_require__("../../../../../src/app/orders/order.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var URL = 'https://evening-anchorage-3159.herokuapp.com/api/';
var EditComponent = (function () {
    function EditComponent(route, http, service, router) {
        var _this = this;
        this.route = route;
        this.http = http;
        this.service = service;
        this.router = router;
        this.navigateTo = '/company/index';
        this.imageSrc = '';
        this.Company = [];
        this.CompanyKitchens = [];
        this.Kitchens = [];
        this.route.params.subscribe(function (params) {
            _this.Id = params['id'];
            _this.Company = _this.service.Companies[_this.Id];
            _this.Kitchens = _this.service.Kitchens;
            _this.CompanyKitchens = _this.Company['kitchens'];
            console.log("fkitchens ", _this.CompanyKitchens);
        });
    }
    EditComponent.prototype.onSubmit = function (form) {
        var _this = this;
        console.log("Edit : ", this.Company);
        this.service.EditCompany('EditCompany', this.Company).then(function (data) {
            console.log("AddCompany : ", data);
            _this.router.navigate([_this.navigateTo]);
        });
    };
    EditComponent.prototype.handleInputChange = function (e) {
        var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
        var pattern = /image-*/;
        var reader = new FileReader();
        if (!file.type.match(pattern)) {
            alert('invalid format');
            return;
        }
        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsDataURL(file);
        console.log("2 : ", reader);
    };
    EditComponent.prototype._handleReaderLoaded = function (e) {
        var reader = e.target;
        this.imageSrc = reader.result;
        console.log("3 : ", this.imageSrc);
    };
    EditComponent.prototype.checkKitchen = function (id) {
        var checked = 0;
        var kitchens1 = this.Company["kitchens"];
        for (var i = 0; i < kitchens1.length; i++) {
            if (kitchens1[i]['resturant_id'] == this.Kitchens[id]['index'])
                checked = 1;
        }
        return checked;
    };
    EditComponent.prototype.changeKitchen = function (id) {
        var exsist = 0;
        console.log("fkitchens1 ", this.CompanyKitchens);
        /*for(let i=0;i<this.CompanyKitchens.length; i++)
        {
            console.log(this.CompanyKitchens[i]['resturant_id']  + " : " + id)
            if(this.CompanyKitchens[i]['resturant_id'] == id)
            {
                console.log("splice" , this.CompanyKitchens);
                this.CompanyKitchens.splice(i, 1);
                exsist = 1;
                console.log("splice1" , this.CompanyKitchens);
            }
        }

        if(exsist == 0)
        {
            for(let j=0;j<this.Kitchens.length; j++)
            {
                console.log("Kitchen " , this.Kitchens[j]['index'] + " : "  + id );
                if(this.Kitchens[j]['index'] == id)
                {
                    this.CompanyKitchens.push(this.Kitchens[j])
                    console.log("splice2" , this.CompanyKitchens);
                }

            }
        }*/
    };
    EditComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-edit',
            template: __webpack_require__("../../../../../src/app/orders/edit/edit.component.html"),
            styles: [__webpack_require__("../../../../../src/app/icons/fontawesome/fontawesome.component.scss"), __webpack_require__("../../../../../src/app/components/buttons/buttons.component.scss"), __webpack_require__("../../../../../src/app/orders/edit/edit.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__order_service__["a" /* OrderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__order_service__["a" /* OrderService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _d || Object])
    ], EditComponent);
    return EditComponent;
    var _a, _b, _c, _d;
}());

//# sourceMappingURL=edit.component.js.map

/***/ }),

/***/ "../../../../../src/app/orders/index/index.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".card\n{\n    text-align: right;\n    direction: rtl;\n\n}\n\n.card-body\n{\n    border-bottom: 1px solid #f2f1f2;\n}\n\n.mr-auto\n{\n    text-align: right;\n    direction: rtl;\n    background-color: red;\n    float: right;\n}\n\n.mr-3\n{\n    background-color: green;\n    float: right;\n}\n\n.IconClass\n{\n    margin-top: 6px;\n    text-align: center;\n    padding-left: -13px !important;\n}\n\n.d-icon{\n    margin-top: -20px;\n}\n\n.titleImage\n{\n    width: 80px;\n    border-radius: 70%;\n    height:80px;\n    margin-top:3px;\n    border: 1px solid #f1f1f1;\n}\n\n.textHeader\n{\n    color: #337ab7;;\n    font-size: 15px;\n    font-weight: bold;\n}\n\n.SearchInput{\n    background-color: white;\n    text-align: right;\n    paddding:5px;\n    margin-bottom: -15px;\n    margin-top: -15px;\n}\n\n.p-3{\n    margin-top: 2px;\n    margin-bottom: 2px;\n}\n\n.sideButton\n{\n    width:90%;\n    cursor: pointer;\n    background-color: #3b5998;\n    color: white;\n    text-align: right;\n    padding: 3px;\n    overflow: hidden;\n}\n\n.sideButtonText\n{\n    margin-right: 10px;\n    font-size: 14px;\n    font-weight: bold;\n    top: 7px !important;\n    position: relative;\n}\n\n.sideButtonTextEmpty{\n    margin-right: 10px;\n    font-size: 14px;\n    font-weight: bold;\n    top: 0px !important;\n    position: relative;\n}\n\n.sideButtonBadge\n{\n    background-color: red;\n    border-radius:50%;\n    font-size: 12px;\n    margin-top: 5px;\n    padding: 3px;\n    width: 25px;\n    height: 25px;\n}\n\n.buttonDivBadge\n{\n    float: right;\n    width: 12%;\n}\n\n.buttonDivText\n{\n    float: right;\n    width: 60%;\n}\n\n\n\n.buttonDivIcon\n{\n    float: left;\n    width: 20%;\n}\n\n.badgeText\n{\n    top: 4px;\n    position: relative;\n}\n\n.fullscreen .datatable-row-center datatable-row-group\n{\n    border-bottom: 20px solid red;\n}\n\n.material {\n    vertical-align: bottom;\n    padding: 8px;\n    line-height: 1.42857143;\n    font-weight: bold;\n    color: rgb(51, 51, 51);\n    background-color: #ffffff;\n}\n\n.striped {\n    vertical-align: bottom;\n    padding: 8px;\n    line-height: 1.42857143;\n    font-weight: bold;\n    color: rgb(51, 51, 51);\n    height: 2000px;\n}\n\n.datatable-row-center\n{\n    float: right !important;\n    background-color: red !important;\n}\n.Rclass\n{\n    background-color: red !important;;\n}\n\n.Cell\n{\n    background-color: red !important;;\n}\n\n.getRowClass\n{\n    background-color: red !important;\n}\n\n\n.datatable,\n.datatable > div,\n.datatable.fixed-header .datatable-header .datatable-header-inner {\n    height: 100%;\n    direction: rtl;\n    text-align: right;\n    background-color: red;\n}\n\ndiv.dataTables_wrapper {\n    direction: rtl;\n    background-color: red;\n}\n\n/* Ensure that the demo table scrolls */\nth, td { white-space: nowrap; }\ndiv.dataTables_wrapper {\n    width: 800px;\n    margin: 0 auto;\n    background-color: red;\n}\n\n.datatable-header-inner\n{\n    direction: rtl;\n    text-align: right;\n    background-color: red;\n}\n\n.divRow\n{\n\n}\n\n.datatable-body-row.active .datatable-row-group {\n    background-color: red !important;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/orders/index/index.component.html":
/***/ (function(module, exports) {

module.exports = "\n<div class=\"row nopadding\" style=\"padding: 0px; background-color: white; margin-top: -15px; margin-left: -17px\">\n    <div class=\"col-lg-2 nopadding\" style=\"background-color: #eff1f1; padding: 0px\" >\n        <div style=\"margin-top: 20px; padding: 10px;\" align=\"center\">\n            <button [routerLink]=\"['/', 'company' , 'index']\"  class=\"btn btn-icon btn-facebook mb-1 mr-1 sideButton\" style=\"background-color: #666\">\n                <div class=\"buttonDivBadge\">\n\n                </div>\n                <div class=\"buttonDivText\">\n                    <span class=\"sideButtonTextEmpty\">חזר לעמוד חברות</span>\n                </div>\n                <div class=\"buttonDivIcon\">\n                    <i class=\"fa fa-chevron-left\"></i>\n                </div>\n            </button>\n\n            <hr>\n\n\n        </div>\n    </div>\n    <div class=\"col-lg-10 nopadding\" style=\"margin-top: 10px; height: 1000px\">\n        <div class=\"row\">\n            <div class=\"col-lg-12\">\n                <div class=\"col-lg-12\">\n                    <input type=\"text\" class=\"form-control mb-3\" placeholder=\"חפש חברה\" style=\"text-align: right; padding: 10px;\" required (keyup)='updateFilter($event)'>\n                </div>\n            </div>\n        </div>\n        <div class=\"row\" style=\"margin-top: 10px; height: 1000px\">\n        <ngx-datatable\n                class=\"striped\"\n                [columnMode]=\"'force'\"\n                [headerHeight]=\"40\"\n                [footerHeight]=\"0\"\n                [rowHeight]=\"70\"\n                [scrollbarV]=\"true\"\n                [scrollbarH]=\"true\"\n                [rowClass]=\"getRowClass\"\n                (sort)=\"onSort($event)\"\n                [rows]=\"ItemsArray\">\n\n           <!-- <ngx-datatable-column name=\"icons\" [width]=\"100\" >\n                <ng-template let-column=\"column\" ngx-datatable-header-template>\n                    actions\n                </ng-template>\n                <ng-template let-rowIndex=\"rowIndex\" let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\n                    <i class=\"fa fa-close IconClass\" style=\"\" (click)=\"DeleteItem()\"></i>\n                    <a [routerLink]=\"['/', 'company' , 'index' , { id: rowIndex, foo: 'foo' }]\" class=\"navigation-link relative\"><i class=\"fa fa-index IconClassMt3\" ></i></a>\n                </ng-template>\n            </ngx-datatable-column> -->\n            <ngx-datatable-column name=\"תאריך הזמנה\" prop=\"order_date\" [sortable]=\"true\" headerClass=\"Rclass\" cellClass=\"Cell\" [width]=\"130\" style=\"padding-top:30px !important; background-color: red\" >\n                <ng-template  let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\n                    <div class=\"divRow\">{{row.order_date}}</div>\n                </ng-template>\n            </ngx-datatable-column>\n            <ngx-datatable-column  [sortable]=\"true\" style=\"padding-top:30px !important;\" [width]=\"100\" >\n                <ng-template   ngx-datatable-header-template><span >שתייה</span></ng-template>\n                <template  let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\n                    <div *ngIf=\"row.DrinkArr[0]\">{{row.DrinkArr[0].name}}</div>\n                    <div *ngIf=\"!row.DrinkArr[0]\">---</div>\n                </template>\n            </ngx-datatable-column>\n            <ngx-datatable-column   [sortable]=\"true\" style=\"padding-top:30px !important;\" [width]=\"100\"  >\n                <ng-template   ngx-datatable-header-template><span >תוספת שנייה</span></ng-template>\n                <template  let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\n                    <div *ngIf=\"row.Mod2firstArr[0]\">{{row.Mod2firstArr[0].name}}</div>\n                    <div *ngIf=\"!row.Mod2firstArr[0]\">לא נבחר</div>\n                </template>\n            </ngx-datatable-column>\n            <ngx-datatable-column  [sortable]=\"true\" style=\"padding-top:30px !important;\" >\n                <ng-template   ngx-datatable-header-template><span >תוספת ראשונה</span></ng-template>\n                <template  let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\n                    <div *ngIf=\"row.Mod1Arr[0]\">{{row.Mod1Arr[0].name}}</div>\n                    <div *ngIf=\"!row.Mod1Arr[0]\">לא נבחר</div>\n                </template>\n            </ngx-datatable-column>\n            <ngx-datatable-column   style=\"padding-top:30px !important;\" [width]=\"150\">\n                <ng-template   ngx-datatable-header-template><span >מנה עיקרית</span></ng-template>\n                <template  let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\n                    <div *ngIf=\"row.MainArr[0]\">{{row.MainArr[0].name}}</div>\n                    <div *ngIf=\"!row.MainArr[0]\">לא נבחר</div>\n                </template>\n            </ngx-datatable-column>\n            <ngx-datatable-column name=\"סוג הזמנה\" prop=\"FoodTypeName\"  [sortable]=\"true\" [width]=\"100\" style=\"padding-top:30px !important;\" >\n                <template let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\n                    <div *ngIf=\"row.FoodTypeName\">{{row.FoodTypeName}}</div>\n                    <div *ngIf=\"!row.FoodTypeName\">לא נבחר</div>\n                </template>\n            </ngx-datatable-column>\n            <ngx-datatable-column  name=\"מסעדה\" prop=\"RestaurantName.name\"  style=\"padding-top:30px !important;\" [width]=\"120\" >\n                <template let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\n                    <div *ngIf=\"row.RestaurantName\">{{row.RestaurantName[0].name}}</div>\n                    <div *ngIf=\"!row.RestaurantName\">לא נבחר</div>\n                </template>\n            </ngx-datatable-column>\n            <ngx-datatable-column  style=\"padding-top:30px !important;\" [width]=\"120\" >\n                <ng-template   ngx-datatable-header-template><span >שם לקוח</span></ng-template>\n                <template let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\n                    <div *ngIf=\"row.MyUserId\">{{row.MyUserId[0].name}}</div>\n                    <div *ngIf=\"!row.MyUserId\">לא נבחר</div>\n                </template>\n            </ngx-datatable-column>\n            <ngx-datatable-column name=\"שם חברה\" prop=\"CompanyName[0].name\" style=\"padding-top:30px !important;\" [width]=\"120\">\n                <template let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\n                    <div>{{row.CompanyName[0].name}}</div>\n                </template>\n            </ngx-datatable-column>\n            <ngx-datatable-column name=\"מספר הזמנה\" prop=\"index\" style=\"padding-top:30px !important;\" [width]=\"80\">\n                <template let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\n                    <div>{{row.index}}</div>\n                </template>\n            </ngx-datatable-column>\n        </ngx-datatable>\n        </div>\n    </div>\n</div>\n\n\n\n\n\n\n"

/***/ }),

/***/ "../../../../../src/app/orders/index/index.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IndexComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__order_service__ = __webpack_require__("../../../../../src/app/orders/order.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__settings_settings_service__ = __webpack_require__("../../../../../src/settings/settings.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var IndexComponent = (function () {
    function IndexComponent(OrderService, settings, route) {
        var _this = this;
        this.OrderService = OrderService;
        this.settings = settings;
        this.route = route;
        this.ItemsArray = [];
        this.ItemsArray1 = [];
        this.host = '';
        this.route.params.subscribe(function (params) {
            _this.Id = params['id'];
            console.log("ssss : ", _this.Id);
            _this.OrderService.GetOrders('GetOrders', _this.Id).then(function (data) {
                console.log("Orders : ", data),
                    _this.ItemsArray = data,
                    _this.ItemsArray1 = data,
                    _this.host = settings.host;
                //console.log(this.ItemsArray[0].logo)
            });
        });
        this.OrderService.GetAllKitchens('GetAllKitchens').then(function (data) {
            console.log("GetAllKitchens : ", data);
        });
    }
    IndexComponent.prototype.ngOnInit = function () {
    };
    IndexComponent.prototype.DeleteItem = function (i) {
        console.log("Del 1 : ", this.ItemsArray[i].id);
        /* this.OrderService.DeleteCompany('DeleteCompany', this.ItemsArray[i].id).then((data: any) => {
             this.ItemsArray = data , console.log("Del 2 : ", data);
         })*/
    };
    IndexComponent.prototype.updateFilter = function (event) {
        /* const val = event.target.value;
         // filter our data
         const temp = this.ItemsArray1.filter(function (d) {
             return d.CompanyName[0]['name'].toLowerCase().indexOf(val) !== -1 || !val;
         });
         // update the rows
         this.ItemsArray = temp;*/
    };
    IndexComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-index',
            template: __webpack_require__("../../../../../src/app/orders/index/index.component.html"),
            styles: [__webpack_require__("../../../../../src/app/icons/fontawesome/fontawesome.component.scss"), __webpack_require__("../../../../../src/app/media/list/list.component.scss"), __webpack_require__("../../../../../src/app/orders/index/index.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__order_service__["a" /* OrderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__order_service__["a" /* OrderService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__settings_settings_service__["a" /* SettingsService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* ActivatedRoute */]) === "function" && _c || Object])
    ], IndexComponent);
    return IndexComponent;
    var _a, _b, _c;
}());

//# sourceMappingURL=index.component.js.map

/***/ }),

/***/ "../../../../../src/app/orders/order.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderModule", function() { return OrderModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__order_routing__ = __webpack_require__("../../../../../src/app/orders/order.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_datatable__ = __webpack_require__("../../../../@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_datatable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_datatable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__order_service__ = __webpack_require__("../../../../../src/app/orders/order.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__edit_edit_component__ = __webpack_require__("../../../../../src/app/orders/edit/edit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__index_index_component__ = __webpack_require__("../../../../../src/app/orders/index/index.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__add_add_component__ = __webpack_require__("../../../../../src/app/orders/add/add.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ng2_file_upload__ = __webpack_require__("../../../../ng2-file-upload/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













var OrderModule = (function () {
    function OrderModule() {
    }
    OrderModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__order_routing__["a" /* OrderRoutes */]),
                __WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_datatable__["NgxDatatableModule"],
                __WEBPACK_IMPORTED_MODULE_6__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_10__ng_bootstrap_ng_bootstrap__["g" /* NgbModule */],
                __WEBPACK_IMPORTED_MODULE_11_ng2_file_upload__["FileUploadModule"],
                __WEBPACK_IMPORTED_MODULE_12__angular_forms__["FormsModule"]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_7__edit_edit_component__["a" /* EditComponent */],
                __WEBPACK_IMPORTED_MODULE_8__index_index_component__["a" /* IndexComponent */],
                __WEBPACK_IMPORTED_MODULE_9__add_add_component__["a" /* AddComponent */]
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_5__order_service__["a" /* OrderService */]]
        })
    ], OrderModule);
    return OrderModule;
}());

//# sourceMappingURL=order.module.js.map

/***/ }),

/***/ "../../../../../src/app/orders/order.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__edit_edit_component__ = __webpack_require__("../../../../../src/app/orders/edit/edit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__add_add_component__ = __webpack_require__("../../../../../src/app/orders/add/add.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__index_index_component__ = __webpack_require__("../../../../../src/app/orders/index/index.component.ts");



var OrderRoutes = [{
        path: '',
        children: [{
                path: 'index',
                component: __WEBPACK_IMPORTED_MODULE_2__index_index_component__["a" /* IndexComponent */],
                data: {
                    heading: 'Company'
                }
            }, {
                path: 'index',
                component: __WEBPACK_IMPORTED_MODULE_0__edit_edit_component__["a" /* EditComponent */],
                data: {
                    heading: 'Edit Company'
                }
            }, {
                path: 'add',
                component: __WEBPACK_IMPORTED_MODULE_1__add_add_component__["a" /* AddComponent */],
                data: {
                    heading: 'Edit Company'
                }
            }]
    }];
//# sourceMappingURL=order.routing.js.map

/***/ }),

/***/ "../../../../../src/app/orders/order.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__("../../../../rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__settings_settings_service__ = __webpack_require__("../../../../../src/settings/settings.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//public this.ServerUrl = "";//http://www.tapper.co.il/salecar/laravel/public/api/";
var OrderService = (function () {
    function OrderService(http, settings) {
        this.http = http;
        this.ServerUrl = "";
        this.headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/x-www-form-urlencoded' });
        this.options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["f" /* RequestOptions */]({ headers: this.headers });
        this.Orders = [];
        this.Companies = [];
        this.CompanyArray = [];
        this.Kitchens = [];
        this.ServerUrl = settings.ServerUrl;
    }
    ;
    OrderService.prototype.GetOrders = function (url, CompanyId) {
        var _this = this;
        var body = new FormData();
        body.append('uid', window.localStorage.identify);
        body.append('cid', CompanyId);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { _this.Orders = data; }).toPromise();
    };
    OrderService.prototype.GetAllKitchens = function (url) {
        var _this = this;
        var body = new FormData();
        body.append('uid', window.localStorage.identify);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { _this.Kitchens = data; }).toPromise();
    };
    OrderService.prototype.AddCompany = function (url, Company) {
        this.CompanyArray = Company;
        var body = 'company=' + JSON.stringify(this.CompanyArray);
        return this.http.post(this.ServerUrl + '' + url, body, this.options).map(function (res) { return res; }).do(function (data) { }).toPromise();
    };
    OrderService.prototype.EditCompany = function (url, Company) {
        this.CompanyArray = Company;
        var body = 'company=' + JSON.stringify(this.CompanyArray);
        return this.http.post(this.ServerUrl + '' + url, body, this.options).map(function (res) { return res; }).do(function (data) { }).toPromise();
    };
    OrderService.prototype.DeleteCompany = function (url, Id) {
        var body = 'id=' + Id;
        return this.http.post(this.ServerUrl + '' + url, body, this.options).map(function (res) { return res.json(); }).do(function (data) { }).toPromise();
    };
    OrderService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__settings_settings_service__["a" /* SettingsService */]) === "function" && _b || Object])
    ], OrderService);
    return OrderService;
    var _a, _b;
}());

;
//# sourceMappingURL=order.service.js.map

/***/ })

});
//# sourceMappingURL=order.module.chunk.js.map