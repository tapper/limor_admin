webpackJsonp(["Main.module.4"],{

/***/ "../../../../../src/app/orders_new/Main.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainModule", function() { return MainModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Main_routing__ = __webpack_require__("../../../../../src/app/orders_new/Main.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_datatable__ = __webpack_require__("../../../../@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_datatable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_datatable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__MainService_service__ = __webpack_require__("../../../../../src/app/orders_new/MainService.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__edit_edit_component__ = __webpack_require__("../../../../../src/app/orders_new/edit/edit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__index_index_component__ = __webpack_require__("../../../../../src/app/orders_new/index/index.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__add_add_component__ = __webpack_require__("../../../../../src/app/orders_new/add/add.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ng2_file_upload__ = __webpack_require__("../../../../ng2-file-upload/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













var MainModule = (function () {
    function MainModule() {
    }
    MainModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__Main_routing__["a" /* MaindRoutes */]),
                __WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_datatable__["NgxDatatableModule"],
                __WEBPACK_IMPORTED_MODULE_6__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_10__ng_bootstrap_ng_bootstrap__["g" /* NgbModule */],
                __WEBPACK_IMPORTED_MODULE_11_ng2_file_upload__["FileUploadModule"],
                __WEBPACK_IMPORTED_MODULE_12__angular_forms__["FormsModule"]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_7__edit_edit_component__["a" /* EditComponent */],
                __WEBPACK_IMPORTED_MODULE_8__index_index_component__["a" /* IndexComponent */],
                __WEBPACK_IMPORTED_MODULE_9__add_add_component__["a" /* AddComponent */]
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_5__MainService_service__["a" /* MainService */]]
        })
    ], MainModule);
    return MainModule;
}());

//# sourceMappingURL=Main.module.js.map

/***/ }),

/***/ "../../../../../src/app/orders_new/Main.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MaindRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__edit_edit_component__ = __webpack_require__("../../../../../src/app/orders_new/edit/edit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__add_add_component__ = __webpack_require__("../../../../../src/app/orders_new/add/add.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__index_index_component__ = __webpack_require__("../../../../../src/app/orders_new/index/index.component.ts");



var MaindRoutes = [{
        path: '',
        children: [{
                path: 'index',
                component: __WEBPACK_IMPORTED_MODULE_2__index_index_component__["a" /* IndexComponent */],
                data: {
                    heading: 'הזמנות'
                }
            }, {
                path: 'index',
                component: __WEBPACK_IMPORTED_MODULE_0__edit_edit_component__["a" /* EditComponent */],
                data: {
                    heading: 'Edit Company'
                }
            }, {
                path: 'add',
                component: __WEBPACK_IMPORTED_MODULE_1__add_add_component__["a" /* AddComponent */],
                data: {
                    heading: 'Edit Company'
                }
            }]
    }];
//# sourceMappingURL=Main.routing.js.map

/***/ }),

/***/ "../../../../../src/app/orders_new/MainService.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MainService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__("../../../../rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__settings_settings_service__ = __webpack_require__("../../../../../src/settings/settings.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//public this.ServerUrl = "";//http://www.tapper.co.il/salecar/laravel/public/api/";
var MainService = (function () {
    function MainService(http, settings) {
        this.http = http;
        this.ServerUrl = "";
        this.headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/x-www-form-urlencoded' });
        this.options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["f" /* RequestOptions */]({ headers: this.headers });
        this.Items = [];
        this.ServerUrl = settings.ServerUrl;
    }
    ;
    MainService.prototype.GetItems = function (url, Id) {
        var _this = this;
        var body = new FormData();
        body.append('id', Id);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { _this.Items = data; }).toPromise();
    };
    MainService.prototype.GetOrders = function (url, data) {
        var body = new FormData();
        body.append("data", JSON.stringify(data));
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res; }).do(function (data) { }).toPromise();
    };
    MainService.prototype.AddItem = function (url, Item, File) {
        this.Items = Item;
        var body = new FormData();
        body.append("file", File);
        body.append("category", JSON.stringify(this.Items));
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res; }).do(function (data) { }).toPromise();
    };
    MainService.prototype.EditItem = function (url, Item, File) {
        console.log("IT : ", File, Item);
        this.Items = Item;
        var body = new FormData();
        body.append("file", File);
        body.append("category", JSON.stringify(this.Items));
        //let body = 'category=' + JSON.stringify(this.Items);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { }).toPromise();
    };
    MainService.prototype.DeleteItem = function (url, Id) {
        var body = 'id=' + Id;
        return this.http.post(this.ServerUrl + '' + url, body, this.options).map(function (res) { return res.json(); }).do(function (data) { }).toPromise();
    };
    MainService.prototype.SaveOrder = function (url, id, order_status, order_remarks) {
        var body = new FormData();
        body.append("id", id);
        body.append("order_status", order_status);
        body.append("order_remarks", order_remarks);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res; }).do(function (data) { }).toPromise();
    };
    MainService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__settings_settings_service__["a" /* SettingsService */]) === "function" && _b || Object])
    ], MainService);
    return MainService;
    var _a, _b;
}());

;
//# sourceMappingURL=MainService.service.js.map

/***/ }),

/***/ "../../../../../src/app/orders_new/add/add.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".FormClass\n{\n    text-align: right;\n    direction: rtl;\n}\n\n.row\n{\n    margin-top: 20px;\n}\n\n.textWhite\n{\n    background-color: white;\n}\n\ninput.ng-invalid.ng-touched\n{\n    border:1px solid red;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/orders_new/add/add.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row FormClass\">\n  <div class=\"col-lg-12\">\n    <div class=\"card\">\n      <div class=\"card-header\">\n        ערוך חברה\n      </div>\n      <div class=\"card-body\">\n        <form (ngSubmit)=\"onSubmit(f)\" #f=\"ngForm\" >\n          <div class=\"row\" >\n            <div class=\"form-group\" class=\"col-lg-6\" *ngFor=\"let row of rows let i=index\" style=\"margin-top: 15px;\">\n              <label for=\"formGroupExampleInput\">הכנס {{rowsNames[i]}} </label>\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" name={{row}} [(ngModel)]=\"Item[row]\"  ngModel required>\n            </div>\n\n            <div class=\"form-group\" class=\"col-lg-6\" style=\"margin-top: 15px;\" *ngIf=\"sub == -1\" >\n              <label for=\"formGroupExampleInput\">תת קטגורייה </label>\n              <select class=\"form-control textWhite\" [(ngModel)]=\"Item.sub_category_id\" name=\"sub_category_id\">\n                <option *ngFor=\"let item of SubCategories let i=index\" [value]=\"item.id\">{{item.title}}</option>\n              </select><br/>\n            </div>\n          </div>\n          <div class=\"row\">\n            <div class=\"form-group\" class=\"col-lg-6\">\n              <label for=\"formGroupExampleInput\">תיאור</label>\n              <textarea rows=\"4\" cols=\"50\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" [(ngModel)]=\"Item.description\" name=\"description\" ngModel required> </textarea>\n            </div>\n            <!--<div class=\"form-group\" class=\"col-lg-6\">-->\n              <!--<label for=\"formGroupExampleInput\">תיאור באנגלית</label>-->\n              <!--<textarea rows=\"4\" cols=\"50\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" [(ngModel)]=\"Item.description_english\" name=\"description_english\" ngModel required> </textarea>-->\n            <!--</div>-->\n          </div>\n          <div class=\"row\">\n\n          </div>\n          <!--<div class=\"row\">-->\n          <!--<img src=\"{{Image}}\" style=\"width:50px; height: 50px\" />-->\n          <!--<input  #fileInput   type=\"file\"(change)=\"onChange($event)\" style=\"margin-right: 20px; margin-top: 10px\"/>-->\n          <!--</div>-->\n          <div class=\"row\">\n            <!--<div class=\"form-group\" class=\"col-lg-6\">-->\n              <!--<div class=\"row\">-->\n                <!--<label class=\"uploader\">-->\n\n                  <!--<img *ngIf=\"changeImage\" src=\"{{changeImage}}\" [class.loaded]=\"imageLoaded\" style=\"width: 100px;\"/>-->\n                  <!--<img *ngIf=\"!changeImage\" src=\"{{host}}{{Item.image}}\" [class.loaded]=\"imageLoaded\" style=\"width: 100px;\"/>-->\n                  <!--<input #fileInput type=\"file\" name=\"file\" accept=\"image/*\" (change)=\"onChange($event)\">-->\n                <!--</label>-->\n\n\n              <!--</div>-->\n            <!--</div>-->\n            <div class=\"form-group\" class=\"col-lg-6\">\n              <div class=\"row\">\n                <input #fileInput type=\"file\"/>\n              </div>\n            </div>\n          </div>\n\n          <div class=\"row\" align=\"left\" style=\"float: left; margin-left: 5px\">\n            <button [disabled]=\"!f.valid\" type=\"submit\"\n                    class=\"btn btn-primary btn-icon loading-demo mr-1 mb-1\"\n                    style=\"padding:0 20px !important; cursor: pointer; width: 205px; text-align: center; font-weight: bold; font-size: 16px\">\n              <span style=\"text-align: center; padding: 10px;  width: 100% !important;\">שלח טופס</span>\n            </button>\n          </div>\n        </form>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/orders_new/add/add.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__MainService_service__ = __webpack_require__("../../../../../src/app/orders_new/MainService.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AddComponent = (function () {
    function AddComponent(route, http, service, router) {
        var _this = this;
        this.route = route;
        this.http = http;
        this.service = service;
        this.router = router;
        this.navigateTo = '/products/index';
        this.imageSrc = '';
        this.folderName = 'products';
        this.rowsNames = ['שם', 'מחיר נמוך', 'מחיר גבוה'];
        this.rows = ['title', 'low_price', 'high_price'];
        this.Item = {
            'title': '',
            'title_english': '',
            'low_price': '',
            'high_price': '',
            'description': '',
            'description_english	': '',
            'image': '',
        };
        console.log("Row : ", this.rows);
        this.route.params.subscribe(function (params) {
            _this.sub = params['sub'];
        });
        this.service.GetItems('GetSubCategoriesById', -1).then(function (data) {
            _this.SubCategories = data;
            _this.isReady = true;
        });
    }
    AddComponent.prototype.onSubmit = function (form) {
        var _this = this;
        console.log(form.value);
        var fi = this.fileInput.nativeElement;
        var fileToUpload;
        if (fi.files && fi.files[0]) {
            fileToUpload = fi.files[0];
        }
        if (this.sub != -1)
            form.value.sub_category_id = this.sub;
        console.log(form.value);
        this.service.AddItem('AddProduct', form.value, fileToUpload).then(function (data) {
            console.log("AddCompany : ", data);
            _this.router.navigate([_this.navigateTo, { id: _this.sub }]);
        });
    };
    AddComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.paramsSub = this.route.params.subscribe(function (params) { return _this.id = params['id']; });
    };
    AddComponent.prototype.ngOnDestroy = function () {
        this.paramsSub.unsubscribe();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("fileInput"),
        __metadata("design:type", Object)
    ], AddComponent.prototype, "fileInput", void 0);
    AddComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-add',
            template: __webpack_require__("../../../../../src/app/orders_new/add/add.component.html"),
            styles: [__webpack_require__("../../../../../src/app/orders_new/add/add.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__MainService_service__["a" /* MainService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__MainService_service__["a" /* MainService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _d || Object])
    ], AddComponent);
    return AddComponent;
    var _a, _b, _c, _d;
}());

//# sourceMappingURL=add.component.js.map

/***/ }),

/***/ "../../../../../src/app/orders_new/edit/edit.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".FormClass\n{\n    text-align: right;\n    direction: rtl;\n}\n\n.row\n{\n    margin-top: 20px;\n}\n\n.textWhite\n{\n    background-color: white;\n}\n\ninput.ng-invalid.ng-touched\n{\n    border:1px solid red;\n}\n\n.SearchInput\n{\n    background-color: white;\n    text-align: right;\n}\n\n.p-3\n{\n    padding: 0px;\n    background-color: red;\n}\n\n.KitchensForm\n{\n    direction: rtl;\n    text-align: right;\n}\n\n.formCheck\n{\n    position: relative;\n    left:20px;\n    text-align: right;\n    width: 50px;\n    background-color: red;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/orders_new/edit/edit.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row FormClass\">\n    <div class=\"col-lg-12\">\n        <div class=\"card\">\n            <div class=\"card-header\">\n                ערוך חברה\n            </div>\n            <div class=\"card-body\">\n                <form (ngSubmit)=\"onSubmit(f)\" #f=\"ngForm\" >\n                    <div class=\"row\" >\n                        <div class=\"form-group\" class=\"col-lg-6\" *ngFor=\"let row of rows let i=index\" style=\"margin-top: 15px;\">\n                            <label for=\"formGroupExampleInput\">הכנס {{rowsNames[i]}} </label>\n                            <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" name={{row}} [(ngModel)]=\"Item[row]\"  ngModel required>\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                        <div class=\"form-group\" class=\"col-lg-6\">\n                            <label for=\"formGroupExampleInput\">תיאור בעיברית</label>\n                            <textarea rows=\"4\" cols=\"50\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" [(ngModel)]=\"Item.description\" name=\"description\" ngModel required> </textarea>\n                        </div>\n                        <div class=\"form-group\" class=\"col-lg-6\">\n                            <label for=\"formGroupExampleInput\">תיאור באנגלית</label>\n                            <textarea rows=\"4\" cols=\"50\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" [(ngModel)]=\"Item.description_english\" name=\"description\" ngModel required> </textarea>\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                        <div class=\"form-group\" class=\"col-lg-6\">\n                            <label for=\"formGroupExampleInput\">תת קטגורייה </label>\n                            <select class=\"form-control textWhite\" [(ngModel)]=\"Item.sub_category_id\" name=\"category\">\n                                <option *ngFor=\"let item of SubCategories let i=index\" [value]=\"item.id\">{{item.title}}</option>\n                            </select><br/>\n                        </div>\n                    </div>\n                    <!--<div class=\"row\">-->\n                        <!--<img src=\"{{Image}}\" style=\"width:50px; height: 50px\" />-->\n                        <!--<input  #fileInput   type=\"file\"(change)=\"onChange($event)\" style=\"margin-right: 20px; margin-top: 10px\"/>-->\n                    <!--</div>-->\n                    <div class=\"row\">\n                    <div class=\"form-group\" class=\"col-lg-6\">\n                        <div class=\"row\">\n                            <label class=\"uploader\">\n\n                                <img *ngIf=\"changeImage\" src=\"{{changeImage}}\" [class.loaded]=\"imageLoaded\" style=\"width: 100px;\"/>\n                                <img *ngIf=\"!changeImage\" src=\"{{host}}{{Item.image}}\" [class.loaded]=\"imageLoaded\" style=\"width: 100px;\"/>\n                                <input #fileInput type=\"file\" name=\"file\" accept=\"image/*\" (change)=\"onChange($event)\">\n                            </label>\n\n\n                        </div>\n                    </div></div>\n\n                    <div class=\"row\" align=\"left\" style=\"float: left; margin-left: 5px\">\n                        <button [disabled]=\"!f.valid\" type=\"submit\"\n                                class=\"btn btn-primary btn-icon loading-demo mr-1 mb-1\"\n                                style=\"padding:0 20px !important; cursor: pointer; width: 205px; text-align: center; font-weight: bold; font-size: 16px\">\n                            <span style=\"text-align: center; padding: 10px;  width: 100% !important;\">שלח טופס</span>\n                        </button>\n                    </div>\n                </form>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/orders_new/edit/edit.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__MainService_service__ = __webpack_require__("../../../../../src/app/orders_new/MainService.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__settings_settings_service__ = __webpack_require__("../../../../../src/settings/settings.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var URL = 'https://evening-anchorage-3159.herokuapp.com/api/';
var EditComponent = (function () {
    function EditComponent(route, http, service, router, settings) {
        var _this = this;
        this.route = route;
        this.http = http;
        this.service = service;
        this.router = router;
        this.settings = settings;
        this.navigateTo = '/products/index';
        this.imageSrc = '';
        this.Items = [];
        this.rowsNames = ['שם בעיברית', 'שם באנגלית', 'מחיר נמוך', 'מחיר גבוה'];
        this.rows = ['title', 'title_english', 'low_price', 'high_price'];
        this.Change = false;
        this.route.params.subscribe(function (params) {
            _this.Id = params['id'];
            _this.Item = _this.service.Items[_this.Id];
            _this.host = settings.host;
            _this.service.GetItems('GetSubCategoriesById', -1).then(function (data) {
                _this.SubCategories = data;
                _this.isReady = true;
                console.log("Sub : ", data);
            });
            console.log("Product", _this.Item);
            console.log("Item : ", _this.Item);
        });
    }
    EditComponent.prototype.onSubmit = function (form) {
        var _this = this;
        console.log("Edit1 : ", this.Item);
        var fi = this.fileInput.nativeElement;
        var fileToUpload;
        if (fi.files && fi.files[0]) {
            fileToUpload = fi.files[0];
            console.log("fff : ", fileToUpload);
        }
        this.Item.change = this.Change.toString();
        //this.Item.change = this.Change;
        this.service.EditItem('EditProduct', this.Item, fileToUpload).then(function (data) {
            console.log("AddCompany : ", data);
            _this.router.navigate([_this.navigateTo]);
        });
    };
    EditComponent.prototype.onChange = function (event) {
        var _this = this;
        this.Change = true;
        var files = event.srcElement.files;
        this.changeImage = event.target.files[0];
        var reader = new FileReader();
        reader.onload = function (e) {
            _this.changeImage = e.target.result;
        };
        reader.readAsDataURL(event.target.files[0]);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("fileInput"),
        __metadata("design:type", Object)
    ], EditComponent.prototype, "fileInput", void 0);
    EditComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-edit',
            template: __webpack_require__("../../../../../src/app/orders_new/edit/edit.component.html"),
            styles: [__webpack_require__("../../../../../src/app/icons/fontawesome/fontawesome.component.scss"), __webpack_require__("../../../../../src/app/components/buttons/buttons.component.scss"), __webpack_require__("../../../../../src/app/orders_new/edit/edit.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__MainService_service__["a" /* MainService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__MainService_service__["a" /* MainService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__settings_settings_service__["a" /* SettingsService */]) === "function" && _e || Object])
    ], EditComponent);
    return EditComponent;
    var _a, _b, _c, _d, _e;
}());

//# sourceMappingURL=edit.component.js.map

/***/ }),

/***/ "../../../../../src/app/orders_new/index/index.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".card\n{\n    text-align: right;\n    direction: rtl;\n}\n\n.page-select {\n    margin: 0 0 10px 0;\n    direction: rtl;\n    text-align: right;\n}\n\n.card-body\n{\n    border-bottom: 1px solid #f2f1f2;\n}\n\n.mr-auto\n{\n    text-align: right;\n    direction: rtl;\n    background-color: red;\n    float: right;\n}\n\n.mr-3\n{\n    background-color: green;\n    float: right;\n}\n\n.IconClass\n{\n    margin-top: 6px;\n    text-align: center;\n    padding-left: -13px !important;\n    background-color: red;\n}\n\n.d-icon{\n    margin-top: -20px;\n}\n\n.titleImage\n{\n    width: 80px;\n    border-radius: 70%;\n    height:80px;\n    margin-top:3px;\n    border: 1px solid #f1f1f1;\n}\n\n.textHeader\n{\n    color: #337ab7;;\n    font-size: 15px;\n    font-weight: bold;\n}\n\n.SearchInput{\n    background-color: white;\n    text-align: right;\n    paddding:5px;\n    margin-bottom: -15px;\n    margin-top: -15px;\n}\n\n.p-3{\n    margin-top: 2px;\n    margin-bottom: 2px;\n}\n\n.sideButton\n{\n    width:90%;\n    cursor: pointer;\n    background-color: #3b5998;\n    color: white;\n    text-align: right;\n    padding: 3px;\n    overflow: hidden;\n}\n\n.sideButtonText\n{\n    margin-right: 10px;\n    font-size: 14px;\n    font-weight: bold;\n    top: 7px !important;\n    position: relative;\n}\n\n.sideButtonTextEmpty{\n    margin-right: 10px;\n    font-size: 14px;\n    font-weight: bold;\n    top: 0px !important;\n    position: relative;\n}\n\n.sideButtonBadge\n{\n    background-color: red;\n    border-radius:50%;\n    font-size: 12px;\n    margin-top: 5px;\n    padding: 3px;\n    width: 25px;\n    height: 25px;\n}\n\n.buttonDivBadge\n{\n    float: right;\n    width: 12%;\n}\n\n.buttonDivText\n{\n    float: right;\n    width: 90%;\n    text-align: right !important;\n}\n\n.SearchInput{\n    background-color: white;\n    text-align: right;\n    paddding:5px;\n    margin-bottom: -15px;\n    margin-top: -15px;\n}\n\n.buttonDivIcon\n{\n    float: left;\n    width: 20%;\n}\n\n.badgeText\n{\n    top: 4px;\n    position: relative;\n}\n\n\nngx-datatable {\n    direction: rtl !important;\n    text-align: right !important;\n}\n\n.yellow-star {\n    color: #ffbd53;\n}\n\n.grey-star {\n    color: grey;\n}\n\n\n\n.sideButtonTextEmpty{\n    margin-right: 10px;\n    font-size: 14px;\n    font-weight: bold;\n    top: 0px !important;\n    position: relative;\n    text-align: right !important;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/orders_new/index/index.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row nopadding\" style=\"padding: 0px; background-color: white; margin-top: -15px; margin-left: -17px\">\n\n            <div class=\"col-lg-12 nopadding\" style=\"margin-top: 10px;\">\n\n                        <!--<div class=\"row\" style=\"direction: rtl; margin-left:0%;\">-->\n                            <!--<div class=\"col-lg-9 nopadding\" style=\"margin-top: 10px;\">-->\n                                <!--<div style=\"width: 95%; float: right ; margin-right: 20px; margin-top: 30px; \">-->\n                                    <!--<input type=\"text\" dir=\"rtl\" class=\"form-control mb-3 SearchInput\" placeholder=\"חש לפי שם מלא\" required (keyup)='updateFilter($event)'>-->\n                                <!--</div>-->\n                            <!--</div>-->\n\n\n                        <!--</div>-->\n\n\n                <div class=\"row\" >\n\n                    <div class=\"form-group\" class=\"col-lg-6\" style=\"margin-top: 15px;\" >\n                        <label >בחירת לקוח </label>\n                        <select class=\"form-control textWhite\" [(ngModel)]=\"fields.user_id\" name=\"user_id\"  >\n                            <option  [value]=\"-1\" selected>כולם</option>\n                            <option *ngFor=\"let row of usersArray\"  [value]=\"row.id\" >{{row.name}}</option>\n                        </select><br/>\n                    </div>\n\n                    <div class=\"form-group\" class=\"col-lg-6\" style=\"margin-top: 15px;\" >\n                        <label >סטטוס הזמנה </label>\n                        <select class=\"form-control textWhite\" [(ngModel)]=\"fields.order_status\" name=\"order_status\"  >\n                            <option  [value]=\"-1\" selected>כולם</option>\n                            <option value=\"0\">חדש</option>\n                            <option value=\"1\">פתוח</option>\n                            <option value=\"2\">סגור</option>\n                            <option value=\"3\">בטיפול</option>\n                            <option value=\"4\">מבוטל</option>\n                        </select><br/>\n                    </div>\n\n                </div>\n\n\n                <div class=\"row\" align=\"left\" style=\"float: left; margin-left: 5px\">\n                    <button  type=\"button\" (click)=\"getItems()\"\n                             class=\"btn btn-primary btn-icon loading-demo mr-1 mb-1\"\n                             style=\"padding:0 20px !important; cursor: pointer; width: 205px; text-align: center; font-weight: bold; font-size: 16px\">\n                        <span style=\"text-align: center; padding: 10px;  width: 100% !important;\">חיפוש</span>\n                    </button>\n\n                </div>\n                <div class=\"paging-btn-wrap\" style=\"float: right;\">\n                    <div class=\"form-group \" class=\"row page-select\" style=\"margin-top: 15px;\" >\n                        <div class=\"col-4\">\n                            <label style=\"vertical-align: -5px;\">בחר דף: </label>\n                        </div>\n                        <div class=\"col-4\">\n                            <select class=\"form-control textWhite\" (change)=\"getItems($event.target.value)\" name=\"order_status\"  >\n                                <option *ngFor=\"let p of pages.pageArr; let i=index\"  [value]=\"p\" [selected]=\"(i + 1) === pages.current\">{{ i + 1}}</option>\n                            </select>\n                        </div>\n                        <div class=\"col-4\">\n                            <span style=\"vertical-align: -5px;\">מתוך {{pages.pageArr.length}}</span>\n                        </div>\n                        <br/>\n                    </div>\n                    <div class=\"row\" align=\"left\" style=\"float: left; margin-left: 5px\">\n                        <button  type=\"button\" (click)=\"getItems(pages.next)\"\n                                [disabled]=\"!pages.next || pages.next.length === 0\"\n                                class=\"btn btn-info btn-icon loading-demo mr-1 mb-1\"\n                                style=\"padding:0 20px !important; cursor: pointer; width: 120px; text-align: center; font-weight: bold; font-size: 10px\">\n                            <span style=\"text-align: center; padding: 10px;  width: 100% !important;\">לדף הבא</span>\n                        </button>\n\n                    </div>\n                    <div class=\"row\" align=\"left\" style=\"float: left; margin-left: 5px\">\n                        <button  type=\"button\" (click)=\"getItems(pages.prev)\"\n                                [disabled]=\"!pages.prev || pages.prev.length === 0\"\n                                class=\"btn btn-info btn-icon loading-demo mr-1 mb-1\"\n                                style=\"padding:0 20px !important; cursor: pointer; width: 120px; text-align: center; font-weight: bold; font-size: 10px\">\n                            <span style=\"text-align: center; padding: 10px;  width: 100% !important;\">לדף הקודם</span>\n                        </button>\n\n                    </div>\n                </div>\n                        <ngx-datatable\n                                [headerHeight]=\"40\"\n                                [footerHeight]=\"'falsey'\"\n                                [rowHeight]=\"'auto'\"\n                                [scrollbarH]=\"true\"\n                                [columnMode]=\"'force'\"\n                                [rows]=\"ItemsArray\">\n\n\n                            <ngx-datatable-column name=\"#\" [sortable]=\"true\"  [width]=\"180\" prop='id'>\n                                <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                                    <strong >{{row.id | number}}</strong>\n                                </ng-template>\n                            </ngx-datatable-column>\n\n                            <ngx-datatable-column name=\"פרטי מזמין\"  [width]=\"180\">\n                                <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                                    <div *ngIf=\"row.user_data.length > 0\">{{row.user_data[0].name}}</div>\n                                    <div *ngIf=\"row.user_data.length > 0\">{{row.user_data[0].address}}</div>\n                                    <div *ngIf=\"row.user_data.length > 0\">{{row.user_data[0].city}}</div>\n                                    <div *ngIf=\"row.user_data.length > 0\">{{row.user_data[0].street}}</div>\n                                    <div *ngIf=\"row.user_data.length > 0\">{{row.user_data[0].email}}</div>\n                                    <div *ngIf=\"row.user_data.length > 0\">{{row.user_data[0].phone}}</div>\n                                </ng-template>\n                            </ngx-datatable-column>\n\n                            <ngx-datatable-column name=\"סכום הזמנה\" [sortable]=\"true\"  [width]=\"180\" prop='sum'>\n                                <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                                    <strong >{{row.sum | number}}</strong>\n                                </ng-template>\n                            </ngx-datatable-column>\n\n                            <ngx-datatable-column name=\"סטטוס הזמנה\"  [width]=\"150\">\n                                <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                                    <select class=\"form-control\" [ngModel]=\"row.order_status\" (change)=\"changeStatus($event,row)\" prop='order_status'>\n                                        <option value=\"0\">חדש</option>\n                                        <option value=\"1\">פתוח</option>\n                                        <option value=\"2\">סגור</option>\n                                        <option value=\"3\">בטיפול</option>\n                                        <option value=\"4\">מבוטל</option>\n                                    </select>\n                                </ng-template>\n                            </ngx-datatable-column>\n\n\n                            <ngx-datatable-column name=\"הערות\"  [width]=\"150\">\n                                <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                                    <textarea [ngModel]=\"row.order_remarks\" class=\"form-control\" dir=\"rtl\" (change)=\"changeRemarks($event,row)\"></textarea>\n                                </ng-template>\n                            </ngx-datatable-column>\n\n                            <ngx-datatable-column name=\"הערות לקוח\" [sortable]=\"true\"  [width]=\"180\" prop='sum'>\n                                <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                                    <strong >{{row.remarks}}</strong>\n                                </ng-template>\n                            </ngx-datatable-column>\n\n                            <ngx-datatable-column name=\"ת.הזמנה\" [sortable]=\"true\"  [width]=\"180\" prop='date'>\n                                <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                                    <strong >{{row.date}} </strong>\n                                </ng-template>\n                            </ngx-datatable-column>\n\n                            <ngx-datatable-column name=\"פעולות\" [sortable]=\"false\" [width]=\"300\" style=\"direction: ltr !important; text-align: left !important;\" align=\"left\">\n                                <ng-template let-rowIndex=\"rowIndex\"  let-row=\"row\" ngx-datatable-cell-template style=\"background-color: red; direction: ltr !important; text-align: left !important; float: left\" align=\"left\">\n                                    <button type=\"button\" class=\"btn success btn-xs\" (click)=\"saveDetails(row)\">שמירה</button>\n                                    <button type=\"button\" class=\"btn btn-info\" *ngIf=\"row.cart\" (click)=\"openDetailsModal(details, row)\">פרטים</button>\n                                    <button type=\"button\" class=\"btn btn-danger\" (click)=\"chatPage(row.user_data[0].id)\">\n                                        <div >\n                                            צ'אט\n                                        </div>\n\n                                    </button>\n                                </ng-template>\n                            </ngx-datatable-column>\n\n                        </ngx-datatable>\n            </div>\n</div>\n\n\n\n\n<ng-template ngbModalContainer></ng-template>\n<ng-template #details let-c=\"close\" let-d=\"dismiss\">\n\n    <div>\n    </div>\n    <div class=\"modal-header text-right\">\n        <h6 class=\"modal-title text-uppercase text-right\">{{selectedItem.title}}</h6>\n        <button type=\"button\" class=\"close\" aria-label=\"סגור\" (click)=\"d()\">\n            <span aria-hidden=\"true\">&times;</span>\n        </button>\n    </div>\n    <div class=\"modal-body text-right\" style=\"direction: rtl\">\n        <div class=\"table-responsive\">\n            <table class=\"table table-bordered table-striped mb-0\">\n\n\n\n                <tbody>\n                <tr>\n                    <td *ngFor=\"let item of selectedItem.cart\">\n                        <div style=\"direction: rtl;\"> שם המוצר: {{item.product.title}}</div>\n                        <div style=\"direction: rtl;\" *ngIf=\"item.product_cat\">קטגוריה: {{item.product_cat.title}}</div>\n                        <div style=\"direction: rtl;\" *ngIf=\"item.product_subcat\">תת קטגוריה: {{item.product_subcat.title}}</div>\n                        <div style=\"direction: rtl;\">כמות: {{item.quan}}</div>\n                        <div style=\"direction: rtl;\">מק\"ט: {{item.product.sku}}</div>\n                        <div style=\"direction: rtl;\">מחיר נמוך: {{item.product.low_price}}</div>\n                        <div style=\"direction: rtl;\">\n                            <img [src]=\"host+item.product.image\" style=\"width:50px; height:50px;\">\n                        </div>\n                    </td>\n                </tr>\n\n\n                </tbody>\n            </table>\n        </div>\n\n\n        <div class=\"m-2\">\n            <button type=\"button\" class=\"btn btn-info btn-block\" (click)=\"c()\">סגור</button>\n        </div>\n    </div>\n</ng-template>\n\n\n\n\n"

/***/ }),

/***/ "../../../../../src/app/orders_new/index/index.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IndexComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__MainService_service__ = __webpack_require__("../../../../../src/app/orders_new/MainService.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__settings_settings_service__ = __webpack_require__("../../../../../src/settings/settings.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var IndexComponent = (function () {
    function IndexComponent(MainService, settings, modalService, route, router) {
        var _this = this;
        this.MainService = MainService;
        this.modalService = modalService;
        this.route = route;
        this.router = router;
        this.ItemsArray = [];
        this.ItemsArray1 = [];
        this.usersArray = [];
        this.host = '';
        this.settings = '';
        this.avatar = '';
        this.pages = {
            next: '',
            prev: '',
            pageArr: [],
            baseUrl: '',
            current: 0
        };
        this.folderName = 'orders';
        this.addButton = '';
        this.fields = {
            "user_id": "-1",
            "order_status": "-1",
        };
        this.route.params.subscribe(function (params) {
            console.log("11 : ", _this.SubCatId);
            _this.host = settings.host;
            _this.avatar = settings.avatar;
            _this.getItems();
            _this.getUsers();
        });
    }
    IndexComponent.prototype.ngOnInit = function () {
    };
    IndexComponent.prototype.getItems = function (nextUrl) {
        var _this = this;
        this.MainService.GetOrders(nextUrl || 'WebGetOrders', this.fields).then(function (data) {
            console.log("WebGetOrders : ", data.json());
            var content = data.json();
            _this.handlePagination(content);
            _this.ItemsArray = content.data;
            _this.ItemsArray1 = content.data;
        });
    };
    IndexComponent.prototype.getUsers = function () {
        var _this = this;
        this.MainService.GetItems('WebgetAllUsers', this.SubCatId).then(function (data) {
            console.log("WebgetUsers : ", data);
            _this.usersArray = data;
        });
    };
    IndexComponent.prototype.chatPage = function (id) {
        this.router.navigate(['/', 'chat', 'index', { id: id }]);
        //this.router.navigate(['chat/index;id='+id])
    };
    IndexComponent.prototype.changeRemarks = function (event, row) {
        row.order_remarks = event.target.value;
    };
    IndexComponent.prototype.changeStatus = function (event, row) {
        row.order_status = event.target.value;
    };
    IndexComponent.prototype.saveDetails = function (row) {
        this.MainService.SaveOrder('webSaveOrderDetails', row.id, row.order_status, row.order_remarks).then(function (data) {
            alert("עודכן בהצלחה");
        });
    };
    IndexComponent.prototype.DeleteItem = function () {
        var _this = this;
        this.MainService.DeleteItem('WebDeleteOrder', this.ItemsArray[this.companyToDelete].id).then(function (data) {
            _this.ItemsArray = data, console.log("Del 2 : ", data);
        });
    };
    IndexComponent.prototype.handlePagination = function (data) {
        this.pages.current = data.current_page;
        this.pages.pageArr = [];
        this.pages.baseUrl = data.path.slice(data.path.lastIndexOf('/'));
        this.pages.baseUrl = this.pages.baseUrl.replace('/', '') + "?page=";
        this.pages.prev = '';
        this.pages.next = '';
        for (var index = 0; index < data.last_page; index++) {
            this.pages.pageArr.push("" + this.pages.baseUrl + (index + 1));
        }
        console.log(this.pages.pageArr);
        if (data.next_page_url) {
            this.pages.next = data.next_page_url.slice(data.next_page_url.lastIndexOf('/'));
            this.pages.next = this.pages.next.replace('/', '');
            console.log(this.pages.next);
        }
        if (data.prev_page_url) {
            this.pages.prev = data.prev_page_url.slice(data.prev_page_url.lastIndexOf('/'));
            this.pages.prev = this.pages.prev.replace('/', '');
            console.log(this.pages.prev);
        }
    };
    IndexComponent.prototype.updateFilter = function (event) {
        // const val = event.target.value;
        // // filter our data
        // const temp = this.ItemsArray1.filter(function (d) {
        //     return d.title.toLowerCase().indexOf(val) !== -1 || !val;
        // });
        // // update the rows
        // this.ItemsArray = temp;
    };
    IndexComponent.prototype.openDetailsModal = function (content, item) {
        console.log("DM : ", content, item);
        this.detailsModal = this.modalService.open(content);
        this.selectedItem = item;
    };
    IndexComponent.prototype.openDeleteModal = function (content, index) {
        this.deleteModal = this.modalService.open(content);
        this.companyToDelete = index;
    };
    IndexComponent.prototype.deleteCompany = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.deleteModal.close();
                this.DeleteItem();
                console.log("Company To Delete : ", this.companyToDelete);
                return [2 /*return*/];
            });
        });
    };
    IndexComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-index',
            template: __webpack_require__("../../../../../src/app/orders_new/index/index.component.html"),
            styles: [__webpack_require__("../../../../../src/app/icons/fontawesome/fontawesome.component.scss"), __webpack_require__("../../../../../src/app/media/list/list.component.scss"), __webpack_require__("../../../../../src/app/orders_new/index/index.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__MainService_service__["a" /* MainService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__MainService_service__["a" /* MainService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__settings_settings_service__["a" /* SettingsService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__ng_bootstrap_ng_bootstrap__["f" /* NgbModal */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__ng_bootstrap_ng_bootstrap__["f" /* NgbModal */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* ActivatedRoute */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__angular_router__["c" /* Router */]) === "function" && _e || Object])
    ], IndexComponent);
    return IndexComponent;
    var _a, _b, _c, _d, _e;
}());

//# sourceMappingURL=index.component.js.map

/***/ })

});
//# sourceMappingURL=Main.module.4.chunk.js.map