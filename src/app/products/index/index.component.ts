import {Component, OnInit} from '@angular/core';
import {MainService} from "../MainService.service";
import {SettingsService} from "../../../settings/settings.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ActivatedRoute} from "@angular/router";
import {log} from "util";

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../media/list/list.component.scss', './index.component.css']
})


export class IndexComponent implements OnInit {

    ItemsArray: any[] = [];
    ItemsArray1: any[] = [];
    host: string = '';
    settings = '';
    avatar = '';
    public folderName:string = 'products';
    public addButton:string = 'הוסף קטגורייה'
    deleteModal: any;
    detailsModal: any;
    selectedItem: any;
    companyToDelete: any;
    public SubCatId:any;
    Category:any;
    pages = {
        next: '',
        prev: '',
        pageArr: [],
        baseUrl: '',
        current: 0
    };
    editMode: boolean = false;
    editRow: any = null;

    rankModal: any;
    categroyrow: any;



    constructor(public MainService: MainService, settings: SettingsService , private modalService: NgbModal , private route: ActivatedRoute) {
        this.route.params.subscribe(params => {
            console.log("ssss")
             this.SubCatId = params['id'];
            if(!this.SubCatId)
                this.SubCatId = "-1";
            console.log("11 : " , this.SubCatId)
            this.host = settings.host;
            this.avatar = settings.avatar;
        });

        this.getItems();
    }

    ngOnInit() {
    }

    async getItems(nextUrl?: string) {
        // console.log("Getitems019 : " ,this.SubCatId )
        // this.MainService.GetItems('getProducts', this.SubCatId ).then((data: any) => {
        //     console.log("111")
        //     console.log("GetCategories12 : ", data)
        //     this.ItemsArray = data;
        //     this.ItemsArray1 = data;
        //     this.Category = this.ItemsArray[0].category_id;
        //     console.log("thisCat : " , this.Category)
        // })
        console.log("111");
        const res: any = await this.MainService.GetItems(nextUrl || 'getProducts', this.SubCatId );
        this.handlePagination(res);
        console.log(res);
        this.ItemsArray =  res.data;
        this.ItemsArray1 = this.ItemsArray;
        
        // this.ItemsArray = content.data;
        // this.ItemsArray1 = content.data;

        if(this.ItemsArray.length > 0)
        this.Category = this.ItemsArray[0].category_id;
        console.log("thisCat : " , this.Category)
    }
    

    handlePagination(data: any) {
        this.pages.current = data.current_page;
        this.pages.pageArr = [];
        this.pages.baseUrl = data.path.slice(data.path.lastIndexOf('/'));
        this.pages.baseUrl = `${this.pages.baseUrl.replace('/', '')}?page=`;
        this.pages.prev = '';
        this.pages.next = '';
        
        for (let index = 0; index < data.last_page; index++) {
            this.pages.pageArr.push(`${this.pages.baseUrl}${index + 1}`);
        }
        console.log(this.pages.pageArr);
        if (data.next_page_url) {
            this.pages.next = data.next_page_url.slice(data.next_page_url.lastIndexOf('/'));
            this.pages.next = this.pages.next.replace('/', '');
            console.log(this.pages.next);
        }

        if (data.prev_page_url) {
            this.pages.prev = data.prev_page_url.slice(data.prev_page_url.lastIndexOf('/'));
            this.pages.prev = this.pages.prev.replace('/', '');    
            console.log(this.pages.prev);
        }

    }


    openPositionModal(content, row) {
        this.rankModal = this.modalService.open(content);
        this.categroyrow = row;
    }

    closeRankModal () {
        this.rankModal.close();
    }

    updatePrdPosition() {
        this.MainService.EditPosition('WebEditProductPosition', this.categroyrow.id,this.categroyrow.position).then((data: any) => {
            this.closeRankModal();
        })
    }

    DeleteItem() {
        this.MainService.DeleteItem('DeleteProduct', this.ItemsArray[this.companyToDelete].id).then((data: any) => {
            this.getItems() , console.log("Del 2 : ", data);
        })
    }

    updateFilter(event) {
        const val = event.target.value;
        // filter our data
        const temp = this.ItemsArray1.filter(function (d) {
            return d.title.toLowerCase().indexOf(val) !== -1 || !val;
        });
        // update the rows
        this.ItemsArray = temp;
    }

    openDetailsModal(content, item){
        console.log("DM : " , content , item)
        this.detailsModal = this.modalService.open(content);
        this.selectedItem = item;
    }

    openDeleteModal(content,index)
    {
        this.deleteModal = this.modalService.open(content);
        this.companyToDelete = index;
    }

    async deleteCompany()
    {
        this.deleteModal.close();
        this.DeleteItem();
        console.log("Company To Delete : " , this.companyToDelete)
    }

}
