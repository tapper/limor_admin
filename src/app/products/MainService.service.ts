
import {Http,Headers,Response, RequestOptions} from "@angular/http";
import {Injectable} from "@angular/core";
import { HttpModule } from '@angular/http';
import 'rxjs/Rx';
import {SettingsService} from "../../settings/settings.service";

import 'rxjs/Rx';

//public this.ServerUrl = "";//http://www.tapper.co.il/salecar/laravel/public/api/";
@Injectable()

export class MainService
{
    public ServerUrl = "";
    public headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
    public options = new RequestOptions({headers:this.headers});

    public Items:any[]=[];

    constructor(private http:Http ,  settings:SettingsService)
    {
        this.ServerUrl = settings.ServerUrl;
    };

    // GetItems(url:string ,Id)
    // {
    //     console.log("GetItems : " , Id)
    //     let body = new FormData();
    //     body.append('id', Id);
    //
    //
    //
    //         try {
    //             return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{
    //                 if(data)
    //                 {
    //                     console.log("Items : " , data);
    //                     this.Items = data;
    //                 }
    //                 else
    //                     alert("no productts");
    //             }).toPromise();
    //         } catch (err) {
    //             console.log(err);
    //         } finally {
    //             //loading.dismiss();
    //         }
    // }
    
    async GetItems(url:string ,Id): Promise<Array<Object>> {
        return new Promise<Array<Object>>(async (resolve, reject) => {
            let body = new FormData();
            body.append('id', Id);
            console.log("s1");
            try {
                let data = await this.http.post(this.ServerUrl + '' + url, body).map(res => res.text() ? res.json() : {}).do((data)=>{}).toPromise();
                if(data)
                {
                    console.log("Items : " , data);
                    this.Items = data.data;
                }
                
                console.log("s2" , data);
                resolve(data);
            } catch (err) {
                console.log("s3");
                //reject(err);
                console.log(err);
            } finally {
                console.log("s4");
            }
        });
    }
    
    
    AddItem(url,Item,File)
    {
        this.Items = Item;
        let body = new FormData();
        body.append("file", File);
        body.append("category", JSON.stringify(this.Items));
        return this.http.post (this.ServerUrl + '' + url, body).map(res => res).do((data)=>{}).toPromise();
    }

    EditPosition(url,id,position)
    {
        let body = new FormData();
        body.append("id", id);
        body.append("position", position);
        return this.http.post (this.ServerUrl + '' + url, body).map(res => res).do((data)=>{}).toPromise();
    }


    EditItem(url,Item,File)
    {
        console.log("IT : " , File , Item)
        this.Items = Item;
        let body = new FormData();
        body.append("file", File);
        body.append("category", JSON.stringify(this.Items));
        //let body = 'category=' + JSON.stringify(this.Items);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{}).toPromise();
    }

    DeleteItem(url,Id)
    {
        let body = 'id=' + Id;
        return this.http.post(this.ServerUrl + '' + url, body, this.options).map(res => res.json()).do((data)=>{}).toPromise();
    }
};


