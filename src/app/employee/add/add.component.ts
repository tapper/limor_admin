import {Component, OnInit, ElementRef, ViewChild, NgZone} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FileUploader} from "ng2-file-upload";
import {Http, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Headers} from '@angular/http';
import {FormBuilder, FormGroup, NgForm, Validators} from "@angular/forms";
import { Ng2UploaderModule } from 'ng2-uploader';
import {EmployeeService} from "../employee.service";


@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent {

    public navigateTo:string = '';
    public imageSrc: string = '';
    public Id:number = 22;

    constructor(private route: ActivatedRoute,private http: Http, public service:EmployeeService , public router:Router) {
        this.route.params.subscribe(params => {
            this.Id = params['id'];
            this.navigateTo = 'employee/index';
        });
    }

    onSubmit(form:NgForm)
    {
        
        form.value.Id = this.Id;
        console.log("Value : " , form.value);
        this.service.AddItem('AddEmployee',form.value).then((data: any) => {
            console.log("AddEmployee : " , data);
            this.router.navigate([this.navigateTo,{id:this.Id }]);
        });
    }

    handleInputChange(e) {
        var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];

        var pattern = /image-*/;
        var reader = new FileReader();

        if (!file.type.match(pattern)) {
            alert('invalid format');
            return;
        }

        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsDataURL(file);
        console.log("2 : " , reader)
    }

    _handleReaderLoaded(e) {
        var reader = e.target;
        this.imageSrc = reader.result;
        console.log("3 : " , this.imageSrc)
    
       /* this.service.AddCompany('AddCompany1',form.value).then((data: any) => {
            console.log("AddCompany : " , data);
            this.router.navigate([this.navigateTo]);
        });*/
    }

    //this.http.post('http://tapper.co.il/salecar/laravel/public/api/GetFile', formData, options)


}
