import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  public form: FormGroup;
  public wrongCredentials: boolean = false;

  constructor(private fb: FormBuilder, private router: Router) {}

  ngOnInit() {
    this.form = this.fb.group ( {
      uname: [null , Validators.compose ( [ Validators.required ] )] , password: [null , Validators.compose ( [ Validators.required ] )]
    } );
  }

  onSubmit() {
    this.wrongCredentials = false;
    let data = 1;
    if(data == 0)
    {
        this.wrongCredentials = true;
    }
    else
    {
        localStorage.setItem('id', '1');
        localStorage.setItem('type', '1');
        this.router.navigate ( [ '/' ] );
    }
  }

}
