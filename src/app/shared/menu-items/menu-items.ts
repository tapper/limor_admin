import { Injectable } from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  name: string;
  type?: string;
}

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

const MENUITEMS = [
  {
    state: '/',
    name: 'ראשי',
    type: 'link',
    icon: 'basic-accelerator'
  }
    ,{
        state: 'suppliers',
        child: 'index',
        name: 'ספקים',
        type: 'new',
        icon: 'basic-message-txt'
    },{
        state: 'category',
        child: 'index',
        name: 'קטגוריות',
        type: 'new',
        icon: 'basic-message-txt'
    },{
        state: 'subCategory',
        child: 'index',
        name: 'תת קטגוריות',
        type: 'new',
        icon: 'basic-message-txt'
    },{
        state: 'productImages',
        child: 'index',
        name: 'תמונות',
        type: 'new',
        icon: 'basic-message-txt'
    },{
        state: 'CampignGallery',
        child: 'index',
        name: 'קמפיינים פרסומיים',
        type: 'new',
        icon: 'basic-message-txt'
    },{
        state: 'products',
        child: 'index',
        name: 'מוצרים',
        type: 'new',
        icon: 'basic-message-txt'
    },
    {
        state: 'orders_new',
        child: 'index',
        name: 'הזמנות',
        type: 'new',
        icon: 'basic-message-txt'
    },
    {
        state: 'users',
        child: 'index',
        name: 'משתמשים',
        type: 'new',
        icon: 'basic-message-txt'
    },
    {
        state: 'chat_messages',
        child: 'index',
        name: 'הודעות',
        type: 'new',
        icon: 'basic-message-txt'
    },
    {
        state: 'app_settings_page',
        child: 'index',
        name: 'הגדרות אפליקציה',
        type: 'new',
        icon: 'basic-message-txt'
    }
];

 @Injectable()
export class MenuItems {
  getAll(): Menu[] {
       return MENUITEMS;
     }

  add(menu: Menu) {
    MENUITEMS.push(menu);
  }
}
