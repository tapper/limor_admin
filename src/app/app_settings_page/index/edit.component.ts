import {Component, OnInit, ElementRef, ViewChild, NgZone} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FileUploader} from "ng2-file-upload";
import {Http, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Headers} from '@angular/http';
import {FormBuilder, FormGroup, NgForm, Validators} from "@angular/forms";
import {Ng2UploaderModule} from 'ng2-uploader';
import {MainService} from "../MainService.service";
import {SettingsService} from "../../../settings/settings.service";


const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';

@Component({
    selector: 'app-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../components/buttons/buttons.component.scss', './edit.component.css']
})

export class EditComponent implements OnInit {

    public Id;
    public navigateTo: string = '/products/index';
    public imageSrc: string = '';
    public Items: any[] = [];
    public changeImage;
   // public rowsNames: any[] = ['שם בעיברית','שם באנגלית','מחיר נמוך','מחיר גבוה'];
   // public rows: any[] = ['title','title_english','low_price','high_price'];
    public Item;
    public host;
    public Image;
    public SubCategories;
    public isReady;
    public Change: boolean = false;
    public suppliersArray:any = [];
    public fields : any = {
        "admin_title" : "",
        "basket_order_text" : ""
    }
    @ViewChild("fileInput") fileInput;

    constructor(private route: ActivatedRoute, private http: Http, public service: MainService, public router: Router, public settings: SettingsService) {


    }

    async ngOnInit() {
        this.service.GetItems('getAppSettings','').then((data: any) => {
            console.log("getAppSettings : ", data);
            this.fields.admin_title = data.admin_title;
            this.fields.basket_order_text = data.basket_order_text;
        });
    }

    onSubmit(form: NgForm) {
        //console.log("Edit1 : " , this.Item);
        //let fi = this.fileInput.nativeElement;
        let fileToUpload;
        //if (fi.files && fi.files[0]) {fileToUpload = fi.files[0]; console.log("fff : ",fileToUpload);}
        //this.Item.change = this.Change.toString();

        //this.Item.change = this.Change;
        this.service.EditItem('WebEditAppSettings', this.fields, fileToUpload).then((data: any) => {
            console.log("WebEditAppSettings : ", data);
            //this.router.navigate([this.navigateTo]);
            alert ("עודכן בהצלחה")
        });
    }


    onChange(event) {
        this.Change = true;
        var files = event.srcElement.files;
        this.changeImage = event.target.files[0];

        let reader = new FileReader();
        reader.onload = (e: any) => {
            this.changeImage = e.target.result;
        }
        reader.readAsDataURL(event.target.files[0]);
    }
}
