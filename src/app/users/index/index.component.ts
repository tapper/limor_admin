import {Component, OnInit} from '@angular/core';
import {MainService} from "../MainService.service";
import {SettingsService} from "../../../settings/settings.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../media/list/list.component.scss', './index.component.css']
})


export class IndexComponent implements OnInit {

    ItemsArray: any[] = [];
    ItemsArray1: any[] = [];
    host: string = '';
    settings = '';
    avatar = '';
    public folderName:string = 'users/';
    public addButton:string = 'הוסף משתמש'
    deleteModal: any;
    detailsModal: any;
    selectedItem: any;
    companyToDelete: any;
    SubCatId:any;

    pages = {
        next: '',
        prev: '',
        pageArr: [],
        baseUrl: '',
        current: 0
    };

    pushModal: any;
    companyToPush: any;
    pushText: string = '';
    sent: boolean = false;
    inProcess: boolean = false;


    constructor(public MainService: MainService, settings: SettingsService , private modalService: NgbModal , private route: ActivatedRoute, public router:Router) {
        this.getItems();
    }

    ngOnInit() {
    }

    getItems(nextUrl?: string)
    {
        this.MainService.GetItems(nextUrl || 'WebgetUsers', this.SubCatId ).then((data: any) => {
            console.log("GetCategories12 : ", data.json());
            const content = data.json();
            this.handlePagination(content);
            this.ItemsArray = content.data;
            this.ItemsArray1 = content.data;
        })
    }

    DeleteItem() {
        this.MainService.DeleteItem('WebDeleteUser', this.ItemsArray[this.companyToDelete].id).then((data: any) => {
            this.getItems();
        })
    }

    updateFilter(event) {
        const val = event.target.value;
        // filter our data
        const temp = this.ItemsArray1.filter(function (d) {
            return d.name.toLowerCase().indexOf(val) !== -1 || !val;
        });
        // update the rows
        this.ItemsArray = temp;
    }

    openDetailsModal(content, item){
        console.log("DM : " , content , item)
        this.detailsModal = this.modalService.open(content);
        this.selectedItem = item;
    }

    openDeleteModal(content,index)
    {
        this.deleteModal = this.modalService.open(content);
        this.companyToDelete = index;
    }

    openPushModal (content, company) {
        this.pushModal = this.modalService.open(content);
        this.companyToPush = company;
    }

    closePushModal () {
        this.pushModal.close();
        this.sent = false;
    }

    handlePagination(data: any) {
        this.pages.current = data.current_page;
        this.pages.pageArr = [];
        this.pages.baseUrl = data.path.slice(data.path.lastIndexOf('/'));
        this.pages.baseUrl = `${this.pages.baseUrl.replace('/', '')}?page=`;
        this.pages.prev = '';
        this.pages.next = '';
        
        for (let index = 0; index < data.last_page; index++) {
            this.pages.pageArr.push(`${this.pages.baseUrl}${index + 1}`);
        }
        console.log(this.pages.pageArr);
        if (data.next_page_url) {
            this.pages.next = data.next_page_url.slice(data.next_page_url.lastIndexOf('/'));
            this.pages.next = this.pages.next.replace('/', '');
            console.log(this.pages.next);
        }

        if (data.prev_page_url) {
            this.pages.prev = data.prev_page_url.slice(data.prev_page_url.lastIndexOf('/'));
            this.pages.prev = this.pages.prev.replace('/', '');    
            console.log(this.pages.prev);
        }

    }


    onGatPage(event) {
        console.log(event.target.value);
    }
    sendPush() {

        this.inProcess = true;
        this.sent = true;

        let userId : any = '';
        if (!this.companyToPush)
            userId = 0;
        else
            userId = this.companyToPush.id;



        this.MainService.SendPush('WebSendUserPush', userId,this.pushText).then((data: any) => {
            this.pushText = '';
            this.sent = true;
            this.inProcess = false;
            this.closePushModal();
        })
    }

    async deleteCompany()
    {
        this.deleteModal.close();
        this.DeleteItem();
        console.log("Company To Delete : " , this.companyToDelete)
    }
}
