import {Component, OnInit} from '@angular/core';
import {MainService} from "../MainService.service";
import {SettingsService} from "../../../settings/settings.service";
import {ActivatedRoute} from "@angular/router";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../media/list/list.component.scss', './index.component.css']
})


export class IndexComponent implements OnInit {

    ItemsArray: any[] = [];
    ItemsArray1: any[] = [];
    host: string = '';
    settings = '';
    deleteModal: any;
    companyToDelete: any;

    public folderName:string = 'suppliers/';
    Id = '';

    constructor(public MainService: MainService, settings: SettingsService , private route: ActivatedRoute , private modalService: NgbModal) {
        console.log("shay")
        this.host = settings.host
    }

    ngOnInit() {
        this.getItems();
    }

    getItems() {
        this.MainService.GetAllItems('WebGetSuppliers',this.Id).then((data: any) => {
            console.log("WebGetSuppliers : ", data) ,
                this.ItemsArray = data,
                this.ItemsArray1 = data

        })
    }



    DeleteItem() {
        console.log("Del 1 : ", this.ItemsArray[this.companyToDelete].id);
        this.MainService.DeleteItem('WebDeleteSupplier', this.ItemsArray[this.companyToDelete].id).then((data: any) => {
            this.getItems() , console.log("Del 2 : ", data);
        })
    }

    updateFilter(event) {
        const val = event.target.value;
        // filter our data
        const temp = this.ItemsArray1.filter(function (d) {
            return d.title.toLowerCase().indexOf(val) !== -1 || !val;
        });
        // update the rows
        this.ItemsArray = temp;
    }

    openDeleteModal(content,index)
    {
        this.deleteModal = this.modalService.open(content);
        this.companyToDelete = index;
    }

    async deleteSubCategory()
    {
        this.deleteModal.close();
        this.DeleteItem();
        console.log("Company To Delete : " , this.companyToDelete)
    }

}
