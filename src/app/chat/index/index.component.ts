import {Component, OnInit} from '@angular/core';
import {EmployeeService} from "../employee.service";
import {SettingsService} from "../../../settings/settings.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../media/list/list.component.scss', './index.component.css',
        './index.component.scss']
})


export class IndexComponent implements OnInit {

    ItemsArray: any[] = [];
    ItemsArray1: any[] = [];
    host: string = '';
    Id:number;


    public chatText = '';
    public ChatArray:any = [];
    Obj: any;
    public screenHeight = screen.height - 100 + 'px';
    public date: any;
    public hours: any;
    public minutes: any;
    public seconds: any;
    public time: any;
    public today: any;
    public dd: any;
    public mm: any;
    public yyyy: any;
    public product_id:number;



    constructor(public EmployeeService:EmployeeService,public settings:SettingsService , private route: ActivatedRoute) {
          this.route.params.subscribe(params => {
              this.Id = params['id'];
              console.log("ssss : " , this.Id )
              this.host = settings.host
              this.getItems(this.Id);
        });
    }

    ngOnInit() {
    }

    getItems(id){
        this.EmployeeService.getChatDetails('getChatDetails', this.Id,0).then((data: any) => {
            console.log("getChatDetails : ", data) ,
                this.ItemsArray = data,
                this.ItemsArray1 = data
        })
    }

    sendMessage(){
        this.date = new Date()
        this.hours = this.date.getHours()
        this.minutes = this.date.getMinutes()
        this.seconds = this.date.getSeconds()

        if (this.hours < 10)
            this.hours = "0" + this.hours

        if (this.minutes < 10)
            this.minutes = "0" + this.minutes
        this.time = this.hours + ':' + this.minutes;


        this.today = new Date();
        this.dd = this.today.getDate();
        this.mm = this.today.getMonth() + 1; //January is 0!
        this.yyyy = this.today.getFullYear();

        if (this.dd < 10) {
            this.dd = '0' + this.dd
        }

        if (this.mm < 10) {
            this.mm = '0' + this.mm
        }

        this.today = this.dd + '/' + this.mm + '/' + this.yyyy;
        //this.newdate = this.today + ' ' + this.time;


        this.Obj = {
            id: this.Id,
            uid: 0,//localStorage.getItem('userid'),
            username: 'admin',//localStorage.getItem('name'),
            product_id: 0,//this.product_id,
            is_Admin: 0,//localStorage.getItem('is_Admin'),
            title: this.chatText,
            date: this.today,
            time: this.time,
            type: '1',
            //image: window.localStorage.user_image
        };

        this.ItemsArray.push(this.Obj);
        this.ItemsArray.reverse();


        this.EmployeeService.addTitle('addChatTitle', this.chatText, this.today,this.time,'admin', '','1',
            '0','1','1',this.Id).then((data: any) => {
            console.log("Weights : ", data), this.chatText = '';
        });


        console.log("this.Obj",this.Obj)
    }

    DeleteItem(i) {
        console.log("Del 00 : ", this.ItemsArray[i].id +  " : " + this.Id);
        this.EmployeeService.DeleteMessage('deleteMessage', this.ItemsArray[i].id).then((data: any) => {
            console.log(data);
            this.ItemsArray.splice(i, 1);
        });
    }

    updateFilter(event) {
        const val = event.target.value;
        // filter our data
        const temp = this.ItemsArray1.filter(function (d) {
            return d.username.toLowerCase().indexOf(val) !== -1 || !val;
        });
        // update the rows
        this.ItemsArray = temp;
    }

}
