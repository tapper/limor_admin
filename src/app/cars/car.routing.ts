import {Routes} from '@angular/router';


import {EditComponent} from "./edit/edit.component";
import {AddComponent} from "./add/add.component";
import {IndexComponent} from "./index/index.component";

export const CarRoutes: Routes = [{
    path: '',
    children: [{
        path: 'index',
        component: IndexComponent,
        data: {
            heading: 'Car'
        }
    },{
        path: 'index',
        component: EditComponent,
        data: {
            heading: 'Edit Car'
        }
    },{
        path: 'add',
        component: AddComponent,
        data: {
            heading: 'Edit Car'
        }
    }]
}];






/*

component: CompanyComponent,
    data: {
        heading: 'Company',
        removeFooter: true
    },



*/