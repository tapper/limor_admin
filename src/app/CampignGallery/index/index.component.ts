import {Component, OnInit} from '@angular/core';
import {MainService} from "../MainService.service";
import {SettingsService} from "../../../settings/settings.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../media/list/list.component.scss', './index.component.css']
})


export class IndexComponent implements OnInit {

    ItemsArray: any[] = [];
    ItemsArray1: any[] = [];
    host: string = '';
    settings = '';
    avatar = '';
    public folderName:string = 'CampignGallery';
    public addButton:string = 'הוספה';

    rankModal: any;
    categroyrow: any;




    constructor(public MainService: MainService, settings: SettingsService, private modalService: NgbModal) {

            this.host = settings.host;
            this.avatar = settings.avatar;
            this.getItems();
    }

    ngOnInit() {
    }

    getItems() {
        this.MainService.GetCategories('WebgetCampiagnImages','0').then((data: any) => {
            console.log("WebgetCampiagnImages : ", data);
            this.ItemsArray = data,
             this.ItemsArray1 = data;
        })
    }

    openRankModal(content, row) {
        this.rankModal = this.modalService.open(content);
        this.categroyrow = row;
    }

    closeRankModal () {
        this.rankModal.close();
    }

    updateCatPosition() {

        this.MainService.EditPosition('WebEditCampignGalleryPosition', this.categroyrow.id,this.categroyrow.position).then((data: any) => {
            this.closeRankModal();
            this.getItems();
        })
    }


    DeleteItem(i) {
        var confirmBox = confirm("האם לאשר מחיקה?");
        if (confirmBox == true) {
            console.log("Del 1 : ", this.ItemsArray[i].id);
            this.MainService.DeleteItem('WebdeleteCampiagnImages', this.ItemsArray[i].id).then((data: any) => {
                this.getItems();
                //this.ItemsArray = data , console.log("Del 2 : ", data);
            })
        }
    }

    updateFilter(event) {
        const val = event.target.value;
        // filter our data
        const temp = this.ItemsArray1.filter(function (d) {
            return d.title.toLowerCase().indexOf(val) !== -1 || !val;
        });
        // update the rows
        this.ItemsArray = temp;
    }

}
