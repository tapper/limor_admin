import {Component, OnInit} from '@angular/core';
import {MainService} from "../MainService.service";
import {SettingsService} from "../../../settings/settings.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {Router,ActivatedRoute} from "@angular/router";

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../media/list/list.component.scss', './index.component.css']
})


export class IndexComponent implements OnInit {

    ItemsArray: any[] = [];
    ItemsArray1: any[] = [];
    usersArray: any[] = [];
    host: string = '';
    settings = '';
    avatar = '';
    pages = {
        next: '',
        prev: '',
        pageArr: [],
        baseUrl: '',
        current: 0
    };

    public folderName:string = 'orders';
    public addButton:string = ''
    deleteModal: any;
    detailsModal: any;
    selectedItem: any;
    companyToDelete: any;
    SubCatId:any;
    Category:any;

    public fields : any = {
        "user_id" : "-1",
        "order_status" : "-1",
    }

    constructor(public MainService: MainService, settings: SettingsService , private modalService: NgbModal , private route: ActivatedRoute,private router: Router) {
        this.route.params.subscribe(params => {

            console.log("11 : " , this.SubCatId)
            this.host = settings.host;
            this.avatar = settings.avatar;
            this.getItems();
            this.getUsers();
        });

    }

    ngOnInit() {
    }



    getItems(nextUrl?: string) {
        this.MainService.GetOrders(nextUrl || 'WebGetOrders', this.fields ).then((data: any) => {
            console.log("WebGetOrders : ", data.json());
            const content = data.json();
            this.handlePagination(content);
            this.ItemsArray = content.data;
            this.ItemsArray1 = content.data;
        })
    }

    getUsers() {
        this.MainService.GetItems('WebgetAllUsers', this.SubCatId ).then((data: any) => {
            console.log("WebgetUsers : ", data)
            this.usersArray = data;
        })
    }

    chatPage(id) {
        this.router.navigate(['/', 'chat', 'index', { id:id}]);
        //this.router.navigate(['chat/index;id='+id])

    }

    changeRemarks(event,row) {
        row.order_remarks = event.target.value;
    }

    changeStatus(event,row) {
        row.order_status = event.target.value;
    }


    saveDetails(row) {
        this.MainService.SaveOrder('webSaveOrderDetails', row.id,row.order_status,row.order_remarks).then((data: any) => {
            alert ("עודכן בהצלחה");
        })
    }


    DeleteItem() {
        this.MainService.DeleteItem('WebDeleteOrder', this.ItemsArray[this.companyToDelete].id).then((data: any) => {
            this.ItemsArray = data , console.log("Del 2 : ", data);
        })
    }

    handlePagination(data: any) {
        this.pages.current = data.current_page;
        this.pages.pageArr = [];
        this.pages.baseUrl = data.path.slice(data.path.lastIndexOf('/'));
        this.pages.baseUrl = `${this.pages.baseUrl.replace('/', '')}?page=`;
        this.pages.prev = '';
        this.pages.next = '';
        
        for (let index = 0; index < data.last_page; index++) {
            this.pages.pageArr.push(`${this.pages.baseUrl}${index + 1}`);
        }
        console.log(this.pages.pageArr);
        if (data.next_page_url) {
            this.pages.next = data.next_page_url.slice(data.next_page_url.lastIndexOf('/'));
            this.pages.next = this.pages.next.replace('/', '');
            console.log(this.pages.next);
        }

        if (data.prev_page_url) {
            this.pages.prev = data.prev_page_url.slice(data.prev_page_url.lastIndexOf('/'));
            this.pages.prev = this.pages.prev.replace('/', '');    
            console.log(this.pages.prev);
        }

    }



    updateFilter(event) {
        // const val = event.target.value;
        // // filter our data
        // const temp = this.ItemsArray1.filter(function (d) {
        //     return d.title.toLowerCase().indexOf(val) !== -1 || !val;
        // });
        // // update the rows
        // this.ItemsArray = temp;
    }

    openDetailsModal(content, item){
        console.log("DM : " , content , item)
        this.detailsModal = this.modalService.open(content);
        this.selectedItem = item;
    }

    openDeleteModal(content,index)
    {
        this.deleteModal = this.modalService.open(content);
        this.companyToDelete = index;
    }

    async deleteCompany()
    {
        this.deleteModal.close();
        this.DeleteItem();
        console.log("Company To Delete : " , this.companyToDelete)
    }

}
