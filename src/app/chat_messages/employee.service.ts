
import {Http,Headers,Response, RequestOptions} from "@angular/http";
import {Injectable} from "@angular/core";
import { HttpModule } from '@angular/http';
import 'rxjs/Rx';
import {SettingsService} from "../../settings/settings.service";



//public this.ServerUrl = "";//http://www.tapper.co.il/salecar/laravel/public/api/";
@Injectable()

export class EmployeeService
{
    public ServerUrl = "";
    public headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
    public options = new RequestOptions({headers:this.headers});

    public Items:any[]=[];
    public Companies:any[]=[];
    public CompanyArray:any[]=[];
    public Kitchens:any[]=[];

    constructor(private http:Http ,  settings:SettingsService)
    {
        this.ServerUrl = settings.ServerUrl;
    };

    GetItems(url:string,CompanyId)
    {
        let body = new FormData();
        body.append('uid', window.localStorage.identify);
        body.append('cid', CompanyId);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{this.Items = data }).toPromise();
    }
    
    AddItem(url,Items)
    {
        this.Items = Items;
        let body = 'items=' + JSON.stringify(this.Items);
        return this.http.post(this.ServerUrl + '' + url, body, this.options).map(res => res).do((data)=>{}).toPromise();
    }

    EditCompany(url,Company)
    {
        this.CompanyArray = Company;
        let body = 'company=' + JSON.stringify(this.CompanyArray);
        return this.http.post(this.ServerUrl + '' + url, body, this.options).map(res => res).do((data)=>{}).toPromise();
    }
    
    DeleteItem(url,Id,Cid)
    {
        let body = new FormData();
        body.append('id', Id);
        body.append('cid', Cid);
        console.log("Del 3 : ", Cid +  " : " + Id);
        return this.http.post(this.ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{this.Items = data }).toPromise();
    }
};


