import {Component, OnInit} from '@angular/core';
import {MainService} from "../MainService.service";
import {SettingsService} from "../../../settings/settings.service";
import {ActivatedRoute} from "@angular/router";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../media/list/list.component.scss', './index.component.css']
})


export class IndexComponent implements OnInit {

    ItemsArray: any[] = [];
    ItemsArray1: any[] = [];
    host: string = '';
    settings = '';
    deleteModal: any;
    companyToDelete: any;
    public folderName:string = 'subCategory';
    Id = '';
    rankModal: any;
    categroyrow: any;

    constructor(public MainService: MainService, settings: SettingsService , private route: ActivatedRoute , private modalService: NgbModal) {
        console.log("shay")
        this.route.params.subscribe(params => {
            this.Id = params['id'];

            if(!this.Id)
                this.Id = "-1";

            console.log("CP : " ,this.Id);
            this.host = settings.host
            this.getItems();
        });
    }

    getItems() {
        this.MainService.GetAllItems('GetSubCategoriesById',this.Id).then((data: any) => {
            console.log("getAllCars : ", data) ,
                this.ItemsArray = data;
                this.ItemsArray1 = data;
        })
    }

    ngOnInit() {
    }

    DeleteItem() {
        console.log("Del 1 : ", this.ItemsArray[this.companyToDelete].id);
        this.MainService.DeleteItem('DeleteSubCategory', this.ItemsArray[this.companyToDelete].id).then((data: any) => {
            //this.ItemsArray = data , console.log("Del 2 : ", data);
            this.getItems();
        })
    }

    updateFilter(event) {
        const val = event.target.value;
        // filter our data
        const temp = this.ItemsArray1.filter(function (d) {
            return d.name.toLowerCase().indexOf(val) !== -1 || !val;
        });
        // update the rows
        this.ItemsArray = temp;
    }

    openRankModal(content, row) {
        this.rankModal = this.modalService.open(content);
        this.categroyrow = row;
    }

    closeRankModal () {
        this.rankModal.close();
    }

    updateCatPosition() {

        this.MainService.EditPosition('WebEditSubCatPosition', this.categroyrow.id,this.categroyrow.position).then((data: any) => {
            this.closeRankModal();
            this.getItems();
        })
    }

    openDeleteModal(content,index)
    {
        this.deleteModal = this.modalService.open(content);
        this.companyToDelete = index;
    }

    async deleteSubCategory()
    {
        this.deleteModal.close();
        this.DeleteItem();
        console.log("Company To Delete : " , this.companyToDelete)
    }

}
