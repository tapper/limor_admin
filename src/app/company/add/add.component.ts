import {Component, OnInit, ElementRef, ChangeDetectionStrategy , ViewChild, NgZone} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

import {Http, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Headers} from '@angular/http';
import {FormBuilder, FormControl , FormGroup, NgForm, Validators} from "@angular/forms";
import { Ng2UploaderModule } from 'ng2-uploader';
import {CompanyService} from "../company.service";

import { FileUploader, FileUploaderOptions } from 'ng2-file-upload/ng2-file-upload';




@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

    public navigateTo:string = '/company/index';
    public paramsSub;
    public id: number;
    public imageSrc: string = '';
    uploader: FileUploader = new FileUploader({
        itemAlias: 'file',
        url: 'http://www.tapper.co.il/foodcloud/laravel1/public/api/GetFile',
        isHTML5: true,
        additionalParameter: {entity_id: 1, entity_type: 'file', media_key: 'file'}
    });

    constructor(private route: ActivatedRoute,private http: Http, public service:CompanyService , public router:Router) {

    }

    onSubmit(form:NgForm)
    {
        console.log(form.value);
        /*this.service.AddCompany('AddCompany1',form.value).then((data: any) => {
            console.log("AddCompany : " , data);
            this.router.navigate([this.navigateTo]);
        });*/
    }
    
    ngOnInit() {
        this.paramsSub = this.route.params.subscribe(params => this.id = params['id']);
    }
    
    ngOnDestroy() {
        this.paramsSub.unsubscribe();
    }
    
    handleInputChange(e) {
        var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];

        var pattern = /image-*/;
        var reader = new FileReader();

        if (!file.type.match(pattern)) {
            alert('invalid format');
            return;
        }

        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsDataURL(file);
        console.log("2 : " , reader)
    }

    _handleReaderLoaded(e) {
        var reader = e.target;
        this.imageSrc = reader.result;
        console.log("3 : " , this.imageSrc)
    
       /* this.service.AddCompany('AddCompany1',form.value).then((data: any) => {
            console.log("AddCompany : " , data);
            this.router.navigate([this.navigateTo]);
        });*/
    }

    //this.http.post('http://tapper.co.il/salecar/laravel/public/api/GetFile', formData, options)


}
